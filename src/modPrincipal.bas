Attribute VB_Name = "modPrincipal"
Option Explicit
Dim c
Public Loro As IAgentCtlCharacterEx
Public cnx As ADODB.Connection
Public Balloon As Balloon
Public xLegajo As Integer
Public lastKeyTab As Boolean
Public lastIndex As Integer

Sub Main()
xLegajo = 0
On Error GoTo catchErr
Set cnx = New ADODB.Connection
cnx.ConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=suquia"
cnx.CursorLocation = adUseClient
cnx.Open
Set Balloon = frmPrincipal.Balloon1
Balloon.ButtonsCaptions = "&Aceptar;&Cancelar;&Anular;&Reintentar;&Ignorar;&S�;&No"
With frmPrincipal.toolPic.ListImages
    .Add 1, , LoadPicture(App.Path & "\bitmaps\new.bmp")
    .Add 2, , LoadPicture(App.Path & "\bitmaps\mod.bmp")
    .Add 3, , LoadPicture(App.Path & "\bitmaps\save.bmp")
    .Add 4, , LoadPicture(App.Path & "\bitmaps\delete.bmp")
    .Add 5, , LoadPicture(App.Path & "\bitmaps\find.bmp")
    .Add 6, , LoadPicture(App.Path & "\bitmaps\print.bmp")
    .Add 7, , LoadPicture(App.Path & "\bitmaps\preview.bmp")
    .Add 8, , LoadPicture(App.Path & "\bitmaps\hc.ico")
    .Add 9, , LoadPicture(App.Path & "\bitmaps\fact.bmp")
End With
With frmPrincipal.Toolbar1
    .Buttons(1).Image = 1
    .Buttons(2).Image = 2
    .Buttons(3).Image = 3
    .Buttons(4).Image = 4
    .Buttons(5).Image = 5
End With
With frmPrincipal.Toolbar2
    .Buttons(1).Image = 6
    .Buttons(2).Image = 7
End With
With frmPrincipal.Toolbar3
    .Buttons(1).Image = 8
    .Buttons(2).Image = 9
End With
App.HelpFile = App.Path & "\suquia.chm"
frmPrincipal.Show
catchErr:
If Err.Number = -2147467259 Then
    frmLoadDb.Show 1
    Resume
End If
End Sub
Public Function DateDB() As String
'DateDB = Right(Date, 4) & "-" & Mid(Date, 4, 2) & "-" & Left(Date, 2)
DateDB = Format(Date, "yyyy-mm-dd")
End Function
Public Function TimeDB() As String
Dim Hh As Integer
If Mid(Time, 10, 1) = "p" Then
Hh = Val(Left(Time, 2)) + 12
Else
Hh = Val(Left(Time, 2))
End If
If Hh = 12 Or Hh = 24 Then: Hh = Hh - 12
TimeDB = Hh & Mid(Time, 3, 6)
End Function

Function abrirrs(strsql As String, blnactualizable As Boolean) As ADODB.Recordset
 Dim rs As ADODB.Recordset
 Set rs = New ADODB.Recordset
 Set rs.ActiveConnection = cnx
 If blnactualizable Then
    rs.Open strsql, , adOpenKeyset, adLockOptimistic
  Else
    rs.Open strsql, , adOpenStatic, adLockReadOnly
 End If
 Set abrirrs = rs
End Function

Sub combodoc(cmb As ComboBox)
cmb.AddItem "DNI"
cmb.AddItem "LE"
cmb.AddItem "LC"
cmb.AddItem "CI"
End Sub

Sub combomes(Combo2 As ComboBox)
Combo2.AddItem "Ene"
Combo2.ItemData(Combo2.NewIndex) = 1
Combo2.AddItem "Feb"
Combo2.ItemData(Combo2.NewIndex) = 2
Combo2.AddItem "Mar"
Combo2.ItemData(Combo2.NewIndex) = 3
Combo2.AddItem "Abr"
Combo2.ItemData(Combo2.NewIndex) = 4
Combo2.AddItem "May"
Combo2.ItemData(Combo2.NewIndex) = 5
Combo2.AddItem "Jun"
Combo2.ItemData(Combo2.NewIndex) = 6
Combo2.AddItem "Jul"
Combo2.ItemData(Combo2.NewIndex) = 7
Combo2.AddItem "Ago"
Combo2.ItemData(Combo2.NewIndex) = 8
Combo2.AddItem "Sep"
Combo2.ItemData(Combo2.NewIndex) = 9
Combo2.AddItem "Oct"
Combo2.ItemData(Combo2.NewIndex) = 10
Combo2.AddItem "Nov"
Combo2.ItemData(Combo2.NewIndex) = 11
Combo2.AddItem "Dic"
Combo2.ItemData(Combo2.NewIndex) = 12
End Sub

Sub cargarcombo(cbo As ComboBox, rs As Recordset)
cbo.Clear
If rs.RecordCount = 0 Then
    Exit Sub
End If
rs.MoveFirst
Do While Not rs.EOF
   cbo.AddItem rs.Fields(1)                   'Descripcion
   cbo.ItemData(cbo.NewIndex) = rs.Fields(0)  'Codigo
   rs.MoveNext
Loop
cbo.ListIndex = 0       '1� elemento
End Sub
Function cargado(ByVal nombreFrm As String) As Boolean
cargado = False
Dim frm
For Each frm In Forms
    If frm.Name = nombreFrm Then cargado = True
Next
End Function
Function nuevoid(ByVal id As String, ByVal tabla As String) As Long
Dim rst As ADODB.Recordset
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
rst.Source = "select max(" & id & ")+1 as max from " & tabla
rst.Open
If rst.RecordCount <> 0 Then
nuevoid = rst!max
Else
nuevoid = 1
End If
rst.Close
Set rst = Nothing
End Function
Sub completar(Combo As ComboBox)
Dim c
Dim xSelStart
For c = 0 To Combo.ListCount - 1
    If UCase(Left(Combo.List(c), Len(Combo.Text))) = UCase(Combo.Text) Then
        xSelStart = Len(Combo.Text)
        Combo.Text = Combo.List(c)
        Combo.SelStart = xSelStart
        Combo.SelLength = Len(Combo.Text)
        Exit Sub
    End If
Next c

End Sub
Sub validar(Combo As ComboBox)
Dim c
For c = 0 To Combo.ListCount - 1
    If Left(Combo.List(c), Len(Combo.Text)) = Combo.Text Then
        Combo.ListIndex = c
        Exit Sub
    End If
Next c
End Sub
Sub imprimirRec(ByVal fecha As String, ByVal Sintomas As String, ByVal Diagnostico As String, ByVal Medicacion As String, ByVal Prescripcion As String, ByVal mascota As String)
Dim appWord As Word.Application
Dim Recetario As Word.Document
Set appWord = New Word.Application
Set Recetario = appWord.Documents.Add
With Recetario.ActiveWindow.Selection
.InsertAfter "Veterinaria Rio Suquia" & vbCrLf
.Font.Size = 18
.Font.Bold = True
.Font.Name = "Arial"
.ParagraphFormat.Alignment = wdAlignParagraphCenter
.InsertParagraphAfter
.EndOf
.InsertAfter "Recetario" & vbCrLf
.Font.Size = 12
.Font.Bold = True
.Font.Name = "Arial"
.ParagraphFormat.Alignment = wdAlignParagraphCenter
.InsertParagraphAfter
.EndOf
.InsertAfter Format(fecha, "dd \de mmmm \de yyyy") & vbCrLf
.Font.Size = 10
.Font.Name = "Arial"
.Font.Bold = False
.ParagraphFormat.Alignment = wdAlignParagraphRight
.InsertParagraphAfter
.EndOf

.InsertAfter "Paciente: "
.Font.Size = 10
.Font.Bold = True
.Font.Name = "Arial"
.EndOf
.InsertAfter mascota
.Font.Size = 10
.Font.Name = "Arial"
.Font.Bold = False
.ParagraphFormat.Alignment = wdAlignParagraphJustify
.InsertParagraphAfter
.EndOf

.InsertAfter "S�ntomas: "
.Font.Size = 10
.Font.Bold = True
.Font.Name = "Arial"
.EndOf
.InsertAfter Sintomas
.Font.Size = 10
.Font.Name = "Arial"
.Font.Bold = False
.ParagraphFormat.Alignment = wdAlignParagraphJustify
.InsertParagraphAfter
.EndOf

.InsertAfter "Diagn�stico: "
.Font.Size = 10
.Font.Bold = True
.Font.Name = "Arial"
.EndOf
.InsertAfter Diagnostico
.Font.Size = 10
.Font.Name = "Arial"
.Font.Bold = False
.ParagraphFormat.Alignment = wdAlignParagraphJustify
.InsertParagraphAfter
.EndOf

.InsertAfter "Medicaci�n: "
.Font.Size = 10
.Font.Bold = True
.Font.Name = "Arial"
.EndOf
.InsertAfter Medicacion
.Font.Size = 10
.Font.Name = "Arial"
.Font.Bold = False
.ParagraphFormat.Alignment = wdAlignParagraphJustify
.InsertParagraphAfter
.EndOf

.InsertAfter "Prescripci�n: "
.Font.Size = 10
.Font.Bold = True
.Font.Name = "Arial"
.EndOf
.InsertAfter Prescripcion
.Font.Size = 10
.Font.Name = "Arial"
.Font.Bold = False
.ParagraphFormat.Alignment = wdAlignParagraphJustify
.InsertParagraphAfter
.EndOf
End With
appWord.Visible = True
'Recetario.PrintOut False
'appWord.Quit False
End Sub
Function Formato(nro As String) As String
  Dim aux As String
  aux = Left(nro, Len(nro) - 6) & "." & Right(nro, 6)
  aux = Left(aux, Len(aux) - 3) & "." & Right(nro, 3)
  Formato = aux
End Function
Function QuitarFormato(nro As String) As String
  Dim aux As String
  aux = Left(nro, Len(nro) - 8) & Right(nro, 7)
  aux = Left(aux, Len(aux) - 4) & Right(nro, 3)
  QuitarFormato = aux
End Function
