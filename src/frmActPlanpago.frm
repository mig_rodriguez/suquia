VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmActPlanpago 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Plan de Pago"
   ClientHeight    =   4740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6465
   Icon            =   "frmActPlanpago.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4740
   ScaleWidth      =   6465
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1290
      TabIndex        =   18
      Top             =   4335
      Width           =   1695
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3450
      TabIndex        =   19
      Top             =   4335
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   4545
      TabIndex        =   17
      Top             =   3855
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   2385
      TabIndex        =   16
      Top             =   3855
      Width           =   1695
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   225
      TabIndex        =   15
      Top             =   3855
      Width           =   1695
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3615
      Left            =   30
      TabIndex        =   0
      Top             =   120
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   6376
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Actualizaci�n"
      TabPicture(0)   =   "frmActPlanpago.frx":1272
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label1(2)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label1(3)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label1(4)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label1(5)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label1(6)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label1(7)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label1(8)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblSer(3)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "fecFin"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "fecInicio"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "xfecFin"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "cmdtarj"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "cmdPrimero"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "CmdAtras"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "CmdAdelante"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "CmdUltimo"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "cmbtarjeta"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Text1(0)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Text1(1)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Text1(2)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "txtdescripcion"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "Text1(3)"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "chkFF"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "chkAF"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).ControlCount=   25
      TabCaption(1)   =   "Listado"
      TabPicture(1)   =   "frmActPlanpago.frx":128E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "gridplanpago"
      Tab(1).ControlCount=   1
      Begin VB.CheckBox chkAF 
         Caption         =   "Quitar Fecha Fin"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   2640
         Visible         =   0   'False
         Width           =   1770
      End
      Begin VB.CheckBox chkFF 
         Caption         =   "Asignar Fecha Fin"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   2640
         Visible         =   0   'False
         Width           =   1815
      End
      Begin MSFlexGridLib.MSFlexGrid gridplanpago 
         Height          =   3015
         Left            =   -74880
         TabIndex        =   28
         Top             =   480
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   5318
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   5160
         MaxLength       =   5
         TabIndex        =   11
         Top             =   1560
         Width           =   855
      End
      Begin VB.TextBox txtdescripcion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1560
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Top             =   3000
         Width           =   4695
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   1560
         MaxLength       =   20
         TabIndex        =   9
         Top             =   2040
         Width           =   2175
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   1560
         MaxLength       =   5
         TabIndex        =   8
         Top             =   1560
         Width           =   855
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   4080
         MaxLength       =   25
         TabIndex        =   7
         Top             =   600
         Width           =   2175
      End
      Begin VB.ComboBox cmbtarjeta 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1080
         Width           =   2175
      End
      Begin VB.CommandButton CmdUltimo 
         Height          =   375
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton CmdAdelante 
         Height          =   375
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton CmdAtras 
         Height          =   375
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   360
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton cmdtarj 
         Caption         =   "+"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3840
         TabIndex        =   6
         Top             =   1080
         Width           =   375
      End
      Begin VB.TextBox xfecFin 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2640
         TabIndex        =   13
         Text            =   "    /    /"
         Top             =   2520
         Visible         =   0   'False
         Width           =   855
      End
      Begin MSFlexGridLib.MSFlexGrid Gridtarjeta 
         Height          =   3015
         Left            =   -74760
         TabIndex        =   20
         Top             =   600
         Width           =   6975
         _ExtentX        =   12303
         _ExtentY        =   5318
         _Version        =   393216
         Cols            =   8
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComCtl2.DTPicker fecInicio 
         Height          =   330
         Left            =   5160
         TabIndex        =   12
         Top             =   2040
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   24576001
         CurrentDate     =   37833
         MaxDate         =   73415
         MinDate         =   32874
      End
      Begin MSComCtl2.DTPicker fecFin 
         Height          =   330
         Left            =   2640
         TabIndex        =   14
         Top             =   2520
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   24576001
         CurrentDate     =   37833
         MaxDate         =   73415
         MinDate         =   32874
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Fin:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   3
         Left            =   240
         TabIndex        =   32
         Top             =   2640
         Width           =   1110
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   8
         Left            =   6120
         TabIndex        =   29
         Top             =   1570
         Width           =   150
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nombre del Plan:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   7
         Left            =   2520
         TabIndex        =   27
         Top             =   720
         Width           =   1440
      End
      Begin VB.Label Label1 
         Caption         =   "Cant. Cuotas:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   26
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Interes:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   5
         Left            =   240
         TabIndex        =   25
         Top             =   2160
         Width           =   1275
      End
      Begin VB.Label Label1 
         Caption         =   "Descripci�n:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   24
         Top             =   3120
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Recargo:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   3960
         TabIndex        =   23
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Fecha de Alta:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   3960
         TabIndex        =   22
         Top             =   2160
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Tarjeta:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   21
         Top             =   1200
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmActPlanpago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private xid_plan As Integer
Dim busqueda As String
Dim rst As ADODB.Recordset
Dim rstDetalle As ADODB.Recordset
Dim rsr As ADODB.Recordset

Private Sub chkFF_Click()
If chkFF.Value = 1 Then
    xfecFin.Visible = False
    fecFin.Enabled = True
    Else
    xfecFin.Visible = True
    fecFin.Enabled = False
End If
End Sub

Private Sub chkAF_Click()
If chkAF.Value = 1 Then
    xfecFin.Visible = True
    Else
    xfecFin.Visible = False
End If
End Sub

Private Sub cmdAdelante_Click()
If rst.RecordCount > 0 Then
  If rst.EOF <> True Then: rst.MoveNext
  If rst.EOF <> True Then
  Else
rst.MoveFirst
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End If
End Sub

Private Sub cmdAtras_Click()
If rst.RecordCount > 0 Then
  If rst.BOF <> True Then: rst.MovePrevious
  If rst.BOF <> True Then
   Else
rst.MoveLast
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End If
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Private Sub cmdPrimero_Click()
If rst.RecordCount > 0 Then
  rst.MoveFirst
  cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
  Refrescar
End If
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub

Private Sub cmdtarj_Click()
frmActTarjeta.Show
Me.Enabled = False
frmActTarjeta.nuevo
End Sub

Private Sub cmdUltimo_Click()
If rst.RecordCount > 0 Then
  rst.MoveLast
  cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
  Refrescar
End If
End Sub

Private Sub Form_Activate()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
If SSTab1.Tab = 1 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
 Else
   If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
     Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
   End If
End If
End Sub

Private Sub Form_Load()
SSTab1.Tab = 0
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.Source = "select * from plan_pago where id_plan > 0 and borrado = 'f' order by id_plan"
rst.CursorType = adOpenDynamic
rst.LockType = adLockOptimistic
rst.Open

Set rstDetalle = New ADODB.Recordset
rstDetalle.ActiveConnection = cnx
rstDetalle.Source = "select * from tarjeta where borrado = 'f' order by id_tarjeta"
rstDetalle.CursorType = adOpenDynamic
rstDetalle.LockType = adLockReadOnly
rstDetalle.Open
cargarcombo

cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
If rst.RecordCount > 0 Then
    busqueda = ""
    Refrescar
    loadGrid
  Else
    MsgBox ("no hay datos para mostrar")
 End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
     Else
        Loro.Stop
        rst.CancelUpdate
        restablecer
        If rst.RecordCount > 0 Then
          rst.MoveFirst
          Refrescar
        End If
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
rst.Close
End Sub

Private Sub Refrescar()
Text1(0) = rst!nombre
If IsNull(rst!fecha_inicio) = True Then fecInicio = DateValue("1/1/1999") Else fecInicio = rst!fecha_inicio
Dim c
For c = 0 To cmbtarjeta.ListCount - 1
  cmbtarjeta.ListIndex = c
  If cmbtarjeta.ItemData(c) = rst!id_tarjeta Then
     Exit For
  End If
Next
On Error GoTo err2
Text1(2) = rst!tipo_interes
On Error GoTo err6
txtDescripcion = rst!descripcion

If Not rst!cantidad_cuotas Then
  Text1(1) = rst!cantidad_cuotas
 Else
  Text1(1) = ""
End If

If Not rst!recargo Then
  Text1(3) = rst!recargo
 Else
  Text1(3) = ""
End If

If Not rst!fecha_baja Then
  xfecFin.Visible = False
  fecFin = rst!fecha_baja
 Else
  xfecFin.Visible = True
End If

err2:
 If Err.Number = 94 Then
   Text1(2) = ""
   Resume Next
 End If
err6:
 If Err.Number = 94 Then
   txtDescripcion = ""
   Resume Next
 End If
End Sub

Private Sub loadGrid()

rst.MoveFirst
gridplanpago.ColWidth(0) = 1350
gridplanpago.ColWidth(1) = 1350
gridplanpago.ColWidth(2) = 600
gridplanpago.ColWidth(3) = 700
gridplanpago.ColWidth(4) = 2000
gridplanpago.Rows = 0
gridplanpago.AddItem "Tarjeta" & vbTab & "Plan" & vbTab & "Cuotas" & vbTab & "Recargo" & vbTab & "Tipo de Interes"
gridplanpago.Rows = 2
gridplanpago.FixedRows = 1
gridplanpago.Rows = 1
While rst.EOF = False
  rstDetalle.MoveFirst
  While rstDetalle.EOF = False
     If rst!id_tarjeta = rstDetalle!id_tarjeta Then
        gridplanpago.AddItem rstDetalle!razon_social & vbTab & rst!nombre & vbTab & rst!cantidad_cuotas & vbTab & rst!recargo & vbTab & rst!tipo_interes
     End If
     rstDetalle.MoveNext
  Wend
  rst.MoveNext
Wend
rst.MoveFirst
End Sub

Sub nuevo()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
cmdPrimero.Enabled = False
SSTab1.TabEnabled(1) = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
chkFF.Visible = True
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 0 To 3
  Text1(c).Enabled = True
  Text1(c).Text = ""
Next c
txtDescripcion.Enabled = True
txtDescripcion.Text = ""
cmbtarjeta.Enabled = True
cmdtarj.Enabled = True
fecInicio.Value = Date
xfecFin.Visible = True
Text1(0).SetFocus
If rst.RecordCount > 0 Then
  rst.MoveLast
  xid_plan = nuevoid("id_plan", "plan_pago")
  Loro.Play "writing"
  rst.AddNew
 Else
  xid_plan = 1
  Loro.Play "writing"
  rst.AddNew
End If
End Sub

Sub modificar()
If rst.RecordCount > 0 Then
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
cmdPrimero.Enabled = False
SSTab1.TabEnabled(1) = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 0 To 3
  Text1(c).Enabled = True
Next c
txtDescripcion.Enabled = True
cmbtarjeta.Enabled = True
cmdtarj.Enabled = True
fecInicio.Value = Date
If xfecFin.Visible = True Then
chkFF.Visible = True
Else
chkAF.Visible = True
fecFin.Enabled = True
End If
Text1(0).SetFocus
xid_plan = rst!id_plan
Loro.Play "writing"
End If
End Sub

Sub cerrar()
Unload Me
End Sub

Sub guardar()
Dim c
For c = 0 To 3
    If c = 2 Then: GoTo sig
    If Text1(c) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
sig:
Next c
Loro.Stop
If xfecFin.Visible = False Then
    If fecFin.Value < fecInicio.Value Then
        MsgRapido "La fecha de Fin no es v�lida", Loro, , , "surprised"
        Exit Sub
    End If
    rst!fecha_baja = fecFin.Value
    Else
    rst!fecha_baja = Null
End If
rst!id_plan = xid_plan
rst!id_tarjeta = cmbtarjeta.ItemData(cmbtarjeta.ListIndex)
rst!nombre = Text1(0).Text
If Trim(Text1(1)) = "" Then
  rst!cantidad_cuotas = Null
 Else
  rst!cantidad_cuotas = Text1(1).Text
End If

rst!tipo_interes = Text1(2).Text

If Trim(Text1(3)) = "" Then
  rst!recargo = Null
 Else
  rst!recargo = Text1(3).Text
End If

If Trim(txtDescripcion) = "" Then
  rst!descripcion = Null
 Else
  rst!descripcion = txtDescripcion.Text
End If
rst!borrado = "f"
rst.Update
restablecer
rst.Requery
loadGrid
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
If SSTab1.Tab = 1 Then
   cmdNuevo.Enabled = False
   cmdModificar.Enabled = False
   cmdBorrar.Enabled = False
   frmPrincipal.desactivararch
   frmPrincipal.imprimir.Enabled = True
   frmPrincipal.vista.Enabled = True
   frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
   frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
 Else
   cmdNuevo.Enabled = True
   cmdModificar.Enabled = True
   cmdBorrar.Enabled = True
   frmPrincipal.activararch
   If rst.RecordCount > 0 Then
      rst.AbsolutePosition = gridplanpago.Row
      Refrescar
   End If
End If
End Sub

Private Sub restablecer()
Dim c
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
cmdPrimero.Enabled = True
SSTab1.TabEnabled(1) = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdUltimo.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
For c = 0 To 3
  Text1(c).Enabled = False
Next c
txtDescripcion.Enabled = False
cmdtarj.Enabled = False
fecInicio.Enabled = False
fecFin.Enabled = False
cmbtarjeta.Enabled = False
chkFF.Visible = False
chkAF.Visible = False
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 1 Or Index = 3 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
       If KeyAscii <> 8 And KeyAscii <> 7 Then
          KeyAscii = 0
       End If
    End If
End If
End Sub

Sub buscar()
busqueda = Input_Box("Buscar Plan de Pago." & Strings.Chr(13) & "Ingrese Raz�n Social:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    If Text1(0) = busqueda Then: rst.MoveNext
    rst.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rst.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbCritical, , "explain"
End If
End Sub

Private Sub Text1_lostfocus(Index As Integer)
For Index = 0 To 3
  Text1(Index) = Trim(Text1(Index))
Next Index
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Plan de Pago Seleccionado", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rst!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rst.Update
rst.Requery
cmdAdelante_Click
loadGrid
End Sub


Sub cargarcombo()
cmbtarjeta.Clear
rstDetalle.Requery
If rstDetalle.RecordCount = 0 Then
    Exit Sub
End If
rstDetalle.MoveFirst
Do While Not rstDetalle.EOF
   cmbtarjeta.AddItem rstDetalle!razon_social
   cmbtarjeta.ItemData(cmbtarjeta.NewIndex) = rstDetalle!id_tarjeta
   rstDetalle.MoveNext
Loop
cmbtarjeta.ListIndex = 0
End Sub

Sub GrarRs()
Set rsr = Nothing
Set rsr = New ADODB.Recordset
rsr.Fields.Append "tarjeta", adVarChar, 30
rsr.Fields.Append "nombre", adVarChar, 30
rsr.Fields.Append "cuotas", adVarChar, 10
rsr.Fields.Append "recargo", adVarChar, 10
rsr.Fields.Append "tipo", adVarChar, 20
rsr.LockType = adLockOptimistic
rsr.CursorType = adOpenKeyset
rsr.Open
Dim i, j As Integer
For i = 1 To gridplanpago.Rows - 1
    rsr.AddNew
    For j = 0 To gridplanpago.Cols - 1
        rsr(j) = gridplanpago.TextMatrix(i, j)
    Next j
    rsr.Update
Next i
End Sub

Sub imprimir()
GrarRs
frmPrincipal.desactivararch
Set rptPlanPago.DataSource = rsr
rptPlanPago.PrintReport True
Unload rptPlanPago
End Sub

Sub preview()
GrarRs
frmPrincipal.desactivararch
Set rptPlanPago.DataSource = rsr
rptPlanPago.Show
End Sub

Private Sub txtDescripcion_LostFocus()
txtDescripcion = Trim(txtDescripcion)
End Sub
