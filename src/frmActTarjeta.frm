VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmActTarjeta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Tarjetas"
   ClientHeight    =   3990
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6795
   Icon            =   "frmActTarjeta.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3990
   ScaleWidth      =   6795
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   390
      TabIndex        =   27
      Top             =   3075
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   2550
      TabIndex        =   26
      Top             =   3075
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   4710
      TabIndex        =   25
      Top             =   3075
      Width           =   1695
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3615
      TabIndex        =   24
      Top             =   3555
      Width           =   1695
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1455
      TabIndex        =   23
      Top             =   3555
      Width           =   1695
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3015
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   5318
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Actualizaci�n"
      TabPicture(0)   =   "frmActTarjeta.frx":1272
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1(15)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label1(14)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label2"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Text1(0)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame2"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "CmdUltimo"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "CmdAdelante"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "CmdAtras"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "cmdPrimero"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Text1(6)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Text1(7)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).ControlCount=   11
      TabCaption(1)   =   "Listado"
      TabPicture(1)   =   "frmActTarjeta.frx":128E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Gridtarjeta"
      Tab(1).ControlCount=   1
      Begin VB.TextBox Text1 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   0
         EndProperty
         Enabled         =   0   'False
         Height          =   330
         Index           =   7
         Left            =   1080
         MaxLength       =   30
         TabIndex        =   8
         Top             =   2520
         Width           =   3375
      End
      Begin VB.TextBox Text1 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   0
         EndProperty
         Enabled         =   0   'False
         Height          =   330
         Index           =   6
         Left            =   5160
         MaxLength       =   13
         TabIndex        =   9
         Top             =   2520
         Width           =   1395
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton CmdAtras 
         Height          =   375
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton CmdAdelante 
         Height          =   375
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton CmdUltimo 
         Height          =   375
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   480
         Width           =   375
      End
      Begin VB.Frame Frame2 
         Caption         =   "Domicilio"
         Height          =   1455
         Left            =   120
         TabIndex        =   10
         Top             =   960
         Width           =   6495
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   3
            Left            =   960
            MaxLength       =   6
            TabIndex        =   5
            Top             =   840
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   2
            Left            =   5040
            MaxLength       =   8
            TabIndex        =   4
            Top             =   360
            Width           =   1395
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   1
            Left            =   960
            MaxLength       =   25
            TabIndex        =   3
            Top             =   360
            Width           =   3375
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   5
            Left            =   3600
            MaxLength       =   5
            TabIndex        =   7
            Top             =   840
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   4
            Left            =   2280
            MaxLength       =   4
            TabIndex        =   6
            Top             =   840
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Nro.:"
            Height          =   255
            Index           =   11
            Left            =   4560
            TabIndex        =   15
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Piso:"
            Height          =   255
            Index           =   10
            Left            =   1800
            TabIndex        =   14
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Dpto.:"
            Height          =   255
            Index           =   9
            Left            =   3120
            TabIndex        =   13
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Torre:"
            Height          =   255
            Index           =   7
            Left            =   120
            TabIndex        =   12
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Calle:"
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   11
            Top             =   360
            Width           =   615
         End
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   3360
         MaxLength       =   29
         TabIndex        =   2
         Top             =   480
         Width           =   3195
      End
      Begin MSFlexGridLib.MSFlexGrid Gridtarjeta 
         Height          =   2535
         Left            =   -74880
         TabIndex        =   1
         Top             =   360
         Width           =   6495
         _ExtentX        =   11456
         _ExtentY        =   4471
         _Version        =   393216
         Cols            =   4
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
      End
      Begin VB.Label Label2 
         Caption         =   "Razon Social:"
         Height          =   255
         Left            =   2280
         TabIndex        =   22
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "TE:"
         Height          =   330
         Index           =   14
         Left            =   4680
         TabIndex        =   21
         Top             =   2520
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "E-mail:"
         Height          =   330
         Index           =   15
         Left            =   240
         TabIndex        =   20
         Top             =   2520
         Width           =   735
      End
   End
End
Attribute VB_Name = "frmActTarjeta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private xid_tarjeta As Integer
Dim busqueda As String
Dim rst As ADODB.Recordset

Private Sub cmdAdelante_Click()
If rst.RecordCount > 0 Then
  If rst.EOF <> True Then: rst.MoveNext
  If rst.EOF <> True Then
  Else
rst.MoveFirst
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End If
End Sub

Private Sub cmdAtras_Click()
If rst.RecordCount > 0 Then
  If rst.BOF <> True Then: rst.MovePrevious
  If rst.BOF <> True Then
   Else
rst.MoveLast
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End If
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Private Sub cmdPrimero_Click()
If rst.RecordCount > 0 Then
  rst.MoveFirst
  cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
  Refrescar
End If
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub

Private Sub cmdUltimo_Click()
If rst.RecordCount > 0 Then
  rst.MoveLast
  cmdUltimo.Enabled = False
  cmdPrimero.Enabled = True
  Refrescar
End If
End Sub

Private Sub Form_Activate()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
If SSTab1.Tab = 1 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
 Else
    If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
     Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
SSTab1.Tab = 0
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.Source = "select * from tarjeta where id_tarjeta>0 and borrado = 'f' order by id_tarjeta"
rst.CursorType = adOpenDynamic
rst.LockType = adLockOptimistic
rst.Open
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
If rst.RecordCount > 0 Then
    busqueda = ""
    Refrescar
    loadGrid
  Else
    MsgBox ("no hay datos para mostrar")
 End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
     Else
        Loro.Stop
        rst.CancelUpdate
        restablecer
        If rst.RecordCount > 0 Then
          rst.MoveFirst
          Refrescar
        End If
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
Set rst = Nothing
If cargado("frmActPlanpago") = True Then
frmActPlanpago.Enabled = True
frmActPlanpago.cargarcombo
End If
End Sub

Private Sub Refrescar()
Text1(0) = rst!razon_social

On Error GoTo err1
Text1(5) = rst!departamento

On Error GoTo err2
Text1(7) = rst!Email

On Error GoTo err4
Text1(1) = rst!calle

On Error GoTo err7
Text1(3) = rst!torre

On Error GoTo err8
Text1(2) = rst!numero

If Not rst!piso Then
  Text1(4) = rst!piso
 Else
  Text1(4) = ""
End If

If Not rst!tel Then
  Text1(6) = rst!tel
 Else
  Text1(6) = ""
End If

err1:
 If Err.Number = 94 Then
   Text1(5) = ""
   Resume Next
 End If
err2:
 If Err.Number = 94 Then
   Text1(7) = ""
   Resume Next
 End If
err4:
 If Err.Number = 94 Then
   Text1(1) = ""
   Resume Next
 End If
err7:
 If Err.Number = 94 Then
   Text1(3) = ""
    Resume Next
 End If
err8:
 If Err.Number = 94 Then
   Text1(2) = ""
    Resume Next
 End If
End Sub

Private Sub loadGrid()
Dim regActual As Long
regActual = rst.AbsolutePosition
rst.MoveFirst
Gridtarjeta.ColWidth(0) = 2200
Gridtarjeta.ColWidth(1) = 1150
Gridtarjeta.ColWidth(2) = 3000
Gridtarjeta.ColWidth(3) = 0
Gridtarjeta.Rows = 0
Gridtarjeta.AddItem "Raz�n Social" & vbTab & "Tel�fono" & vbTab & "e-mail"
Gridtarjeta.Rows = 2
Gridtarjeta.FixedRows = 1
Gridtarjeta.Rows = 1
While rst.EOF = False
  Gridtarjeta.AddItem rst!razon_social & vbTab & rst!tel & vbTab & rst!Email & vbTab & rst!id_tarjeta
  rst.MoveNext
Wend
rst.AbsolutePosition = regActual
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
If SSTab1.Tab = 1 Then
   cmdNuevo.Enabled = False
   cmdModificar.Enabled = False
   cmdBorrar.Enabled = False
   frmPrincipal.desactivararch
   frmPrincipal.imprimir.Enabled = True
   frmPrincipal.vista.Enabled = True
   frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
   frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
 Else
   cmdNuevo.Enabled = True
   cmdModificar.Enabled = True
   cmdBorrar.Enabled = True
   frmPrincipal.activararch
   If rst.RecordCount > 0 Then
      rst.AbsolutePosition = Gridtarjeta.Row
      Refrescar
   End If
End If
End Sub

Sub nuevo()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
SSTab1.TabEnabled(1) = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 0 To 7
  Text1(c).Enabled = True
  Text1(c).Text = ""
Next c
If rst.RecordCount > 0 Then
  rst.MoveLast
  xid_tarjeta = nuevoid("id_tarjeta", "tarjeta")
  Loro.Play "writing"
  rst.AddNew
 Else
  xid_tarjeta = 1
  Loro.Play "writing"
  rst.AddNew
End If
End Sub

Sub modificar()
If rst.RecordCount > 0 Then
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
SSTab1.TabEnabled(1) = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 0 To 7
  Text1(c).Enabled = True
Next c
xid_tarjeta = rst!id_tarjeta
Loro.Play "writing"
End If
End Sub

Sub cerrar()
Unload Me
End Sub
Sub guardar()
Dim c
For c = 0 To 7
    If c = 3 Or c = 4 Or c = 5 Or c = 6 Or c = 7 Then: GoTo sig
    If Text1(c) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
sig:
Next c
Loro.Stop

rst!id_tarjeta = xid_tarjeta
rst!razon_social = Text1(0).Text
rst!calle = Text1(1).Text

If Text1(3) = "" Then
  rst!torre = Null
 Else
  rst!torre = Text1(3).Text
End If

If Text1(4) = "" Then
  rst!piso = Null
 Else
  rst!piso = Text1(4).Text
End If

If Text1(5) = "" Then
  rst!departamento = Null
 Else
  rst!departamento = Text1(5).Text
End If

rst!numero = Text1(2).Text

If Text1(6) = "" Then
  rst!tel = Null
 Else
  rst!tel = Text1(6).Text
End If

If Text1(7) = "" Then
  rst!Email = Null
 Else
  rst!Email = Text1(7).Text
End If
rst!borrado = "f"
rst.Update
restablecer
rst.Requery
loadGrid
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub

Private Sub restablecer()
Dim c
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
SSTab1.TabEnabled(1) = True
cmdPrimero.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdUltimo.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
For c = 0 To 7
  Text1(c).Enabled = False
Next c
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 4 Or Index = 6 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
       If KeyAscii <> 8 And KeyAscii <> 7 Then
          KeyAscii = 0
       End If
    End If
End If
End Sub

Sub buscar()
busqueda = ""
busqueda = Input_Box("Buscar Tarjeta." & Strings.Chr(13) & "Ingrese Raz�n Social:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    If Text1(1) = busqueda Then: rst.MoveNext
    rst.Find "razon_social like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rst.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbCritical, , "explain"
End If
End Sub

Private Sub Text1_lostfocus(Index As Integer)
For Index = 0 To 7
  Text1(Index) = Trim(Text1(Index))
Next Index
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar la Tarjeta Seleccionada", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rst!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rst.Update
rst.Requery
cmdAdelante_Click
loadGrid
End Sub

Sub imprimir()
frmPrincipal.desactivararch
Set rptTarjetas.DataSource = rst
rptTarjetas.PrintReport True
Unload rptTarjetas
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptTarjetas.DataSource = rst
rptTarjetas.Show
End Sub

