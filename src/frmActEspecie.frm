VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmActEspecie 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizacion de Especies"
   ClientHeight    =   4755
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5010
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActEspecie.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4755
   ScaleWidth      =   5010
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2558
      TabIndex        =   9
      Top             =   4320
      Width           =   1335
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   998
      TabIndex        =   8
      Top             =   4320
      Width           =   1335
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   3398
      TabIndex        =   7
      Top             =   3840
      Width           =   1335
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   1838
      TabIndex        =   6
      Top             =   3840
      Width           =   1335
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   278
      TabIndex        =   5
      Top             =   3840
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   3015
      Index           =   1
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   4575
      Begin VB.Frame FraGen 
         Caption         =   "Nuevo G�nero"
         Height          =   1335
         Left            =   1200
         TabIndex        =   20
         Top             =   600
         Visible         =   0   'False
         Width           =   2895
         Begin VB.CommandButton cmdCerrar 
            Caption         =   "&Cancelar"
            Height          =   330
            Left            =   1560
            TabIndex        =   23
            Top             =   840
            Width           =   1095
         End
         Begin VB.CommandButton cmdAgen 
            Caption         =   "&Aceptar"
            Default         =   -1  'True
            Height          =   330
            Left            =   240
            TabIndex        =   22
            Top             =   840
            Width           =   1095
         End
         Begin VB.TextBox txtGen 
            Height          =   330
            Left            =   240
            MaxLength       =   20
            TabIndex        =   21
            Top             =   360
            Width           =   2415
         End
      End
      Begin VB.CommandButton NueGen 
         Caption         =   "+"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4200
         TabIndex        =   3
         Top             =   1080
         Width           =   375
      End
      Begin VB.ComboBox txtGenero 
         Enabled         =   0   'False
         Height          =   345
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1080
         Width           =   2775
      End
      Begin VB.CommandButton cmdUltimo 
         Height          =   375
         Left            =   2820
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   120
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         Height          =   375
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   120
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         Height          =   375
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   120
         Width           =   375
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   1380
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   120
         Width           =   375
      End
      Begin VB.TextBox txtDescripcion 
         Enabled         =   0   'False
         Height          =   855
         Left            =   120
         MaxLength       =   100
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   2040
         Width           =   4215
      End
      Begin VB.TextBox txtNombre 
         Enabled         =   0   'False
         Height          =   330
         Left            =   1320
         MaxLength       =   20
         TabIndex        =   1
         Top             =   600
         Width           =   2775
      End
      Begin VB.Label lbldes 
         Caption         =   "Descripci�n:"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label lblEspecie 
         Caption         =   "G�nero:"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label lblNombre 
         Caption         =   "Nombre:"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   3015
      Index           =   2
      Left            =   240
      TabIndex        =   15
      Top             =   600
      Width           =   4575
      Begin MSFlexGridLib.MSFlexGrid grdEspecies 
         Height          =   2895
         Left            =   120
         TabIndex        =   19
         Top             =   0
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   5106
         _Version        =   393216
         Rows            =   1
         Cols            =   3
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   3615
      Left            =   120
      TabIndex        =   14
      Top             =   120
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   6376
      TabWidthStyle   =   2
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmActEspecie"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mintCurFrame, xid_especie, c, orden As Integer
Dim busqueda As String
Dim rst As ADODB.Recordset
Dim band As Boolean

Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
    If cmdsalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
band = False
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenDynamic
rst.LockType = adLockOptimistic
rst.Source = "select distinct genero from especie where borrado = 'f'"
rst.Open
comboGenero
rst.Close
rst.Source = "select * from especie where borrado = 'f' order by nombre"
rst.Open
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
busqueda = ""
Refrescar
loadGrid
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdsalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        Loro.Stop
        rst.CancelUpdate
        restablecer
        rst.MoveFirst
        Refrescar
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
rst.Close
If cargado("frmActRaza") = True Then frmActRaza.Enabled = True: frmActRaza.comboEspecie
End Sub

' MOVER ESPECIE

Private Sub cmdAdelante_Click()
If rst.EOF <> True Then: rst.MoveNext
If rst.EOF <> True Then
Else
rst.MoveFirst
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAtras_Click()
If rst.BOF <> True Then: rst.MovePrevious
If rst.BOF <> True Then
Else
rst.MoveLast
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdPrimero_Click()
rst.MoveFirst
cmdUltimo.Enabled = True
cmdPrimero.Enabled = False
Refrescar
End Sub

Private Sub cmdUltimo_Click()
rst.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rst.MoveFirst
    rst.Find "id_especie = " & grdEspecies.TextMatrix(grdEspecies.Row, 2)
    Refrescar
    mintCurFrame = 1
    band = False
Else
    If TabStrip1.SelectedItem.Index = mintCurFrame Then: Exit Sub
        Frame1(TabStrip1.SelectedItem.Index).Visible = True
        Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        Refrescar
    End If
mintCurFrame = TabStrip1.SelectedItem.Index
End If
End Sub

Private Sub Refrescar()
If rst.RecordCount = 0 Then: Exit Sub
txtNombre = rst!nombre
txtGenero.ListIndex = 0
 For c = 0 To txtGenero.ListCount - 1
 txtGenero.ListIndex = c
 If txtGenero.List(c) = rst("genero") Then
  Exit For
 End If
 Next c
' cargar foto
On Error GoTo err1
txtDescripcion = rst!descripcion
On Error GoTo 0
err1:
If Err.Number = 94 Then
txtDescripcion = ""
Resume Next
End If
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Sub nuevo()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdsalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
txtNombre.Enabled = True
txtGenero.Enabled = True
txtGenero.ListIndex = -1
txtDescripcion.Enabled = True
NueGen.Enabled = True
txtNombre = ""
txtGenero.ListIndex = 0
txtDescripcion = ""
If rst.RecordCount <> 0 Then
rst.MoveLast
xid_especie = nuevoid("id_especie", "especie")
End If
Loro.Play "writing"
rst.AddNew
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Sub modificar()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdsalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
txtNombre.Enabled = True
txtGenero.Enabled = True
NueGen.Enabled = True
txtDescripcion.Enabled = True
xid_especie = rst!id_especie
Loro.Play "writing"
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Sub borrar()
If FraGen.Visible = True Then: MsgRapido "Primero cargue el nuevo G�nero", Loro, vbInformation, , "surprised": Exit Sub
If Mensaje("Est� seguro de querer eliminar la Especie Seleccionada", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rst!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rst.Update
rst.Requery
cmdAdelante_Click
loadGrid
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Sub guardar()
If FraGen.Visible = True Then: MsgRapido "Primero cargue el nuevo G�nero", Loro, vbInformation, , "surprised": Exit Sub
If txtNombre = "" Or txtGenero = "" Then
    MsgRapido "Los datos est�n incompletos", Loro, vbCritical, , "surprised"
    Exit Sub
End If
Loro.Stop
rst!id_especie = xid_especie
rst!nombre = txtNombre.Text
rst!genero = txtGenero.Text
rst!descripcion = txtDescripcion
rst!borrado = "f"
rst.Update
restablecer
rst.Requery
loadGrid
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub

Sub cerrar()
Unload Me
End Sub

Private Sub restablecer()
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdsalir.Caption = "&Salir"
cmdGuardar.Enabled = False
TabStrip1.Enabled = True
cmdPrimero.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdUltimo.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
txtNombre.Enabled = False
txtGenero.Enabled = False
txtDescripcion.Enabled = False
NueGen.Enabled = False
End Sub

Private Sub loadGrid()
Dim regActual As Long
regActual = rst.AbsolutePosition
grdEspecies.ColWidth(0) = 2000
grdEspecies.ColWidth(1) = 2000
grdEspecies.ColWidth(2) = 0
grdEspecies.Rows = 0
grdEspecies.AddItem "Nombre" & vbTab & "G�nero"
grdEspecies.Rows = 2
grdEspecies.FixedRows = 1
grdEspecies.Rows = 1
If rst.RecordCount <> 0 Then
    rst.MoveFirst
    While rst.EOF = False
    grdEspecies.AddItem rst!nombre & vbTab & rst!genero & vbTab & rst!id_especie
    rst.MoveNext
    Wend
    rst.MoveFirst
End If
rst.AbsolutePosition = regActual
End Sub

Private Sub grdespecies_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Private Sub cmdCerrar_Click()
FraGen.Visible = False
txtDescripcion.SetFocus
End Sub

Private Sub NueGen_Click()
FraGen.Visible = True
txtGen.SetFocus
End Sub

Private Sub cmdAgen_Click()
txtGenero.AddItem txtGen
orden = orden + 1
txtGenero.ItemData(txtGenero.NewIndex) = orden
txtGenero.ListIndex = orden - 1
FraGen.Visible = False
txtDescripcion.SetFocus
End Sub

Private Sub comboGenero()
txtGenero.Clear
If rst.RecordCount = 0 Then
    Exit Sub
End If
orden = 0
rst.MoveFirst
Do While Not rst.EOF
    orden = orden + 1
   txtGenero.AddItem rst!genero 'Descripcion
   txtGenero.ItemData(txtGenero.NewIndex) = orden
   rst.MoveNext
Loop
txtGenero.ListIndex = 0
End Sub

Sub buscar()
Dim regActual
busqueda = Input_Box("Buscar Especie." & Chr(20) & "Ingrese el Nombre:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    regActual = rst.AbsolutePosition
    If InStr(1, txtNombre, busqueda, vbTextCompare) = 0 Then: rst.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rst.EOF <> True Then
    Refrescar
Else
    rst.AbsolutePosition = regActual
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbExclamation, , "explain"
End If
End Sub

Sub imprimir()
frmPrincipal.desactivararch
Set rptListaEspecies.DataSource = rst
rptListaEspecies.PrintReport True
Unload rptListaEspecies
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptListaEspecies.DataSource = rst
rptListaEspecies.Show
End Sub
