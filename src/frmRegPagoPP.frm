VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRegPagoPP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Pago de Servicio PrePago"
   ClientHeight    =   4395
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6060
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegPagoPP.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4395
   ScaleWidth      =   6060
   Begin MSComCtl2.UpDown UpDown 
      Height          =   330
      Left            =   4065
      TabIndex        =   11
      Top             =   3360
      Width           =   195
      _ExtentX        =   344
      _ExtentY        =   582
      _Version        =   393216
      Value           =   1
      BuddyControl    =   "txtPago"
      BuddyDispid     =   196609
      OrigLeft        =   3360
      OrigTop         =   2640
      OrigRight       =   3600
      OrigBottom      =   3015
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtPago 
      Alignment       =   1  'Right Justify
      Height          =   330
      Left            =   3203
      TabIndex        =   10
      Text            =   "1"
      Top             =   3360
      Width           =   855
   End
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   1935
      Left            =   120
      TabIndex        =   8
      Top             =   840
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   3413
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedRows       =   0
      FixedCols       =   0
      AllowBigSelection=   0   'False
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      AllowUserResizing=   3
   End
   Begin VB.CommandButton cmdCerrar 
      Cancel          =   -1  'True
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   3120
      TabIndex        =   7
      Top             =   3840
      Width           =   1575
   End
   Begin VB.CommandButton cmdRegistrar 
      Caption         =   "&Registrar Pago"
      Height          =   375
      Left            =   1320
      TabIndex        =   6
      Top             =   3840
      Width           =   1575
   End
   Begin VB.TextBox txtMonto 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0FFFF&
      Enabled         =   0   'False
      Height          =   330
      Left            =   4680
      TabIndex        =   5
      Top             =   2880
      Width           =   975
   End
   Begin VB.TextBox txtMeses 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   330
      Left            =   1920
      TabIndex        =   4
      Top             =   2880
      Width           =   975
   End
   Begin VB.ComboBox cmbCliente 
      Height          =   345
      Left            =   1823
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   3375
   End
   Begin VB.Label Label1 
      Caption         =   "Mascotas Afiliadas:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   600
      Width           =   1815
   End
   Begin VB.Label lblPagos 
      Caption         =   "Meses a Pagar:"
      Height          =   255
      Left            =   1763
      TabIndex        =   9
      Top             =   3360
      Width           =   1335
   End
   Begin VB.Label lblMonto 
      Caption         =   "Monto Adeudado:"
      Height          =   255
      Left            =   3000
      TabIndex        =   3
      Top             =   2880
      Width           =   1455
   End
   Begin VB.Label lblMeses 
      Caption         =   "Meses Adeudados:"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   2880
      Width           =   1695
   End
   Begin VB.Label lblCliente 
      Caption         =   "Cliente:"
      Height          =   255
      Left            =   863
      TabIndex        =   0
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "frmRegPagoPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim cmd As ADODB.Command

Private Sub cmbCliente_Click()
rst.Source = "select m.nombre as mascota,s.nombre as plan,d.monto,p.monto_total,p.fecha_ultimo_pago from mascota_hc m,servicio s,detalle_sp d,servicio_prepago p where m.id_hc=d.id_hc and s.id_servicio=d.id_servicio and d.id_prepago=p.id_prepago and p.id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
rst.Open
txtMeses = Int((Date - DateValue(Format(rst!fecha_ultimo_pago, "d/mm/yyyy"))) / 30)
txtMonto = "$ " & txtMeses * rst!monto_total
grdDetalle.Clear
grdDetalle.Rows = 0
grdDetalle.AddItem "Mascota" & vbTab & "Pre-Pago" & vbTab & "Monto Mensual ($)"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
While Not rst.EOF
    grdDetalle.AddItem rst!mascota & vbTab & rst!plan & vbTab & rst!monto
    rst.MoveNext
Wend
rst.Close
If txtMeses = 0 Then
    txtPago = 0
    cmdRegistrar.Enabled = False
Else
    txtPago = txtMeses
    cmdRegistrar.Enabled = True
End If
UpDown.max = Val(txtMeses)
End Sub

Private Sub cmdCerrar_Click()
Unload Me
End Sub

Private Sub cmdRegistrar_Click()
'If mensaje("�Desea poner al dia este cliente?", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
'cmd.CommandText = "update servicio_prepago set fecha_ultimo_pago='" & DateDB & "' where id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
'cmd.Execute
Me.Enabled = False
frmFactura.PagarPrepago cmbCliente.ItemData(cmbCliente.ListIndex), txtPago
cmbCliente_Click
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
rst.Source = "select s.id_cliente,c.nombre,c.apellido from cliente c,servicio_prepago s where s.id_cliente=c.id_cliente and s.id_cliente>0 and borrado='f'"
rst.Open
While Not rst.EOF
    cmbCliente.AddItem rst!apellido & ", " & rst!nombre
    cmbCliente.ItemData(cmbCliente.NewIndex) = rst!id_cliente
    rst.MoveNext
Wend
rst.Close
cmbCliente.ListIndex = 0
grdDetalle.ColWidth(0) = 1400
grdDetalle.ColWidth(1) = 2500
grdDetalle.ColWidth(2) = 1700
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cargado("frmVerMorosidad") = True Then frmVerMorosidad.Enabled = True
frmPrincipal.desactivararch
End Sub

Private Sub txtPago_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtPago_Validate(Cancel As Boolean)
If Val(txtPago) > Val(txtMeses) Then txtPago = txtMeses
End Sub
