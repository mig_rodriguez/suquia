VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmActArticulo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Art�culos"
   ClientHeight    =   6195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5820
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActArticulo.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6195
   ScaleWidth      =   5820
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2963
      TabIndex        =   7
      Top             =   5760
      Width           =   1215
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1643
      TabIndex        =   3
      Top             =   5760
      Width           =   1215
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   3623
      TabIndex        =   6
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   2303
      TabIndex        =   5
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Default         =   -1  'True
      Height          =   375
      Left            =   983
      TabIndex        =   4
      Top             =   5280
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   4575
      Index           =   1
      Left            =   135
      TabIndex        =   13
      Top             =   390
      Width           =   5535
      Begin VB.CommandButton cmdQuitar 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3300
         TabIndex        =   36
         Top             =   4300
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3300
         TabIndex        =   35
         Top             =   3840
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.ListBox lstTodos 
         Height          =   735
         Left            =   3600
         TabIndex        =   34
         Top             =   3840
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.ListBox lstProveedores 
         Height          =   735
         Left            =   1320
         TabIndex        =   32
         Top             =   3840
         Width           =   1935
      End
      Begin VB.TextBox txtArt 
         Enabled         =   0   'False
         Height          =   330
         Index           =   7
         Left            =   1320
         MaxLength       =   20
         TabIndex        =   31
         Top             =   2160
         Width           =   4215
      End
      Begin VB.TextBox txtArt 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   1320
         MaxLength       =   30
         TabIndex        =   30
         Top             =   1200
         Width           =   4215
      End
      Begin VB.ComboBox cmbRubro 
         Enabled         =   0   'False
         Height          =   345
         Left            =   1320
         TabIndex        =   29
         Top             =   2640
         Width           =   4215
      End
      Begin VB.TextBox txtArt 
         Enabled         =   0   'False
         Height          =   570
         Index           =   6
         Left            =   1320
         MaxLength       =   99
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   28
         Top             =   3120
         Width           =   4215
      End
      Begin VB.TextBox txtArt 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   1320
         MaxLength       =   30
         TabIndex        =   27
         Top             =   720
         Width           =   4215
      End
      Begin VB.TextBox txtArt 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   330
         Index           =   5
         Left            =   4920
         MaxLength       =   11
         TabIndex        =   2
         Top             =   1680
         Width           =   615
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   420
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         Height          =   375
         Left            =   900
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         Height          =   375
         Left            =   1380
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdUltimo 
         Height          =   375
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdNuerub 
         Caption         =   "+"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         TabIndex        =   14
         Top             =   2640
         Width           =   375
      End
      Begin VB.TextBox txtArt 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   1320
         MaxLength       =   10
         TabIndex        =   0
         Top             =   1680
         Width           =   615
      End
      Begin VB.TextBox txtArt 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   3240
         MaxLength       =   10
         TabIndex        =   1
         Top             =   1680
         Width           =   615
      End
      Begin VB.TextBox txtArt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   3180
         TabIndex        =   15
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Proveedores:"
         Height          =   225
         Index           =   9
         Left            =   60
         TabIndex        =   33
         Top             =   3840
         Width           =   1095
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Nombre:"
         Height          =   225
         Index           =   0
         Left            =   60
         TabIndex        =   24
         Top             =   720
         Width           =   720
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Precio en $:"
         Height          =   225
         Index           =   1
         Left            =   3900
         TabIndex        =   23
         Top             =   1680
         Width           =   975
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n:"
         Height          =   225
         Index           =   4
         Left            =   60
         TabIndex        =   22
         Top             =   3120
         Width           =   1035
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "N�mero:"
         Height          =   225
         Index           =   5
         Left            =   2400
         TabIndex        =   21
         Top             =   240
         Width           =   720
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Rubro:"
         Height          =   225
         Index           =   6
         Left            =   660
         TabIndex        =   20
         Top             =   2640
         Width           =   555
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Marca:"
         Height          =   225
         Index           =   7
         Left            =   60
         TabIndex        =   19
         Top             =   1200
         Width           =   540
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Stock Total:"
         Height          =   225
         Index           =   2
         Left            =   60
         TabIndex        =   18
         Top             =   1680
         Width           =   945
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Stock M�nimo:"
         Height          =   225
         Index           =   3
         Left            =   2040
         TabIndex        =   17
         Top             =   1680
         Width           =   1140
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Presentaci�n:"
         Height          =   225
         Index           =   8
         Left            =   60
         TabIndex        =   16
         Top             =   2160
         Width           =   1140
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   4575
      Index           =   2
      Left            =   135
      TabIndex        =   25
      Top             =   390
      Width           =   5535
      Begin MSFlexGridLib.MSFlexGrid grdArt 
         Height          =   4455
         Left            =   0
         TabIndex        =   26
         Top             =   120
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   7858
         _Version        =   393216
         Rows            =   1
         Cols            =   4
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5055
      Left            =   30
      TabIndex        =   12
      Top             =   45
      Width           =   5760
      _ExtentX        =   10160
      _ExtentY        =   8916
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmActArticulo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rsArt As ADODB.Recordset
Dim rsRub As ADODB.Recordset
Dim rstprov As ADODB.Recordset
Dim cmd As ADODB.Command
Dim c, mintCurFrame As Integer
Dim band As Boolean
Dim busqueda As String

Private Sub cmdagregar_Click()
If lstTodos.ListIndex = -1 Then Exit Sub
lstProveedores.AddItem lstTodos.List(lstTodos.ListIndex)
lstProveedores.ItemData(lstProveedores.NewIndex) = lstTodos.ItemData(lstTodos.ListIndex)
lstTodos.RemoveItem lstTodos.ListIndex
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Art�culo Seleccionado", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rsArt!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rsArt.Update
rsArt.Requery
cmdAdelante_Click
loadGrid
End Sub

Private Sub cmdQuitar_Click()
If lstProveedores.ListIndex = -1 Then Exit Sub
lstTodos.AddItem lstProveedores.List(lstProveedores.ListIndex)
lstTodos.ItemData(lstTodos.NewIndex) = lstProveedores.ItemData(lstProveedores.ListIndex)
lstProveedores.RemoveItem lstProveedores.ListIndex
End Sub

Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
 If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
band = False
Set cmd = New ADODB.Command
Set rsArt = New ADODB.Recordset
Set rsRub = New ADODB.Recordset
Set rstprov = New ADODB.Recordset
Set rsArt = abrirrs("select * from articulo where id_articulo > 0 and borrado = 'f' order by nombre", True)
Set rsRub = abrirrs("select * from rubro where id_rubro < 10000 and id_tipo = 10001 and borrado = 'f' order by id_rubro", False)
rstprov.ActiveConnection = cnx
cmd.ActiveConnection = cnx
rstprov.CursorType = adOpenForwardOnly
rstprov.LockType = adLockReadOnly
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
cargarRubro
loadGrid
rsArt.MoveFirst
Refrescar
End Sub
Private Sub lstProveedores_DblClick()
If lstTodos.Visible = True Then
    cmdQuitar_Click
End If
End Sub
Private Sub lstTodos_DblClick()
cmdagregar_Click
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rsArt.MoveFirst
    rsArt.Find "id_articulo = " & grdArt.TextMatrix(grdArt.Row, 3)
    Refrescar
    mintCurFrame = 1
    band = False
Else
If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
End If
End Sub
Private Sub Refrescar()
txtArt(0) = rsArt!id_articulo
txtArt(1) = rsArt!nombre
txtArt(2) = rsArt!marca
txtArt(3) = rsArt!stock_total
txtArt(4) = rsArt!stock_minimo
txtArt(5) = rsArt!Precio
txtArt(6) = rsArt!descripcion
txtArt(7) = rsArt!presentacion
For c = 0 To cmbRubro.ListCount - 1
cmbRubro.ListIndex = c
If cmbRubro.ItemData(c) = rsArt!id_rubro Then
    Exit For
End If
Next
rstprov.Source = "select p.razon_social,x.id_proveedor from proveedor p,prov_item x where x.id_proveedor=p.id_proveedor and x.tipo_item=3 and x.id_item=" & txtArt(0)
rstprov.Open
lstProveedores.Clear
While Not rstprov.EOF
    lstProveedores.AddItem rstprov!razon_social
    lstProveedores.ItemData(lstProveedores.NewIndex) = rstprov!id_proveedor
    rstprov.MoveNext
Wend
rstprov.Close
End Sub

Public Sub cargarRubro()
cmbRubro.Clear
rsRub.Requery
If rsRub.RecordCount = 0 Then
    Exit Sub
End If
rsRub.MoveFirst
Do While Not rsRub.EOF
   cmbRubro.AddItem rsRub!nombre
   cmbRubro.ItemData(cmbRubro.NewIndex) = rsRub!id_rubro
   rsRub.MoveNext
Loop
cmbRubro.ListIndex = 0
End Sub

'   Mover Art�culo

Private Sub cmdPrimero_Click()
rsArt.MoveFirst
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAdelante_Click()
If rsArt.EOF <> True Then: rsArt.MoveNext
If rsArt.EOF <> True Then
Else
rsArt.MoveFirst
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAtras_Click()
If rsArt.BOF <> True Then: rsArt.MovePrevious
If rsArt.BOF <> True Then
Else
rsArt.MoveLast
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdUltimo_Click()
rsArt.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

'   NUEVO ART�CULO

Private Sub cmdNuevo_Click()
nuevo
End Sub
Sub nuevo()
cmdPrimero.Enabled = False
cmdUltimo.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdModificar.Enabled = False
cmdNuevo.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
cmdNuerub.Enabled = True
rsArt.MoveLast
txtArt(0) = nuevoid("id_articulo", "articulo")
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Loro.Play "write"
rsArt.AddNew
For c = 1 To 7
txtArt(c).Enabled = True
txtArt(c) = ""
Next
lstProveedores.Clear
lstTodos.Visible = True
cmdAgregar.Visible = True
cmdQuitar.Visible = True
cmbRubro.Enabled = True
rstprov.Source = "select id_proveedor,razon_social from proveedor where borrado='f' order by razon_social"
rstprov.Open
lstTodos.Clear
While Not rstprov.EOF
    lstTodos.AddItem rstprov!razon_social
    lstTodos.ItemData(lstTodos.NewIndex) = rstprov!id_proveedor
    rstprov.MoveNext
Wend
rstprov.Close
txtArt(1).SetFocus
End Sub

'   MODIFICAR ART�CULO

Private Sub cmdModificar_Click()
modificar
End Sub
Sub modificar()
cmdPrimero.Enabled = False
cmdUltimo.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
cmdNuerub.Enabled = True
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Loro.Play "write"
For c = 1 To 7
txtArt(c).Enabled = True
Next
cmbRubro.Enabled = True
lstTodos.Visible = True
cmdAgregar.Visible = True
cmdQuitar.Visible = True
rstprov.Source = "select id_proveedor,razon_social from proveedor where borrado='f' order by razon_social"
rstprov.Open
lstTodos.Clear
Dim a
Dim listado As Boolean
While Not rstprov.EOF
    listado = False
    For a = 0 To lstProveedores.ListCount - 1
        If lstProveedores.ItemData(a) = rstprov!id_proveedor Then listado = True: Exit For
    Next a
    If listado = False Then
        lstTodos.AddItem rstprov!razon_social
        lstTodos.ItemData(lstTodos.NewIndex) = rstprov!id_proveedor
    End If
    rstprov.MoveNext
Wend
rstprov.Close
txtArt(1).SetFocus
End Sub

'   GUARDAR

Private Sub cmdGuardar_Click()
guardar
End Sub
Sub guardar()
For c = 1 To 5
If Trim(txtArt(c)) = "" Then
    If c >= 3 And c <= 4 Then
        txtArt(c) = 0
    Else
        MsgRapido "Faltan Datos Necesarios, Por Favor Compl�telos", Loro, , , "surprised"
        txtArt(c).SetFocus
        Exit Sub
    End If
End If
Next c
If lstProveedores.ListCount < 1 Then MsgRapido "Debe indicar al menos un proveedor.", Loro, vbCritical, , "surprised": Exit Sub
rsArt!id_articulo = txtArt(0)
rsArt!nombre = txtArt(1)
rsArt!marca = txtArt(2)
rsArt!stock_total = txtArt(3)
rsArt!stock_minimo = txtArt(4)
rsArt!descripcion = txtArt(6)
rsArt!Precio = txtArt(5)
rsArt!presentacion = txtArt(7)
rsArt!id_rubro = cmbRubro.ItemData(cmbRubro.ListIndex)
rsArt!borrado = "f"
cmd.CommandText = "delete from prov_item where tipo_item=3 and id_item=" & txtArt(0)
cmd.Execute
Dim a
For a = 0 To lstProveedores.ListCount - 1
    cmd.CommandText = "insert into prov_item(tipo_item,id_item,id_proveedor) values(3," & txtArt(0) & "," & lstProveedores.ItemData(a) & ")"
    cmd.Execute
Next a
rsArt.Update
rsArt.Requery
rsArt.Find "id_articulo = " & txtArt(0)
restablecer
cmdAdelante.SetFocus
loadGrid
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub

Private Sub restablecer()
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdNuevo.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
TabStrip1.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
cmdNuerub.Enabled = False
For c = 1 To 7
txtArt(c).Enabled = False
Next
cmbRubro.Enabled = False
lstTodos.Visible = False
cmdAgregar.Visible = False
cmdQuitar.Visible = False
Refrescar
End Sub

Sub cmdsalir_Click()
Unload Me
End Sub


' SOLO N�MEROS

Private Sub txtArt_KeyPress(Index As Integer, KeyAscii As Integer)
If Index >= 3 And Index <= 5 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
    End If
End If
End Sub

Private Sub txtArt_Validate(Index As Integer, Cancel As Boolean)
If Index >= 3 And Index <= 5 Then
    If txtArt(Index) <> "" Then
    If IsNumeric(txtArt(Index)) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtArt(Index) = ""
    End If
    End If
End If
End Sub

Sub buscar()
busqueda = Input_Box("Buscar Art�culo" & Chr(13) & "Ingrese el Nombre del Art�culo:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    rsArt.Filter = ""
    If txtArt(1) = busqueda Then: rstmas.MoveNext
    rsArt.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rsArt.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, , , "explain"
End If
End Sub

Private Sub cmdNuerub_Click()
frmActRubro.Show
Me.Enabled = False
frmActRubro.nuevo
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        rsArt.CancelUpdate
        restablecer
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
Set rsArt = Nothing
Set rsRub = Nothing
End Sub

Private Sub grdArt_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Private Sub loadGrid()
Dim regActual As Long
regActual = rsArt.AbsolutePosition
rsArt.MoveFirst
grdArt.ColWidth(0) = 2400
grdArt.ColWidth(1) = 2400
grdArt.ColWidth(2) = 630
grdArt.ColWidth(3) = 0
grdArt.Rows = 0
grdArt.AddItem "Nombre" & vbTab & "Marca" & vbTab & "Stock"
grdArt.Rows = 2
grdArt.FixedRows = 1
grdArt.Rows = 1
While rsArt.EOF = False
grdArt.AddItem rsArt!nombre & vbTab & rsArt!marca & vbTab & rsArt!stock_total & vbTab & rsArt!id_articulo
rsArt.MoveNext
Wend
rsArt.AbsolutePosition = regActual
End Sub

Sub imprimir()
frmPrincipal.desactivararch
Set rptArticulos.DataSource = rsArt
rptArticulos.PrintReport True
Unload rptArticulos
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptArticulos.DataSource = rsArt
rptArticulos.Show
End Sub
