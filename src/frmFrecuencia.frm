VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmFrecuencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Generar Informe de Frecuencia de Atenciones"
   ClientHeight    =   4935
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5730
   Icon            =   "frmFrecuencia.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   5730
   Begin VB.CommandButton cmdVista 
      Caption         =   "&Vista Previa"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   111
      TabIndex        =   5
      Top             =   1440
      Width           =   1700
   End
   Begin MSFlexGridLib.MSFlexGrid gRes 
      Height          =   2655
      Left            =   120
      TabIndex        =   6
      Top             =   2160
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   4683
      _Version        =   393216
      Cols            =   3
      FixedCols       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.UpDown udMin 
      Height          =   330
      Left            =   4854
      TabIndex        =   1
      Top             =   120
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   393216
      Value           =   1
      BuddyControl    =   "txtMin"
      BuddyDispid     =   196610
      OrigLeft        =   4800
      OrigTop         =   120
      OrigRight       =   5040
      OrigBottom      =   450
      Max             =   9999
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtMin 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4238
      TabIndex        =   0
      Text            =   "1"
      Top             =   120
      Width           =   615
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3919
      TabIndex        =   4
      Top             =   1440
      Width           =   1700
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2010
      TabIndex        =   2
      Top             =   1440
      Width           =   1700
   End
   Begin MSComCtl2.DTPicker fDesde 
      Height          =   330
      Left            =   1425
      TabIndex        =   8
      Top             =   720
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   24641537
      CurrentDate     =   37858
      MaxDate         =   55153
      MinDate         =   32874
   End
   Begin MSComCtl2.DTPicker fHasta 
      Height          =   330
      Left            =   4185
      TabIndex        =   9
      Top             =   720
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   24641537
      CurrentDate     =   37858
      MaxDate         =   55153
      MinDate         =   32874
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000000&
      Index           =   0
      X1              =   120
      X2              =   5880
      Y1              =   600
      Y2              =   600
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   225
      TabIndex        =   11
      Top             =   720
      Width           =   1155
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Hasta:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   2985
      TabIndex        =   10
      Top             =   720
      Width           =   1095
   End
   Begin VB.Label Label4 
      Caption         =   "Asistencias:"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Seleccione el n�mero m�nimo de asistencias:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   398
      TabIndex        =   3
      Top             =   120
      Width           =   3780
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000000&
      Index           =   1
      X1              =   120
      X2              =   5640
      Y1              =   600
      Y2              =   600
   End
End
Attribute VB_Name = "frmFrecuencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rst As ADODB.Recordset

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Height = 2300
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
fHasta.Value = Date
fHasta.MaxDate = Date
fDesde.MaxDate = Date
fDesde.Value = fDesde.MinDate
cmdVista.Visible = False
cmdAceptar.Left = 1045
cmdCancelar.Left = 2990
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
Set rst = Nothing
End Sub

Private Sub cmdcancelar_Click()
If cmdCancelar.Caption = "&Salir" Then
If rst.State = adStateOpen Then rst.Close
Unload Me
Else
cmdCancelar.Caption = "&Salir"
cmdAceptar.Caption = "&Aceptar"
frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
frmPrincipal.vista.Enabled = False
frmPrincipal.imprimir.Enabled = False
Me.Height = 2300
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
crearGrilla
fDesde.Enabled = True
fHasta.Enabled = True
udMin.Enabled = True
txtMin.Enabled = True
cmdVista.Visible = False
cmdAceptar.Left = 1045
cmdCancelar.Left = 2990
End If
End Sub

Private Sub cmdaceptar_Click()
If cmdAceptar.Caption = "&Aceptar" Then
 If fDesde.Value > fHasta.Value Then
    MsgRapido "El Rango de fechas es incorrecto, Por Favor Corr�jalo", Loro, , , "surprised"
    fDesde.SetFocus
    Exit Sub
 End If
 Set rst = abrirrs("select c.nombre,c.apellido,count(*) as cantidad,sum(f.monto_total) as total from cliente c,factura f where f.id_cliente <> 0 and f.id_cliente = c.id_cliente and (f.fecha >= '" & fDesde.Value & "' and f.fecha <= '" & fHasta.Value & "') group by f.id_cliente having count(*)>= " & txtMin & " ", False)
 If rst.RecordCount = 0 Then
    MsgRapido "No existen datos con estas caracteristicas", Loro, vbInformation, "Atenci�n", "explain"
    Exit Sub
 End If
 crearGrilla
 Me.Height = 5300
 Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
 Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
 cmdCancelar.Caption = "&Cancelar"
 cmdAceptar.Caption = "&Imprimir"
 fDesde.Enabled = False
 fHasta.Enabled = False
 udMin.Enabled = False
 txtMin.Enabled = False
 cmdVista.Visible = True
 cmdAceptar.Left = 2010
 cmdCancelar.Left = 3920
 cmdVista.Left = 110
 cmdVista.SetFocus
 frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
 frmPrincipal.vista.Enabled = True
 frmPrincipal.imprimir.Enabled = True
Else
 imprimir
End If
End Sub

Private Sub cmdVista_Click()
preview
End Sub

' SOLO N�MEROS

Private Sub Txtmin_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
        KeyAscii = 0
    End If
End If
End Sub

Private Sub Txtmin_Validate(Cancel As Boolean)
If IsNumeric(txtMin) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtMin = 1
End If
End Sub

Private Sub crearGrilla()
gRes.Clear
gRes.ColWidth(0) = 3300
gRes.ColWidth(1) = 1000
gRes.ColWidth(2) = 1000
gRes.Rows = 0
gRes.AddItem "           Cliente" & vbTab & "Frecuencia" & vbTab & "Total $"
gRes.Rows = 2
gRes.FixedRows = 1
gRes.Rows = 1
rst.MoveFirst
Do While Not rst.EOF
    gRes.AddItem rst(1) & ", " & rst(0) & vbTab & rst(2) & vbTab & rst(3)
    rst.MoveNext
Loop
rst.MoveFirst
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptFrecuencia.DataSource = rst
rptFrecuencia.Sections(1).Controls(6).Caption = Format(fDesde.Value, "long date") & "  y  " & Format(fHasta.Value, "long date")
rptFrecuencia.Show
End Sub
Sub imprimir()
frmPrincipal.desactivararch
Set rptFrecuencia.DataSource = rst
rptFrecuencia.Sections(1).Controls(6).Caption = Format(fDesde.Value, "long date") & "  y  " & Format(fHasta.Value, "long date")
rptFrecuencia.PrintReport True
Unload rptMascotas
End Sub
