VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmVerMorosidad 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Verificar Morosidad"
   ClientHeight    =   5190
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6465
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVerMorosidad.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5190
   ScaleWidth      =   6465
   Begin VB.CommandButton cmdPagar 
      Cancel          =   -1  'True
      Caption         =   "&Pagar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   4665
      TabIndex        =   12
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datos de Prepago"
      Height          =   2655
      Left            =   0
      TabIndex        =   8
      Top             =   2400
      Width           =   6375
      Begin VB.TextBox txtDeuda 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   360
         Left            =   3780
         TabIndex        =   11
         Top             =   2160
         Width           =   1200
      End
      Begin MSFlexGridLib.MSFlexGrid GrdRes 
         Height          =   1815
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   3201
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         HighLight       =   0
         SelectionMode   =   1
         MergeCells      =   3
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Monto Total Adeudado:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1485
         TabIndex        =   10
         Top             =   2160
         Width           =   2220
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Seleccione Cliente"
      Height          =   1695
      Left            =   585
      TabIndex        =   3
      Top             =   120
      Width           =   3735
      Begin VB.Frame Fradoc 
         Caption         =   "Documento"
         Height          =   735
         Left            =   440
         TabIndex        =   5
         Top             =   840
         Width           =   2895
         Begin VB.TextBox txtNroDoc 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Left            =   1080
            MaxLength       =   8
            TabIndex        =   7
            Top             =   240
            Width           =   1575
         End
         Begin VB.TextBox txtTipodoc 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Left            =   240
            MaxLength       =   8
            TabIndex        =   6
            Top             =   240
            Width           =   615
         End
      End
      Begin VB.ComboBox cmbCliente 
         Height          =   345
         Left            =   120
         TabIndex        =   4
         Text            =   "cmbCliente"
         Top             =   360
         Width           =   3495
      End
   End
   Begin VB.CommandButton cmdVolver 
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   4665
      TabIndex        =   1
      Top             =   840
      Width           =   1215
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Default         =   -1  'True
      Height          =   375
      Left            =   4665
      TabIndex        =   0
      Top             =   360
      Width           =   1215
   End
   Begin VB.Label lblResultado 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   3030
      TabIndex        =   2
      Top             =   2040
      Width           =   75
   End
End
Attribute VB_Name = "frmVerMorosidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rsc As ADODB.Recordset
Dim rst As ADODB.Recordset
Dim nocomp, band As Boolean
Dim xMes, xDeuda As Integer

Private Sub cmdPagar_Click()
frmRegPagoPP.Show
Dim c
For c = 0 To frmRegPagoPP.cmbCliente.ListCount - 1
If frmRegPagoPP.cmbCliente.List(c) = cmbCliente.List(cmbCliente.ListIndex) Then
    frmRegPagoPP.cmbCliente.ListIndex = c
    Exit For
End If
Next
frmVerMorosidad.Enabled = False

End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
Set rsc = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
Set rsc = abrirrs("select id_cliente, apellido, nombre, tipo_documento, nro_documento from cliente where borrado = 'f' and id_cliente> 0 order by apellido", False)
comboCliente
loadGrid
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Sub cmdBuscar_Click()
band = False
xDeuda = 0
GrdRes.Rows = 1
rst.Source = "select s.nombre as servicio, s.precio, sp.fecha_ultimo_pago as fup, sp.monto_total as mt, m.nombre as mascota from servicio_prepago sp, servicio s, detalle_sp dp, mascota_hc m where sp.id_cliente = '" & cmbCliente.ItemData(cmbCliente.ListIndex) & "' and s.id_rubro = 7 and dp.id_servicio = s.id_servicio and sp.id_prepago = dp.id_prepago and dp.id_hc = m.id_hc order by sp.id_prepago"
rst.Open
If rst.RecordCount = 0 Then
    lblResultado.Caption = "El cliente seleccionado no posee PrePago."
    GrdRes.Rows = 1
    txtDeuda = ""
    cmdPagar.Enabled = False
Else
    GrdRes.MergeCells = flexMergeRestrictColumns
    rst.MoveFirst
    While rst.EOF = False
        xMes = Int((Date - rst!fup) / 30) '(Year(Date) - Year(rst!fup)) * 12 + (Month(Date) - Month(rst!fup))
        GrdRes.AddItem rst!mascota & vbTab & rst!servicio & vbTab & " $ " & rst!Precio & vbTab & rst!fup & vbTab & xMes & vbTab & " $ " & rst!Precio * xMes
        rst.MoveNext
    Wend
    rst.MoveFirst
    While rst.EOF = False
        xMes = Int((Date - rst!fup) / 30)
        If xMes >= 1 Then
            band = True
            xDeuda = xDeuda + (rst!Precio * xMes)
'"Monto total adeudado: $" & Int(((Date - rst!fecha_ultimo_pago) / 30)) * rst!monto_total
        End If
        rst.MoveNext
    Wend
    If band = True Then
        lblResultado.Caption = "El Cliente Presenta Morosidad"
        cmdPagar.Enabled = True
        txtDeuda.Text = " $  " & xDeuda
    Else
        lblResultado.Caption = "Cliente al D�a."
        txtDeuda = ""
        cmdPagar.Enabled = False
    End If
End If
rst.Close
End Sub

Private Sub cmdvolver_Click()
Unload Me
End Sub

Private Sub loadGrid()
GrdRes.ColWidth(0) = 990
GrdRes.ColWidth(1) = 2055
GrdRes.ColWidth(2) = 630
GrdRes.ColWidth(3) = 1065
GrdRes.ColWidth(4) = 630
GrdRes.ColWidth(5) = 630
GrdRes.Rows = 0
GrdRes.AddItem "Mascota" & vbTab & "Prepago" & vbTab & "Precio" & vbTab & "Ult.Pago " & vbTab & "Meses" & vbTab & "Monto"
GrdRes.Rows = 2
GrdRes.FixedRows = 1
GrdRes.Rows = 1
End Sub

Private Sub comboCliente()
cmbCliente.Clear
If rsc.RecordCount = 0 Then
    Exit Sub
End If
rsc.MoveFirst
Do While Not rsc.EOF
   cmbCliente.AddItem rsc!apellido + ", " + rsc!nombre  'Descripcion
   cmbCliente.ItemData(cmbCliente.NewIndex) = rsc!id_cliente
   rsc.MoveNext
Loop
cmbCliente.ListIndex = 0
End Sub

Private Sub cmbCliente_Click()
rsc.MoveFirst
rsc.Find "id_cliente = " & cmbCliente.ItemData(cmbCliente.ListIndex)
txtNroDoc = rsc!nro_documento
txtTipodoc = rsc!tipo_documento
End Sub

Private Sub cmbcliente_Change()
If nocomp = False Then completar cmbCliente Else: nocomp = False
End Sub

Private Sub cmbcliente_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyBack Then cmbCliente.Text = Left(cmbCliente.Text, cmbCliente.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbCliente
End Sub

Private Sub cmbcliente_LostFocus()
validar cmbCliente
End Sub
