VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRegTurno 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Turno"
   ClientHeight    =   4680
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7485
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegTurno.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4680
   ScaleWidth      =   7485
   Begin VB.Frame fraMotivo 
      Caption         =   "Motivo de la visita:"
      Height          =   3255
      Left            =   1395
      TabIndex        =   10
      Top             =   960
      Visible         =   0   'False
      Width           =   4695
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   2400
         TabIndex        =   13
         Top             =   2760
         Width           =   1455
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   840
         TabIndex        =   12
         Top             =   2760
         Width           =   1455
      End
      Begin VB.TextBox txtMotivo 
         Height          =   2415
         Left            =   120
         MaxLength       =   256
         MultiLine       =   -1  'True
         TabIndex        =   11
         Top             =   240
         Width           =   4455
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H000000FF&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   255
      Left            =   2040
      TabIndex        =   16
      Top             =   3840
      Width           =   495
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FF0000&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   3840
      Width           =   495
   End
   Begin MSFlexGridLib.MSFlexGrid grdHora 
      Height          =   2655
      Left            =   82
      TabIndex        =   3
      Top             =   1080
      Width           =   4080
      _ExtentX        =   7197
      _ExtentY        =   4683
      _Version        =   393216
      Rows            =   1
      Cols            =   5
      FixedRows       =   0
      FocusRect       =   0
      HighLight       =   2
      FillStyle       =   1
      SelectionMode   =   1
   End
   Begin VB.ComboBox cmbMascota 
      Height          =   345
      Left            =   4755
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   600
      Width           =   2295
   End
   Begin VB.ComboBox cmbCliente 
      Height          =   345
      Left            =   1290
      Sorted          =   -1  'True
      TabIndex        =   1
      Text            =   "cmbCliente"
      Top             =   600
      Width           =   2295
   End
   Begin VB.CommandButton cmbAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   2115
      TabIndex        =   5
      Top             =   4200
      Width           =   1455
   End
   Begin VB.CommandButton cmbCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3915
      TabIndex        =   6
      Top             =   4200
      Width           =   1455
   End
   Begin MSComCtl2.MonthView Calendario 
      Height          =   2670
      Left            =   4282
      TabIndex        =   4
      Top             =   1080
      Width           =   3120
      _ExtentX        =   5503
      _ExtentY        =   4710
      _Version        =   393216
      ForeColor       =   -2147483630
      BackColor       =   -2147483633
      Appearance      =   1
      StartOfWeek     =   24444929
      TitleBackColor  =   32768
      TrailingForeColor=   -2147483633
      CurrentDate     =   37721
   End
   Begin VB.ComboBox cmbServicio 
      Height          =   345
      Left            =   3255
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   120
      Width           =   2295
   End
   Begin VB.Label Label2 
      Caption         =   "No Utilizado"
      Height          =   255
      Left            =   2760
      TabIndex        =   17
      Top             =   3840
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Utilizado"
      Height          =   255
      Left            =   840
      TabIndex        =   15
      Top             =   3840
      Width           =   855
   End
   Begin VB.Label lblMascota 
      Caption         =   "Mascota:"
      Height          =   255
      Left            =   3915
      TabIndex        =   9
      Top             =   600
      Width           =   855
   End
   Begin VB.Label lblCliente 
      Caption         =   "Cliente:"
      Height          =   255
      Left            =   435
      TabIndex        =   8
      Top             =   600
      Width           =   735
   End
   Begin VB.Label lblServicio 
      Caption         =   "Tipo de Turno:"
      Height          =   255
      Left            =   1935
      TabIndex        =   7
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmRegTurno"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim cmd As ADODB.Command
Dim nocomp As Boolean
Private Sub Calendario_SelChange(ByVal StartDate As Date, ByVal EndDate As Date, Cancel As Boolean)
loadTurno Format(Calendario.Value, "yyyy-mm-d")
End Sub

Private Sub cmbAceptar_Click()
Dim c
If Calendario.DayOfWeek = mvwSunday Then MsgRapido "No puede asignarse un turno en Domingo.", Loro, vbCritical, , "surprised": Exit Sub
If Calendario.Value < Date Then MsgRapido "No puede asignarse una fecha pasada.", Loro, vbCritical, , "surprised": Exit Sub
If Calendario.Value = Date And TimeValue(Left(grdHora.TextMatrix(grdHora.Row, 0), 5)) < Time Then MsgRapido "No puede asignarse una hora pasada.", Loro, vbCritical, , "surprised": Exit Sub
If cmbMascota.ListIndex = -1 Then Exit Sub
If cmbMascota.ItemData(cmbMascota.ListIndex) = 0 Then Exit Sub
For c = grdHora.Row To grdHora.RowSel
    If grdHora.TextMatrix(c, 1) <> "" Then
        MsgRapido "El horario ya se encuentra asignado.", Loro, vbInformation, , "decline"
        Exit Sub
    End If
Next c
txtMotivo = " "
If cmbServicio.ItemData(cmbServicio.ListIndex) = 3 Then
    fraMotivo.Visible = True
    cmbServicio.Enabled = False
    cmbMascota.Enabled = False
    cmbCliente.Enabled = False
    grdHora.Enabled = False
    Calendario.Enabled = False
    cmbAceptar.Enabled = False
    cmbCancelar.Enabled = False
Else
    grabar
End If

End Sub
Sub grabar()
Dim c
Dim xNro_turno As Long
Dim xHora As String
Dim temp As ADODB.Recordset
Set temp = New ADODB.Recordset
temp.ActiveConnection = cnx
temp.Source = "select max(nro_turno) from turno"
rst.Source = "select max(nro_turno) from turno"
rst.Open
xNro_turno = rst(0).Value
rst.Close
For c = grdHora.Row To grdHora.RowSel
xNro_turno = xNro_turno + 1
xHora = Format(TimeValue(Left(grdHora.TextMatrix(c, 0), 5)), "h:mm:ss")
cmd.CommandText = "insert into turno values(" & xNro_turno & ",'" & DateDB & "'," & cmbMascota.ItemData(cmbMascota.ListIndex) & "," & cmbServicio.ItemData(cmbServicio.ListIndex) & ",'" & Format(Calendario.Value, "yyyy-mm-d") & "','" & xHora & "','" & txtMotivo & "',1)"
cmd.Execute
Next c
loadTurno Format(Calendario.Value, "yyyy-mm-d")
temp.Open
Set rptTurno.DataSource = temp
rptTurno.Sections(1).Controls(3).Caption = "Turno para " & cmbServicio.List(cmbServicio.ListIndex)
rptTurno.Sections(1).Controls(5).Caption = "Para el dia " & Calendario.Value & " a las " & xHora & " hs."
End Sub

Private Sub cmbCancelar_Click()
Unload Me
End Sub

Private Sub cmbcliente_Change()
If nocomp = False Then completar cmbCliente Else: nocomp = False
End Sub

Private Sub cmbCliente_Click()
cmbAceptar.Enabled = True
rst.Source = "select nombre,id_hc from mascota_hc where borrado='f' and isnull(fecha_dec)=1 and id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
rst.Open
cmbMascota.Clear
While rst.EOF = False
    cmbMascota.AddItem rst!nombre
    cmbMascota.ItemData(cmbMascota.NewIndex) = rst!id_hc
    rst.MoveNext
Wend
rst.Close
If cmbMascota.ListCount <> 0 Then: cmbMascota.ListIndex = 0
End Sub

Private Sub cmbcliente_KeyPress(KeyAscii As Integer)
cmbAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbCliente.Text = Left(cmbCliente.Text, cmbCliente.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbCliente
End Sub

Private Sub cmbcliente_LostFocus()
validar cmbCliente
End Sub

Private Sub cmbServicio_Click()
loadTurno Format(Calendario.Value, "yyyy-mm-d")
End Sub

Private Sub cmdaceptar_Click()
If Trim(txtMotivo) = "" Then MsgRapido "Debe describir el motivo de la visita.", Loro, vbCritical, , "surprised": Exit Sub
fraMotivo.Visible = False
cmbServicio.Enabled = True
cmbMascota.Enabled = True
cmbCliente.Enabled = True
grdHora.Enabled = True
Calendario.Enabled = True
cmbAceptar.Enabled = True
cmbCancelar.Enabled = True
grabar
loadTurno Format(Calendario.Value, "yyyy-mm-d")
End Sub

Private Sub cmdcancelar_Click()
fraMotivo.Visible = False
cmbServicio.Enabled = True
cmbMascota.Enabled = True
cmbCliente.Enabled = True
grdHora.Enabled = True
Calendario.Enabled = True
cmbAceptar.Enabled = True
cmbCancelar.Enabled = True
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Top = 945
Me.Left = 1980
Calendario.Value = Date
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenDynamic
rst.LockType = adLockOptimistic
rst.Source = "select cliente.nombre,cliente.apellido,cliente.id_cliente from cliente,mascota_hc where cliente.id_cliente=mascota_hc.id_cliente and isnull(mascota_hc.fecha_dec)=1 group by id_cliente"
rst.Open
While rst.EOF = False
    cmbCliente.AddItem rst!apellido & ", " & rst!nombre
    cmbCliente.ItemData(cmbCliente.NewIndex) = rst!id_cliente
    rst.MoveNext
Wend
rst.Close
cmbCliente.ListIndex = 0
cmbServicio.AddItem "Att. a Domicilio"
cmbServicio.ItemData(cmbServicio.NewIndex) = 3
cmbServicio.AddItem "Peluquer�a"
cmbServicio.ItemData(cmbServicio.NewIndex) = 9
cmbServicio.AddItem "Cirug�a"
cmbServicio.ItemData(cmbServicio.NewIndex) = 8
cmbServicio.ListIndex = 0
cmbCliente_Click
loadTurno DateDB
grdHora.ColWidth(0) = 675
grdHora.ColWidth(1) = 1980
grdHora.ColWidth(2) = 1080
grdHora.ColWidth(3) = 0
grdHora.ColWidth(4) = 0
End Sub
Private Sub loadTurno(dia As String)
Dim fila As Byte
rst.Source = "select c.apellido,c.nombre,m.nombre as mascota,t.fecha,t.hora,t.nro_turno,t.motivo,t.estado from cliente c, mascota_hc m,turno t where t.id_hc=m.id_hc and m.id_cliente=c.id_cliente and t.id_servicio=" & cmbServicio.ItemData(cmbServicio.ListIndex) & " and t.fecha='" & dia & "' and t.estado<>2"
rst.Open
grdHora.Clear
grdHora.Rows = 0
grdHora.AddItem "Hora" & vbTab & "Cliente" & vbTab & "Mascota" & vbTab & "Motivo"
grdHora.Rows = 2
grdHora.FixedRows = 1
grdHora.Rows = 1
grdHora.AddItem "9:00 -"
grdHora.AddItem "9:30 -"
grdHora.AddItem "10:00 -"
grdHora.AddItem "10:30 -"
grdHora.AddItem "11:00 -"
grdHora.AddItem "11:30 -"
grdHora.AddItem "12:00 -"
grdHora.AddItem "12:30 -"
grdHora.AddItem "13:00 -"
grdHora.AddItem "13:30 -"
grdHora.AddItem "14:00 -"
grdHora.AddItem "14:30 -"
grdHora.AddItem "15:00 -"
grdHora.AddItem "15:30 -"
grdHora.AddItem "16:00 -"
grdHora.AddItem "16:30 -"
grdHora.AddItem "17:00 -"
grdHora.AddItem "17:30 -"
grdHora.AddItem "18:00 -"
grdHora.AddItem "18:30 -"
grdHora.AddItem "19:00 -"
grdHora.AddItem "19:30 -"
grdHora.AddItem "20:00 -"
grdHora.AddItem "20:30 -"
While rst.EOF = False
    fila = Hour(rst!hora) * 2 - 17
    If Minute(rst!hora) <> 0 Then fila = fila + 1
    grdHora.TextMatrix(fila, 1) = rst!apellido & ", " & rst!nombre
    grdHora.TextMatrix(fila, 2) = rst!mascota
    grdHora.TextMatrix(fila, 3) = rst!motivo
    grdHora.TextMatrix(fila, 4) = rst!nro_turno
    grdHora.Row = fila
    grdHora.Col = 1
    grdHora.ColSel = 3
    If rst!estado = 3 Then grdHora.CellBackColor = vbBlue
    If rst!estado = 4 Then grdHora.CellBackColor = vbRed
rst.MoveNext
Wend
rst.Close
End Sub

Sub cancelarTurno()
Dim c
If Calendario.Value < Date Then Exit Sub
If Calendario.Value = Date And grdHora.Row <= Hour(Time) * 2 - 17 Then Exit Sub
If grdHora.TextMatrix(grdHora.Row, 1) = "" Then Exit Sub
If Mensaje("�Desea cancelar el turno seleccionado?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbYes Then
    For c = grdHora.Row To grdHora.RowSel
        cmd.CommandText = "update turno set estado=2 where nro_turno=" & Val(grdHora.TextMatrix(c, 4))
        cmd.Execute
    Next c
    loadTurno Format(Calendario.Value, "yyyy-mm-d")
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub grdHora_DblClick()
Dim c
If Calendario.Value > Date Then Exit Sub
If Calendario.Value = Date And grdHora.Row >= Hour(Time) * 2 - 17 Then Exit Sub
If grdHora.TextMatrix(grdHora.Row, 1) = "" Then Exit Sub
Select Case Mensaje("�El turno fue utilizado?", Loro, Balloon, vbYesNoCancel + vbQuestion, , "getattention")
Case vbYes
    For c = grdHora.Row To grdHora.RowSel
        cmd.CommandText = "update turno set estado=3 where nro_turno=" & Val(grdHora.TextMatrix(c, 4))
        cmd.Execute
    Next c
Case vbNo
    For c = grdHora.Row To grdHora.RowSel
        cmd.CommandText = "update turno set estado=4 where nro_turno=" & Val(grdHora.TextMatrix(c, 4))
        cmd.Execute
    Next c
Case vbCancel
End Select
loadTurno Format(Calendario.Value, "yyyy-mm-d")
End Sub
