VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRegTrat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Tratamiento"
   ClientHeight    =   5310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6735
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegTrat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5310
   ScaleWidth      =   6735
   Begin VB.CommandButton cmdAgregar 
      Caption         =   "+"
      Height          =   255
      Left            =   6360
      TabIndex        =   22
      ToolTipText     =   "M�s Medicaci�n"
      Top             =   3120
      Width           =   255
   End
   Begin VB.TextBox txtAcciones 
      Height          =   855
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   3840
      Width           =   6495
   End
   Begin VB.TextBox txtDosis 
      Height          =   330
      Left            =   4440
      MaxLength       =   20
      TabIndex        =   8
      Top             =   3120
      Width           =   1815
   End
   Begin VB.TextBox txtSintomas 
      Height          =   330
      Left            =   1560
      MaxLength       =   50
      TabIndex        =   5
      Top             =   2160
      Width           =   3975
   End
   Begin VB.ComboBox cmbTratamiento 
      Height          =   345
      Left            =   1560
      Sorted          =   -1  'True
      TabIndex        =   6
      Text            =   "cmbTratamiento"
      Top             =   2640
      Width           =   3375
   End
   Begin VB.ComboBox cmbMedicacion 
      Height          =   345
      Left            =   1560
      Sorted          =   -1  'True
      TabIndex        =   7
      Text            =   "cmbMedicacion"
      Top             =   3120
      Width           =   2175
   End
   Begin VB.TextBox txtDescripcion 
      Height          =   855
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   1200
      Width           =   6495
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   1680
      TabIndex        =   10
      Top             =   4800
      Width           =   1575
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3480
      TabIndex        =   11
      Top             =   4800
      Width           =   1575
   End
   Begin MSComCtl2.UpDown UpDown 
      Height          =   330
      Left            =   3675
      TabIndex        =   3
      Top             =   600
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   393216
      Value           =   1
      BuddyControl    =   "txtPeso"
      BuddyDispid     =   196618
      OrigLeft        =   2040
      OrigTop         =   840
      OrigRight       =   2280
      OrigBottom      =   1215
      Max             =   10000000
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtPeso 
      Height          =   330
      Left            =   2932
      TabIndex        =   2
      Text            =   "10"
      Top             =   600
      Width           =   735
   End
   Begin VB.TextBox txtHora 
      Enabled         =   0   'False
      Height          =   330
      Left            =   4140
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox txtFecha 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1980
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label lblAcciones 
      Caption         =   "Acciones Tomadas:"
      Height          =   255
      Left            =   2520
      TabIndex        =   21
      Top             =   3600
      Width           =   1695
   End
   Begin VB.Label lblDosis 
      Caption         =   "Dosis:"
      Height          =   255
      Left            =   3840
      TabIndex        =   20
      Top             =   3120
      Width           =   615
   End
   Begin VB.Label lblSintomas 
      Caption         =   "Diagn�stico:"
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Label lblTratamiento 
      Caption         =   "Tratamiento:"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   2640
      Width           =   1095
   End
   Begin VB.Label lblDescripcion 
      Caption         =   "S�ntomas:"
      Height          =   255
      Left            =   2940
      TabIndex        =   16
      Top             =   960
      Width           =   855
   End
   Begin VB.Label lblMedicacion 
      Caption         =   "Medicaci�n:"
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   3120
      Width           =   1095
   End
   Begin VB.Label lblPeso 
      Caption         =   "Peso:"
      Height          =   255
      Left            =   2400
      TabIndex        =   14
      Top             =   600
      Width           =   495
   End
   Begin VB.Label lblGrs 
      Caption         =   "Kgrs."
      Height          =   255
      Left            =   3960
      TabIndex        =   15
      Top             =   600
      Width           =   375
   End
   Begin VB.Label lblHora 
      Caption         =   "Hora:"
      Height          =   255
      Left            =   3660
      TabIndex        =   13
      Top             =   120
      Width           =   495
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   1380
      TabIndex        =   12
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "frmRegTrat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim hold As Boolean
Dim xid_hc As Integer
Dim xPeso As String
Dim nocomp As Boolean

Private Sub cmbMedicacion_Change()
If nocomp = False Then completar cmbMedicacion Else: nocomp = False
End Sub

Private Sub cmbMedicacion_Click()
If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) = 0 Then txtDosis = ""
cmdAceptar.Enabled = True
End Sub

Private Sub cmbMedicacion_KeyPress(KeyAscii As Integer)
cmdAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbMedicacion.Text = Left(cmbMedicacion.Text, cmbMedicacion.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbMedicacion
End Sub

Private Sub cmbMedicacion_LostFocus()
validar cmbMedicacion
End Sub

Private Sub cmbTratamiento_Change()
If nocomp = False Then completar cmbTratamiento Else: nocomp = False
End Sub

Private Sub cmbTratamiento_Click()
cmdAceptar.Enabled = True
End Sub

Private Sub cmbTratamiento_KeyPress(KeyAscii As Integer)
cmdAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbTratamiento.Text = Left(cmbTratamiento.Text, cmbTratamiento.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbTratamiento
End Sub

Private Sub cmbTratamiento_LostFocus()
validar cmbTratamiento
End Sub

Private Sub cmdaceptar_Click()
Dim op
Dim xPrecio, PrecSer
op = Mensaje("�Desea guardar los datos ingresados?", Loro, Balloon, vbQuestion + vbYesNoCancel, , "getattention")
Select Case op
    Case vbYes
        If Trim(txtPeso) = "" Or Trim(txtSintomas) = "" Or Trim(txtDescripcion) = "" Or Trim(txtAcciones) = "" Then
            MsgRapido "Los datos est�n incompletos", Loro, , , "surprised"
            Exit Sub
        End If
        If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) <> 0 And Trim(txtDosis) = "" Then
            MsgRapido "Los datos est�n incompletos", Loro, , , "surprised"
            Exit Sub
        End If
        Dim rst As ADODB.Recordset
        Set rst = New ADODB.Recordset
        rst.ActiveConnection = cnx
        rst.CursorType = adOpenForwardOnly
        rst.LockType = adLockOptimistic
        If cmbTratamiento.ItemData(cmbTratamiento.ListIndex) = 0 Or cmbTratamiento.ItemData(cmbTratamiento.ListIndex) = 8 Then
            PrecSer = Input_Box("Ingrese el precio del Tratamiento.", Loro, Balloon, , "write")
            If IsNumeric(PrecSer) = False Then MsgRapido "Ingrese un precio correcto", Loro, vbCritical, "", "sad": Exit Sub
        Else
            rst.Source = "select precio from servicio where id_servicio=" & cmbTratamiento.ItemData(cmbTratamiento.ListIndex)
            rst.Open
            PrecSer = rst!Precio
            rst.Close
        End If
        If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) <> 0 Then
            rst.Source = "select id_medicamento, stock_total,precio from medicamento where id_medicamento=" & cmbMedicacion.ItemData(cmbMedicacion.ListIndex)
            rst.Open
            If rst!stock_total = 0 Then
                MsgRapido "El stock del medicamento seleccionado se ha agotado.", Loro, vbInformation, , "surprised"
                rst.Close
                Exit Sub
            End If
            rst!stock_total = rst!stock_total - 1
            rst.Update
            xPrecio = rst!Precio
            rst.Close
        End If
        Dim cmd As ADODB.Command
        Set cmd = New ADODB.Command
        cmd.ActiveConnection = cnx
        cmd.CommandText = "insert into detalle_hc values(" & xid_hc & ",'" & DateDB & "','" & TimeDB & "'," & txtPeso & ",'" & txtSintomas & "'," & cmbTratamiento.ItemData(cmbTratamiento.ListIndex) & "," & cmbMedicacion.ItemData(cmbMedicacion.ListIndex) & ",'" & txtDescripcion & "'," & xLegajo & ",'" & Trim(txtDosis) & "','" & Trim(txtAcciones) & "')"
        cmd.Execute
        If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) <> 0 Then
            cmd.CommandText = "insert into nofacturado(id_cliente,fecha,hora,tipo,id_item,precio) values(" & frmhistoria.cmbCliente.ItemData(frmhistoria.cmbCliente.ListIndex) & ",'" & DateDB & "','" & TimeDB & "','med'," & cmbMedicacion.ItemData(cmbMedicacion.ListIndex) & "," & xPrecio & ")"
            cmd.Execute
        End If
        cmd.CommandText = "insert into nofacturado(id_cliente,fecha,hora,tipo,id_item,precio) values(" & frmhistoria.cmbCliente.ItemData(frmhistoria.cmbCliente.ListIndex) & ",'" & DateDB & "','" & TimeDB & "','ser'," & cmbTratamiento.ItemData(cmbTratamiento.ListIndex) & "," & PrecSer & ")"
        cmd.Execute
        If Mensaje("�Desea Imprimir las Acciones Tomadas?", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbYes Then imprimirRec txtFecha, txtDescripcion, txtSintomas, cmbMedicacion.Text & " " & txtDosis, txtAcciones, frmhistoria.txtNombre
        frmhistoria.loadGrid
        frmhistoria.Enabled = True
        Unload Me
    Case vbNo
        frmhistoria.Enabled = True
        Unload Me
    Case vbCancel
        Exit Sub
End Select
End Sub

Private Sub cmdagregar_Click()
Dim xPrecio
If Trim(txtPeso) = "" Or Trim(txtSintomas) = "" Or Trim(txtDescripcion) = "" Or Trim(txtAcciones) = "" Then
    MsgRapido "Los datos est�n incompletos", Loro, , , "surprised"
    Exit Sub
End If
Dim rst As ADODB.Recordset
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenForwardOnly
rst.LockType = adLockOptimistic
If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) <> 0 Then
    rst.Source = "select id_medicamento, stock_total,precio from medicamento where id_medicamento=" & cmbMedicacion.ItemData(cmbMedicacion.ListIndex)
    rst.Open
    If rst!stock_total = 0 Then
        MsgRapido "El stock del medicamento seleccionado se ha agotado.", Loro, vbInformation, , "surprised"
        rst.Close
        Exit Sub
    End If
    rst!stock_total = rst!stock_total - 1
    rst.Update
    xPrecio = rst!Precio
    rst.Close
End If
Dim cmd As ADODB.Command
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
cmd.CommandText = "insert into detalle_hc values(" & xid_hc & ",'" & DateDB & "','" & TimeDB & "'," & txtPeso & ",'" & txtSintomas & "'," & cmbTratamiento.ItemData(cmbTratamiento.ListIndex) & "," & cmbMedicacion.ItemData(cmbMedicacion.ListIndex) & ",'" & txtDescripcion & "'," & xLegajo & ",'" & Trim(txtDosis) & "','" & Trim(txtAcciones) & "')"
cmd.Execute
If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) <> 0 Then
    cmd.CommandText = "insert into nofacturado(id_cliente,fecha,hora,tipo,id_item,precio) values(" & frmhistoria.cmbCliente.ItemData(frmhistoria.cmbCliente.ListIndex) & ",'" & DateDB & "','" & TimeDB & "','med'," & cmbMedicacion.ItemData(cmbMedicacion.ListIndex) & "," & xPrecio & ")"
    cmd.Execute
End If
frmhistoria.loadGrid
End Sub

Private Sub cmdcancelar_Click()
frmhistoria.Enabled = True
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
txtPeso = xPeso
End Sub

Private Sub Form_Load()
frmRegTrat.Top = 585
frmRegTrat.Left = 2175
Dim rstCombo As ADODB.Recordset
Set rstCombo = New ADODB.Recordset
rstCombo.ActiveConnection = cnx
rstCombo.Source = "select id_medicamento, nombre from medicamento where id_rubro=3 and borrado='f' order by id_medicamento"
rstCombo.CursorType = adOpenForwardOnly
rstCombo.LockType = adLockReadOnly
rstCombo.Open
txtFecha = Date
txtHora = Time
While rstCombo.EOF = False
    cmbMedicacion.AddItem rstCombo!nombre
    cmbMedicacion.ItemData(cmbMedicacion.NewIndex) = rstCombo!id_medicamento
    rstCombo.MoveNext
Wend
cmbMedicacion.ListIndex = 0
rstCombo.Close
rstCombo.Source = "select nombre, id_servicio from servicio where id_rubro=5 and id_servicio<>9 and borrado='f' order by id_servicio"
rstCombo.Open
While rstCombo.EOF = False
    cmbTratamiento.AddItem rstCombo!nombre
    cmbTratamiento.ItemData(cmbTratamiento.NewIndex) = rstCombo!id_servicio
    rstCombo.MoveNext
Wend
cmbTratamiento.ListIndex = 0
rstCombo.Close
frmhistoria.Enabled = False
End Sub
Private Sub txtDosis_KeyPress(KeyAscii As Integer)
If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) = 0 Then KeyAscii = 0
End Sub

Private Sub txtPeso_KeyPress(KeyAscii As Integer)
If KeyAscii = 46 And InStr(txtPeso, ".") <> 0 Then KeyAscii = 0: Exit Sub
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtPeso_Validate(Cancel As Boolean)
If txtPeso <> "" Then
    If IsNumeric(txtPeso) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtPeso = ""
    End If
End If
End Sub
Sub setID(paramId_hc As Integer, paramPeso As String)
xid_hc = paramId_hc
xPeso = paramPeso
End Sub
'Private Sub UpDown_DownClick()
'txtPeso = Val(txtPeso) - 0.1
'End Sub
'
'Private Sub UpDown_UpClick()
'txtPeso = Val(txtPeso) + 0.1
'End Sub
