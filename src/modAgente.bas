Attribute VB_Name = "modAgente"
Public Function mensaje(Prompt As String, Personaje As IAgentCtlCharacterEx, Balloon As Balloon, Optional Buttons As VbMsgBoxStyle = vbOKOnly, Optional Title As String = "", Optional Animacion As String = "Alert") As VbMsgBoxResult
If Title = "" Then: Title = App.Title
If Personaje.Visible = False Then
    mensaje = MsgBox(Prompt, Buttons, Title)
Else
    On Error Resume Next
    Personaje.Play Animacion
    On Error GoTo 0
    Personaje.Speak "\map=" + Chr(34) + Prompt + Chr(34) + "=" + Chr(34) + "" + Chr(34) + "\"
    If Buttons < 7 Then
    mensaje = Balloon.MsgBalloon(Prompt, Buttons, , Personaje)
    Else
    mensaje = Balloon.MsgBalloon(Prompt, Buttons, Title, Personaje)
    End If
End If
End Function
Public Sub MsgRapido(Prompt As String, Personaje As IAgentCtlCharacterEx, Optional Buttons As VbMsgBoxStyle = vbOKOnly, Optional Title As String = "", Optional Animacion As String = "alert")
If Title = "" Then: Title = App.Title
If Personaje.Visible = False Then
    MsgBox Prompt, Buttons, Title
Else
    Loro.Stop
    On Error Resume Next
    Personaje.Play Animacion
    On Error GoTo 0
    Personaje.Speak Prompt
End If
End Sub
Public Function Input_Box(Prompt As String, Personaje As IAgentCtlCharacterEx, Balloon As Balloon, Optional Default As String = "", Optional Animacion As String = "Alert", Optional Title As String = "", Optional XPos As Integer, Optional YPos As Integer) As String
If Title = "" Then: Title = App.Title
If XPos = 0 Then XPos = 3000
If YPos = 0 Then YPos = 3000
If Personaje.Visible = False Then
    Input_Box = InputBox(Prompt, Title, Default, XPos, YPos)
Else
    On Error Resume Next
    Personaje.Play Animacion
    On Error GoTo 0
    Personaje.Speak "\map=" + Chr(34) + Prompt + Chr(34) + "=" + Chr(34) + "" + Chr(34) + "\"
    Input_Box = Balloon.InputBalloon(Prompt, , Default, Personaje)
End If
End Function
