VERSION 5.00
Begin VB.Form frmRegistrar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registro de Suquia"
   ClientHeight    =   3495
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6060
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegistrar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3495
   ScaleWidth      =   6060
   Begin VB.CommandButton cmdVolver 
      Caption         =   "&Volver"
      Height          =   495
      Left            =   2303
      TabIndex        =   10
      Top             =   2880
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "Activar"
      Height          =   1455
      Left            =   143
      TabIndex        =   3
      Top             =   1200
      Width           =   5775
      Begin VB.CommandButton cmdActivar 
         Caption         =   "Activar"
         Height          =   375
         Left            =   3960
         TabIndex        =   9
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox txtKey 
         ForeColor       =   &H80000007&
         Height          =   330
         Left            =   2160
         TabIndex        =   8
         Top             =   960
         Width           =   1575
      End
      Begin VB.TextBox txtSoftwareCode 
         BackColor       =   &H80000013&
         Enabled         =   0   'False
         Height          =   330
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label lblKey 
         Caption         =   "Clave de activaci�n:"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label lblNota 
         Caption         =   "Si tiene una clave de activaci�n, ingresela aqui y presione Activar."
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   5415
      End
      Begin VB.Label lblCodigo 
         Caption         =   "C�digo del Software:"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Estado"
      Height          =   975
      Left            =   143
      TabIndex        =   0
      Top             =   120
      Width           =   5775
      Begin VB.Label lblDias 
         Caption         =   "Label1"
         Height          =   225
         Left            =   345
         TabIndex        =   2
         Top             =   600
         Width           =   5370
      End
      Begin VB.Label lblStatus 
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   225
         Left            =   345
         TabIndex        =   1
         Top             =   240
         Width           =   5370
      End
   End
End
Attribute VB_Name = "frmRegistrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdActivar_Click()
SaveSetting "Suquia", "Settings", "KeyCode", txtKey
frmPrincipal.Lock.Register txtKey
Form_Load
End Sub

Private Sub cmdVolver_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
If frmPrincipal.Lock.RegisteredUser = False Then
    lblStatus.Caption = "Versi�n de Prueba"
Else
    lblStatus.Caption = "Versi�n Registrada"
    txtKey = GetSetting("Suquia", "Settings", "KeyCode")
    txtKey.BackColor = &H80000013
    txtKey.Locked = True
    txtKey.Enabled = False
    cmdActivar.Enabled = False
End If
lblDias.Caption = "Dias de uso: " & frmPrincipal.Lock.UsedDays
txtSoftwareCode = frmPrincipal.Lock.SoftwareCode

End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub
