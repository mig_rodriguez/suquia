VERSION 5.00
Object = "{F5BE8BC2-7DE6-11D0-91FE-00C04FD701A5}#2.0#0"; "AGENTCTL.DLL"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{879115B9-8D7C-43CA-ADFE-8B489017BF42}#1.0#0"; "ACTIVELOCK.OCX"
Object = "{968C5DE0-8D94-4696-9AD7-8C98F12EFE3B}#3.1#0"; "BLNDIALOG.OCX"
Begin VB.MDIForm frmPrincipal 
   BackColor       =   &H8000000C&
   Caption         =   "Suquia"
   ClientHeight    =   5040
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   8190
   Icon            =   "frmPrincipal.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   Picture         =   "frmPrincipal.frx":1272
   WindowState     =   2  'Maximized
   Begin BlnDialog.Balloon Balloon1 
      Left            =   1920
      Top             =   840
      _ExtentX        =   820
      _ExtentY        =   767
      ButtonPicture   =   "frmPrincipal.frx":9F6A
      OptionPicture   =   "frmPrincipal.frx":D454
      SuggestPicture  =   "frmPrincipal.frx":DB66
      URLButtonPicture=   ""
      URLOptionPicture=   ""
      URLIconPicture  =   ""
      URLSuggestPicture=   ""
   End
   Begin activelock1884.ActiveLock Lock 
      Left            =   2400
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   820
      SoftwareName    =   "SuquiaSoft"
      SoftwarePassword=   "41679"
      LiberationKeyLength=   16
      SoftwareCodeLength=   6
      LockToHardDrive =   0   'False
      LockToWindowsSerial=   -1  'True
      LockToRandomNumber=   -1  'True
      LockToComputerName=   0   'False
      LockToMACAddress=   0   'False
      UseDataLock     =   0   'False
      RegistryPath    =   "Codes"
      RegistryKey     =   "WinSoft"
      RegistryName    =   "Retsiger"
      RegistryHive    =   "HKCU"
      LockToCustomString=   ""
      HashAlgorithm   =   0
      RegCounterKey   =   "Retnuoc"
      RegLiberationKey=   "Noitarebil"
      RegLastRunDateKey=   "Etadnurtsal"
      RegInitialRunDateKey=   "Atednurlaitini"
      RegRandomKey    =   "Modnar"
      EncKey          =   "Default"
      RegEncKey       =   -1  'True
   End
   Begin MSComDlg.CommonDialog cuadros 
      Left            =   360
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList toolPic 
      Left            =   840
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   0
      Top             =   4695
      Width           =   8190
      _ExtentX        =   14446
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   8
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            Bevel           =   0
            Enabled         =   0   'False
            Object.Width           =   18
            MinWidth        =   18
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   0
            Object.Width           =   10585
            MinWidth        =   10585
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   1
            Object.Width           =   1323
            MinWidth        =   1323
            TextSave        =   "MAY�S"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            Object.Width           =   1323
            MinWidth        =   1323
            TextSave        =   "N�M"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   3
            Enabled         =   0   'False
            Object.Width           =   1324
            MinWidth        =   1324
            TextSave        =   "INS"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Bevel           =   0
            Object.Width           =   1588
            MinWidth        =   1588
            TextSave        =   "07:25 p.m."
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Bevel           =   0
            Object.Width           =   1764
            MinWidth        =   1764
            TextSave        =   "30/11/2003"
         EndProperty
      EndProperty
   End
   Begin ComCtl3.CoolBar CoolBar1 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8190
      _ExtentX        =   14446
      _ExtentY        =   688
      _CBWidth        =   8190
      _CBHeight       =   390
      _Version        =   "6.0.8169"
      Child1          =   "Toolbar1"
      MinWidth1       =   1800
      MinHeight1      =   330
      Width1          =   1800
      NewRow1         =   0   'False
      Child2          =   "Toolbar2"
      MinWidth2       =   795
      MinHeight2      =   330
      Width2          =   990
      NewRow2         =   0   'False
      Child3          =   "Toolbar3"
      MinHeight3      =   330
      Width3          =   8895
      NewRow3         =   0   'False
      Begin MSComctlLib.Toolbar Toolbar3 
         Height          =   330
         Left            =   3210
         TabIndex        =   4
         Top             =   30
         Width           =   4890
         _ExtentX        =   8625
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         Style           =   1
         ImageList       =   "toolPic"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "conhc"
               Object.ToolTipText     =   "Historia Clinica"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "facturar"
               Object.ToolTipText     =   "Facturar"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   330
         Left            =   165
         TabIndex        =   3
         Top             =   30
         Width           =   1800
         _ExtentX        =   3175
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         Style           =   1
         ImageList       =   "toolPic"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   5
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "nuevo"
               Object.ToolTipText     =   "Nuevo"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "modificar"
               Object.ToolTipText     =   "Modificar"
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "guardar"
               Object.ToolTipText     =   "Guardar"
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "borrar"
               Object.ToolTipText     =   "Borrar"
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "buscar"
               Object.ToolTipText     =   "Buscar"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar2 
         Height          =   330
         Left            =   2190
         TabIndex        =   2
         Top             =   30
         Width           =   795
         _ExtentX        =   1402
         _ExtentY        =   582
         ButtonWidth     =   609
         ButtonHeight    =   582
         Style           =   1
         ImageList       =   "toolPic"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "imprimir"
               Object.ToolTipText     =   "Imprimir"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "preview"
               Object.ToolTipText     =   "Vista Previa"
            EndProperty
         EndProperty
      End
   End
   Begin AgentObjectsCtl.Agent Agent1 
      Left            =   1440
      Top             =   840
      _cx             =   847
      _cy             =   847
   End
   Begin VB.Menu archivo 
      Caption         =   "&Archivo"
      Begin VB.Menu nuevo 
         Caption         =   "&Nuevo"
         Enabled         =   0   'False
         Shortcut        =   ^N
      End
      Begin VB.Menu cerrar 
         Caption         =   "&Cerrar"
         Enabled         =   0   'False
      End
      Begin VB.Menu sep1 
         Caption         =   "-"
      End
      Begin VB.Menu guardar 
         Caption         =   "&Guardar"
         Enabled         =   0   'False
      End
      Begin VB.Menu modificar 
         Caption         =   "&Modificar"
         Enabled         =   0   'False
      End
      Begin VB.Menu borrar 
         Caption         =   "&Borrar"
         Enabled         =   0   'False
      End
      Begin VB.Menu sep2 
         Caption         =   "-"
      End
      Begin VB.Menu config 
         Caption         =   "C&onfigurar Impresora"
      End
      Begin VB.Menu vista 
         Caption         =   "&Vista Preliminar"
         Enabled         =   0   'False
      End
      Begin VB.Menu imprimir 
         Caption         =   "&Imprimir"
         Enabled         =   0   'False
         Shortcut        =   ^P
      End
      Begin VB.Menu sep3 
         Caption         =   "-"
      End
      Begin VB.Menu salir 
         Caption         =   "&Salir"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu edicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu deshacer 
         Caption         =   "&Deshacer"
         Shortcut        =   ^Z
      End
      Begin VB.Menu rehacer 
         Caption         =   "Re&hacer"
      End
      Begin VB.Menu sep4 
         Caption         =   "-"
      End
      Begin VB.Menu cortar 
         Caption         =   "Cor&tar"
         Shortcut        =   ^X
      End
      Begin VB.Menu copiar 
         Caption         =   "&Copiar"
         Shortcut        =   ^C
      End
      Begin VB.Menu pegar 
         Caption         =   "Peg&ar"
         Shortcut        =   ^V
      End
      Begin VB.Menu borrar2 
         Caption         =   "&Borrar"
         Shortcut        =   {DEL}
      End
      Begin VB.Menu sep5 
         Caption         =   "-"
      End
      Begin VB.Menu buscar 
         Caption         =   "B&uscar"
         Enabled         =   0   'False
         Shortcut        =   ^B
      End
      Begin VB.Menu todo 
         Caption         =   "&Seleccionar Todo"
         Shortcut        =   ^E
      End
   End
   Begin VB.Menu atencion 
      Caption         =   "A&tenci�n"
      Begin VB.Menu actclientes 
         Caption         =   "Actualizar &Clientes"
      End
      Begin VB.Menu actmascota 
         Caption         =   "Actualizar &Mascotas"
      End
      Begin VB.Menu actraza 
         Caption         =   "Actualizar R&azas"
      End
      Begin VB.Menu actespecie 
         Caption         =   "Actualizar &Especies"
      End
      Begin VB.Menu actserv 
         Caption         =   "Actualizar &Servicios"
      End
      Begin VB.Menu sep6 
         Caption         =   "-"
      End
      Begin VB.Menu conhc 
         Caption         =   "Consultar &Historias Cl�nicas"
      End
      Begin VB.Menu sep16 
         Caption         =   "-"
      End
      Begin VB.Menu turnos 
         Caption         =   "Registrar T&urnos"
      End
      Begin VB.Menu reclamo 
         Caption         =   "Registrar &Reclamo"
      End
      Begin VB.Menu sep12 
         Caption         =   "-"
      End
      Begin VB.Menu infturnos 
         Caption         =   "Informe de &Turnos"
      End
      Begin VB.Menu infenferm 
         Caption         =   "Informe de &Enfermedades"
      End
      Begin VB.Menu cruza 
         Caption         =   "Informe de Animales para Cru&za"
      End
   End
   Begin VB.Menu prepago 
      Caption         =   "&Prepago"
      Begin VB.Menu actprepago 
         Caption         =   "&Actualizar Preprago"
      End
      Begin VB.Menu afiliar 
         Caption         =   "A&filiar a Prepago"
      End
      Begin VB.Menu vtaPP 
         Caption         =   "&Informar sobre Prepagos"
      End
      Begin VB.Menu sep8 
         Caption         =   "-"
      End
      Begin VB.Menu recpago 
         Caption         =   "Registrar &Pago"
      End
      Begin VB.Menu vermoros 
         Caption         =   "Verificar &Morosidad"
      End
      Begin VB.Menu infmoros 
         Caption         =   "Informe de M&orosidad"
      End
   End
   Begin VB.Menu ventas 
      Caption         =   "Ve&ntas"
      Begin VB.Menu actmedicam 
         Caption         =   "Actualizar &Medicamentos"
      End
      Begin VB.Menu actartic 
         Caption         =   "Actualizar &Art�culos"
      End
      Begin VB.Menu acttarjeta 
         Caption         =   "Actualizar &Tarjetas"
      End
      Begin VB.Menu actplanp 
         Caption         =   "Actualizar &Planes de Pago"
      End
      Begin VB.Menu sep13 
         Caption         =   "-"
      End
      Begin VB.Menu facturar 
         Caption         =   "&Facturar"
      End
   End
   Begin VB.Menu compras 
      Caption         =   "&Compras"
      Begin VB.Menu actproveedores 
         Caption         =   "Actualizar &Proveedores"
      End
      Begin VB.Menu sep7 
         Caption         =   "-"
      End
      Begin VB.Menu stock 
         Caption         =   "Controlar &Stock"
      End
      Begin VB.Menu orden 
         Caption         =   "Emitir &Orden de Compra"
      End
      Begin VB.Menu mercaderia 
         Caption         =   "Tramitar �rdenes de Compra"
      End
      Begin VB.Menu pago 
         Caption         =   "Registrar Pa&go a Proveedores"
      End
      Begin VB.Menu sep14 
         Caption         =   "-"
      End
      Begin VB.Menu recpedido 
         Caption         =   "&Reclamar Pedido"
      End
   End
   Begin VB.Menu info 
      Caption         =   "&Informes y Reportes"
      Begin VB.Menu inffrec 
         Caption         =   "Informe de &Frecuencia de Atenciones"
      End
      Begin VB.Menu infoc 
         Caption         =   "Informe de �rdenes de &Compra"
      End
      Begin VB.Menu infclihab 
         Caption         =   "Informe de Clientes &habituales de Peluquer�a"
      End
      Begin VB.Menu infventas 
         Caption         =   "Informe de &Ventas"
      End
      Begin VB.Menu consaProv 
         Caption         =   "Consulta de Reclamos a Proveedores"
      End
      Begin VB.Menu sep9 
         Caption         =   "-"
      End
      Begin VB.Menu estPac 
         Caption         =   "Estad�sticas de &Pacientes"
      End
      Begin VB.Menu estRec 
         Caption         =   "Estad�sticas de &Reclamos"
      End
      Begin VB.Menu estvta 
         Caption         =   "Estad�sticas de V&entas"
      End
   End
   Begin VB.Menu sistema 
      Caption         =   "&Sistema"
      Begin VB.Menu actempleado 
         Caption         =   "Actualizar &Empleados"
      End
      Begin VB.Menu rubro 
         Caption         =   "Actualizar &Rubros"
      End
      Begin VB.Menu sep15 
         Caption         =   "-"
      End
      Begin VB.Menu sesion 
         Caption         =   "&Inicio de Sesi�n"
      End
      Begin VB.Menu password 
         Caption         =   "Cambiar &Contrase�a"
      End
      Begin VB.Menu actPerm 
         Caption         =   "Actualizaci�n de &Permisos"
      End
      Begin VB.Menu servidor 
         Caption         =   "Configurar &Servidor"
      End
   End
   Begin VB.Menu ventana 
      Caption         =   "&Ventana"
      Begin VB.Menu cascada 
         Caption         =   "&Cascada"
      End
      Begin VB.Menu mosaico 
         Caption         =   "&Mosaico"
      End
      Begin VB.Menu iconos 
         Caption         =   "&Organizar �conos"
      End
   End
   Begin VB.Menu ayuda 
      Caption         =   "A&yuda"
      Begin VB.Menu contenido 
         Caption         =   "&Contenido"
      End
      Begin VB.Menu registrar 
         Caption         =   "&Registrar Suquia"
      End
      Begin VB.Menu asistente 
         Caption         =   "Mostrar As&istente"
      End
      Begin VB.Menu sep11 
         Caption         =   "-"
      End
      Begin VB.Menu acerca 
         Caption         =   "&Acerca de Suquia..."
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim prevState As Boolean
Dim animaciones() As String
Dim c
Dim AnimationName
Dim noAgent As Boolean
Const HH_DISPLAY_TOPIC = &H0
Const HH_SET_WIN_TYPE = &H4
Const HH_GET_WIN_TYPE = &H5
Const HH_GET_WIN_HANDLE = &H6
Const HH_DISPLAY_TEXT_POPUP = &HE
Const HH_HELP_CONTEXT = &HF
Const HH_TP_HELP_CONTEXTMENU = &H10
Const HH_TP_HELP_WM_HELP = &H11
Const HH_CLOSE_ALL = &H12
Private Declare Function HtmlHelp Lib "hhctrl.ocx" Alias "HtmlHelpA" (ByVal hwndCaller As Long, ByVal pszFile As String, ByVal uCommand As Long, ByVal dwData As Long) As Long

Private Sub acerca_Click()
frmAbout.Show
End Sub

Private Sub actclientes_Click()
frmactcliente.Show
End Sub


Private Sub actespecie_Click()
frmActEspecie.Show
End Sub

Private Sub actPerm_Click()
frmActPermiso.Show
End Sub

Private Sub actplanp_Click()
frmActPlanpago.Show
End Sub

Private Sub actprepago_Click()
frmActPP.Show
End Sub

Private Sub actproveedores_Click()
frmActProveedor.Show
End Sub

Private Sub actraza_Click()
frmActRaza.Show
End Sub

Private Sub acttarjeta_Click()
frmActTarjeta.Show
End Sub

Private Sub afiliar_Click()
frmActPrePago.Show
End Sub

Private Sub Agent1_Command(ByVal UserInput As Object)
Dim anim As String
Select Case UserInput.Name
    Case "propiedades"
        Agent1.PropertySheet.Visible = True
    Case "galeria"
        Agent1.ShowDefaultCharacterProperties
    Case "animar"
        Loro.Stop
otra:
        anim = animaciones(Int((c - 1) * Rnd + 0))
        If Left(anim, 4) = "Look" Or Left(anim, 4) = "Idle" Then: GoTo otra
        Loro.Play anim
End Select
End Sub

Private Sub Agent1_DefaultCharacterChange(ByVal GUID As String)
loadAnim
End Sub

Private Sub asistente_Click()
If noAgent = False Then Loro.Show
End Sub
Private Sub Balloon1_OnSuggestClick(IDSuggest As Integer)
Select Case IDSuggest
    Case 1
        Balloon.TipBalloon "Si desea desplazarse al campo anterior presione <SHIF>+<TAB>", 1, , , 10000, , Loro
End Select
End Sub

Private Sub borrar_Click()
ActiveForm.borrar
End Sub

Private Sub borrar2_Click()
On Error Resume Next
If ActiveForm.ActiveControl.SelText <> "" Then
ActiveForm.ActiveControl.SelText = ""
Else
ActiveForm.ActiveControl.SelLength = 1
ActiveForm.ActiveControl.SelText = ""
End If
ActiveForm.cancelarTurno
End Sub

Private Sub buscar_Click()
ActiveForm.buscar
End Sub

Private Sub cascada_Click()
frmPrincipal.Arrange (0)
End Sub

Private Sub cerrar_Click()
Unload ActiveForm
End Sub

Private Sub config_Click()
cuadros.ShowPrinter
End Sub

Private Sub conhc_Click()
frmhistoria.Show
End Sub

Private Sub consaProv_Click()
frmConsaProv.Show
End Sub

Private Sub contenido_Click()
HtmlHelp 0, App.HelpFile, HH_DISPLAY_TOPIC, 0
End Sub

Private Sub copiar_Click()
On Error Resume Next
Clipboard.SetText ActiveForm.ActiveControl.SelText
ActiveForm.ActiveControl.EditCopy
End Sub

Private Sub cortar_Click()
On Error Resume Next
Clipboard.SetText ActiveForm.ActiveControl.SelText
ActiveForm.ActiveControl.SelText = ""
End Sub

Private Sub cruza_Click()
frmInfoCruza.Show
End Sub

Private Sub estPac_Click()
frmEstPac.Show
End Sub

Private Sub estRec_Click()
frmestdereclamos.Show
End Sub

Private Sub estvta_Click()
frmEstVentas.Show
End Sub

Private Sub facturar_Click()
frmFactura.Show
End Sub

Private Sub guardar_Click()
ActiveForm.guardar
End Sub

Private Sub iconos_Click()
frmPrincipal.Arrange (3)
End Sub

Private Sub imprimir_Click()
ActiveForm.imprimir
End Sub

Private Sub indice_Click()
HtmlHelp hWnd, App.HelpFile, HH_HELP_CONTEXT, 0
End Sub

Private Sub infclihab_Click()
frmInfoHab.Show
End Sub

Private Sub infenferm_Click()
frmInfoEnferm.Show
End Sub

Private Sub inffrec_Click()
frmFrecuencia.Show
End Sub

Private Sub infmoros_Click()
frmInfoMorosidad.Show
End Sub

Private Sub infoc_Click()
frmInfoOc.Show
End Sub

Private Sub infturnos_Click()
frmInfoTurnos.Show
End Sub

Private Sub infventas_Click()
frmInfoVta.Show
End Sub

Private Sub Lock_Registration(WasSuccessful As Boolean)
    If WasSuccessful Then
        MsgRapido "Gracias!", Loro, vbInformation, , "congratulate"
    Else
        MsgRapido "La clave ingresada no es correcta.", Loro, vbExclamation, , "sad"
    End If
End Sub


Private Sub MDIForm_Load()
noAgent = False
On Error GoTo errHandle
Agent1.Characters.Load "loro"
', App.Path & "\bitmaps\peedy.acs"
Set Loro = Agent1.Characters("loro")
Loro.Top = 400
Loro.Left = 650
Loro.Commands.Add "propiedades", "&Propiedades"
Loro.Commands.Add "galeria", "&Galeria"
Loro.Commands.Add "animar", "&Animar"
loadAnim
checkTrial
Loro.Show
If noAgent = True Then Loro.Hide
Loro.Play "Greet"
Loro.Speak "A sus �rdenes se�or."
checkLote
frmLogin.Show
errHandle:
If Err.Number = -2147213289 Then
Agent1.Characters.Load "loro", App.Path & "\bitmaps\default.acs"
noAgent = True
Resume Next
End If
End Sub
Private Sub loadAnim()
c = 0
For Each AnimationName In Loro.AnimationNames
c = c + 1
Next
ReDim animaciones(c)
c = 0
For Each AnimationName In Loro.AnimationNames
    animaciones(c) = AnimationName
    c = c + 1
Next
End Sub

Private Sub MDIForm_Resize()
If Me.WindowState = 1 Then
    Loro.Hide True
    prevState = True
Else
    If prevState = True Then prevState = False: If noAgent = False Then Loro.Show True
End If
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
If Mensaje("�Desea abandonar Suquia?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbYes Then
    cnx.Close
    HtmlHelp Me.hWnd, "", HH_CLOSE_ALL, 0
Else
    Cancel = 1
End If
End Sub

Private Sub mercaderia_Click()
frmRegMercaderia.Show
End Sub

Private Sub modificar_Click()
ActiveForm.modificar
End Sub

Private Sub mosaico_Click()
frmPrincipal.Arrange (1)
End Sub

Private Sub nuevo_Click()
ActiveForm.nuevo
End Sub

Private Sub pago_Click()
frmRegPagoProv.Show
End Sub

Private Sub password_Click()
frmPassword.Show
End Sub

Private Sub pegar_Click()
On Error Resume Next
ActiveForm.ActiveControl.SelText = Clipboard.GetText
End Sub

Private Sub reclamo_Click()
frmRegReclamo.Show
End Sub

Private Sub recpago_Click()
frmRegPagoPP.Show
End Sub

Private Sub recpedido_Click()
frmReclamoPed.Show
End Sub

Private Sub registrar_Click()
frmRegistrar.Show
End Sub

Private Sub rubro_Click()
frmActRubro.Show
End Sub

Private Sub salir_Click()
Unload frmPrincipal
End Sub

Private Sub servidor_Click()
frmServidor.Show
End Sub

Private Sub sesion_Click()
frmLogin.Show
End Sub

Private Sub stock_Click()
frmConStock.Show
End Sub

Private Sub todo_Click()
On Error Resume Next
ActiveForm.ActiveControl.SelStart = 0
ActiveForm.ActiveControl.SelLength = Len(ActiveForm.ActiveControl)
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button.Key
    Case "nuevo"
    ActiveForm.nuevo
    Case "modificar"
    ActiveForm.modificar
    Case "guardar"
    ActiveForm.guardar
    Case "borrar"
    ActiveForm.borrar
    Case "buscar"
    ActiveForm.buscar
   End Select
End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
    Case "imprimir"
    ActiveForm.imprimir
    Case "preview"
    ActiveForm.preview
End Select
End Sub

Private Sub Toolbar3_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
    Case "conhc"
    frmhistoria.Show
    Case "facturar"
    frmFactura.Show
End Select
End Sub

Private Sub turnos_Click()
frmRegTurno.Show
End Sub

Private Sub vermoros_Click()
frmVerMorosidad.Show
End Sub

Private Sub vista_Click()
ActiveForm.preview
End Sub
Sub activararch()
Dim c
With frmPrincipal
.nuevo.Enabled = True
.cerrar.Enabled = True
.modificar.Enabled = True
.borrar.Enabled = True
.imprimir.Enabled = True
.buscar.Enabled = True
.vista.Enabled = True
For c = 1 To 5 Step 1
.Toolbar1.Buttons.Item(c).Enabled = True
Next c
.Toolbar1.Buttons.Item(3).Enabled = False
For c = 1 To 2 Step 1
.Toolbar2.Buttons.Item(c).Enabled = True
Next c
End With
End Sub
Sub desactivararch()
Dim c
With frmPrincipal
.nuevo.Enabled = False
.cerrar.Enabled = False
.modificar.Enabled = False
.guardar.Enabled = False
.borrar.Enabled = False
.imprimir.Enabled = False
.buscar.Enabled = False
.vista.Enabled = False
For c = 1 To 5 Step 1
.Toolbar1.Buttons.Item(c).Enabled = False
Next c
For c = 1 To 2 Step 1
.Toolbar2.Buttons.Item(c).Enabled = False
Next c
End With
End Sub
Private Sub checkTrial()
With frmPrincipal.Lock
If .RegisteredUser = True Then: Exit Sub
If .LastRunDate > Now Then
    MsgRapido "Este programa de prueba ha detectado que el reloj ha sido retrocedido.", Loro, vbCritical, , "sad"
    End
End If
If .UsedDays < 31 Then
    MsgBox "Esta es una version de prueba." & Chr(13) & "El per�odo de evaluaci�n es de 30 dias," & Chr(13) & "luego del cual debe registrarse para continuar su uso." & Chr(13) & "Vea la opci�n 'Registrar Suquia' del men� 'Ayuda' para m�s informaci�n.", vbInformation
Else
    MsgBox "El periodo de evaluaci�n ha concluido"
    End
End If
End With
End Sub
Private Sub checkLote()
Dim rst As ADODB.Recordset
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenStatic
rst.Source = "select * from lote l,proveedor p,medicamento m where l.id_proveedor=p.id_proveedor and l.id_item=m.id_medicamento and tipo_item=2 and fecha_vto<'" & DateDB & "'"
rst.Open
If rst.RecordCount <> 0 Then frmVencido.cargar rst
Set rst = Nothing
End Sub
Private Sub actempleado_Click()
frmactempleado.Show
End Sub

Private Sub orden_Click()
frmOrdenCompra.Show
End Sub

Private Sub actmascota_Click()
frmactmascota.Show
End Sub

Private Sub actartic_Click()
frmActArticulo.Show
End Sub

Private Sub actserv_Click()
frmActServicio.Show
End Sub

Private Sub actmedicam_Click()
frmActMedicamento.Show
End Sub

Private Sub vtaPP_Click()
frmVtaPP.Show
End Sub
