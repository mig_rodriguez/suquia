VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmestdereclamos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Estadisticas de Reclamos"
   ClientHeight    =   6675
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7845
   Icon            =   "frmestdereclamo.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6675
   ScaleWidth      =   7845
   Begin MSChart20Lib.MSChart Grafico1 
      Height          =   4815
      Left            =   240
      OleObjectBlob   =   "frmestdereclamo.frx":1272
      TabIndex        =   7
      ToolTipText     =   "Hacer doble click para ver detalles"
      Top             =   960
      Width           =   7455
   End
   Begin VB.CommandButton cmdvolver 
      Caption         =   "&Volver"
      Height          =   375
      Left            =   2895
      TabIndex        =   0
      Top             =   5880
      Width           =   2055
   End
   Begin VB.CommandButton cmdsalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   6480
      TabIndex        =   6
      Top             =   6240
      Width           =   1215
   End
   Begin VB.ComboBox cmba�o 
      Height          =   315
      Left            =   4995
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   600
      Width           =   1095
   End
   Begin VB.ComboBox cmbmes 
      Height          =   315
      Left            =   2835
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   600
      Width           =   1695
   End
   Begin VB.Label lbltitulo 
      Alignment       =   2  'Center
      Caption         =   "Label3"
      BeginProperty Font 
         Name            =   "OCR A Extended"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1035
      TabIndex        =   5
      Top             =   120
      Width           =   5775
   End
   Begin VB.Label Label2 
      Caption         =   "de"
      Height          =   255
      Left            =   4635
      TabIndex        =   4
      Top             =   600
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Fecha desde:"
      Height          =   255
      Left            =   1755
      TabIndex        =   1
      Top             =   600
      Width           =   1095
   End
End
Attribute VB_Name = "frmestdereclamos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim rstserie As ADODB.Recordset
Dim band As Integer
Dim fe

Private Sub cmba�o_Click()
 'MsgBox " " & cmba�o.Text & " / " & cmbmes.ItemData(cmbmes.ListIndex) & " / " & 1
 If band = 1 Then
  graficar
 End If
End Sub

Private Sub cmbmes_Click()
 'MsgBox " " & cmba�o.Text & " / " & cmbmes.ItemData(cmbmes.ListIndex) & " / " & 1
 If band = 1 Then
  graficar
 End If
End Sub

Private Sub cmdsalir_Click()
 Unload Me
End Sub

Private Sub cmdvolver_Click()
 graficar
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
cmbmes.AddItem "Enero"
cmbmes.ItemData(cmbmes.NewIndex) = 1
cmbmes.AddItem "Febrero"
cmbmes.ItemData(cmbmes.NewIndex) = 2
cmbmes.AddItem "Marzo"
cmbmes.ItemData(cmbmes.NewIndex) = 3
cmbmes.AddItem "Abril"
cmbmes.ItemData(cmbmes.NewIndex) = 4
cmbmes.AddItem "Mayo"
cmbmes.ItemData(cmbmes.NewIndex) = 5
cmbmes.AddItem "Junio"
cmbmes.ItemData(cmbmes.NewIndex) = 6
cmbmes.AddItem "Julio"
cmbmes.ItemData(cmbmes.NewIndex) = 7
cmbmes.AddItem "Agosto"
cmbmes.ItemData(cmbmes.NewIndex) = 8
cmbmes.AddItem "Septiembre"
cmbmes.ItemData(cmbmes.NewIndex) = 9
cmbmes.AddItem "Octubre"
cmbmes.ItemData(cmbmes.NewIndex) = 10
cmbmes.AddItem "Noviembre"
cmbmes.ItemData(cmbmes.NewIndex) = 11
cmbmes.AddItem "Diciembre"
cmbmes.ItemData(cmbmes.NewIndex) = 12
cmba�o.AddItem "1995"
cmba�o.AddItem "1996"
cmba�o.AddItem "1997"
cmba�o.AddItem "1998"
cmba�o.AddItem "1999"
cmba�o.AddItem "2000"
cmba�o.AddItem "2001"
cmba�o.AddItem "2002"
cmba�o.AddItem "2003"
cmbmes.ListIndex = 0
cmba�o.ListIndex = 0
band = 1
graficar
End Sub
Private Sub graficar()
Dim c
lblTitulo.Caption = "Reclamos en General"
fe = cmba�o.Text & "-" & cmbmes.ItemData(cmbmes.ListIndex) & "-01"
rst.Source = "select tipo, count(tipo)as conteo from reclamo where nro_reclamo>0 and fecha >= '" & fe & "' group by tipo"
rst.Open
Grafico1.ColumnCount = rst.RecordCount
For c = 1 To Grafico1.ColumnCount
    Grafico1.Column = c
    Grafico1.Data = 0
Next c
c = 0
While Not rst.EOF
    If c < 10 Then c = c + 1
    Grafico1.Column = c
    Grafico1.Data = Val(rst!conteo)
    Grafico1.ColumnLabel = rst!tipo
    rst.MoveNext
Wend
Grafico1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "Cantidad"
rst.Close
End Sub

Private Sub grafico1_SeriesActivated(Series As Integer, MouseFlags As Integer, Cancel As Integer)
 If lblTitulo.Caption = "Reclamos en General" Then
 Dim c, suma
 Dim valor
 Set rstserie = New ADODB.Recordset
 rstserie.ActiveConnection = cnx
 rstserie.LockType = adLockReadOnly
 rstserie.CursorType = adOpenForwardOnly
 rstserie.Source = "select tipo from reclamo where nro_reclamo>0 and fecha >= '" & fe & "' group by tipo"
 rstserie.Open
 rstserie.MoveFirst
 For c = 1 To Series
     If c = Series Then
        valor = rstserie(0)
       Else
        rstserie.MoveNext
     End If
 Next
 rstserie.Close
 Select Case valor
  
  Case "Art�culos"
    lblTitulo.Caption = "Reclamos de Art�culos"
    rstserie.Source = "select articulo.nombre, count(reclamo.id_item)as conteo From reclamo, articulo where ((reclamo.fecha >= '" & fe & "') and (reclamo.tipo_item =3) and (reclamo.id_item=articulo.id_articulo)) group by articulo.nombre order by conteo desc"
    rstserie.Open
    If rstserie.RecordCount < 11 Then
       Grafico1.ColumnCount = rstserie.RecordCount
      Else
       Grafico1.ColumnCount = 10
    End If
    For c = 1 To Grafico1.ColumnCount
       Grafico1.Column = c
       Grafico1.Data = 0
    Next c
    c = 0
    suma = 0
    While Not rstserie.EOF
      If c < 10 Then c = c + 1
      Grafico1.Column = c
      If c >= 10 Then
         suma = suma + Val(rstserie!conteo)
        Else
         Grafico1.Data = Val(rstserie!conteo)
         Grafico1.ColumnLabel = rstserie(0)
      End If
      rstserie.MoveNext
    Wend
    If rstserie.RecordCount >= 10 Then
       Grafico1.Data = suma
       Grafico1.Column = 10
       Grafico1.ColumnLabel = "Otros"
    End If
    Grafico1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "Cantidad"
    rstserie.Close

  Case "Atenci�n"
    lblTitulo.Caption = "Reclamos de Atenci�n"
    rstserie.Source = "select empleado.apellido, count(reclamo.id_item)as conteo From reclamo, empleado where  ((reclamo.fecha >= '" & fe & "') and (reclamo.tipo_item =5) and (reclamo.id_item=empleado.legajo)) group by empleado.legajo order by conteo desc"
    rstserie.Open
    If rstserie.RecordCount < 11 Then
       Grafico1.ColumnCount = rstserie.RecordCount
      Else
       Grafico1.ColumnCount = 10
    End If
    For c = 1 To Grafico1.ColumnCount
       Grafico1.Column = c
       Grafico1.Data = 0
    Next c
    c = 0
    suma = 0
    While Not rstserie.EOF
      If c < 10 Then c = c + 1
      Grafico1.Column = c
      If c >= 10 Then
         suma = suma + Val(rstserie!conteo)
        Else
         Grafico1.Data = Val(rstserie!conteo)
         Grafico1.ColumnLabel = rstserie(0)
      End If
      rstserie.MoveNext
    Wend
    If rstserie.RecordCount >= 10 Then
       Grafico1.Data = suma
       Grafico1.Column = 10
       Grafico1.ColumnLabel = "Otros"
    End If
    Grafico1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "Cantidad"
    rstserie.Close
   
  Case "Medicamentos"
    lblTitulo.Caption = "Reclamos de Medicamentos"
    rstserie.Source = "select medicamento.nombre, count(reclamo.id_item)as conteo From reclamo, medicamento where ((reclamo.fecha >= '" & fe & "') and (reclamo.tipo_item =2) and (reclamo.id_item=medicamento.id_medicamento)) group by medicamento.nombre order by conteo desc"
    rstserie.Open
    If rstserie.RecordCount < 11 Then
       Grafico1.ColumnCount = rstserie.RecordCount
      Else
       Grafico1.ColumnCount = 10
    End If
    For c = 1 To Grafico1.ColumnCount
       Grafico1.Column = c
       Grafico1.Data = 0
    Next c
    c = 0
    suma = 0
    While Not rstserie.EOF
      If c < 10 Then c = c + 1
      Grafico1.Column = c
      If c >= 10 Then
         suma = suma + Val(rstserie!conteo)
        Else
         Grafico1.Data = Val(rstserie!conteo)
         Grafico1.ColumnLabel = rstserie(0)
      End If
      rstserie.MoveNext
    Wend
    If rstserie.RecordCount >= 10 Then
       Grafico1.Data = suma
       Grafico1.Column = 10
       Grafico1.ColumnLabel = "Otros"
    End If
    Grafico1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "Cantidad"
    rstserie.Close
  
  Case "Pre-Pago"
    lblTitulo.Caption = "Reclamos del Pre-Pago"
    rstserie.Source = "select servicio.nombre, count(reclamo.id_item)as conteo From reclamo, servicio where ((reclamo.fecha >= '" & fe & "') and (reclamo.tipo_item =4) and (reclamo.id_item=servicio.id_servicio)) group by servicio.nombre order by conteo desc"
    rstserie.Open
    If rstserie.RecordCount < 11 Then
       Grafico1.ColumnCount = rstserie.RecordCount
      Else
       Grafico1.ColumnCount = 10
    End If
    For c = 1 To Grafico1.ColumnCount
       Grafico1.Column = c
       Grafico1.Data = 0
    Next c
    c = 0
    suma = 0
    While Not rstserie.EOF
      If c < 10 Then c = c + 1
      Grafico1.Column = c
      If c >= 10 Then
         suma = suma + Val(rstserie!conteo)
        Else
         Grafico1.Data = Val(rstserie!conteo)
         Grafico1.ColumnLabel = rstserie(0)
      End If
      rstserie.MoveNext
    Wend
    If rstserie.RecordCount >= 10 Then
       Grafico1.Data = suma
       Grafico1.Column = 10
       Grafico1.ColumnLabel = "Otros"
    End If
    Grafico1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "Cantidad"
    rstserie.Close
  
  Case "Servicios"
    lblTitulo.Caption = "Reclamos de Servicios"
    rstserie.Source = "select servicio.nombre, count(reclamo.id_item)as conteo From reclamo, servicio where ((reclamo.fecha >= '" & fe & "') and (reclamo.tipo_item =1) and (reclamo.id_item=servicio.id_servicio)) group by servicio.nombre order by conteo desc"
    rstserie.Open
    If rstserie.RecordCount < 11 Then
       Grafico1.ColumnCount = rstserie.RecordCount
      Else
       Grafico1.ColumnCount = 10
    End If
    For c = 1 To Grafico1.ColumnCount
       Grafico1.Column = c
       Grafico1.Data = 0
    Next c
    c = 0
    suma = 0
    While Not rstserie.EOF
      If c < 10 Then c = c + 1
      Grafico1.Column = c
      If c >= 10 Then
         suma = suma + Val(rstserie!conteo)
        Else
         Grafico1.Data = Val(rstserie!conteo)
         Grafico1.ColumnLabel = rstserie(0)
      End If
      rstserie.MoveNext
    Wend
    If rstserie.RecordCount >= 10 Then
       Grafico1.Data = suma
       Grafico1.Column = 10
       Grafico1.ColumnLabel = "Otros"
    End If
    Grafico1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "Cantidad"
    rstserie.Close
 End Select
 End If
End Sub

