VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmVencido 
   Caption         =   "Medicamentos Vencidos"
   ClientHeight    =   3705
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6315
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVencido.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   3705
   ScaleWidth      =   6315
   Begin VB.CommandButton cmdAceptar 
      Cancel          =   -1  'True
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   2250
      TabIndex        =   2
      Top             =   3240
      Width           =   1815
   End
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   2415
      Left            =   210
      TabIndex        =   0
      Top             =   720
      Visible         =   0   'False
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   4260
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin VB.Label lblTitulo 
      Caption         =   "LOS SIGUIENTES LOTES DE MEDICAMENTOS HAN CADUCADO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   270
      TabIndex        =   1
      Top             =   240
      Width           =   6015
   End
End
Attribute VB_Name = "frmVencido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim cmd As ADODB.Command
Dim rstVenc As ADODB.Recordset
Private Sub cmdaceptar_Click()
Unload Me
End Sub

Private Sub Form_Load()
Me.Width = 9090
Me.Height = 4530
Me.Left = 1365
Me.Top = 1350
'Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
'Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
loadGrid
End Sub
Sub cargar(ByRef rst As ADODB.Recordset)
Set rstVenc = rst
Me.Show
Set rst = Nothing
End Sub

Private Sub Form_Resize()
On Error Resume Next
lbltitulo.Left = (Me.ScaleWidth - lbltitulo.Width) / 2
grdDetalle.Left = (Me.ScaleWidth - grdDetalle.Width) / 2
grdDetalle.Width = Me.ScaleWidth
grdDetalle.Height = Me.ScaleHeight - 1500
cmdAceptar.Left = (Me.ScaleWidth - cmdAceptar.Width) / 2
cmdAceptar.Top = grdDetalle.Top + grdDetalle.Height + 300
End Sub

Private Sub grdDetalle_DblClick()
If Mensaje("�Desea dar de baja el lote vencido?", Loro, Balloon, vbQuestion + vbYesNo, , "surprised") = vbNo Then Exit Sub
cmd.CommandText = "delete from lote where nro_lote=" & Val(grdDetalle.TextMatrix(grdDetalle.Row, 0))
cmd.Execute
rstVenc.Requery
If rstVenc.RecordCount <> 0 Then loadGrid
End Sub
Private Sub loadGrid()
grdDetalle.FixedRows = 0
grdDetalle.FixedCols = 0
grdDetalle.Rows = 0
grdDetalle.Cols = 5
grdDetalle.ColWidth(0) = 555
grdDetalle.ColWidth(1) = 1650
grdDetalle.ColWidth(2) = 1650
grdDetalle.ColWidth(3) = 1575
grdDetalle.ColWidth(4) = 1725
'grdDetalle.ColWidth(5) = 1680
grdDetalle.AddItem "Lote" & vbTab & "Fecha de Creaci�n" & vbTab & "Nombre" & vbTab & "Proveedor" & vbTab & "Fecha Vencimiento"
rstVenc.MoveFirst
While Not rstVenc.EOF
    grdDetalle.AddItem rstVenc!nro_lote & vbTab & rstVenc!fecha_creacion & vbTab & rstVenc!nombre & vbTab & rstVenc!razon_social & vbTab & rstVenc!fecha_vto
    rstVenc.MoveNext
Wend
grdDetalle.FixedRows = 1
grdDetalle.Visible = True
End Sub
