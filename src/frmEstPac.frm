VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmEstPac 
   Caption         =   "Estadísticas de Pacientes"
   ClientHeight    =   4605
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5415
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmEstPac.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4605
   ScaleWidth      =   5415
   Begin VB.CommandButton cmdAceptar 
      Cancel          =   -1  'True
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   1920
      TabIndex        =   3
      Top             =   4080
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   3255
      Index           =   1
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   4935
      Begin MSChart20Lib.MSChart Grafico1 
         Height          =   3135
         Left            =   -360
         OleObjectBlob   =   "frmEstPac.frx":1272
         TabIndex        =   2
         Top             =   0
         Width           =   4575
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   3255
      Index           =   2
      Left            =   240
      TabIndex        =   4
      Top             =   480
      Width           =   4935
      Begin MSChart20Lib.MSChart Grafico2 
         Height          =   3135
         Left            =   -120
         OleObjectBlob   =   "frmEstPac.frx":37A0
         TabIndex        =   5
         Top             =   0
         Width           =   4575
      End
   End
   Begin MSComctlLib.TabStrip Strip 
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   6588
      TabMinWidth     =   -1089216512
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Composición según Especie"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Utilización de Turnos"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmEstPac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Private mintCurFrame As Integer

Private Sub cmdaceptar_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Width = 6960
Me.Height = 5970
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
graficar
graficar2
End Sub

Private Sub Form_Resize()
If Me.Width < 6150 And Me.WindowState = 0 Then Me.Width = 6150
If Me.Height < 3960 And Me.WindowState = 0 Then Me.Height = 3960
On Error Resume Next
cmdaceptar.Left = (Me.ScaleWidth - cmdaceptar.Width) / 2
cmdaceptar.Top = Me.ScaleHeight - cmdaceptar.Height
Strip.Width = Me.ScaleWidth - 200
Strip.Height = Me.ScaleHeight - 600
Frame1(1).Width = Strip.Width - 140
Frame1(1).Height = Strip.Height - 390
Frame1(2).Width = Strip.Width - 140
Frame1(2).Height = Strip.Height - 390
Grafico1.Width = Me.ScaleWidth
Grafico1.Height = Frame1(1).Height * 0.85
Grafico2.Width = Me.ScaleWidth
Grafico2.Height = Frame1(1).Height * 0.9
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub Strip_Click()
If Strip.SelectedItem.Index = mintCurFrame Then Exit Sub
Frame1(Strip.SelectedItem.Index).Visible = True
Frame1(mintCurFrame).Visible = False
mintCurFrame = Strip.SelectedItem.Index
'If mintCurFrame = 1 Then graficar Else graficar2
End Sub
Private Sub graficar()
Dim c, total As Integer, otros As Integer
rst.Source = "select r.especie,count(m.id_hc) as conteo,e.nombre from mascota_hc m,raza r,especie e where m.id_raza=r.id_raza and r.especie=e.id_especie group by r.especie order by conteo desc"
rst.Open
For c = 1 To Grafico1.ColumnCount
    Grafico1.Column = c
    Grafico1.Data = 0
Next c
c = 0
While Not rst.EOF
    total = total + Val(rst!conteo)
    rst.MoveNext
Wend
rst.MoveFirst
While Not rst.EOF
    If c < 5 Then c = c + 1 Else otros = otros + rst!conteo
    Grafico1.Column = c
    Grafico1.Data = Val(rst!conteo)
    Grafico1.ColumnLabelIndex = 1
    Grafico1.ColumnLabel = rst!nombre
    Grafico1.ColumnLabelIndex = 2
    Grafico1.ColumnLabel = Round((rst!conteo * 100) / total, 2) & " %"
    rst.MoveNext
Wend
rst.Close
Grafico1.Column = 5
Grafico1.ColumnLabelIndex = 1
Grafico1.ColumnLabel = "Otros"
Grafico1.ColumnLabelIndex = 2
Grafico1.ColumnLabel = Round((otros * 100) / total, 2) & " %"
End Sub
Private Sub graficar2()
Dim c
Dim max As Integer
rst.Source = "select count(nro_turno) as conteo,estado from turno group by estado order by estado"
rst.Open
For c = 1 To Grafico2.ColumnCount
    Grafico2.Column = c
    Grafico2.Data = 0
    Select Case c
        Case 1
            Grafico2.ColumnLabel = "Pendientes"
        Case 2
            Grafico2.ColumnLabel = "Cancelados"
        Case 3
            Grafico2.ColumnLabel = "Utilizados"
        Case 4
            Grafico2.ColumnLabel = "No Utilizados"
    End Select
Next c
c = 1
While Not rst.EOF
    Grafico2.Column = c
    Grafico2.Data = Val(rst!conteo)
    If rst!conteo > max Then max = rst!conteo
    c = c + 1
    rst.MoveNext
Wend
rst.Close
End Sub
