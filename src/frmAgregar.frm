VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAgregar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Agegar Item"
   ClientHeight    =   2085
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3735
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAgregar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2085
   ScaleWidth      =   3735
   ShowInTaskbar   =   0   'False
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   1996
      TabIndex        =   4
      Top             =   1080
      Width           =   240
      _ExtentX        =   344
      _ExtentY        =   582
      _Version        =   393216
      Value           =   1
      BuddyControl    =   "txtCantidad"
      BuddyDispid     =   196611
      OrigLeft        =   2280
      OrigTop         =   1080
      OrigRight       =   2520
      OrigBottom      =   1455
      Max             =   100000
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2160
      TabIndex        =   6
      Top             =   1560
      Width           =   975
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   600
      TabIndex        =   5
      Top             =   1560
      Width           =   975
   End
   Begin VB.TextBox txtCantidad 
      Alignment       =   1  'Right Justify
      Height          =   330
      Left            =   1140
      MaxLength       =   2
      TabIndex        =   3
      Text            =   "1"
      Top             =   1080
      Width           =   855
   End
   Begin VB.OptionButton optMedicamento 
      Caption         =   "Medicamento"
      Height          =   255
      Left            =   1140
      TabIndex        =   8
      Top             =   120
      Width           =   1455
   End
   Begin VB.OptionButton optServicio 
      Caption         =   "Servicio"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Value           =   -1  'True
      Width           =   975
   End
   Begin VB.OptionButton optArticulo 
      Caption         =   "Art�culo"
      Height          =   255
      Left            =   2640
      TabIndex        =   9
      Top             =   120
      Width           =   975
   End
   Begin VB.ComboBox cmbServicio 
      Height          =   345
      Left            =   1140
      TabIndex        =   0
      Text            =   "cmbServicio"
      Top             =   600
      Width           =   2295
   End
   Begin VB.ComboBox cmbArticulo 
      Height          =   345
      Left            =   1140
      TabIndex        =   2
      Text            =   "cmbArticulo"
      Top             =   600
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.ComboBox cmbMedicamento 
      Height          =   345
      Left            =   1140
      TabIndex        =   1
      Text            =   "cmbMedicamento"
      Top             =   600
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label lblStock 
      Alignment       =   2  'Center
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   1080
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.Label lblCantidad 
      Caption         =   "Cantidad:"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label lblNombre 
      Caption         =   "Nombre:"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   600
      Width           =   735
   End
End
Attribute VB_Name = "frmAgregar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim destino As Form
Dim maxStock As Long
Dim nocomp As Boolean

Private Sub cmbArticulo_Change()
If nocomp = False Then completar cmbArticulo Else: nocomp = False
End Sub

Private Sub cmbArticulo_Click()
If cmbArticulo.ListIndex = -1 Then cmbArticulo.ListIndex = 0
rst.Source = "select stock_total,stock_minimo from articulo where id_articulo=" & cmbArticulo.ItemData(cmbArticulo.ListIndex)
rst.Open
maxStock = rst!stock_total
If destino.Name = "frmConStock" Then lblStock.Caption = "Stock Actual: " & rst!stock_total & " un.   Stock Min.: " & rst!stock_minimo & " un."
rst.Close
txtCantidad = 1
cmdAceptar.Enabled = True
End Sub

Private Sub cmbArticulo_KeyPress(KeyAscii As Integer)
cmdAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbArticulo.Text = Left(cmbArticulo.Text, cmbArticulo.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbArticulo
End Sub

Private Sub cmbArticulo_LostFocus()
validar cmbArticulo
End Sub

Private Sub cmbMedicamento_Change()
If nocomp = False Then completar cmbMedicamento Else: nocomp = False
End Sub

Private Sub cmbMedicamento_Click()
If cmbMedicamento.ListIndex = -1 Then cmbMedicamento.ListIndex = 0
rst.Source = "select stock_total,stock_min from medicamento where id_medicamento=" & cmbMedicamento.ItemData(cmbMedicamento.ListIndex)
rst.Open
maxStock = rst!stock_total
If destino.Name = "frmConStock" Then lblStock.Caption = "Stock Actual: " & rst!stock_total & " un.   Stock Min.: " & rst!stock_min & " un."
rst.Close
txtCantidad = 1
cmdAceptar.Enabled = True
End Sub

Private Sub cmbMedicamento_KeyPress(KeyAscii As Integer)
cmdAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbMedicamento.Text = Left(cmbMedicamento.Text, cmbMedicamento.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbMedicamento
End Sub

Private Sub cmbMedicamento_LostFocus()
validar cmbMedicamento
End Sub

Private Sub cmbServicio_Change()
If nocomp = False Then completar cmbServicio Else: nocomp = False
End Sub

Private Sub cmbServicio_Click()
If cmbServicio.ListIndex = -1 Then cmbServicio.ListIndex = 0
maxStock = UpDown1.max
cmdAceptar.Enabled = True
End Sub

Private Sub cmbServicio_KeyPress(KeyAscii As Integer)
cmdAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbServicio.Text = Left(cmbServicio.Text, cmbServicio.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbServicio
End Sub

Private Sub cmbServicio_LostFocus()
validar cmbServicio
End Sub

Private Sub cmdaceptar_Click()
If optServicio.Value = True Then: destino.agregar 1, cmbServicio.ItemData(cmbServicio.ListIndex), txtCantidad
If optMedicamento.Value = True Then: destino.agregar 2, cmbMedicamento.ItemData(cmbMedicamento.ListIndex), txtCantidad
If optArticulo.Value = True Then: destino.agregar 3, cmbArticulo.ItemData(cmbArticulo.ListIndex), txtCantidad
If Val(txtCantidad) > maxStock Then
    If maxStock > 0 Then
        MsgRapido "La cantidad solicitada supera el Stock disponible en " & Val(txtCantidad) - maxStock & " unidades.", Loro, vbInformation, , "surprised"
    Else
        MsgRapido "La cantidad solicitada supera el Stock disponible en " & Val(txtCantidad) & " unidades.", Loro, vbInformation, , "surprised"
    End If
End If
Unload Me
End Sub

Private Sub cmdcancelar_Click()
destino.Enabled = True
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.cerrar.Enabled = False
End Sub

Private Sub Form_Load()
frmAgregar.Top = 2055
frmAgregar.Left = 6345
destino.Enabled = False
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
rst.Source = "select nombre,id_servicio from servicio where borrado='f' and id_rubro<>7 order by nombre"
rst.Open
While rst.EOF = False
    cmbServicio.AddItem rst!nombre
    cmbServicio.ItemData(cmbServicio.NewIndex) = rst!id_servicio
    rst.MoveNext
Wend
rst.Close
rst.Source = "select nombre,id_medicamento from medicamento where id_medicamento>0 and borrado='f' order by nombre"
rst.Open
While rst.EOF = False
    cmbMedicamento.AddItem rst!nombre
    cmbMedicamento.ItemData(cmbMedicamento.NewIndex) = rst!id_medicamento
    rst.MoveNext
Wend
rst.Close
rst.Source = "select nombre,id_articulo from articulo where borrado='f' order by nombre"
rst.Open
While rst.EOF = False
    cmbArticulo.AddItem rst!nombre
    cmbArticulo.ItemData(cmbArticulo.NewIndex) = rst!id_articulo
    rst.MoveNext
Wend
rst.Close
cmbServicio.ListIndex = 0
cmbMedicamento.ListIndex = 0
cmbArticulo.ListIndex = 0
End Sub

Private Sub optArticulo_Click()
If optArticulo.Value = True Then
cmbServicio.Visible = False
cmbMedicamento.Visible = False
cmbArticulo.Visible = True
End If
cmbArticulo_Click
txtCantidad_Validate False
End Sub

Private Sub optMedicamento_Click()
If optMedicamento.Value = True Then
cmbServicio.Visible = False
cmbMedicamento.Visible = True
cmbArticulo.Visible = False
End If
cmbMedicamento_Click
txtCantidad_Validate False
End Sub

Private Sub optServicio_Click()
If optServicio.Value = True Then
cmbServicio.Visible = True
cmbMedicamento.Visible = False
cmbArticulo.Visible = False
End If
cmbServicio_Click
txtCantidad_Validate False
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtCantidad_Validate(Cancel As Boolean)
If txtCantidad <> "" Then
    If IsNumeric(txtCantidad) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtCantidad = ""
    End If
End If
'If Val(txtCantidad) > UpDown1.Max Then: txtCantidad = UpDown1.Max
End Sub
Sub mostrar(formulario As Form)
Set destino = formulario
End Sub
