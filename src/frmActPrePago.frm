VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmActPrePago 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Afiliaci�n a Servicio Prepago"
   ClientHeight    =   4755
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6585
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActPrePago.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4755
   ScaleWidth      =   6585
   Begin VB.TextBox txtMonto 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   330
      Left            =   4920
      TabIndex        =   13
      Top             =   2520
      Width           =   1335
   End
   Begin VB.CommandButton cmdQuitar 
      Caption         =   "&Quitar"
      Height          =   375
      Left            =   3645
      TabIndex        =   11
      Top             =   3840
      Width           =   1575
   End
   Begin VB.CommandButton cmdAgregar 
      Caption         =   "A&gregar"
      Default         =   -1  'True
      Height          =   375
      Left            =   1365
      TabIndex        =   10
      Top             =   3840
      Width           =   1575
   End
   Begin VB.ComboBox cmbPlan 
      Height          =   345
      Left            =   4365
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   3000
      Width           =   1935
   End
   Begin VB.ComboBox cmbMascota 
      Height          =   345
      Left            =   1245
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   3000
      Width           =   1575
   End
   Begin VB.CommandButton cmdAceptar 
      Cancel          =   -1  'True
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   2505
      TabIndex        =   5
      Top             =   4320
      Width           =   1575
   End
   Begin VB.TextBox txtFechaInicio 
      Enabled         =   0   'False
      Height          =   330
      Left            =   4845
      TabIndex        =   4
      Top             =   240
      Width           =   1215
   End
   Begin VB.ComboBox cmbCliente 
      Height          =   345
      Left            =   885
      Sorted          =   -1  'True
      TabIndex        =   3
      Text            =   "cmbCliente"
      Top             =   240
      Width           =   2535
   End
   Begin MSFlexGridLib.MSFlexGrid grdLista 
      Height          =   1695
      Left            =   165
      TabIndex        =   0
      Top             =   720
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   2990
      _Version        =   393216
      Rows            =   1
      Cols            =   6
      FixedRows       =   0
      FixedCols       =   0
   End
   Begin VB.Label lblPrecio 
      AutoSize        =   -1  'True
      Height          =   225
      Left            =   1845
      TabIndex        =   15
      Top             =   3480
      Width           =   45
   End
   Begin VB.Label lblNoAdherido 
      Alignment       =   2  'Center
      Caption         =   "EL CLIENTE SELECCIONADO NO POSEE MASCOTAS EN PREPAGO"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1440
      TabIndex        =   14
      Top             =   1200
      Visible         =   0   'False
      Width           =   3735
   End
   Begin VB.Label lblMonto 
      Caption         =   "Monto:"
      Height          =   255
      Left            =   4320
      TabIndex        =   12
      Top             =   2520
      Width           =   615
   End
   Begin VB.Label lblPlanes 
      Caption         =   "Planes PrePago:"
      Height          =   255
      Left            =   2925
      TabIndex        =   7
      Top             =   3000
      Width           =   1455
   End
   Begin VB.Label lblMascotas 
      Caption         =   "Mascotas:"
      Height          =   255
      Left            =   285
      TabIndex        =   6
      Top             =   3000
      Width           =   855
   End
   Begin VB.Label lblFechaInicio 
      Caption         =   "Fecha de Inicio:"
      Height          =   225
      Left            =   3525
      TabIndex        =   2
      Top             =   240
      Width           =   1290
   End
   Begin VB.Label lblCliente 
      Caption         =   "Cliente:"
      Height          =   225
      Left            =   165
      TabIndex        =   1
      Top             =   240
      Width           =   630
   End
End
Attribute VB_Name = "frmActPrePago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim xid_prepago
Dim cmd As ADODB.Command
Dim rst As ADODB.Recordset
Dim rstPrecios As ADODB.Recordset
Dim nocomp As Boolean

Private Sub cmbcliente_Change()
If nocomp = False Then completar cmbCliente Else: nocomp = False
End Sub

Private Sub cmbCliente_Click()
cmdAgregar.Enabled = True
cmbMascota.Clear
cmbMascota.Enabled = True
rst.Source = "select nombre,id_hc from mascota_hc where borrado='f' and isnull(fecha_dec)=1 and id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
rst.Open
While Not rst.EOF
    cmbMascota.AddItem rst!nombre
    cmbMascota.ItemData(cmbMascota.NewIndex) = rst!id_hc
    rst.MoveNext
Wend
If rst.RecordCount = 0 Then
    cmbMascota.Enabled = False
Else
    cmbMascota.ListIndex = 0
End If
rst.Close
rst.Source = "select * from servicio_prepago where id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
rst.Open
If rst.RecordCount = 0 Then
    lblNoAdherido.Visible = True
    grdLista.Visible = False
    grdLista.Clear
    txtFechaInicio = ""
    txtMonto = ""
    rst.Close
Else
    lblNoAdherido.Visible = False
    txtFechaInicio = rst!fecha_inicio
    txtMonto = rst!monto_total
    grdLista.Clear
    xid_prepago = rst!id_prepago
    rst.Close
    rst.Source = "select d.id_hc,d.monto,d.fecha_inicio,d.id_servicio,m.nombre,s.nombre as plan from detalle_sp d,mascota_hc m,servicio s where d.id_hc=m.id_hc and d.id_servicio=s.id_servicio and d.fecha_fin='0000-00-00' and d.id_prepago=" & xid_prepago
    rst.Open
    grdLista.Rows = 0
    grdLista.AddItem "Mascota" & vbTab & "Fecha de Inicio" & vbTab & "Plan" & vbTab & "Monto"
    grdLista.Rows = 2
    grdLista.FixedRows = 1
    grdLista.Rows = 1
    While Not rst.EOF
        grdLista.AddItem rst!nombre & vbTab & rst!fecha_inicio & vbTab & rst!plan & vbTab & rst!monto & vbTab & rst!id_hc & vbTab & rst!id_servicio
        rst.MoveNext
    Wend
    rst.Close
    If grdLista.Rows = 1 Then
        lblNoAdherido.Visible = True
        grdLista.Visible = False
        grdLista.Clear
        txtFechaInicio = ""
        txtMonto = ""
    Else
        grdLista.Visible = True
    End If
End If
End Sub

Private Sub cmbcliente_KeyPress(KeyAscii As Integer)
cmdAgregar.Enabled = False
If KeyAscii = vbKeyBack Then cmbCliente.Text = Left(cmbCliente.Text, cmbCliente.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbCliente
End Sub

Private Sub cmbcliente_LostFocus()
validar cmbCliente
End Sub

Private Sub cmbPlan_Click()
rstPrecios.Source = "select precio from servicio where id_servicio=" & cmbPlan.ItemData(cmbPlan.ListIndex)
rstPrecios.Open
lblPrecio.Caption = "Costo mensual del plan: $" & rstPrecios!Precio
rstPrecios.Close
lblPrecio.Left = (Me.ScaleWidth - lblPrecio.Width) / 2
End Sub

Private Sub cmdAceptar_Click()
Unload Me
End Sub

Private Sub cmdagregar_Click()
Dim nuevoid As Long
Dim xPrecio As Double
Dim enLista As Boolean
Dim xid_mascota As Long
Dim c
If cmbMascota.ListCount = 0 Then Exit Sub
For c = 1 To grdLista.Rows - 1
    If Val(grdLista.TextMatrix(c, 4)) = cmbMascota.ItemData(cmbMascota.ListIndex) Then: enLista = True
Next c
If enLista = True Then MsgRapido "La mascota ya posee un plan Pre-Pago.", Loro, vbInformation, , "surprised": Exit Sub
If mensaje("�Desea agregar esta mascota al Plan?", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
If grdLista.Visible = False Then
    rst.Source = "select max(id_prepago) as id from servicio_prepago"
    rst.Open
    nuevoid = rst!id + 1
    rst.Close
    cmd.CommandText = "insert into servicio_prepago values(" & nuevoid & "," & cmbCliente.ItemData(cmbCliente.ListIndex) & ",'" & DateDB & "','" & DateDB & "'," & 0 & ")"
    cmd.Execute
End If
rst.Source = "select precio from servicio where id_servicio=" & cmbPlan.ItemData(cmbPlan.ListIndex)
rst.Open
xPrecio = rst!Precio
rst.Close
xid_mascota = cmbMascota.ItemData(cmbMascota.ListIndex)
cmbCliente_Click
cmd.CommandText = "update servicio_prepago set monto_total=monto_total+" & xPrecio & " where id_prepago=" & xid_prepago
cmd.Execute
cmd.CommandText = "insert into detalle_sp values(" & xid_prepago & "," & xid_mascota & "," & xPrecio & ",'" & DateDB & "','0000-00-00'," & cmbPlan.ItemData(cmbPlan.ListIndex) & ")"
cmd.Execute
cmbCliente_Click
End Sub

Private Sub cmdQuitar_Click()
Dim xmonto As Double
If grdLista.Visible = False Then: Exit Sub
Dim c
xmonto = Val(grdLista.TextMatrix(grdLista.Row, 3))
cmd.CommandText = "delete from detalle_sp where id_hc=" & grdLista.TextMatrix(grdLista.Row, 4)
If mensaje("�Desea dar de baja esta mascota del Plan?", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
cmd.Execute
cmd.CommandText = "update servicio_prepago set monto_total=monto_total-" & xmonto & " where id_prepago=" & xid_prepago
cmd.Execute
cmbCliente_Click
If grdLista.Visible = True Then: Exit Sub
cmd.CommandText = "delete from servicio_prepago where id_prepago=" & xid_prepago
cmd.Execute
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
Set rstPrecios = New ADODB.Recordset
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rstPrecios.ActiveConnection = cnx
rst.Source = "select nombre,apellido,id_cliente from cliente where id_cliente>0 and borrado='f' order by id_cliente"
rst.CursorType = adOpenForwardOnly
rstPrecios.CursorType = adOpenForwardOnly
rst.LockType = adLockReadOnly
rstPrecios.LockType = adLockReadOnly
rst.Open
While Not rst.EOF
    cmbCliente.AddItem rst!apellido & ", " & rst!nombre
    cmbCliente.ItemData(cmbCliente.NewIndex) = rst!id_cliente
    rst.MoveNext
Wend
rst.Close
cmbCliente.ListIndex = 0
rst.Source = "select nombre, id_servicio from servicio where id_rubro=7 and borrado='f'"
rst.Open
While Not rst.EOF
    cmbPlan.AddItem rst!nombre
    cmbPlan.ItemData(cmbPlan.NewIndex) = rst!id_servicio
    rst.MoveNext
Wend
rst.Close
cmbPlan.ListIndex = 0
grdLista.ColWidth(0) = 1290
grdLista.ColWidth(1) = 1335
grdLista.ColWidth(2) = 2385
grdLista.ColWidth(3) = 1140
grdLista.ColWidth(4) = 0
grdLista.ColWidth(5) = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

