VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmInfoMorosidad 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de Clientes Morosos"
   ClientHeight    =   4200
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5670
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfoMorosidad.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4200
   ScaleWidth      =   5670
   Begin VB.CommandButton cmdCarta 
      Caption         =   "C&arta de Aviso"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3068
      TabIndex        =   6
      Top             =   3240
      Width           =   1455
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1148
      TabIndex        =   5
      Top             =   3240
      Width           =   1455
   End
   Begin VB.CommandButton cmdCerrar 
      Cancel          =   -1  'True
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   2063
      TabIndex        =   4
      Top             =   3720
      Width           =   1695
   End
   Begin MSFlexGridLib.MSFlexGrid grdMorosos 
      Height          =   2295
      Left            =   0
      TabIndex        =   3
      Top             =   840
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   4048
      _Version        =   393216
      Rows            =   0
      Cols            =   4
      FixedRows       =   0
      FixedCols       =   0
      BackColorSel    =   16777088
      ForeColorSel    =   0
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
   End
   Begin VB.CommandButton cmdGenerar 
      Caption         =   "&Mostrar"
      Default         =   -1  'True
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   240
      Width           =   975
   End
   Begin VB.TextBox txtAdeudados 
      Alignment       =   1  'Right Justify
      Height          =   330
      Left            =   3720
      TabIndex        =   1
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Lista de Morosos:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label lblAdeudados 
      AutoSize        =   -1  'True
      Caption         =   "Cantidad m�nima de meses adeudados:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   3435
   End
End
Attribute VB_Name = "frmInfoMorosidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim Tiempo As Integer
Private Declare Function apiShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Sub MandarMail()
rst.Source = "select nombre,apellido,email,sexo from cliente where id_cliente=" & grdMorosos.TextMatrix(grdMorosos.Row, 3)
rst.Open
If IsNull(rst!Email) = True Then MsgRapido "No es posible enviar la carta por E-Mail ya que no se cuenta con la direcci�n del cliente.", Loro, vbCritical, , "sad": rst.Close: Exit Sub
Dim Mensaje As String
If rst!sexo = "m" Then Mensaje = "Se�or " Else Mensaje = "Se�ora "
Mensaje = Mensaje & rst!nombre & " " & rst!apellido & "    "
Mensaje = Mensaje & "De nuestra mayor consideraci�n:" & " " & " " & "Mediante la presente queremos hacerle saber que a la fecha usted est� adeudando " & grdMorosos.TextMatrix(grdMorosos.Row, 1)
If Val(grdMorosos.TextMatrix(grdMorosos.Row, 1)) >= 2 Then Mensaje = Mensaje & " meses " Else Mensaje = Mensaje & " mes "
Mensaje = Mensaje & "en concepto de cuotas impagas, por ser afiliado al servicio de Atenci�n Pre Paga de la veterinaria." & "   " & "  " & "El monto que adeuda suma $ " & grdMorosos.TextMatrix(grdMorosos.Row, 2) & ". Rogamos haga efectivo el pago del mismo en lo inmediato."
Mensaje = Mensaje & "    " & "   " & "Atentamente: Veterinaria Rio Suquia"
apiShellExecute Me.hWnd, "OPEN", "mailto:" & rst!Email & "?subject=Carta de Aviso de Morosidad&body=" & Mensaje, "", App.Path, 1
rst.Close
End Sub

Private Sub cmdCerrar_Click()
Unload Me
End Sub

Private Sub cmdGenerar_Click()
If txtAdeudados = "" Then Exit Sub
rst.Source = "select s.monto_total,s.fecha_ultimo_pago,s.id_cliente,c.nombre,c.apellido from servicio_prepago s,cliente c where s.id_cliente=c.id_cliente and s.id_prepago>0"
rst.Open
grdMorosos.Rows = 1
While Not rst.EOF
    Tiempo = Int((Date - rst!fecha_ultimo_pago) / 30) '(Year(Date) - Year(rst!fecha_ultimo_pago)) * 12 + (Month(Date) - Month(rst!fecha_ultimo_pago))
    If Tiempo >= Val(txtAdeudados) Then
        grdMorosos.AddItem rst!apellido & ", " & rst!nombre & vbTab & Tiempo & vbTab & Tiempo * rst!monto_total & vbTab & rst!id_cliente
    End If
    rst.MoveNext
Wend
If grdMorosos.Rows > 1 Then
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
        cmdImprimir.Enabled = True
        cmdCarta.Enabled = True
    Else
        cmdImprimir.Enabled = False
        cmdCarta.Enabled = False
        MsgRapido "No existen datos", Loro, vbInformation, , "explain"
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
    End If
rst.Close
End Sub

Private Sub cmdImprimir_Click()
info 1
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
frmPrincipal.vista.Enabled = True
frmPrincipal.imprimir.Enabled = True

End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
prepararGrilla
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub txtAdeudados_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
If KeyAscii <> 8 And KeyAscii <> 7 Then
KeyAscii = 0
End If
End If
End Sub

Private Sub prepararGrilla()
grdMorosos.Clear
grdMorosos.Rows = 0
grdMorosos.AddItem "Cliente" & vbTab & "Meses Adeudados" & vbTab & "Monto Adeudado"
grdMorosos.Rows = 2
grdMorosos.FixedRows = 1
grdMorosos.Rows = 1
grdMorosos.ColWidth(0) = 2370
grdMorosos.ColWidth(1) = 1665
grdMorosos.ColWidth(2) = 1515
grdMorosos.ColWidth(3) = 0
End Sub
Sub imprimir()
info 1
End Sub
Sub preview()
info 2
End Sub
Private Sub info(accion As Byte)
Dim rstInfo As ADODB.Recordset
Set rstInfo = New ADODB.Recordset
rstInfo.LockType = adLockOptimistic
rstInfo.CursorType = adOpenKeyset
rstInfo.Fields.Append "cliente", adVarChar, 50
rstInfo.Fields.Append "meses", adVarChar, 10
rstInfo.Fields.Append "deuda", adVarChar, 20
rstInfo.Open
Dim c
For c = 1 To grdMorosos.Rows - 1
    rstInfo.AddNew
    rstInfo!cliente = grdMorosos.TextMatrix(c, 0)
    rstInfo!meses = grdMorosos.TextMatrix(c, 1)
    rstInfo!deuda = grdMorosos.TextMatrix(c, 2)
    rstInfo.Update
Next c
Select Case accion
    Case 1
        Set rptInfoMorosidad.DataSource = rstInfo
        rptInfoMorosidad.PrintReport
    Case 2
        Set rptInfoMorosidad.DataSource = rstInfo
        rptInfoMorosidad.Show
        
End Select
End Sub

Private Sub cmdCarta_Click()
If grdMorosos.Row < 1 Then Exit Sub
If Mensaje("�Desea enviar la carta de aviso por Correo Electr�nico?", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbYes Then
    MandarMail
End If
Dim rstCA As ADODB.Recordset
Set rstCA = New ADODB.Recordset
rstCA.LockType = adLockOptimistic
rstCA.CursorType = adOpenKeyset
rstCA.Fields.Append "cliente", adVarChar, 50
rstCA.Fields.Append "meses", adVarChar, 10
rstCA.Fields.Append "deuda", adVarChar, 20
rstCA.Open
Dim c
rstCA.AddNew
rstCA!cliente = grdMorosos.TextMatrix(grdMorosos.Row, 0)
rstCA!meses = grdMorosos.TextMatrix(grdMorosos.Row, 1)
rstCA!deuda = grdMorosos.TextMatrix(grdMorosos.Row, 2)
rstCA.Update
rstCA.MoveFirst
Set CartaAviso.DataSource = rstCA
CartaAviso.Show
End Sub
