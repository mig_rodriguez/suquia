VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmActPP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Servicio Prepago"
   ClientHeight    =   5565
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7110
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActPP.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5565
   ScaleWidth      =   7110
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1545
      TabIndex        =   15
      Top             =   5040
      Width           =   1695
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3705
      TabIndex        =   16
      Top             =   5040
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   4785
      TabIndex        =   14
      Top             =   4440
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   2640
      TabIndex        =   13
      Top             =   4440
      Width           =   1695
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   480
      TabIndex        =   12
      Top             =   4440
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Index           =   1
      Left            =   240
      TabIndex        =   18
      Top             =   480
      Width           =   6615
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "+"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6240
         TabIndex        =   10
         Top             =   2520
         Width           =   375
      End
      Begin VB.CommandButton cmdQuitar 
         Caption         =   "-"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6240
         TabIndex        =   11
         Top             =   3000
         Width           =   375
      End
      Begin MSFlexGridLib.MSFlexGrid grdDetalle 
         Height          =   1215
         Left            =   360
         TabIndex        =   9
         Top             =   2520
         Width           =   5775
         _ExtentX        =   10186
         _ExtentY        =   2143
         _Version        =   393216
         Rows            =   1
         Cols            =   6
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdUltimo 
         Height          =   375
         Left            =   1920
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         Height          =   375
         Left            =   1440
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         Height          =   375
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   480
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox txtDescripcion 
         Enabled         =   0   'False
         Height          =   570
         Left            =   1440
         MaxLength       =   100
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   1680
         Width           =   5055
      End
      Begin VB.TextBox txtPrecio 
         Enabled         =   0   'False
         Height          =   330
         Left            =   1440
         MaxLength       =   10
         TabIndex        =   5
         Top             =   1200
         Width           =   2175
      End
      Begin VB.TextBox txtNombre 
         Enabled         =   0   'False
         Height          =   330
         Left            =   1440
         MaxLength       =   30
         TabIndex        =   4
         Top             =   720
         Width           =   2175
      End
      Begin MSComCtl2.DTPicker DtInicio 
         Height          =   375
         Left            =   5100
         TabIndex        =   6
         Top             =   720
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   24379393
         CurrentDate     =   37869
      End
      Begin MSComCtl2.DTPicker DtFin 
         Height          =   375
         Left            =   5100
         TabIndex        =   7
         Top             =   1200
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   24379393
         CurrentDate     =   37869
      End
      Begin VB.Label Label1 
         Caption         =   "Descuentos:"
         Height          =   255
         Left            =   360
         TabIndex        =   26
         Top             =   2280
         Width           =   1095
      End
      Begin VB.Label lblDescripcion 
         Caption         =   "Descripci�n:"
         Height          =   255
         Left            =   360
         TabIndex        =   23
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label lblPrecio 
         Caption         =   "Precio:"
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   1200
         Width           =   615
      End
      Begin VB.Label lblFin 
         Caption         =   "Fecha de Fin:"
         Height          =   255
         Left            =   3780
         TabIndex        =   21
         Top             =   1200
         Width           =   1155
      End
      Begin VB.Label lblInicio 
         Caption         =   "Fecha de Inicio:"
         Height          =   255
         Left            =   3780
         TabIndex        =   20
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label lblNombre 
         Caption         =   "Nombre:"
         Height          =   255
         Left            =   360
         TabIndex        =   19
         Top             =   720
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3735
      Index           =   2
      Left            =   240
      TabIndex        =   24
      Top             =   480
      Width           =   6615
      Begin MSFlexGridLib.MSFlexGrid grdPP 
         Height          =   3615
         Left            =   0
         TabIndex        =   25
         Top             =   100
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   6376
         _Version        =   393216
         Rows            =   1
         Cols            =   6
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   4215
      Left            =   120
      TabIndex        =   17
      Top             =   120
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   7435
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmActPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mintCurFrame, xid_servicio As Integer
Dim busqueda As String
Dim rst As ADODB.Recordset
Dim rstDet As ADODB.Recordset
Dim rstAg As ADODB.Recordset
Dim cmd As ADODB.Command

Private Sub cmdagregar_Click()
frmAgregarPP.Show
End Sub

Private Sub cmdQuitar_Click()
If grdDetalle.Rows > 2 Then
grdDetalle.RemoveItem (grdDetalle.Row)
Else
setGrid
cmdQuitar.Enabled = False
End If
End Sub

Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
    If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
DtInicio.Value = Date
grdDetalle.ColWidth(0) = 1125
grdDetalle.ColWidth(1) = 3480
grdDetalle.ColWidth(2) = 1065
grdDetalle.ColWidth(3) = 0
grdDetalle.ColWidth(4) = 0
grdDetalle.ColWidth(5) = 0
Set rst = New ADODB.Recordset
Set rstDet = New ADODB.Recordset
Set rstAg = New ADODB.Recordset
Set cmd = New ADODB.Command
rst.ActiveConnection = cnx
rstDet.ActiveConnection = cnx
rstAg.ActiveConnection = cnx
cmd.ActiveConnection = cnx
rst.LockType = adLockOptimistic
rstDet.LockType = adLockReadOnly
rstAg.LockType = adLockReadOnly
rst.CursorType = adOpenDynamic
rstDet.CursorType = adOpenDynamic
rstAg.CursorType = adOpenForwardOnly
rst.Source = "select id_servicio,nombre,descripcion,precio,fecha_inicio,fecha_fin,id_rubro,borrado from servicio where borrado='f' and id_rubro=7 order by id_servicio"
rstDet.Source = "select v.id_prepago,v.id_item,v.tipo_item,v.descuento,if(v.tipo_item=1,'Servicio',if(v.tipo_item=2,'Medicamento','Articulo')) as categoria,if(v.tipo_item=1,s.nombre,if(v.tipo_item=2,m.nombre,a.nombre)) as nombre from vademecum v left join articulo a on v.id_item=a.id_articulo left join medicamento m on v.id_item=m.id_medicamento left join servicio s on v.id_item=s.id_servicio"
rst.Open
rstDet.Open
listar
rst.MoveFirst
Refrescar
End Sub

Sub agregar(xTipo_Item As Integer, xId_Item As Integer, xPorcentaje As Integer)
Dim cat As String
Dim c
Select Case xTipo_Item
    Case 1
        rstAg.Source = "select id_servicio as id,nombre from servicio where id_servicio=" & xId_Item
        cat = "Servicio"
    Case 2
        rstAg.Source = "select id_medicamento as id,nombre from medicamento where id_medicamento=" & xId_Item
        cat = "Medicamento"
    Case 3
        rstAg.Source = "select id_articulo as id,nombre from articulo where id_articulo=" & xId_Item
        cat = "Art�culo"
End Select
rstAg.Open
For c = 1 To grdDetalle.Rows - 1
If Val(grdDetalle.TextMatrix(c, 3)) = xId_Item And Val(grdDetalle.TextMatrix(c, 4)) = xTipo_Item Then
    MsgRapido "El item ya se encuentra en el detalle.", Loro, vbExclamation, , "surprised"
    rstAg.Close
    frmActPP.Enabled = True
    Exit Sub
End If
Next
grdDetalle.AddItem cat & vbTab & rstAg!nombre & vbTab & xPorcentaje & "%" & vbTab & xId_Item & vbTab & xTipo_Item & vbTab & xPorcentaje / 100
rstAg.Close
cmdQuitar.Enabled = True
frmActPP.Enabled = True
End Sub

Private Sub setGrid()
grdDetalle.Clear
grdDetalle.Rows = 0
grdDetalle.AddItem "Categoria" & vbTab & "Nombre" & vbTab & "Descuento"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
End Sub
Private Sub listar()
Dim regActual As Long
regActual = rst.AbsolutePosition
rst.MoveFirst
grdPP.ColWidth(0) = 2295
grdPP.ColWidth(1) = 660
grdPP.ColWidth(2) = 1125
grdPP.ColWidth(3) = 1050
grdPP.ColWidth(4) = 1365
grdPP.ColWidth(5) = 0
grdPP.Clear
grdPP.Rows = 0
grdPP.AddItem "Nombre" & vbTab & "Precio" & vbTab & "Fecha Inicio" & vbTab & "Fecha Fin" & vbTab & "Descripci�n"
grdPP.Rows = 2
grdPP.FixedRows = 1
grdPP.Rows = 1
While Not rst.EOF
    grdPP.AddItem rst!nombre & vbTab & rst!Precio & vbTab & rst!fecha_inicio & vbTab & rst!fecha_fin & vbTab & rst!descripcion & vbTab & rst!id_servicio
    rst.MoveNext
Wend
rst.AbsolutePosition = regActual
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        Loro.Stop
        rst.CancelUpdate
        restablecer
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
Set rst = Nothing
End Sub
Private Sub grdPP_DblClick()
Frame1(1).Visible = True
Frame1(2).Visible = False
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
frmPrincipal.activararch
rst.MoveFirst
rst.Find "id_servicio = " & grdPP.TextMatrix(grdPP.Row, 5)
Refrescar
mintCurFrame = 1
TabStrip1.Tabs(1).Selected = True
End Sub

Private Sub TabStrip1_Click()
If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
End Sub
Private Sub Refrescar()
On Error Resume Next
txtNombre = rst!nombre
txtPrecio = rst!Precio
txtDescripcion = rst!descripcion
DtInicio.Value = Format(rst!fecha_inicio, "dd/mm/yyyy")
DtFin.Value = Format(rst!fecha_fin, "dd/mm/yyyy")
On Error GoTo 0
setGrid
rstDet.Filter = "id_prepago='" & rst!id_servicio & "'"
rstDet.MoveFirst
While Not rstDet.EOF
    grdDetalle.AddItem rstDet!categoria & vbTab & rstDet!nombre & vbTab & rstDet!descuento * 100 & "%" & vbTab & rstDet!id_item & vbTab & rstDet!tipo_item & vbTab & rstDet!descuento
    rstDet.MoveNext
Wend
End Sub
Private Sub cmdPrimero_Click()
rst.MoveFirst
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAdelante_Click()
If rst.EOF <> True Then: rst.MoveNext
If rst.EOF <> True Then
Refrescar
Else
rst.MoveFirst
Refrescar
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
End Sub

Private Sub cmdAtras_Click()
If rst.BOF <> True Then: rst.MovePrevious
If rst.BOF <> True Then
Refrescar
Else
rst.MoveLast
Refrescar
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
End Sub

Private Sub cmdUltimo_Click()
rst.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub
Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Pre-Pago Seleccionado", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rst!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rst.Update
rst.Requery
cmdAdelante_Click
listar
End Sub
Sub guardar()
Dim c
If txtDescripcion = "" Then: txtDescripcion = " "
If Trim(txtNombre) = "" Or Trim(txtPrecio) = "" Or grdDetalle.Rows < 2 Then
    MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
    Exit Sub
End If
If DtInicio.Value > DtFin.Value Then
    MsgRapido "La fecha de finalizaci�n es incorrecta.", Loro, vbCritical, "surprised"
    Exit Sub
End If
Loro.Stop
rst!id_servicio = xid_servicio
rst!nombre = txtNombre.Text
rst!descripcion = txtDescripcion.Text
rst!Precio = txtPrecio.Text
rst!fecha_inicio = DateDB
rst!fecha_fin = Format(DtFin.Value, "yyyy-mm-dd")
rst!id_rubro = 7
rst!borrado = "f"
rst.Update
rst.Requery
rst.Find "id_servicio = " & xid_servicio
cmd.CommandText = "delete from vademecum where id_prepago=" & xid_servicio
cmd.Execute
For c = 1 To grdDetalle.Rows - 1
    cmd.CommandText = "insert into vademecum(id_prepago,id_item,tipo_item,descuento) values(" & xid_servicio & "," & Val(grdDetalle.TextMatrix(c, 3)) & "," & Val(grdDetalle.TextMatrix(c, 4)) & "," & Val(grdDetalle.TextMatrix(c, 5)) & ")"
    cmd.Execute
Next c
rstDet.Requery
restablecer
listar
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
cmdAdelante.SetFocus
End Sub
Sub modificar()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
txtNombre.Enabled = True
txtPrecio.Enabled = True
txtDescripcion.Enabled = True
DtFin.Enabled = True
cmdAgregar.Enabled = True
cmdQuitar.Enabled = True
xid_servicio = rst!id_servicio
txtNombre.SetFocus
Loro.Play "writing"
End Sub
Sub nuevo()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
txtNombre.Enabled = True
txtNombre = ""
txtPrecio.Enabled = True
txtPrecio = ""
txtDescripcion.Enabled = True
txtDescripcion = ""
DtFin.Enabled = True
cmdAgregar.Enabled = True
setGrid
DtInicio.Value = Date
DtFin.Value = Date + 365 * 50
rst.MoveLast
xid_servicio = nuevoid("id_servicio", "servicio")
txtNombre.SetFocus
Loro.Play "writing"
rst.AddNew
End Sub
Sub cerrar()
Unload Me
End Sub

Private Sub txtPrecio_KeyPress(KeyAscii As Integer)
If InStr(txtPrecio, ".") <> 0 And KeyAscii = 46 Then: KeyAscii = 0
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtPrecio_Validate(Cancel As Boolean)
If txtPrecio <> "" Then
    If IsNumeric(txtPrecio) = False Then
        MsgRapido "El dato no es v�lido.", Loro, vbCritical, , "surprised"
        Cancel = True
        txtPrecio = ""
    End If
End If
End Sub
Sub imprimir()
'frmPrincipal.desactivararch
'Set rptListaClientes.DataSource = rst
'rptListaClientes.PrintReport True
'Unload rptListaClientes
End Sub
Sub preview()
'frmPrincipal.desactivararch
'Set rptListaClientes.DataSource = rst
'rptListaClientes.Show
End Sub
Sub buscar()
Dim regActual
busqueda = Input_Box("Buscar PrePago." & Chr(13) & "Ingrese el Nombre:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    regActual = rst.AbsolutePosition
    rst.MoveFirst
    If InStr(1, txtNombre, busqueda, vbTextCompare) = 0 Then rst.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rst.EOF <> True Then
    Refrescar
Else
    rst.AbsolutePosition = regActual
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbCritical, , "explain"
End If
End Sub
Private Sub restablecer()
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
TabStrip1.Enabled = True
cmdPrimero.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdUltimo.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
txtNombre.Enabled = False
txtPrecio.Enabled = False
txtDescripcion.Enabled = False
DtFin.Enabled = False
cmdAgregar.Enabled = False
cmdQuitar.Enabled = False
Refrescar
End Sub
