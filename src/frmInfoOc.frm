VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmInfoOc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de �rdenes de Compra"
   ClientHeight    =   5010
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6495
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfoOc.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5010
   ScaleWidth      =   6495
   Begin MSFlexGridLib.MSFlexGrid grdInfo 
      Height          =   3015
      Left            =   60
      TabIndex        =   8
      Top             =   1920
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   5318
      _Version        =   393216
      Rows            =   1
      Cols            =   8
      FixedRows       =   0
      FixedCols       =   0
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Mostrar"
      Default         =   -1  'True
      Height          =   375
      Left            =   1800
      TabIndex        =   3
      Top             =   1200
      Width           =   1335
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3360
      TabIndex        =   4
      Top             =   1200
      Width           =   1335
   End
   Begin VB.ComboBox cmbEstado 
      Height          =   345
      Left            =   2700
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   120
      Width           =   1815
   End
   Begin MSComCtl2.DTPicker DtInicio 
      Height          =   375
      Left            =   1860
      TabIndex        =   1
      Top             =   600
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   24510465
      CurrentDate     =   35796
      MinDate         =   32874
   End
   Begin MSComCtl2.DTPicker DtFin 
      Height          =   375
      Left            =   4140
      TabIndex        =   2
      Top             =   600
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   24510465
      CurrentDate     =   37861
      MinDate         =   32874
   End
   Begin VB.Label Label1 
      Caption         =   "�rdenes de Compra:"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1680
      Width           =   1815
   End
   Begin VB.Label lblInicio 
      Caption         =   "Desde el"
      Height          =   255
      Left            =   1020
      TabIndex        =   7
      Top             =   600
      Width           =   855
   End
   Begin VB.Label lblFin 
      Caption         =   "Hasta el"
      Height          =   255
      Left            =   3300
      TabIndex        =   6
      Top             =   600
      Width           =   735
   End
   Begin VB.Label lblEstado 
      Caption         =   "Estado:"
      Height          =   255
      Left            =   1980
      TabIndex        =   5
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "frmInfoOc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset

Private Sub cmdaceptar_Click()
Dim xInicio, xFin
grdInfo.Rows = 1
If DtInicio.Value > DtFin.Value Then
    MsgRapido "Error: Fecha desde mayor que fecha hasta", Loro, vbCritical, , "surprised"
    Exit Sub
Else
    xInicio = Format(DtInicio, "yyyy-mm-dd")
    xFin = Format(DtFin, "yyyy-mm-dd")
    If cmbEstado.ItemData(cmbEstado.ListIndex) = 1 Or cmbEstado.ItemData(cmbEstado.ListIndex) = 0 Then
        rst.Source = "select o.nro_orden,o.fecha_creacion,o.estado,o.fecha_recibido,o.pagada,o.total_estimado,count(o.nro_orden) as cant_items,nro_remito from orden_compra o,detalle_oc d where d.nro_orden=o.nro_orden and o.estado=1 and o.fecha_creacion>='" & xInicio & "' and o.fecha_creacion<='" & xFin & "' group by o.nro_orden"
        rst.Open
        While Not rst.EOF
            grdInfo.AddItem rst!nro_orden & vbTab & rst!fecha_creacion & vbTab & "Pendiente" & vbTab & rst!fecha_recibido & vbTab & "" & vbTab & rst!total_estimado & vbTab & rst!cant_items & vbTab & rst!nro_remito
            If rst!pagada = "v" Then grdInfo.TextMatrix(grdInfo.Rows - 1, 4) = "Si" Else grdInfo.TextMatrix(grdInfo.Rows - 1, 4) = "No"
            rst.MoveNext
        Wend
        rst.Close
    End If
    If cmbEstado.ItemData(cmbEstado.ListIndex) = 2 Or cmbEstado.ItemData(cmbEstado.ListIndex) = 0 Then
        rst.Source = "select o.nro_orden,o.fecha_creacion,o.estado,o.fecha_recibido,o.pagada,o.total_estimado,count(o.nro_orden) as cant_items,nro_remito from orden_compra o,detalle_oc d where d.nro_orden=o.nro_orden and o.estado=2 and o.fecha_creacion>='" & xInicio & "' and o.fecha_creacion<='" & xFin & "' group by o.nro_orden"
        rst.Open
        While Not rst.EOF
            grdInfo.AddItem rst!nro_orden & vbTab & rst!fecha_creacion & vbTab & "Enviada" & vbTab & rst!fecha_recibido & vbTab & "" & vbTab & rst!total_estimado & vbTab & rst!cant_items & vbTab & rst!nro_remito
            If rst!pagada = "v" Then grdInfo.TextMatrix(grdInfo.Rows - 1, 4) = "Si" Else grdInfo.TextMatrix(grdInfo.Rows - 1, 4) = "No"
            rst.MoveNext
        Wend
        rst.Close
    End If
    If cmbEstado.ItemData(cmbEstado.ListIndex) = 3 Or cmbEstado.ItemData(cmbEstado.ListIndex) = 0 Then
        rst.Source = "select o.nro_orden,o.fecha_creacion,o.estado,o.fecha_recibido,o.pagada,o.total_estimado,count(o.nro_orden) as cant_items,nro_remito from orden_compra o,detalle_oc d where d.nro_orden=o.nro_orden and o.estado=3 and o.fecha_creacion>='" & xInicio & "' and o.fecha_creacion<='" & xFin & "' group by o.nro_orden"
        rst.Open
        While Not rst.EOF
            grdInfo.AddItem rst!nro_orden & vbTab & rst!fecha_creacion & vbTab & "Cancelada" & vbTab & rst!fecha_recibido & vbTab & "" & vbTab & rst!total_estimado & vbTab & rst!cant_items & vbTab & rst!nro_remito
            If rst!pagada = "v" Then grdInfo.TextMatrix(grdInfo.Rows - 1, 4) = "Si" Else grdInfo.TextMatrix(grdInfo.Rows - 1, 4) = "No"
            rst.MoveNext
        Wend
        rst.Close
    End If
    If cmbEstado.ItemData(cmbEstado.ListIndex) = 4 Or cmbEstado.ItemData(cmbEstado.ListIndex) = 0 Then
        rst.Source = "select o.nro_orden,o.fecha_creacion,o.estado,o.fecha_recibido,o.pagada,o.total_estimado,count(o.nro_orden) as cant_items,nro_remito from orden_compra o,detalle_oc d where d.nro_orden=o.nro_orden and o.estado=4 and o.fecha_creacion>='" & xInicio & "' and o.fecha_creacion<='" & xFin & "' group by o.nro_orden"
        rst.Open
        While Not rst.EOF
            grdInfo.AddItem rst!nro_orden & vbTab & rst!fecha_creacion & vbTab & "Recibida" & vbTab & rst!fecha_recibido & vbTab & "" & vbTab & rst!total_estimado & vbTab & rst!cant_items & vbTab & rst!nro_remito
            If rst!pagada = "v" Then grdInfo.TextMatrix(grdInfo.Rows - 1, 4) = "Si" Else grdInfo.TextMatrix(grdInfo.Rows - 1, 4) = "No"
            rst.MoveNext
        Wend
        rst.Close
    End If
    If grdInfo.Rows > 1 Then
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
        grdInfo.Row = 1
        grdInfo.RowSel = grdInfo.Rows - 1
        grdInfo.Col = 0
        grdInfo.ColSel = 0
        grdInfo.Sort = 3
        grdInfo.RowSel = 1
    Else
        MsgRapido "No existen datos", Loro, vbInformation, , "explain"
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
    End If
End If
End Sub

Private Sub cmdsalir_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
frmPrincipal.vista.Enabled = True
frmPrincipal.imprimir.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 3
cmbEstado.AddItem "Todos"
cmbEstado.ItemData(cmbEstado.NewIndex) = 0
cmbEstado.AddItem "Pendiente"
cmbEstado.ItemData(cmbEstado.NewIndex) = 1
cmbEstado.AddItem "Enviado"
cmbEstado.ItemData(cmbEstado.NewIndex) = 2
cmbEstado.AddItem "Cancelado"
cmbEstado.ItemData(cmbEstado.NewIndex) = 3
cmbEstado.AddItem "Recibido"
cmbEstado.ItemData(cmbEstado.NewIndex) = 4
cmbEstado.ListIndex = 0
DtFin.MaxDate = Date
DtFin = Date
DtInicio.MaxDate = Date
loadGrid
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
Set rst = Nothing
End Sub

Private Sub loadGrid()
grdInfo.ColWidth(0) = 765
grdInfo.ColWidth(1) = 1065
grdInfo.ColWidth(2) = 1005
grdInfo.ColWidth(3) = 1470
grdInfo.ColWidth(4) = 735
grdInfo.ColWidth(5) = 1000
grdInfo.ColWidth(6) = 1605

grdInfo.Rows = 0
grdInfo.AddItem "N�mero" & vbTab & "Fecha" & vbTab & "Estado" & vbTab & "Fecha de Recep." & vbTab & "Pagada" & vbTab & "Monto" & vbTab & "Cantidad de Items" & "N� de Remito"
grdInfo.Rows = 2
grdInfo.FixedRows = 1
grdInfo.Rows = 1
End Sub
Sub imprimir()
info 2
End Sub
Sub preview()
info 1
End Sub
Private Sub info(ByVal accion As Byte)
Dim rstInfo As ADODB.Recordset
Dim c
Set rstInfo = New ADODB.Recordset
rstInfo.CursorType = adOpenKeyset
rstInfo.LockType = adLockOptimistic
rstInfo.Fields.Append "numero", adVarChar, 10
rstInfo.Fields.Append "fecha", adVarChar, 10
rstInfo.Fields.Append "estado", adVarChar, 15
rstInfo.Fields.Append "fecha_rec", adVarChar, 10
rstInfo.Fields.Append "pagada", adVarChar, 2
rstInfo.Fields.Append "monto", adVarChar, 15
rstInfo.Fields.Append "items", adVarChar, 10
rstInfo.Fields.Append "remito", adVarChar, 20
rstInfo.Open
For c = 1 To grdInfo.Rows - 1
    rstInfo.AddNew
    rstInfo!numero = grdInfo.TextMatrix(c, 0)
    rstInfo!fecha = grdInfo.TextMatrix(c, 1)
    rstInfo!estado = grdInfo.TextMatrix(c, 2)
    rstInfo!fecha_rec = grdInfo.TextMatrix(c, 3)
    rstInfo!pagada = grdInfo.TextMatrix(c, 4)
    rstInfo!monto = grdInfo.TextMatrix(c, 5)
    rstInfo!items = grdInfo.TextMatrix(c, 6)
    rstInfo!remito = grdInfo.TextMatrix(c, 7)
    rstInfo.Update
Next c
Set rptInfoOc.DataSource = rstInfo
rptInfoOc.Sections(1).Controls(6).Caption = Format(DtInicio.Value, "long date") & "  y  " & Format(DtFin.Value, "long date")
Select Case accion
    Case 1
        rptInfoOc.Show
    Case 2
        rptInfoOc.PrintReport True
        Unload rptInfoOc
End Select
Set rstInfo = Nothing
End Sub

