VERSION 5.00
Begin VB.Form frmPassword 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cambiar Password"
   ClientHeight    =   2055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5385
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPassword.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2055
   ScaleWidth      =   5385
   Begin VB.ComboBox cmbEmpleado 
      Height          =   345
      Left            =   1440
      TabIndex        =   9
      Text            =   "Combo1"
      Top             =   120
      Width           =   3735
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3960
      TabIndex        =   7
      Top             =   1320
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   3960
      TabIndex        =   6
      Top             =   840
      Width           =   1215
   End
   Begin VB.TextBox txtConfirma 
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   2160
      MaxLength       =   15
      PasswordChar    =   "*"
      TabIndex        =   5
      Top             =   1560
      Width           =   1575
   End
   Begin VB.TextBox txtNueva 
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   2160
      MaxLength       =   15
      PasswordChar    =   "*"
      TabIndex        =   4
      Top             =   1080
      Width           =   1575
   End
   Begin VB.TextBox txtAnterior 
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   2160
      MaxLength       =   15
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Empleado:"
      Height          =   225
      Left            =   240
      TabIndex        =   8
      Top             =   120
      Width           =   900
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Confirmar Password:"
      Height          =   225
      Left            =   240
      TabIndex        =   2
      Top             =   1560
      Width           =   1755
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Password Nuevo:"
      Height          =   225
      Left            =   240
      TabIndex        =   1
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Password Anterior:"
      Height          =   225
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   1560
   End
End
Attribute VB_Name = "frmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim empleado As String
Dim rstemp As ADODB.Recordset

Private Sub cmbempleado_LostFocus()
txtAnterior = ""
txtNueva = ""
txtConfirma = ""
End Sub

Private Sub Form_Load()
Me.Top = 200
Me.Left = 200
Set rstemp = New ADODB.Recordset
Set rstemp = abrirrs("select legajo, nombre, apellido, password,nivel_acceso from empleado where borrado = 'f' order by apellido", True)
Cargar_Combo
rstemp.MoveFirst
Do While Not rstemp.EOF
If cmbempleado.Text = Mid(frmPrincipal.StatusBar1.Panels(3).Text, 10) Then
Exit Do
End If
cmbempleado.ListIndex = cmbempleado.ListIndex + 1
Loop
End Sub

Private Sub Cargar_Combo()
cmbempleado.Clear
rstemp.MoveFirst
If rstemp.RecordCount = 0 Then
    Exit Sub
End If
Do While Not rstemp.EOF
   cmbempleado.AddItem rstemp.Fields(2) & ", " & rstemp.Fields(1)                 'Descripcion
   cmbempleado.ItemData(cmbempleado.NewIndex) = rstemp.Fields(0)
   rstemp.MoveNext
Loop
cmbempleado.ListIndex = 0
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub cmdaceptar_Click()
rstemp.MoveFirst
rstemp.Find "legajo = " & cmbempleado.ItemData(cmbempleado.ListIndex)
'If Trim(txtAnterior.Text) = "" Then MsgRapido "Faltan Datos Necesarios, Por Favor Compl�telos", Loro, , , "surprised": txtAnterior.SetFocus: Exit Sub
If Trim(txtNueva.Text) = "" Then MsgRapido "Faltan Datos Necesarios, Por Favor Compl�telos", Loro, , , "surprised": txtNueva.SetFocus: Exit Sub
If Trim(txtConfirma.Text) = "" Then MsgRapido "Faltan Datos Necesarios, Por Favor Compl�telos", Loro, , , "surprised": txtConfirma.SetFocus: Exit Sub
If rstemp!password <> Trim(txtAnterior.Text) Then
    MsgRapido "El Password es Incorrecto", Loro, , , "surprised"
    txtAnterior.SetFocus
    Exit Sub
End If
If Trim(txtNueva.Text) <> Trim(txtConfirma.Text) Then
    MsgRapido "El Password es Incorrecto", Loro, , , "surprised"
    txtConfirma.Text = ""
    txtConfirma.SetFocus
    Exit Sub
End If
rstemp!password = Trim(txtNueva.Text)
rstemp!nivel_acceso = "f"
rstemp.Update
MsgRapido "Su Password Fue Cambiado Se�or " & rstemp!nombre & " " & rstemp!apellido, Loro, vbInformation, , "explain"
rstemp.Requery
Unload Me
End Sub

