VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRegLote 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Lote"
   ClientHeight    =   2385
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3660
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegLote.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   2385
   ScaleWidth      =   3660
   Begin VB.TextBox txtCantidad 
      Height          =   330
      Left            =   2183
      TabIndex        =   2
      Top             =   1440
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2003
      TabIndex        =   4
      Top             =   1920
      Width           =   1335
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   323
      TabIndex        =   3
      Top             =   1920
      Width           =   1335
   End
   Begin VB.OptionButton optDevolver 
      Caption         =   "Devolver"
      Height          =   255
      Left            =   1823
      TabIndex        =   6
      Top             =   120
      Width           =   1095
   End
   Begin VB.OptionButton optRecibir 
      Caption         =   "Recibir"
      Height          =   255
      Left            =   743
      TabIndex        =   5
      Top             =   120
      Value           =   -1  'True
      Width           =   975
   End
   Begin MSComCtl2.DTPicker dtVto 
      Height          =   375
      Left            =   2183
      TabIndex        =   0
      Top             =   480
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   24707073
      CurrentDate     =   37840
      MaxDate         =   55153
   End
   Begin VB.TextBox txtPrecio 
      Height          =   330
      Left            =   2183
      TabIndex        =   1
      Top             =   960
      Width           =   1335
   End
   Begin VB.Label lblCantidad 
      Caption         =   "Cantidad Comprada:"
      Height          =   255
      Left            =   143
      TabIndex        =   9
      Top             =   1440
      Width           =   2055
   End
   Begin VB.Label lblPrecio 
      Caption         =   "Precio de Compra:        $"
      Height          =   255
      Left            =   143
      TabIndex        =   8
      Top             =   960
      Width           =   2055
   End
   Begin VB.Label lblVto 
      Caption         =   "Fecha de Vencimiento:"
      Height          =   255
      Left            =   143
      TabIndex        =   7
      Top             =   480
      Width           =   1935
   End
End
Attribute VB_Name = "frmRegLote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim xPrecio As Double
Dim xId_Item As Long
Dim xTipo_Item As Byte
Dim xid_proveedor As Long
Dim xCantidad As Long
Dim xNro_orden As Long

Private Sub cmdaceptar_Click()
Dim rst As ADODB.Recordset
Dim cmd As ADODB.Command
If optRecibir = True Then
    If Trim(txtPrecio) = "" Or Trim(txtCantidad) = "" Then
        MsgRapido "Debe ingresar el precio y catidad de compra.", Loro, vbCritical
        Exit Sub
    End If
    Dim xNro_Lote As Long
    Set rst = New ADODB.Recordset
    Set cmd = New ADODB.Command
    rst.ActiveConnection = cnx
    cmd.ActiveConnection = cnx
    rst.Source = "select max(nro_lote)as max from lote"
    rst.CursorType = adOpenForwardOnly
    rst.LockType = adLockReadOnly
    rst.Open
    xNro_Lote = rst!max + 1
    rst.Close
    If xTipo_Item = 2 Then
        cmd.CommandText = "insert into lote(nro_lote,fecha_creacion,id_proveedor,id_item,tipo_item,cantidad,fecha_vto,precio_costo) values(" & xNro_Lote & ",'" & DateDB & "'," & xid_proveedor & "," & xId_Item & "," & xTipo_Item & "," & txtCantidad & ",'" & Format(dtVto.Value, "yyyy-mm-d") & "'," & Val(txtPrecio) & ")"
        cmd.Execute
        cmd.CommandText = "update medicamento set stock_total=stock_total + " & txtCantidad & " where id_medicamento=" & xId_Item
    Else
        cmd.CommandText = "insert into lote(nro_lote,fecha_creacion,id_proveedor,id_item,tipo_item,cantidad,precio_costo) values(" & xNro_Lote & ",'" & DateDB & "'," & xid_proveedor & "," & xId_Item & "," & xTipo_Item & "," & txtCantidad & "," & Val(txtPrecio) & ")"
        cmd.Execute
        cmd.CommandText = "update articulo set stock_total=stock_total + " & txtCantidad & " where id_articulo=" & xId_Item
    End If
    cmd.Execute
    cmd.CommandText = "update detalle_oc set estado=1 where nro_orden=" & xNro_orden & " and id_item=" & xId_Item & " and tipo_item=" & xTipo_Item
    cmd.Execute
    cmd.CommandText = "update detalle_oc set cantidad=" & txtCantidad & " where nro_orden=" & xNro_orden & " and id_item=" & xId_Item & " and tipo_item=" & xTipo_Item
    cmd.Execute
    cmd.CommandText = "update detalle_oc set precio_estimado=" & Val(txtPrecio) * txtCantidad & " where nro_orden=" & xNro_orden & " and id_item=" & xId_Item & " and tipo_item=" & xTipo_Item
    cmd.Execute
    frmRegMercaderia.grdDetalle.TextMatrix(frmRegMercaderia.grdDetalle.Row, 5) = "Almacenado"
    frmRegMercaderia.grdDetalle.TextMatrix(frmRegMercaderia.grdDetalle.Row, 2) = txtCantidad
    frmRegMercaderia.grdDetalle.TextMatrix(frmRegMercaderia.grdDetalle.Row, 3) = txtPrecio
    frmRegMercaderia.grdDetalle.TextMatrix(frmRegMercaderia.grdDetalle.Row, 4) = txtCantidad * Val(txtPrecio)
    frmRegMercaderia.recargarTotal
    Set cmd = Nothing
    Set rst = Nothing
Else
    Set cmd = New ADODB.Command
    cmd.ActiveConnection = cnx
    cmd.CommandText = "update detalle_oc set estado=2 where nro_orden=" & xNro_orden & " and id_item=" & xId_Item & " and tipo_item=" & xTipo_Item
    cmd.Execute
    frmRegMercaderia.grdDetalle.TextMatrix(frmRegMercaderia.grdDetalle.Row, 5) = "Devuelto"
    Set cmd = Nothing
    frmReclamoPed.txtNroOrden = xNro_orden
    frmReclamoPed.Show
    frmReclamoPed.buscar
End If
Unload Me
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
frmRegMercaderia.Enabled = False
If xTipo_Item = 3 Then dtVto.Enabled = False
dtVto.Value = Date
txtCantidad = xCantidad
txtPrecio = xPrecio
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmRegMercaderia.Enabled = True
End Sub

Private Sub optDevolver_Click()
txtPrecio.Enabled = False
txtPrecio.BackColor = &H80000013
txtCantidad.Enabled = False
txtCantidad.BackColor = &H80000013
dtVto.Enabled = False
End Sub

Private Sub optRecibir_Click()
txtPrecio.Enabled = True
txtPrecio.BackColor = &H80000005
txtCantidad.Enabled = True
txtCantidad.BackColor = &H80000005
dtVto.Enabled = True
If xTipo_Item = 3 Then dtVto.Enabled = False
End Sub
Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
KeyAscii = 0
End If
End If
End Sub

Private Sub txtPrecio_KeyPress(KeyAscii As Integer)
If KeyAscii = 46 And InStr(txtPrecio, ".") Then KeyAscii = 0
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
KeyAscii = 0
End If
End If
End Sub

Private Sub txtPrecio_Validate(Cancel As Boolean)
If Trim(txtPrecio) = "" Then Exit Sub
If Not IsNumeric(Trim(txtPrecio)) Then
    MsgRapido "El precio no es v�lido", Loro, vbCritical, , "decline"
    txtPrecio = ""
End If
End Sub
Sub cargar(ByVal IdProveedor As Long, ByVal TipoItem As Byte, ByVal IdItem As Long, ByVal Cantidad As Long, ByVal NroOrden As Long, ByVal Precio As Double)
xId_Item = IdItem
xTipo_Item = TipoItem
xid_proveedor = IdProveedor
xCantidad = Cantidad
xNro_orden = NroOrden
xPrecio = Precio
Me.Show
End Sub
