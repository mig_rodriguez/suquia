VERSION 5.00
Begin VB.Form frmRegReclamo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Reclamo"
   ClientHeight    =   3915
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6525
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegReclamo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3915
   ScaleWidth      =   6525
   Begin VB.CommandButton cmdnuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   5160
      TabIndex        =   10
      Top             =   240
      Width           =   1095
   End
   Begin VB.CommandButton Cmdaceptar 
      Cancel          =   -1  'True
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   1200
      TabIndex        =   4
      Top             =   3360
      Width           =   1335
   End
   Begin VB.ComboBox cmbtipo 
      Height          =   345
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   720
      Width           =   3135
   End
   Begin VB.ComboBox cmbcliente 
      Height          =   345
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   240
      Width           =   3135
   End
   Begin VB.ComboBox cmbempleado 
      Height          =   345
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1200
      Width           =   3135
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2843
      TabIndex        =   5
      Top             =   3360
      Width           =   1335
   End
   Begin VB.TextBox txtDescripcion 
      Height          =   975
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   2160
      Width           =   5055
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Tipo de Reclamo:"
      Height          =   225
      Left            =   240
      TabIndex        =   9
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Cliente:"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   240
      Width           =   975
   End
   Begin VB.Label lbltipo 
      Caption         =   "Empleado:"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   1200
      Width           =   1095
   End
   Begin VB.Label lblDescripcion 
      Caption         =   "Descripci�n:"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   1800
      Width           =   1095
   End
End
Attribute VB_Name = "frmRegReclamo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim rste As ADODB.Recordset
Dim rsr As ADODB.Recordset
Dim nocomp As Boolean
Dim nroreclamo As Integer
Dim xTipo As Integer

Private Sub cmbcliente_Change()
If nocomp = False Then completar cmbcliente Else: nocomp = False
End Sub

Private Sub cmbcliente_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyBack Then cmbcliente.Text = Left(cmbcliente.Text, cmbcliente.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbcliente
End Sub

Private Sub cmbcliente_LostFocus()
validar cmbcliente
End Sub

Private Sub cmbempleado_Change()
If nocomp = False Then completar cmbempleado Else: nocomp = False
End Sub

Private Sub cmbempleado_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyBack Then cmbempleado.Text = Left(cmbempleado.Text, cmbempleado.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbempleado
End Sub

Private Sub cmbempleado_LostFocus()
validar cmbempleado
End Sub

Private Sub cmdAceptar_Click()
 rsr.MoveLast
 nroreclamo = (1 + rsr(0))
 If Trim(txtDescripcion) = "" Then
    MsgRapido "Describa el reclamo.", Loro, vbExclamation, , "surprised"
    Exit Sub
 End If
 Loro.Play "write"
 rsr.AddNew
 rsr(0) = nroreclamo
 rsr!fecha = Date
 rsr!id_empleado = xLegajo
 rsr!id_cliente = cmbcliente.ItemData(cmbcliente.ListIndex)
 rsr!descripcion = txtDescripcion
 rsr!tipo = cmbtipo.Text
 rsr!tipo_item = xTipo
 rsr!id_item = cmbempleado.ItemData(cmbempleado.ListIndex)
 rsr.Update
 rsr.Requery
 MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
 txtDescripcion = ""
End Sub

Private Sub cmdcancelar_Click()
 Unload Me
End Sub

Private Sub cmdnuevo_Click()
 frmactcliente.Show
End Sub

Private Sub Form_Activate()
 frmPrincipal.desactivararch
 frmPrincipal.cerrar.Enabled = True
 cmbcliente.Clear
 Set rst = New ADODB.Recordset
 rst.ActiveConnection = cnx
 rst.CursorType = adOpenForwardOnly
 rst.LockType = adLockReadOnly
 rst.Source = "select id_cliente,nombre,apellido from cliente where id_cliente >0 order by apellido"
 rst.Open
 While Not rst.EOF
    cmbcliente.AddItem rst!apellido & ", " & rst!nombre
    cmbcliente.ItemData(cmbcliente.NewIndex) = rst!id_cliente
    rst.MoveNext
 Wend
 rst.Close
 cmbcliente.ListIndex = 0
End Sub

Private Sub Form_Load()
 Me.Left = 3390
 Me.Top = 930
 Set rste = New ADODB.Recordset
 rste.ActiveConnection = cnx
 rste.CursorType = adOpenStatic
 rste.LockType = adLockReadOnly
 Set rsr = New ADODB.Recordset
 rsr.ActiveConnection = cnx
 rsr.Source = "select * from reclamo order by nro_reclamo"
 rsr.CursorType = adOpenDynamic
 rsr.LockType = adLockOptimistic
 rsr.Open
 cmbtipo.AddItem "Art�culos"
 cmbtipo.AddItem "Servicios"
 cmbtipo.AddItem "Atenci�n"
 cmbtipo.AddItem "Pre-Pago"
 cmbtipo.AddItem "Medicamentos"
 cmbtipo.ListIndex = 0
End Sub

Private Sub cmbtipo_Click()
Select Case cmbtipo.List(cmbtipo.ListIndex)
    Case "Art�culos"
        lbltipo.Caption = "Art�culo: "
        xTipo = 3
        rste.Source = "select nombre, id_articulo as id from articulo order by nombre"
    Case "Servicios"
        lbltipo.Caption = "Servicio: "
        xTipo = 1
        rste.Source = "select nombre, id_servicio as id from servicio where id_servicio > 0 and id_rubro <> 7 order by nombre"
    Case "Pre-Pago"
        lbltipo.Caption = "Pre-Pago: "
        xTipo = 4
        rste.Source = "select nombre, id_servicio as id from servicio where id_rubro = 7 order by nombre"
    Case "Atenci�n"
        lbltipo.Caption = "Empleado: "
        xTipo = 5
        rste.Source = "select concat(apellido,', ', nombre) as nombre, legajo as id from empleado order by apellido"
    Case "Medicamentos"
        lbltipo.Caption = "Medicamento: "
        xTipo = 2
        rste.Source = "select nombre, id_medicamento as id from medicamento where id_medicamento > 0 order by nombre"
End Select
rste.Open
cmbempleado.Clear
While rste.EOF = False
    cmbempleado.AddItem rste!nombre
    cmbempleado.ItemData(cmbempleado.NewIndex) = rste!id
    rste.MoveNext
 Wend
 cmbempleado.ListIndex = 0
rste.Close
End Sub

Private Sub Form_Unload(Cancel As Integer)
 frmPrincipal.desactivararch
End Sub
