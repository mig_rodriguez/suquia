VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccione Usuario"
   ClientHeight    =   1965
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4305
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   1965
   ScaleWidth      =   4305
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar Sesi�n"
      Height          =   375
      Left            =   1485
      TabIndex        =   6
      Top             =   1320
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2925
      TabIndex        =   5
      Top             =   1320
      Width           =   1335
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   45
      TabIndex        =   4
      Top             =   1320
      Width           =   1335
   End
   Begin VB.TextBox txtPass 
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   1560
      MaxLength       =   18
      PasswordChar    =   "*"
      TabIndex        =   3
      Text            =   "15581"
      Top             =   720
      Width           =   2415
   End
   Begin VB.ComboBox cmbEmpleado 
      Height          =   345
      Left            =   1560
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   240
      Width           =   2415
   End
   Begin VB.Label lblPass 
      Caption         =   "Contrase�a:"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   720
      Width           =   1095
   End
   Begin VB.Label lblEmpleado 
      Caption         =   "Empleado:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim rstPerm As ADODB.Recordset

Private Sub cmdaceptar_Click()
txtPass_LostFocus
rst.MoveFirst
If rst!legajo <> cmbEmpleado.ItemData(cmbEmpleado.ListIndex) Then
    rst.Find "legajo=" & cmbEmpleado.ItemData(cmbEmpleado.ListIndex)
End If
If txtPass = rst!password Then
    xLegajo = rst!legajo
    frmPrincipal.StatusBar1.Panels(3).Text = "Usuario: " & rst!apellido & ", " & rst!nombre
    desactivarMenu
    activarMenu rst!legajo
    If rst!nivel_acceso = "v" Then
        MsgRapido "Recuerde que, como nuevo usuario, debe cambiar su contrase�a.", Loro, vbInformation, , "getattention"
        frmPassword.Show
    End If
    rst.Close
    If frmPrincipal.conhc.Enabled = True Then
        rst.Source = "select fecha_fininter from mascota_hc where fecha_fininter='" & Format(Date, "yyyy-mm-dd") & "'"
        rst.Open
        If rst.RecordCount > 0 Then frmAltas.Show
        rst.Close
    End If
    Unload Me
Else
    MsgRapido "La contrase�a es incorrecta.", Loro, vbCritical, , "surprised"
End If
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub cmdCerrar_Click()
xLegajo = 0
desactivarMenu
frmPrincipal.StatusBar1.Panels(3).Text = ""
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Top = 0
Me.Left = 0
If xLegajo = 0 Then
    desactivarMenu
    cmdCerrar.Enabled = False
End If
Set rstPerm = New ADODB.Recordset
rstPerm.ActiveConnection = cnx
rstPerm.CursorType = adOpenDynamic
rstPerm.LockType = adLockReadOnly
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenStatic
rst.LockType = adLockReadOnly
rst.Source = "select nombre,apellido,legajo,password,nivel_acceso from empleado where borrado='f'"
rst.Open
While rst.EOF = False
    cmbEmpleado.AddItem rst!apellido & ", " & rst!nombre
    cmbEmpleado.ItemData(cmbEmpleado.NewIndex) = rst!legajo
    rst.MoveNext
Wend
cmbEmpleado.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
If xLegajo = 0 Then
'    MsgRapido "Debe seleccionar un empleado.", Loro, vbCritical, , "surprised"
'    Cancel = 1
    frmPrincipal.sistema.Enabled = True
    frmPrincipal.sesion.Enabled = True
End If
End Sub

Private Sub txtPass_GotFocus()
frmPrincipal.cortar.Enabled = False
frmPrincipal.copiar.Enabled = False
frmPrincipal.pegar.Enabled = False
frmPrincipal.todo.Enabled = False
frmPrincipal.borrar2.Enabled = False
End Sub

Private Sub txtPass_LostFocus()
frmPrincipal.cortar.Enabled = True
frmPrincipal.copiar.Enabled = True
frmPrincipal.pegar.Enabled = True
frmPrincipal.todo.Enabled = True
frmPrincipal.borrar2.Enabled = True
End Sub
Private Sub activarMenu(ByVal leg As Long)
rstPerm.Source = "select * from permiso p,perm_emp e where p.id_permiso=e.id_permiso and e.legajo=" & leg
rstPerm.Open
With frmPrincipal
While Not rstPerm.EOF
    If rstPerm!atencion = "v" Then .atencion.Enabled = True
    If rstPerm!compras = "v" Then .compras.Enabled = True
    If rstPerm!prepago = "v" Then .prepago.Enabled = True
    'If rstPerm!prodyser = "v" Then .prodyser.Enabled = True
    If rstPerm!ventas = "v" Then .ventas.Enabled = True
    If rstPerm!sistema = "v" Then .sistema.Enabled = True
    If rstPerm!info = "v" Then .info.Enabled = True
    If rstPerm!conhc = "v" Then .conhc.Enabled = True: frmPrincipal.Toolbar3.Buttons(1).Enabled = True
    If rstPerm!actclientes = "v" Then .actclientes.Enabled = True
    If rstPerm!actmascota = "v" Then .actmascota.Enabled = True
    If rstPerm!actraza = "v" Then .actraza.Enabled = True
    If rstPerm!turnos = "v" Then .turnos.Enabled = True
    If rstPerm!reclamo = "v" Then .reclamo.Enabled = True
    If rstPerm!cruza = "v" Then .cruza.Enabled = True
    If rstPerm!infturnos = "v" Then .infturnos.Enabled = True
    If rstPerm!infenferm = "v" Then .infenferm.Enabled = True
    If rstPerm!inffrec = "v" Then .inffrec.Enabled = True
    If rstPerm!orden = "v" Then .orden.Enabled = True
    If rstPerm!mercaderia = "v" Then .mercaderia.Enabled = True
    If rstPerm!stock = "v" Then .stock.Enabled = True
    If rstPerm!recpedido = "v" Then .recpedido.Enabled = True
    If rstPerm!actproveedores = "v" Then .actproveedores.Enabled = True
    If rstPerm!pago = "v" Then .pago.Enabled = True
    If rstPerm!infventas = "v" Then .infventas.Enabled = True
    If rstPerm!infoc = "v" Then .infoc.Enabled = True
    If rstPerm!actprepago = "v" Then .actprepago.Enabled = True
    If rstPerm!afiliar = "v" Then .afiliar.Enabled = True
    If rstPerm!recpago = "v" Then .recpago.Enabled = True
    If rstPerm!vermoros = "v" Then .vermoros.Enabled = True
    If rstPerm!infmoros = "v" Then .infmoros.Enabled = True
    If rstPerm!actserv = "v" Then .actserv.Enabled = True
    If rstPerm!actmedicam = "v" Then .actmedicam.Enabled = True
    If rstPerm!actartic = "v" Then .actartic.Enabled = True
    If rstPerm!infclihab = "v" Then .infclihab.Enabled = True
    If rstPerm!facturar = "v" Then .facturar.Enabled = True: frmPrincipal.Toolbar3.Buttons(2).Enabled = True
    If rstPerm!estvta = "v" Then .estvta.Enabled = True
    If rstPerm!acttarjeta = "v" Then .acttarjeta.Enabled = True
    If rstPerm!actplanp = "v" Then .actplanp.Enabled = True
    If rstPerm!actempleado = "v" Then .actempleado.Enabled = True
    If rstPerm!servidor = "v" Then .servidor.Enabled = True
    If rstPerm!rubro = "v" Then .rubro.Enabled = True
    If rstPerm!estPac = "v" Then .estPac.Enabled = True
    If rstPerm!actespecie = "v" Then .actespecie.Enabled = True
    If rstPerm!vtaPP = "v" Then .vtaPP.Enabled = True
    If rstPerm!estRec = "v" Then .estRec.Enabled = True
    If rstPerm!actPerm = "v" Then .actPerm.Enabled = True
    If rstPerm!consaProv = "v" Then .consaProv.Enabled = True
    .password.Enabled = True
    .sesion.Enabled = True
    rstPerm.MoveNext
Wend
End With
rstPerm.Close

'If Mid(Acceso, 1, 1) = 1 Then
'    With frmPrincipal
'    .prodyser.Enabled = True
'    .infclihab.Enabled = True
'    .actserv.Enabled = True
'    .sistema.Enabled = True
'    .sesion.Enabled = True
'    .actempleado.Enabled = True
'    .prepago.Enabled = True
'    .infmoros.Enabled = True
'    .compras.Enabled = True
'    .infoc.Enabled = True
'    .actprepago.Enabled = True
'    .rubro.Enabled = True
'    End With
'End If
'If Mid(Acceso, 2, 1) = 1 Then
'    With frmPrincipal
'    .prepago.Enabled = True
'    .afiliar.Enabled = True
'    .recpago.Enabled = False
'    .vermoros.Enabled = False
'    .atencion.Enabled = True
'    .actclientes.Enabled = True
'    .actmascota.Enabled = True
'    .cruza.Enabled = True
'    .turnos.Enabled = True
'    .reclamo.Enabled = True
'    .infturnos.Enabled = True
'    .inffrec.Enabled = True
'    .actraza.Enabled = True
'    .ventas.Enabled = True
'    .facturar.Enabled = True
'    .sistema.Enabled = True
'    .sesion.Enabled = True
'    End With
'End If
'If Mid(Acceso, 3, 1) = 1 Then
'    With frmPrincipal
'    .ventas.Enabled = True
'    .estvta.Enabled = True
'    .facturar.Enabled = True
'    .acttarjeta.Enabled = True
'    .actplanp.Enabled = True
'    .infventas.Enabled = True
'    .prodyser.Enabled = True
'    .actartic.Enabled = True
'    .actmedicam.Enabled = True
'    .sistema.Enabled = True
'    .sesion.Enabled = True
'    End With
'End If
'If Mid(Acceso, 4, 1) = 1 Then
'    With frmPrincipal
'    .compras.Enabled = True
'    .mercaderia.Enabled = True
'    .recpedido.Enabled = True
'    .stock.Enabled = True
'    .orden.Enabled = True
'    .pago.Enabled = True
'    .actproveedores.Enabled = True
'    .sistema.Enabled = True
'    .sesion.Enabled = True
'    End With
'End If
'If Mid(Acceso, 5, 1) = 1 Then
'    With frmPrincipal
'    .atencion.Enabled = True
'    .conhc.Enabled = True
'    .infenferm.Enabled = True
'    .sistema.Enabled = True
'    .sesion.Enabled = True
'    End With
'End If
'If Mid(Acceso, 6, 1) = 1 Then
'    With frmPrincipal
'    .atencion.Enabled = True
'    .compras.Enabled = True
'    .prepago.Enabled = True
'    .prodyser.Enabled = True
'    .ventas.Enabled = True
'    .sistema.Enabled = True
'    .conhc.Enabled = True
'    .actclientes.Enabled = True
'    .actmascota.Enabled = True
'    .actraza.Enabled = True
'    .turnos.Enabled = True
'    .reclamo.Enabled = True
'    .cruza.Enabled = True
'    .infturnos.Enabled = True
'    .infenferm.Enabled = True
'    .inffrec.Enabled = True
'    .orden.Enabled = True
'    .mercaderia.Enabled = True
'    .stock.Enabled = True
'    .recpedido.Enabled = True
'    .actproveedores.Enabled = True
'    .pago.Enabled = True
'    .infventas.Enabled = True
'    .infoc.Enabled = True
'    .afiliar.Enabled = True
'    .actprepago.Enabled = True
'    .recpago.Enabled = True
'    .vermoros.Enabled = True
'    .infmoros.Enabled = True
'    .actserv.Enabled = True
'    .actartic.Enabled = True
'    .actmedicam.Enabled = True
'    .infclihab.Enabled = True
'    .facturar.Enabled = True
'    .estvta.Enabled = True
'    .acttarjeta.Enabled = True
'    .actplanp.Enabled = True
'    .actempleado.Enabled = True
'    .sesion.Enabled = True
'    .servidor.Enabled = True
'    .rubro.Enabled = True
'    End With
'End If
End Sub
Private Sub desactivarMenu()
With frmPrincipal
.atencion.Enabled = False
.compras.Enabled = False
.prepago.Enabled = False
'.prodyser.Enabled = False
.ventas.Enabled = False
.sistema.Enabled = False
.conhc.Enabled = False
.actclientes.Enabled = False
.actmascota.Enabled = False
.actraza.Enabled = False
.turnos.Enabled = False
.reclamo.Enabled = False
.cruza.Enabled = False
.infturnos.Enabled = False
.infenferm.Enabled = False
.inffrec.Enabled = False
.orden.Enabled = False
.mercaderia.Enabled = False
.stock.Enabled = False
.recpedido.Enabled = False
.actproveedores.Enabled = False
.pago.Enabled = False
.infventas.Enabled = False
.infoc.Enabled = False
.actprepago.Enabled = False
.afiliar.Enabled = False
.recpago.Enabled = False
.vermoros.Enabled = False
.infmoros.Enabled = False
.actserv.Enabled = False
.actartic.Enabled = False
.actmedicam.Enabled = False
.infclihab.Enabled = False
.facturar.Enabled = False
.estvta.Enabled = False
.acttarjeta.Enabled = False
.actplanp.Enabled = False
.actempleado.Enabled = False
.sesion.Enabled = False
.servidor.Enabled = False
.rubro.Enabled = False
.info.Enabled = False
.estPac.Enabled = False
.password.Enabled = False
.actespecie.Enabled = False
.vtaPP.Enabled = False
.estRec.Enabled = False
.actPerm.Enabled = False
.consaProv.Enabled = False
.Toolbar3.Buttons(1).Enabled = False
.Toolbar3.Buttons(2).Enabled = False
End With
End Sub
