VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmInfoHab 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Clientes Habituales a Peluquer�a y Lavander�a"
   ClientHeight    =   3330
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5670
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfoHab.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3330
   ScaleWidth      =   5670
   Begin MSFlexGridLib.MSFlexGrid grdInfo 
      Height          =   1815
      Left            =   75
      TabIndex        =   6
      Top             =   1440
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   3201
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedRows       =   0
      FixedCols       =   0
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Mostrar"
      Default         =   -1  'True
      Height          =   375
      Left            =   1388
      TabIndex        =   1
      Top             =   720
      Width           =   1335
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2948
      TabIndex        =   0
      Top             =   720
      Width           =   1335
   End
   Begin MSComCtl2.DTPicker DtInicio 
      Height          =   375
      Left            =   1391
      TabIndex        =   2
      Top             =   120
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   24641537
      CurrentDate     =   35796
      MinDate         =   32874
   End
   Begin MSComCtl2.DTPicker DtFin 
      Height          =   375
      Left            =   3791
      TabIndex        =   3
      Top             =   120
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   24641537
      CurrentDate     =   37861
      MinDate         =   32874
   End
   Begin VB.Label Label1 
      Caption         =   "Clientes Habituales:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   1815
   End
   Begin VB.Label lblInicio 
      Caption         =   "Desde el"
      Height          =   255
      Left            =   544
      TabIndex        =   5
      Top             =   120
      Width           =   855
   End
   Begin VB.Label lblFin 
      Caption         =   "Hasta el"
      Height          =   255
      Left            =   2944
      TabIndex        =   4
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "frmInfoHab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset

Private Sub cmdaceptar_Click()
Dim xInicio, xFin
grdInfo.Rows = 1
If DtInicio.Value > DtFin.Value Then
    MsgRapido "Error: Fecha desde mayor que fecha hasta", Loro, vbCritical, , "surprised"
    Exit Sub
Else
    xInicio = Format(DtInicio, "yyyy-mm-dd")
    xFin = Format(DtFin, "yyyy-mm-dd")
    rst.Source = "select c.nombre,c.apellido,c.tel,count(c.id_cliente) as visitas from cliente c,factura f,detalle_factura d where f.nro_factura=d.nro_factura and f.id_cliente=c.id_cliente and d.tipo_item=1 and d.id_item=9 and f.fecha>='" & xInicio & "' and f.fecha<='" & xFin & "' group by c.id_cliente"
    rst.Open
    If rst.RecordCount <> 0 Then
        Me.Height = 3705
        While Not rst.EOF
            grdInfo.AddItem rst!apellido & ", " & rst!nombre & vbTab & rst!tel & vbTab & rst!visitas
            rst.MoveNext
        Wend
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        MsgRapido "No existen datos", Loro, vbInformation, , "explain"
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
    End If
    rst.Close
End If
End Sub

Private Sub cmdsalir_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
frmPrincipal.vista.Enabled = True
frmPrincipal.imprimir.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 3
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
DtFin = Date
DtFin.MaxDate = Date
DtInicio.MaxDate = Date
loadGrid
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub loadGrid()
grdInfo.ColWidth(0) = 3525
grdInfo.ColWidth(1) = 1125
grdInfo.ColWidth(2) = 780
grdInfo.Rows = 0
grdInfo.AddItem "Cliente" & vbTab & "Tel�fono" & vbTab & "Visitas"
grdInfo.Rows = 2
grdInfo.FixedRows = 1
grdInfo.Rows = 1
End Sub

Sub imprimir()
rst.Open
Set rptInfoHab.DataSource = rst
rptInfoHab.Sections(1).Controls(6).Caption = Format(DtInicio.Value, "long date") & "  y  " & Format(DtFin.Value, "long date")
rptInfoHab.PrintReport True
Unload rptInfoHab
rst.Close
End Sub
Sub preview()
rst.Open
Set rptInfoHab.DataSource = rst
rptInfoHab.Sections(1).Controls(6).Caption = Format(DtInicio.Value, "long date") & "  y  " & Format(DtFin.Value, "long date")
rptInfoHab.Show
rst.Close
End Sub
