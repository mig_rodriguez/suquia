VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRegDec 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Deceso"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6735
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegDec.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3090
   ScaleWidth      =   6735
   Begin VB.TextBox txtAcciones 
      Height          =   855
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   1440
      Width           =   6495
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   1680
      TabIndex        =   5
      Top             =   2520
      Width           =   1575
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3480
      TabIndex        =   6
      Top             =   2520
      Width           =   1575
   End
   Begin MSComCtl2.UpDown UpDown 
      Height          =   330
      Left            =   3675
      TabIndex        =   3
      Top             =   600
      Width           =   240
      _ExtentX        =   344
      _ExtentY        =   582
      _Version        =   393216
      Value           =   1
      BuddyControl    =   "txtPeso"
      BuddyDispid     =   196612
      OrigLeft        =   2040
      OrigTop         =   840
      OrigRight       =   2280
      OrigBottom      =   1215
      Max             =   10000000
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtPeso 
      Height          =   330
      Left            =   2932
      TabIndex        =   2
      Text            =   "10"
      Top             =   600
      Width           =   735
   End
   Begin VB.TextBox txtHora 
      Enabled         =   0   'False
      Height          =   330
      Left            =   4140
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
   Begin VB.TextBox txtFecha 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1980
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label lblAcciones 
      Caption         =   "Motivo del Deceso:"
      Height          =   255
      Left            =   2580
      TabIndex        =   11
      Top             =   1080
      Width           =   1575
   End
   Begin VB.Label lblPeso 
      Caption         =   "Peso:"
      Height          =   255
      Left            =   2400
      TabIndex        =   9
      Top             =   600
      Width           =   495
   End
   Begin VB.Label lblGrs 
      Caption         =   "Kgrs."
      Height          =   255
      Left            =   3960
      TabIndex        =   10
      Top             =   600
      Width           =   375
   End
   Begin VB.Label lblHora 
      Caption         =   "Hora:"
      Height          =   255
      Left            =   3660
      TabIndex        =   8
      Top             =   120
      Width           =   495
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   1380
      TabIndex        =   7
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "frmRegDec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim xid_hc As Integer
Dim xPeso As String

Private Sub cmdaceptar_Click()
If Mensaje("Esta Seguro de Registrar el Deceso del Paciente", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then Exit Sub
If Trim(txtPeso) = "" Or Trim(txtAcciones) = "" Then
    MsgRapido "Los datos est�n incompletos", Loro, , , "surprised"
    Exit Sub
End If
Dim cmd As ADODB.Command
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
cmd.CommandText = "insert into detalle_hc values(" & xid_hc & ",'" & DateDB & "','" & TimeDB & "'," & txtPeso & ",'Deceso de la Mascota',0,0,'" & Trim(txtAcciones) & "'," & xLegajo & ",' ',' ')"
cmd.Execute
cmd.CommandText = "update mascota_hc set fecha_dec='" & DateDB & "' where id_hc=" & xid_hc
cmd.Execute
frmhistoria.loadGrid
frmhistoria.Enabled = True
frmhistoria.cmdAnalisis.Enabled = False
frmhistoria.cmdRegDec.Enabled = False
frmhistoria.cmdTratamiento.Enabled = False
frmhistoria.cmdVacunacion.Enabled = False
frmhistoria.cmdInternacion.Enabled = False
frmhistoria.txtFechaDec.BackColor = &HFF&
frmhistoria.txtFechaDec = Date
Unload Me
End Sub

Private Sub cmdcancelar_Click()
frmhistoria.Enabled = True
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
txtPeso = xPeso
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
txtFecha = Date
txtHora = Time
frmhistoria.Enabled = False
End Sub
Private Sub txtPeso_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtPeso_Validate(Cancel As Boolean)
If txtPeso <> "" Then
    If IsNumeric(txtPeso) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtPeso = ""
    End If
End If
End Sub
Sub setID(paramId_hc As Integer, paramPeso As String)
xid_hc = paramId_hc
xPeso = paramPeso
End Sub
'Private Sub UpDown_DownClick()
'txtPeso = Val(txtPeso) - 0.1
'End Sub
'
'Private Sub UpDown_UpClick()
'txtPeso = Val(txtPeso) + 0.1
'End Sub
