VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRegAnalisis 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Resultados de An�lisis Cl�nicos"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5655
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegAnalisis.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4605
   ScaleWidth      =   5655
   Begin VB.ComboBox cmbProv 
      Height          =   345
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1080
      Width           =   3015
   End
   Begin MSComCtl2.DTPicker dtHora 
      Height          =   330
      Left            =   3480
      TabIndex        =   1
      Top             =   120
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   582
      _Version        =   393216
      Format          =   24641538
      CurrentDate     =   37906
   End
   Begin MSComCtl2.DTPicker dtFecha 
      Height          =   330
      Left            =   1200
      TabIndex        =   0
      Top             =   120
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   582
      _Version        =   393216
      Format          =   24641537
      CurrentDate     =   37906
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3240
      TabIndex        =   7
      Top             =   4080
      Width           =   1455
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   1320
      TabIndex        =   6
      Top             =   4080
      Width           =   1455
   End
   Begin VB.TextBox txtResultados 
      Height          =   1575
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   1920
      Width           =   5415
   End
   Begin VB.ComboBox cmbAnalisis 
      Height          =   345
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   600
      Width           =   3015
   End
   Begin VB.TextBox txtDiagnostico 
      Height          =   330
      Left            =   1320
      MaxLength       =   50
      TabIndex        =   5
      Top             =   3600
      Width           =   4215
   End
   Begin VB.Label Label1 
      Caption         =   "Realizado por:"
      Height          =   255
      Left            =   480
      TabIndex        =   11
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label lblResultados 
      Caption         =   "Resultados:"
      Height          =   255
      Left            =   2280
      TabIndex        =   12
      Top             =   1560
      Width           =   1095
   End
   Begin VB.Label lblAnalisis 
      Caption         =   "Tipo de An�lisis:"
      Height          =   255
      Left            =   480
      TabIndex        =   10
      Top             =   600
      Width           =   1455
   End
   Begin VB.Label lblDiagnostico 
      Caption         =   "Diagn�stico:"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   3600
      Width           =   1095
   End
   Begin VB.Label lblHora 
      Caption         =   "Hora:"
      Height          =   255
      Left            =   2880
      TabIndex        =   9
      Top             =   120
      Width           =   495
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   480
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "frmRegAnalisis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim xid_hc As Integer
Dim xPeso As String

Private Sub cmdaceptar_Click()
Dim op
op = Mensaje("�Desea guardar los datos ingresados?", Loro, Balloon, vbQuestion + vbYesNoCancel, , "getattention")
Select Case op
    Case vbYes
If Trim(txtDiagnostico) = "" Or Trim(txtResultados) = "" Then
    MsgRapido "Los datos est�n incompletos", Loro, , , "surprised"
    Exit Sub
End If
Dim cmd As ADODB.Command
Dim rst As ADODB.Recordset
Dim xFecha As String
Dim xHora As String
Dim xPrecio
xFecha = Format(dtFecha.Value, "yyyy-mm-dd")
xHora = Format(dtHora.Value, "hh:mm:ss")
Set cmd = New ADODB.Command
Set rst = New ADODB.Recordset
cmd.ActiveConnection = cnx
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
cmd.CommandText = "insert into detalle_hc values(" & xid_hc & ",'" & xFecha & "','" & xHora & "'," & xPeso & ",'" & Trim(txtDiagnostico) & "'," & cmbAnalisis.ItemData(cmbAnalisis.ListIndex) & ",0,'" & Trim(txtResultados) & "'," & xLegajo & ",' ','An�lisis Realizado por: " & cmbProv.List(cmbProv.ListIndex) & "')"
cmd.Execute
rst.Source = "select precio from servicio where id_servicio=" & cmbAnalisis.ItemData(cmbAnalisis.ListIndex)
rst.Open
xPrecio = rst!Precio
rst.Close
cmd.CommandText = "insert into nofacturado(id_cliente,fecha,hora,tipo,id_item,precio) values(" & frmhistoria.cmbCliente.ItemData(frmhistoria.cmbCliente.ListIndex) & ",'" & DateDB & "','" & TimeDB & "','ser'," & cmbAnalisis.ItemData(cmbAnalisis.ListIndex) & "," & xPrecio & ")"
cmd.Execute
frmhistoria.loadGrid
frmhistoria.Enabled = True
Unload Me
    Case vbNo
        frmhistoria.Enabled = True
        Unload Me
    Case vbCancel
        Exit Sub
End Select
End Sub

Private Sub cmdcancelar_Click()
frmhistoria.Enabled = True
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
frmRegAnalisis.Top = 825
frmRegAnalisis.Left = 2955
dtFecha = Date
dtHora = Time
Dim rst As ADODB.Recordset
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenForwardOnly
rst.LockType = adLockReadOnly
rst.Source = "select nombre,id_servicio from servicio where id_rubro=6 and borrado='f'"
rst.Open
While rst.EOF = False
    cmbAnalisis.AddItem rst!nombre
    cmbAnalisis.ItemData(cmbAnalisis.NewIndex) = rst!id_servicio
    rst.MoveNext
Wend
rst.Close
rst.Source = "select p.razon_social,a.id_proveedor from proveedor p,prov_analisis a where p.id_proveedor=a.id_proveedor order by a.id_proveedor"
rst.Open
cmbProv.AddItem "Laboratorio Propio"
While Not rst.EOF
    cmbProv.AddItem rst!razon_social
    rst.MoveNext
Wend
rst.Close
cmbProv.ListIndex = 0
cmbAnalisis.ListIndex = 0
frmhistoria.Enabled = False
End Sub
Sub setID(paramId_hc As Integer, paramPeso As String)
xid_hc = paramId_hc
xPeso = paramPeso
End Sub

