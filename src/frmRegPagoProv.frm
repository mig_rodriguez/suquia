VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRegPagoProv 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Pago a Proveedores"
   ClientHeight    =   4920
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7440
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegPagoProv.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4920
   ScaleWidth      =   7440
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Default         =   -1  'True
      Height          =   375
      Left            =   3383
      TabIndex        =   26
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox txtProv 
      Enabled         =   0   'False
      Height          =   330
      Left            =   5303
      TabIndex        =   25
      Top             =   120
      Width           =   2055
   End
   Begin VB.OptionButton chkCheque 
      Caption         =   "Cheque"
      Height          =   255
      Left            =   4193
      TabIndex        =   3
      Top             =   1080
      Width           =   975
   End
   Begin VB.OptionButton chkEfectivo 
      Caption         =   "Efectivo"
      Height          =   255
      Left            =   3113
      TabIndex        =   2
      Top             =   1080
      Value           =   -1  'True
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cheque:"
      Enabled         =   0   'False
      Height          =   1095
      Left            =   233
      TabIndex        =   17
      Top             =   1320
      Width           =   6975
      Begin MSComCtl2.DTPicker dtFecha 
         Height          =   330
         Left            =   5520
         TabIndex        =   8
         Top             =   600
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   582
         _Version        =   393216
         Format          =   24641537
         CurrentDate     =   37906
      End
      Begin VB.TextBox txtBanco 
         Height          =   330
         Left            =   840
         TabIndex        =   4
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox txtCta 
         Height          =   330
         Left            =   840
         TabIndex        =   6
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox txtNroCheque 
         Height          =   330
         Left            =   3840
         TabIndex        =   7
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox txtTitular 
         Height          =   330
         Left            =   3840
         TabIndex        =   5
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label lblBanco 
         Caption         =   "Banco:"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   615
      End
      Begin VB.Label lblTitular 
         Caption         =   "Titular:"
         Height          =   255
         Left            =   2520
         TabIndex        =   19
         Top             =   240
         Width           =   735
      End
      Begin VB.Label lblCta 
         Caption         =   "Cuenta:"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   735
      End
      Begin VB.Label lblNroCheque 
         Caption         =   "N� de Cheque:"
         Height          =   255
         Left            =   2520
         TabIndex        =   22
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label lblFecha 
         Caption         =   "Fecha:"
         Height          =   255
         Left            =   5760
         TabIndex        =   20
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.TextBox txtTotal 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   330
      Left            =   5663
      TabIndex        =   10
      Text            =   "0"
      Top             =   3960
      Width           =   1380
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   3833
      TabIndex        =   12
      Top             =   4440
      Width           =   1335
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   2273
      TabIndex        =   11
      Top             =   4440
      Width           =   1335
   End
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   1095
      Left            =   293
      TabIndex        =   9
      Top             =   2760
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   1931
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedRows       =   0
      FixedCols       =   0
      AllowBigSelection=   0   'False
      FillStyle       =   1
   End
   Begin VB.TextBox txtFacProv 
      Enabled         =   0   'False
      Height          =   330
      Left            =   3413
      MaxLength       =   10
      TabIndex        =   1
      Top             =   600
      Width           =   2295
   End
   Begin VB.TextBox txtNroOrden 
      Height          =   330
      Left            =   1703
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Detalle de Orden de Compra:"
      Height          =   255
      Left            =   323
      TabIndex        =   24
      Top             =   2520
      Width           =   2415
   End
   Begin VB.Label lblTotal 
      Caption         =   "Total:"
      Height          =   255
      Left            =   5063
      TabIndex        =   23
      Top             =   3960
      Width           =   495
   End
   Begin VB.Label lblPago 
      Caption         =   "Pago en"
      Height          =   255
      Left            =   2273
      TabIndex        =   16
      Top             =   1080
      Width           =   735
   End
   Begin VB.Label lblFacProv 
      Caption         =   "Nro. Factura Prov.:"
      Height          =   255
      Left            =   1733
      TabIndex        =   15
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label lblProv 
      Caption         =   "Proveedor:"
      Height          =   255
      Left            =   4328
      TabIndex        =   14
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblNroOrden 
      Caption         =   "Orden de Compra:"
      Height          =   255
      Left            =   83
      TabIndex        =   13
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "frmRegPagoProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim cmd As ADODB.Command
Dim xId_prov As Long

Private Sub chkCheque_Click()
If chkCheque.Value = True Then Frame1.Enabled = True Else Frame1.Enabled = False
End Sub

Private Sub chkEfectivo_Click()
If chkEfectivo.Value = True Then Frame1.Enabled = False Else Frame1.Enabled = True
End Sub
Private Sub cmdaceptar_Click()
If Trim(txtNroOrden) = "" Or Trim(txtFacProv) = "" Then Exit Sub
If chkCheque.Value = True And (Trim(txtBanco) = "" Or Trim(txtTitular) = "" Or Trim(txtCta) = "" Or Trim(txtNroCheque) = "") Then
    MsgRapido "Faltan datos del Cheque.", Loro, vbCritical, , "surprised"
    Exit Sub
End If
Dim xNro_pago As Long
xNro_pago = nuevoid("nro_pago", "pago_proveedor")
If chkEfectivo.Value = True Then
    cmd.CommandText = "insert into pago_proveedor(nro_pago,nro_facturaprov,fecha,id_proveedor) values(" & xNro_pago & "," & txtFacProv & ",'" & DateDB & "'," & xId_prov & ")"
Else
    cmd.CommandText = "insert into pago_proveedor(nro_pago,nro_facturaprov,fecha,id_proveedor,banco,titular,cuenta,nro_cheque,fecha_cheque) values(" & xNro_pago & "," & txtFacProv & ",'" & DateDB & "'," & xId_prov & ",'" & txtBanco & "','" & txtTitular & "','" & txtCta & "','" & txtNroCheque & "','" & Format(dtFecha.Value, "yyyy-mm-dd") & "')"
End If
cmd.Execute
cmd.CommandText = "update orden_compra set pagada='v' where nro_orden=" & txtNroOrden
cmd.Execute
txtNroOrden = ""
If cargado("frmRegMercaderia") = True Then frmRegMercaderia.buscar: Unload Me
End Sub

Private Sub cmdBuscar_Click()
If txtNroOrden = "" Then Exit Sub
rst.Source = "select o.pagada,o.total_estimado,p.razon_social,d.id_proveedor from orden_compra o,detalle_oc d,proveedor p where d.nro_orden=o.nro_orden and d.id_proveedor=p.id_proveedor and o.pagada='f' and o.nro_orden=" & txtNroOrden
rst.Open
If rst.RecordCount = 0 Then
    rst.Close
    MsgRapido "La Orden de Compra solicitada no existe o ya se encuentra pagada.", Loro, vbInformation, , "explain"
    txtNroOrden = ""
    Exit Sub
End If
txtFacProv.Enabled = True
txtProv = rst!razon_social
txtTotal = rst!total_estimado
xId_prov = rst!id_proveedor
rst.Close
grdDetalle.Clear
grdDetalle.Rows = 0
grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Costo Total ($)"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
rst.Source = "select a.nombre,d.cantidad,d.precio_estimado from detalle_oc d,articulo a where d.id_item=a.id_articulo and d.tipo_item=3 and d.nro_orden=" & txtNroOrden
rst.Open
While Not rst.EOF
    grdDetalle.AddItem rst!nombre & vbTab & rst!Cantidad & vbTab & rst!precio_estimado
    rst.MoveNext
Wend
rst.Close
rst.Source = "select m.nombre,d.cantidad,d.precio_estimado from detalle_oc d,medicamento m where d.id_item=m.id_medicamento and d.tipo_item=2 and d.nro_orden=" & txtNroOrden
rst.Open
While Not rst.EOF
    grdDetalle.AddItem rst!nombre & vbTab & rst!Cantidad & vbTab & rst!precio_estimado
    rst.MoveNext
Wend
rst.Close
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
dtFecha.Value = Date
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenForwardOnly
rst.LockType = adLockReadOnly
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
grdDetalle.Rows = 0
grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Costo Total ($)"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
grdDetalle.ColWidth(0) = 3990
grdDetalle.ColWidth(1) = 1365
grdDetalle.ColWidth(2) = 1380
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub txtFacProv_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtNroOrden_Change()
If Trim(txtProv) <> "" Then
    txtTotal = "0"
    txtProv = ""
    txtFacProv = ""
    txtBanco = ""
    txtTitular = ""
    txtCta = ""
    txtNroCheque = ""
    chkEfectivo.Value = True
    txtFacProv.Enabled = False
    grdDetalle.Clear
    grdDetalle.Rows = 0
    grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Costo Total ($)"
    grdDetalle.Rows = 2
    grdDetalle.FixedRows = 1
    grdDetalle.Rows = 1
End If
End Sub

Private Sub txtNroOrden_GotFocus()
cmdBuscar.Default = True
End Sub

Private Sub txtNroOrden_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtNroOrden_LostFocus()
cmdAceptar.Default = True
End Sub
Sub buscar()
cmdBuscar_Click
End Sub
