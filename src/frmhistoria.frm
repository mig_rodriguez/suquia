VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmhistoria 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Historia Cl�nica"
   ClientHeight    =   7200
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11940
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmhistoria.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7200
   ScaleWidth      =   11940
   Begin VB.Frame fraRecetario 
      Caption         =   "Recetario"
      Height          =   3975
      Left            =   2640
      TabIndex        =   39
      Top             =   1920
      Visible         =   0   'False
      Width           =   5535
      Begin VB.CommandButton cmdCan 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   3030
         TabIndex        =   42
         Top             =   3480
         Width           =   1455
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   1470
         TabIndex        =   41
         Top             =   3480
         Width           =   1455
      End
      Begin VB.TextBox txtRecetario 
         Height          =   3135
         Left            =   120
         MaxLength       =   256
         MultiLine       =   -1  'True
         TabIndex        =   40
         Top             =   240
         Width           =   5295
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   1575
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   5895
      Begin VB.ComboBox cmbCliente 
         Height          =   345
         Left            =   2280
         Sorted          =   -1  'True
         TabIndex        =   0
         Text            =   "cmbCliente"
         Top             =   120
         Width           =   2655
      End
      Begin VB.CommandButton cmdCancelar 
         Cancel          =   -1  'True
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   3120
         TabIndex        =   3
         Top             =   1080
         Width           =   1455
      End
      Begin VB.CommandButton cmdHistoria 
         Caption         =   "&Historia Cl�nica"
         Default         =   -1  'True
         Enabled         =   0   'False
         Height          =   375
         Left            =   1320
         TabIndex        =   2
         Top             =   1080
         Width           =   1455
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   4440
         TabIndex        =   5
         Top             =   120
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.ComboBox cmbMascota 
         Enabled         =   0   'False
         Height          =   345
         Left            =   2280
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   600
         Width           =   2655
      End
      Begin VB.Label lblMascota 
         Caption         =   "Mascotas:"
         Height          =   255
         Left            =   600
         TabIndex        =   12
         Top             =   600
         Width           =   855
      End
      Begin VB.Label lblCliente 
         Caption         =   "Seleccione Cliente:"
         Height          =   255
         Left            =   600
         TabIndex        =   11
         Top             =   120
         Width           =   2055
      End
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   7455
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Visible         =   0   'False
      Width           =   12030
      Begin VB.CommandButton cmdRecetario 
         Caption         =   "&Recetario"
         Height          =   375
         Left            =   4575
         TabIndex        =   38
         Top             =   6720
         Width           =   1335
      End
      Begin VB.CommandButton cmdRegDec 
         Caption         =   "Registrar &Deceso"
         Height          =   375
         Left            =   5123
         TabIndex        =   37
         Top             =   1870
         Width           =   1695
      End
      Begin VB.TextBox txtPrepago 
         Enabled         =   0   'False
         Height          =   330
         Left            =   8520
         TabIndex        =   35
         Top             =   1920
         Width           =   3246
      End
      Begin VB.Frame Frame3 
         Caption         =   "Datos de la Mascota"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1695
         Left            =   143
         TabIndex        =   15
         Top             =   120
         Width           =   11655
         Begin VB.TextBox txtDuenio 
            Enabled         =   0   'False
            Height          =   330
            Left            =   5040
            TabIndex        =   24
            Top             =   240
            Width           =   2775
         End
         Begin VB.TextBox txtNombre 
            Enabled         =   0   'False
            Height          =   330
            Left            =   1080
            TabIndex        =   23
            Top             =   240
            Width           =   2775
         End
         Begin VB.TextBox txtSexo 
            Enabled         =   0   'False
            Height          =   330
            Left            =   1080
            TabIndex        =   22
            Top             =   1200
            Width           =   2775
         End
         Begin VB.TextBox txtTatuaje 
            Enabled         =   0   'False
            Height          =   330
            Left            =   5040
            TabIndex        =   21
            Top             =   1200
            Width           =   2775
         End
         Begin VB.TextBox txtPelo 
            Enabled         =   0   'False
            Height          =   330
            Left            =   5040
            TabIndex        =   20
            Top             =   720
            Width           =   2775
         End
         Begin VB.TextBox txtRaza 
            Enabled         =   0   'False
            Height          =   330
            Left            =   1080
            TabIndex        =   19
            Top             =   720
            Width           =   2775
         End
         Begin VB.TextBox txtFechaAlta 
            Enabled         =   0   'False
            Height          =   330
            Left            =   10200
            TabIndex        =   18
            Top             =   720
            Width           =   1095
         End
         Begin VB.TextBox txtFechaNac 
            Enabled         =   0   'False
            Height          =   330
            Left            =   10200
            TabIndex        =   17
            Top             =   240
            Width           =   1095
         End
         Begin VB.TextBox txtFechaDec 
            Enabled         =   0   'False
            Height          =   330
            Left            =   10200
            TabIndex        =   16
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label lblDue�o 
            Caption         =   "Due�o:"
            Height          =   255
            Left            =   3960
            TabIndex        =   33
            Top             =   240
            Width           =   615
         End
         Begin VB.Label lblNombre 
            Caption         =   "Nombre:"
            Height          =   255
            Left            =   240
            TabIndex        =   32
            Top             =   240
            Width           =   855
         End
         Begin VB.Label lblRaza 
            Caption         =   "Raza:"
            Height          =   255
            Left            =   240
            TabIndex        =   31
            Top             =   720
            Width           =   495
         End
         Begin VB.Label lblSexo 
            Caption         =   "Sexo:"
            Height          =   255
            Left            =   240
            TabIndex        =   30
            Top             =   1200
            Width           =   495
         End
         Begin VB.Label lblTatuaje 
            Caption         =   "Nro. Tatuaje:"
            Height          =   255
            Left            =   3960
            TabIndex        =   29
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label lblPelo 
            AutoSize        =   -1  'True
            Caption         =   "Pelaje:"
            Height          =   225
            Left            =   3960
            TabIndex        =   28
            Top             =   720
            Width           =   570
         End
         Begin VB.Label lblFechaAlta 
            Caption         =   "Fecha de Alta:"
            Height          =   255
            Left            =   8280
            TabIndex        =   27
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblFechaNac 
            Caption         =   "Fecha de Nacimiento:"
            Height          =   255
            Left            =   8280
            TabIndex        =   26
            Top             =   240
            Width           =   1815
         End
         Begin VB.Label lblFechaDec 
            Caption         =   "Fecha de Deceso:"
            Height          =   255
            Left            =   8280
            TabIndex        =   25
            Top             =   1200
            Width           =   1575
         End
      End
      Begin VB.CommandButton cmdInternacion 
         Caption         =   "Registrar &Internaci�n"
         Height          =   495
         Left            =   8175
         TabIndex        =   9
         Top             =   6120
         Width           =   2070
      End
      Begin VB.CommandButton cmdTratamiento 
         Caption         =   "Registrar &Tratamiento"
         Height          =   495
         Left            =   6015
         TabIndex        =   8
         Top             =   6120
         Width           =   2070
      End
      Begin VB.CommandButton cmdAnalisis 
         Caption         =   "Registrar &An�lisis"
         Height          =   495
         Left            =   3855
         TabIndex        =   7
         Top             =   6120
         Width           =   2070
      End
      Begin VB.CommandButton cmdVacunacion 
         Caption         =   "Registrar &Vacunaci�n"
         Height          =   495
         Left            =   1695
         TabIndex        =   6
         Top             =   6120
         Width           =   2070
      End
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Salir"
         Height          =   375
         Left            =   6030
         TabIndex        =   10
         Top             =   6720
         Width           =   1335
      End
      Begin MSFlexGridLib.MSFlexGrid grdHistoria 
         Height          =   3735
         Left            =   150
         TabIndex        =   34
         Top             =   2280
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   6588
         _Version        =   393216
         Rows            =   1
         Cols            =   9
         FixedRows       =   0
         FixedCols       =   2
         WordWrap        =   -1  'True
         FillStyle       =   1
         AllowUserResizing=   3
      End
      Begin VB.Label Label1 
         Caption         =   "Plan Prepago:"
         Height          =   255
         Left            =   7320
         TabIndex        =   36
         Top             =   1920
         Width           =   1215
      End
      Begin VB.Label lblConsultas 
         Caption         =   "Consultas Anteriores:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   2040
         Width           =   1935
      End
   End
End
Attribute VB_Name = "frmhistoria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim nocomp As Boolean
Dim rst As ADODB.Recordset
Dim rstDetalle As ADODB.Recordset
Dim rstDesc As ADODB.Recordset
Dim xPeso As String
Dim Descuentos() As Long

Private Sub cmbcliente_Change()
If nocomp = False Then completar cmbCliente Else nocomp = False
End Sub

Private Sub cmbCliente_Click()
cmdBuscar_Click
End Sub

Private Sub cmbcliente_KeyPress(KeyAscii As Integer)
cmbMascota.Enabled = False
cmdHistoria.Enabled = False
If KeyAscii = vbKeyBack Then cmbCliente.Text = Left(cmbCliente.Text, cmbCliente.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then
    validar cmbCliente
    'If cmbCliente.ListIndex <> -1 Then cmbcliente_Click
End If
End Sub

Private Sub cmbcliente_LostFocus()
validar cmbCliente
'If cmbMascota.Enabled = True And cmbMascota.Visible = True Then cmbMascota.SetFocus
'If cmbCliente.ListIndex <> -1 Then cmbcliente_Click
End Sub

Private Sub cmdaceptar_Click()
Dim temp As ADODB.Recordset
Set temp = New ADODB.Recordset
temp.ActiveConnection = cnx
temp.Source = "select max(id_hc) from mascota_hc"
temp.Open
Set rptRecetario.DataSource = temp
rptRecetario.Sections(1).Controls(3).Caption = "Recetario: " & txtNombre
rptRecetario.Sections(1).Controls(4).Caption = txtRecetario
Frame2.Enabled = True
fraRecetario.Visible = False
End Sub

Private Sub cmdAnalisis_Click()
frmRegAnalisis.Show
frmRegAnalisis.setID cmbMascota.ItemData(cmbMascota.ListIndex), xPeso
End Sub

Private Sub cmdBuscar_Click()
If rst.State = adStateOpen Then: rst.Close
rst.Source = "select nombre,id_hc from mascota_hc where borrado='f' and id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
rst.Open
cmbMascota.Clear
While rst.EOF = False
    cmbMascota.AddItem rst!nombre
    cmbMascota.ItemData(cmbMascota.NewIndex) = rst!id_hc
    rst.MoveNext
Wend
If cmbMascota.ListCount = 0 Then
    'If Me.Visible = True Then MsgRapido "El Cliente seleccionado no posee ninguna mascota.", Loro, vbInformation, , "dontrecognize"
    cmbMascota.Enabled = False
    cmdHistoria.Enabled = False
    Exit Sub
End If
cmbMascota.ListIndex = 0
cmbMascota.Enabled = True
cmdHistoria.Enabled = True
End Sub

Private Sub cmdCan_Click()
Frame2.Enabled = True
fraRecetario.Visible = False
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub cmdHistoria_Click()
If rst.State = adStateOpen Then: rst.Close
rst.Source = "select * from mascota_hc m, raza r,cliente c where m.id_raza=r.id_raza and c.id_cliente=m.id_cliente and id_hc=" & cmbMascota.ItemData(cmbMascota.ListIndex)
rst.Open
txtNombre = rst(3).Value
txtRaza = rst!nombre_raza
If IsNull(rst!nro_tatuaje) = True Then txtTatuaje = "" Else txtTatuaje = rst!nro_tatuaje
txtPelo = rst!pelo
txtFechaNac = rst(5).Value
txtFechaAlta = rst!fecha_alta
txtDuenio = rst!apellido & ", " & rst!nombre
On Error Resume Next
If IsNull(rst!fecha_dec) = True Then
    txtFechaDec = ""
    txtFechaDec.BackColor = &H80000005
    cmdVacunacion.Enabled = True
    cmdAnalisis.Enabled = True
    cmdTratamiento.Enabled = True
    cmdRegDec.Enabled = True
    cmdInternacion.Enabled = True
Else
    txtFechaDec = rst!fecha_dec
    txtFechaDec.BackColor = &HFF&
    cmdVacunacion.Enabled = False
    cmdAnalisis.Enabled = False
    cmdTratamiento.Enabled = False
    cmdRegDec.Enabled = False
    cmdInternacion.Enabled = False
End If
On Error GoTo 0
If rst(11).Value = "m" Then
    txtSexo = "Macho"
Else
    txtSexo = "Hembra"
End If
If rst!internado = "v" Then
cmdInternacion.Caption = "Registrar &Observaci�n"
Else
cmdInternacion.Caption = "Registrar &Internaci�n"
End If
loadGrid
frmhistoria.Caption = "Historia Cl�nica"
Loro.Play "announce"
'frmhistoria.Height = 7605
'frmhistoria.Width = 12030
Frame1.Visible = False
Frame2.Visible = True
Me.WindowState = 2
cmdsalir.Cancel = True
rstDesc.Source = "select s.nombre,d.id_hc,v.tipo_item,v.id_item,v.descuento from detalle_sp d,vademecum v,servicio s where s.id_servicio=d.id_servicio and d.id_servicio=v.id_prepago and d.id_hc=" & rst!id_hc
rstDesc.Open
If rstDesc.RecordCount = 0 Then txtPrepago = "Ninguno" Else txtPrepago = rstDesc!nombre
If rstDesc.RecordCount <> 0 Then ReDim Descuentos(rstDesc.RecordCount - 1, 2) Else ReDim Descuentos(0, 0)
While Not rstDesc.EOF
    Descuentos(rstDesc.AbsolutePosition - 1, 0) = rstDesc!tipo_item
    Descuentos(rstDesc.AbsolutePosition - 1, 1) = rstDesc!id_item
    Descuentos(rstDesc.AbsolutePosition - 1, 2) = rstDesc!descuento * 100
    rstDesc.MoveNext
Wend
rstDesc.Close
End Sub

Private Sub cmdInternacion_Click()
Dim internado As Boolean
If rst!internado = "f" Then
internado = False
Else
internado = True
End If
frmInternacion.setID cmbMascota.ItemData(cmbMascota.ListIndex), xPeso, internado
frmInternacion.Show
End Sub

Private Sub cmdRecetario_Click()
fraRecetario.Visible = True
Frame2.Enabled = False

End Sub

Private Sub cmdRegDec_Click()
frmRegDec.Show
frmRegDec.setID cmbMascota.ItemData(cmbMascota.ListIndex), xPeso
End Sub

Private Sub cmdsalir_Click()
Frame2.Visible = False
Frame1.Visible = True
txtFechaDec = ""
frmhistoria.Caption = "Consulta de Historia Cl�nica"
cmdCancelar.Cancel = True
Me.Visible = False
Me.WindowState = 0
Me.Visible = True
frmhistoria.Width = 6255
frmhistoria.Height = 2205

End Sub

Private Sub cmdTratamiento_Click()
frmRegTrat.Show
frmRegTrat.setID cmbMascota.ItemData(cmbMascota.ListIndex), xPeso
End Sub

Private Sub cmdVacunacion_Click()
frmRegVac.Show
frmRegVac.setID cmbMascota.ItemData(cmbMascota.ListIndex), xPeso
End Sub


Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
frmhistoria.Left = 915
frmhistoria.Top = 330
Set rstDesc = New ADODB.Recordset
rstDesc.ActiveConnection = cnx
rstDesc.CursorType = adOpenDynamic
rstDesc.LockType = adLockReadOnly
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.Source = "select c.nombre, c.apellido, c.id_cliente, c.nro_documento from cliente c,mascota_hc m where c.id_cliente>0 and c.borrado='f' and c.id_cliente=m.id_cliente group by c.id_cliente"
rst.CursorType = adOpenDynamic
rst.LockType = adLockReadOnly
rst.Open
Set rstDetalle = New ADODB.Recordset
rstDetalle.ActiveConnection = cnx
rstDetalle.CursorType = adOpenDynamic
rstDetalle.LockType = adLockReadOnly
While rst.EOF = False
    cmbCliente.AddItem rst!apellido & ", " & rst!nombre
    cmbCliente.ItemData(cmbCliente.NewIndex) = rst!id_cliente
    rst.MoveNext
Wend
rst.Close
cmbCliente.ListIndex = 0
frmhistoria.Width = 6255
frmhistoria.Height = 2205
End Sub
Sub loadGrid()
grdHistoria.Clear
If rstDetalle.State = adStateOpen Then: rstDetalle.Close
rstDetalle.Source = "select d.fecha,d.hora,d.peso,d.sintomas,m.nombre,s.nombre,d.detalle_servicio,d.dosis,d.accion from detalle_hc d,servicio s,medicamento m where d.id_servicio=s.id_servicio and d.id_medicamento=m.id_medicamento and id_hc=" & rst!id_hc & " order by d.fecha,d.hora"
rstDetalle.Open
grdHistoria.Rows = 0
grdHistoria.AddItem "Fecha" & vbTab & "Hora" & vbTab & "Peso" & vbTab & "S�ntomas" & vbTab & "Diagn�stico" & vbTab & "Medicaci�n" & vbTab & "Dosis" & vbTab & "Tratamiento" & vbTab & "Acciones Tomadas"
grdHistoria.ColWidth(0) = 1050
grdHistoria.ColWidth(1) = 1230
grdHistoria.ColWidth(2) = 975
grdHistoria.ColWidth(3) = 2205
grdHistoria.ColWidth(4) = 2055
grdHistoria.ColWidth(5) = 1860
grdHistoria.ColWidth(6) = 1200
grdHistoria.ColWidth(7) = 1665
grdHistoria.ColWidth(8) = 1845
grdHistoria.Rows = 2
grdHistoria.FixedRows = 1
grdHistoria.Rows = 1
While rstDetalle.EOF = False
grdHistoria.AddItem rstDetalle!fecha & vbTab & Format(rstDetalle!hora, "HH:mm") & vbTab & rstDetalle!peso & " Kgrs." & vbTab & rstDetalle!detalle_servicio & vbTab & rstDetalle!Sintomas & vbTab & rstDetalle(4).Value & vbTab & rstDetalle!dosis & vbTab & rstDetalle(5).Value & vbTab & rstDetalle!accion
rstDetalle.MoveNext
Wend
If rstDetalle.RecordCount <> 0 Then
rstDetalle.MoveLast
xPeso = rstDetalle!peso
End If
If IsNumeric(xPeso) = False Then: xPeso = 10
rstDetalle.Close
Dim c
For c = 2 To grdHistoria.Cols - 1
    grdHistoria.ColAlignment(c) = 0
Next c
grdHistoria.Row = 0
grdHistoria.Col = 0
grdHistoria.ColSel = grdHistoria.Cols - 1
grdHistoria.CellAlignment = 4
grdHistoria.ColSel = 1
grdHistoria.RowSel = grdHistoria.Rows - 1
grdHistoria.CellAlignment = 4
On Error Resume Next
grdHistoria.Row = 1
grdHistoria.Col = 1
grdHistoria.RowSel = 1
grdHistoria.ColSel = 1
End Sub

Private Sub Form_Resize()
If Frame1.Visible = False And Frame2.Visible = False Then Exit Sub
If Frame1.Visible = True Then Me.WindowState = 0: Exit Sub
Me.WindowState = 2
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub
Sub Reload()
cmdHistoria_Click
End Sub
Function getDes(ByVal xTipo_Item As Byte, xId_Item As Long) As Byte
If UBound(Descuentos, 2) = 0 Then
    getDes = 0
    Exit Function
End If
Dim c
For c = LBound(Descuentos, 1) To UBound(Descuentos, 1)
    If Descuentos(c, 0) = xTipo_Item And Descuentos(c, 1) = xId_Item Then
        getDes = Descuentos(c, 2)
        Exit Function
    End If
Next c
End Function
'Sub completar()
'Dim c
'Dim xSelStart
'For c = 0 To cmbCliente.ListCount - 1
'    If UCase(Left(cmbCliente.List(c), Len(cmbCliente.Text))) = UCase(cmbCliente.Text) Then
'        xSelStart = Len(cmbCliente.Text)
'        cmbCliente.Text = cmbCliente.List(c)
'        cmbCliente.SelStart = xSelStart
'        cmbCliente.SelLength = Len(cmbCliente.Text)
'        Exit Sub
'    End If
'Next c
'End Sub
'Sub validar()
'Dim c
'For c = 0 To cmbCliente.ListCount - 1
'    If Left(cmbCliente.List(c), Len(cmbCliente.Text)) = cmbCliente.Text Then
'        cmbCliente.ListIndex = c
'        Exit Sub
'    End If
'Next c
'End Sub

Private Sub grdHistoria_DblClick()
If grdHistoria.TextMatrix(grdHistoria.Row, 8) = "Fin de Internaci�n" Then
    frmInternacion.setID cmbMascota.ItemData(cmbMascota.ListIndex), xPeso, True
    frmInternacion.Show
    frmInternacion.cmdNuevaObs.Enabled = False
Else
    If grdHistoria.Row > 0 Then
    If Mensaje("�Desea Imprimir el Recetario?", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbYes Then imprimirRec grdHistoria.TextMatrix(grdHistoria.Row, 0), grdHistoria.TextMatrix(grdHistoria.Row, 3), grdHistoria.TextMatrix(grdHistoria.Row, 4), grdHistoria.TextMatrix(grdHistoria.Row, 5) & " " & grdHistoria.TextMatrix(grdHistoria.Row, 6), grdHistoria.TextMatrix(grdHistoria.Row, 8), txtNombre
    End If
End If
End Sub
