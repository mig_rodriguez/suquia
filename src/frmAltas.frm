VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmAltas 
   Caption         =   "Medicamentos Vencidos"
   ClientHeight    =   3705
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6315
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAltas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   3705
   ScaleWidth      =   6315
   Begin VB.CommandButton cmdAceptar 
      Cancel          =   -1  'True
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   2250
      TabIndex        =   2
      Top             =   3240
      Width           =   1815
   End
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   2415
      Left            =   210
      TabIndex        =   0
      Top             =   720
      Visible         =   0   'False
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   4260
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin VB.Label lblTitulo 
      Caption         =   "LAS SIGUIENTES MASCOTAS DEBEN SER DADAS DE ALTA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   270
      TabIndex        =   1
      Top             =   240
      Width           =   6015
   End
End
Attribute VB_Name = "frmAltas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim cmd As ADODB.Command
Private Sub cmdaceptar_Click()
Unload Me
End Sub

Private Sub Form_Load()
Me.Width = 9090
Me.Height = 4530
Me.Left = 1365
Me.Top = 1350
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
loadGrid
End Sub

Private Sub Form_Resize()
On Error Resume Next
lblTitulo.Left = (Me.ScaleWidth - lblTitulo.Width) / 2
grdDetalle.Left = (Me.ScaleWidth - grdDetalle.Width) / 2
grdDetalle.Width = Me.ScaleWidth
grdDetalle.Height = Me.ScaleHeight - 1500
cmdAceptar.Left = (Me.ScaleWidth - cmdAceptar.Width) / 2
cmdAceptar.Top = grdDetalle.Top + grdDetalle.Height + 300
End Sub

Private Sub loadGrid()
grdDetalle.FixedRows = 0
grdDetalle.FixedCols = 0
grdDetalle.Rows = 0
grdDetalle.Cols = 4
grdDetalle.ColWidth(0) = 2580
grdDetalle.ColWidth(1) = 1650
grdDetalle.ColWidth(2) = 1875
grdDetalle.ColWidth(3) = 1575
rst.Source = "select c.nombre,c.apellido,m.nombre as mascota,m.fecha_internacion,f.jaula from cliente c, mascota_hc m,ficha_internacion f where m.id_cliente=c.id_cliente and m.id_hc=f.id_hc and m.fecha_fininter='" & Format(Date, "yyyy-mm-dd") & "' group by m.id_hc"
rst.Open
grdDetalle.AddItem "Cliente" & vbTab & "Mascota" & vbTab & "Fecha de Internación" & vbTab & "Jaula"
While Not rst.EOF
    grdDetalle.AddItem rst!apellido & ", " & rst!nombre & vbTab & rst!mascota & vbTab & rst!fecha_internacion & vbTab & rst!jaula
    rst.MoveNext
Wend
rst.Close
grdDetalle.FixedRows = 1
grdDetalle.Visible = True
End Sub
