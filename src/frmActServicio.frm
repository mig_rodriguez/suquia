VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmActServicio 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Servicio"
   ClientHeight    =   5700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5010
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActServicio.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5700
   ScaleWidth      =   5010
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4095
      Index           =   1
      Left            =   150
      TabIndex        =   19
      Top             =   400
      Width           =   4695
      Begin VB.TextBox txtSer 
         Enabled         =   0   'False
         Height          =   570
         Index           =   2
         Left            =   240
         MaxLength       =   99
         TabIndex        =   8
         Top             =   3360
         Width           =   4215
      End
      Begin VB.TextBox txtSer 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   2160
         MaxLength       =   11
         TabIndex        =   1
         Top             =   1200
         Width           =   975
      End
      Begin VB.CommandButton cmdPrimero 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdUltimo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Width           =   375
      End
      Begin VB.ComboBox cmbRubro 
         Enabled         =   0   'False
         Height          =   345
         Left            =   2160
         TabIndex        =   6
         Top             =   2640
         Width           =   2295
      End
      Begin VB.TextBox xfecFin 
         Enabled         =   0   'False
         Height          =   330
         Left            =   2160
         TabIndex        =   3
         Text            =   "    /    /"
         Top             =   2160
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.CheckBox chkFF 
         Caption         =   "Asignar Fecha Fin"
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   2280
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CommandButton cmdNuerub 
         Caption         =   "+"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   360
         TabIndex        =   7
         Top             =   2640
         Width           =   375
      End
      Begin VB.CheckBox chkAF 
         Caption         =   "Quitar Fecha Fin"
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   2280
         Visible         =   0   'False
         Width           =   1770
      End
      Begin VB.TextBox txtSer 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   2160
         MaxLength       =   30
         TabIndex        =   0
         Top             =   720
         Width           =   2295
      End
      Begin VB.TextBox txtSer 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   3360
         TabIndex        =   21
         Top             =   240
         Width           =   1095
      End
      Begin MSComCtl2.DTPicker fecFin 
         Height          =   330
         Left            =   2160
         TabIndex        =   4
         Top             =   2160
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   24576001
         CurrentDate     =   37833
         MaxDate         =   73415
         MinDate         =   32874
      End
      Begin MSComCtl2.DTPicker fecInicio 
         Height          =   330
         Left            =   2160
         TabIndex        =   2
         Top             =   1680
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   24576001
         CurrentDate     =   37833
         MaxDate         =   73415
         MinDate         =   32874
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Nombre del Servicio :"
         Height          =   225
         Index           =   0
         Left            =   360
         TabIndex        =   29
         Top             =   840
         Width           =   1755
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Precio en $:"
         Height          =   225
         Index           =   1
         Left            =   360
         TabIndex        =   28
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Inicio:"
         Height          =   225
         Index           =   2
         Left            =   360
         TabIndex        =   27
         Top             =   1800
         Width           =   1290
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Fecha de Fin:"
         Height          =   225
         Index           =   3
         Left            =   360
         TabIndex        =   26
         Top             =   2280
         Width           =   1110
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n:"
         Height          =   225
         Index           =   4
         Left            =   360
         TabIndex        =   25
         Top             =   3120
         Width           =   1035
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "N�mero:"
         Height          =   225
         Index           =   5
         Left            =   2400
         TabIndex        =   24
         Top             =   360
         Width           =   720
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Rubro:"
         Height          =   225
         Index           =   6
         Left            =   960
         TabIndex        =   23
         Top             =   2760
         Width           =   555
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2558
      TabIndex        =   13
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1238
      TabIndex        =   9
      Top             =   5280
      Width           =   1215
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   3218
      TabIndex        =   12
      Top             =   4800
      Width           =   1215
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   1920
      TabIndex        =   11
      Top             =   4800
      Width           =   1215
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   578
      TabIndex        =   10
      Top             =   4800
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4095
      Index           =   2
      Left            =   150
      TabIndex        =   20
      Top             =   400
      Width           =   4695
      Begin MSFlexGridLib.MSFlexGrid grdSer 
         Height          =   3735
         Left            =   80
         TabIndex        =   30
         Top             =   240
         Width           =   4550
         _ExtentX        =   8043
         _ExtentY        =   6588
         _Version        =   393216
         Rows            =   1
         Cols            =   3
         FixedRows       =   0
         FixedCols       =   0
         ForeColor       =   -2147483642
         FocusRect       =   0
         SelectionMode   =   1
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   4575
      Left            =   50
      TabIndex        =   18
      Top             =   50
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   8070
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmActServicio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rsSer As ADODB.Recordset
Dim rsRub As ADODB.Recordset
Dim c, mintCurFrame As Integer
Dim band As Boolean
Dim busqueda As String

Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
 If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
band = False
Set rsSer = New ADODB.Recordset
Set rsRub = New ADODB.Recordset
Set rsSer = abrirrs("select * from servicio where id_servicio > 0 and id_rubro <> 7 and borrado = 'f' order by nombre", True)
Set rsRub = abrirrs("select * from rubro where id_rubro <> 7 and id_rubro < 10000 and id_tipo = 10003 and borrado = 'f' order by id_rubro", False)
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
cargarRubro
loadGrid
rsSer.MoveFirst
Refrescar
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rsSer.MoveFirst
    rsSer.Find "id_servicio = " & grdSer.TextMatrix(grdSer.Row, 2)
    Refrescar
    mintCurFrame = 1
    band = False
Else
If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
End If
End Sub

Private Sub Refrescar()
txtSer(0) = rsSer!id_servicio
txtSer(1) = rsSer!nombre
txtSer(3) = rsSer!Precio
txtSer(2) = rsSer!descripcion
fecInicio = rsSer!fecha_inicio
If rsSer!fecha_fin <> "" Then
    xfecFin.Visible = False
    fecFin = rsSer!fecha_fin
    Else
    xfecFin.Visible = True
End If
For c = 0 To cmbRubro.ListCount - 1
cmbRubro.ListIndex = c
If cmbRubro.ItemData(c) = rsSer!id_rubro Then
    Exit For
End If
Next
End Sub

Public Sub cargarRubro()
cmbRubro.Clear
rsRub.Requery
If rsRub.RecordCount = 0 Then
    Exit Sub
End If
rsRub.MoveFirst
Do While Not rsRub.EOF
   cmbRubro.AddItem rsRub!nombre
   cmbRubro.ItemData(cmbRubro.NewIndex) = rsRub!id_rubro
   rsRub.MoveNext
Loop
cmbRubro.ListIndex = 0
End Sub

'   Mover Servicio

Private Sub cmdPrimero_Click()
rsSer.MoveFirst
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAdelante_Click()
If rsSer.EOF <> True Then: rsSer.MoveNext
If rsSer.EOF <> True Then
Refrescar
Else
rsSer.MoveFirst
Refrescar
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
End Sub

Private Sub cmdAtras_Click()
If rsSer.BOF <> True Then: rsSer.MovePrevious
If rsSer.BOF <> True Then
Refrescar
Else
rsSer.MoveLast
Refrescar
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
End Sub

Private Sub cmdUltimo_Click()
rsSer.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

'   NUEVO SERVICIO

Private Sub cmdNuevo_Click()
nuevo
End Sub
Sub nuevo()
cmdPrimero.Enabled = False
cmdUltimo.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdModificar.Enabled = False
cmdNuevo.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
cmdNuerub.Enabled = True
TabStrip1.Enabled = False
rsSer.MoveLast
txtSer(0) = nuevoid("id_servicio", "servicio")
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Loro.Play "write"
rsSer.AddNew
For c = 1 To 3
txtSer(c).Enabled = True
txtSer(c) = ""
Next
chkFF.Visible = True
cmbRubro.Enabled = True
fecInicio.Value = Date
xfecFin.Visible = True
txtSer(1).SetFocus
End Sub

'   MODIFICAR SERVICIO

Private Sub cmdModificar_Click()
modificar
End Sub
Sub modificar()
cmdPrimero.Enabled = False
cmdUltimo.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
cmdNuerub.Enabled = True
TabStrip1.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Loro.Play "write"
For c = 1 To 3
txtSer(c).Enabled = True
Next
If xfecFin.Visible = True Then
chkFF.Visible = True
Else
chkAF.Visible = True
fecFin.Enabled = True
End If
cmbRubro.Enabled = True
fecInicio.Enabled = True
txtSer(1).SetFocus
End Sub

'   GUARDAR

Private Sub cmdGuardar_Click()
guardar
End Sub
Sub guardar()
If Trim(txtSer(1)) = "" Then MsgRapido "Faltan Datos Necesarios, Por Favor Compl�telos", Loro, , , "surprised": Exit Sub
If Trim(txtSer(3)) = "" Then txtSer(3) = " "
If xfecFin.Visible = False Then
    If fecFin.Value < fecInicio.Value Then
        MsgRapido "La fecha de Fin no es v�lida", Loro, , , "surprised"
        Exit Sub
    End If
    rsSer!fecha_fin = fecFin.Value
    Else
    rsSer!fecha_fin = Null
End If
rsSer!id_servicio = txtSer(0)
rsSer!nombre = txtSer(1)
rsSer!descripcion = txtSer(2)
rsSer!Precio = txtSer(3)
rsSer!fecha_inicio = fecInicio.Value
rsSer!id_rubro = cmbRubro.ItemData(cmbRubro.ListIndex)
rsSer!borrado = "f"
rsSer.Update
rsSer.Requery
rsSer.Find "id_servicio =" & txtSer(0)
restablecer
loadGrid
cmdAdelante.SetFocus
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Servicio Seleccionado", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rsSer!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rsSer.Update
rsSer.Requery
cmdAdelante_Click
loadGrid
End Sub

Private Sub chkFF_Click()
If chkFF.Value = 1 Then
    xfecFin.Visible = False
    fecFin.Enabled = True
    Else
    xfecFin.Visible = True
    fecFin.Enabled = False
End If
End Sub

Private Sub chkAF_Click()
If chkAF.Value = 1 Then
    xfecFin.Visible = True
    Else
    xfecFin.Visible = False
End If
End Sub

Private Sub restablecer()
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdNuevo.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
cmdNuerub.Enabled = False
TabStrip1.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
chkFF.Visible = False
chkAF.Visible = False
For c = 0 To 3
txtSer(c).Enabled = False
Next
fecInicio.Enabled = False
fecFin.Enabled = False
cmbRubro.Enabled = False
Refrescar
End Sub

Sub cmdsalir_Click()
Unload Me
End Sub

' SOLO N�MEROS

Private Sub Txtser_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 3 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
    End If
End If
End Sub

Private Sub Txtser_Validate(Index As Integer, Cancel As Boolean)
If Index = 3 Then
    If txtSer(Index) <> "" Then
    If IsNumeric(txtSer(Index)) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtSer(Index) = ""
    End If
    End If
End If
End Sub

Sub buscar()
busqueda = Input_Box("Buscar Servicio" & Chr(13) & "Ingrese el Nombre del Servicio:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    rsSer.Filter = ""
    If txtSer(1) = busqueda Then: rstmas.MoveNext
    rsSer.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rsSer.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, , , "explain"
End If
End Sub

Private Sub cmdNuerub_Click()
frmActRubro.Show
Me.Enabled = False
frmActRubro.nuevo
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        rsSer.CancelUpdate
        restablecer
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
Set rsSer = Nothing
Set rsRub = Nothing
End Sub

Private Sub grdSer_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Private Sub loadGrid()
Dim regActual As Long
regActual = rsSer.AbsolutePosition
rsSer.MoveFirst
grdSer.ColWidth(0) = 1800
grdSer.ColWidth(1) = 2600
grdSer.ColWidth(2) = 0
grdSer.Rows = 0
grdSer.AddItem "Nombre" & vbTab & "Descripci�n"
grdSer.Rows = 2
grdSer.FixedRows = 1
grdSer.Rows = 1
While rsSer.EOF = False
grdSer.AddItem rsSer!nombre & vbTab & rsSer!descripcion & vbTab & rsSer!id_servicio
rsSer.MoveNext
Wend
rsSer.AbsolutePosition = regActual
End Sub

Sub imprimir()
frmPrincipal.desactivararch
Set rptServicios.DataSource = rsSer
rptServicios.PrintReport True
Unload rptServicios
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptServicios.DataSource = rsSer
rptServicios.Show
End Sub


