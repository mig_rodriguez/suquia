VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmVtaPP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Servicios de Atenci�n Prepago"
   ClientHeight    =   5955
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8055
   ClipControls    =   0   'False
   Icon            =   "frmVtaPP.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5955
   ScaleWidth      =   8055
   Begin TabDlg.SSTab SSTab1 
      Height          =   3015
      Left            =   120
      TabIndex        =   14
      Top             =   2400
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   5318
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      TabCaption(0)   =   "Servicios"
      TabPicture(0)   =   "frmVtaPP.frx":1272
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "GriItem(1)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Medicamentos"
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "GriItem(2)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Art�culos"
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "GriItem(3)"
      Tab(2).ControlCount=   1
      Begin MSFlexGridLib.MSFlexGrid GriItem 
         Height          =   2415
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   480
         Width           =   7575
         _ExtentX        =   13361
         _ExtentY        =   4260
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid GriItem 
         Height          =   2415
         Index           =   3
         Left            =   -74880
         TabIndex        =   16
         Top             =   480
         Width           =   7575
         _ExtentX        =   13361
         _ExtentY        =   4260
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid GriItem 
         Height          =   2415
         Index           =   2
         Left            =   -74880
         TabIndex        =   17
         Top             =   480
         Width           =   7575
         _ExtentX        =   13361
         _ExtentY        =   4260
         _Version        =   393216
         Cols            =   8
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   0
         SelectionMode   =   1
         AllowUserResizing=   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4260
      TabIndex        =   13
      Top             =   5520
      Width           =   1935
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   1860
      TabIndex        =   12
      Top             =   5520
      Width           =   1935
   End
   Begin VB.TextBox txtNombre 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1680
      MaxLength       =   30
      TabIndex        =   6
      Top             =   720
      Width           =   3615
   End
   Begin VB.TextBox txtPrecio 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6720
      MaxLength       =   10
      TabIndex        =   5
      Top             =   720
      Width           =   1095
   End
   Begin VB.TextBox txtDes 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   570
      Left            =   1680
      MaxLength       =   100
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   1200
      Width           =   6135
   End
   Begin VB.CommandButton cmdPrimero 
      Height          =   375
      Left            =   120
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   120
      Width           =   375
   End
   Begin VB.CommandButton cmdAtras 
      Height          =   375
      Left            =   600
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   120
      Width           =   375
   End
   Begin VB.CommandButton cmdAdelante 
      Height          =   375
      Left            =   1080
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   120
      Width           =   375
   End
   Begin VB.CommandButton cmdUltimo 
      Height          =   375
      Left            =   1560
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   120
      Width           =   375
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000003&
      BorderStyle     =   6  'Inside Solid
      X1              =   0
      X2              =   8145
      Y1              =   1920
      Y2              =   1920
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Beneficios del Servicio Prepago"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   11
      Top             =   2040
      Width           =   3060
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Servicios De Atenci�n Prepaga Disponibles"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   2280
      TabIndex        =   10
      Top             =   240
      Width           =   4110
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Nombre del Plan:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   120
      TabIndex        =   9
      Top             =   720
      Width           =   1440
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Precio en $:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   5640
      TabIndex        =   8
      Top             =   720
      Width           =   975
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Descripci�n:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   120
      TabIndex        =   7
      Top             =   1320
      Width           =   1035
   End
End
Attribute VB_Name = "frmVtaPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rsPP As ADODB.Recordset
Dim rsVad As ADODB.Recordset
Dim rst As ADODB.Recordset
Dim c As Integer
Dim band As Boolean
Dim busqueda As String
Dim cat As String

'Private Sub cmdagregar_Click()
'frmAgrePP.Show
'End Sub

Private Sub Form_Activate()
frmPrincipal.activararch
frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
frmPrincipal.vista.Enabled = True
frmPrincipal.imprimir.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
band = False
Set rsPP = New ADODB.Recordset
Set rsVad = New ADODB.Recordset
Set rst = New ADODB.Recordset
Set rsPP = abrirrs("select * from servicio where id_rubro = 7 and borrado = 'f' order by id_servicio", True)
Set rsVad = abrirrs("select * from vademecum ", True)
Set rst = abrirrs("select nombre from empleado", False)
rst.Close
rsPP.MoveFirst
cargarG1
cargarG2
cargarG3
Refrescar
End Sub

Private Sub Refrescar()
'On Error Resume Next
For c = 1 To 3
GriItem(c).Rows = 1
Next
txtNombre = rsPP!nombre
txtPrecio = rsPP!Precio
txtDes = rsPP!descripcion
rsVad.Filter = ""
If rsVad.RecordCount = 0 Then Exit Sub
rsVad.MoveFirst
rsVad.Filter = " id_prepago = '" & rsPP!id_servicio & "'"
If rsVad.RecordCount <> 0 Then
 rsVad.MoveFirst
 Do While Not rsVad.EOF
 Select Case rsVad!tipo_item
    Case 1
        rst.Source = "select id_servicio as id,nombre, descripcion, precio from servicio where id_servicio=" & rsVad!id_item
        rst.Open
        GriItem(1).AddItem "Servicio" & vbTab & rst!nombre & vbTab & " $ " & rst!Precio & vbTab & rsVad!descuento * 100 & " % " & vbTab & " $ " & rst!Precio - (rst!Precio * rsVad!descuento) & vbTab & rst!descripcion
        rst.Close
    Case 2
        rst.Source = "select id_medicamento as id,nombre, monodroga, presentacion, descripcion, precio from medicamento where id_medicamento=" & rsVad!id_item
        rst.Open
        GriItem(2).AddItem "Medicamento" & vbTab & rst!nombre & vbTab & rst!monodroga & vbTab & rst!presentacion & vbTab & " $ " & rst!Precio & vbTab & rsVad!descuento * 100 & " % " & vbTab & " $ " & rst!Precio - (rst!Precio * rsVad!descuento) & vbTab & rst!descripcion
        rst.Close
    Case 3
        rst.Source = "select id_articulo as id,nombre, presentacion, descripcion, precio from articulo where id_articulo=" & rsVad!id_item
        rst.Open
        GriItem(3).AddItem "Art�culo" & vbTab & rst!nombre & vbTab & rst!presentacion & vbTab & " $ " & rst!Precio & vbTab & rsVad!descuento * 100 & " % " & vbTab & " $ " & rst!Precio - (rst!Precio * rsVad!descuento) & vbTab & rst!descripcion
        rst.Close
 End Select
 rsVad.MoveNext
 Loop
End If
End Sub

'   Mover Servicio prepago

Private Sub cmdPrimero_Click()
rsPP.MoveFirst
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAdelante_Click()
If rsPP.EOF <> True Then: rsPP.MoveNext
If rsPP.EOF <> True Then
Refrescar
Else
rsPP.MoveFirst
Refrescar
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
End Sub

Private Sub cmdAtras_Click()
If rsPP.BOF <> True Then: rsPP.MovePrevious
If rsPP.BOF <> True Then
Refrescar
Else
rsPP.MoveLast
Refrescar
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
End Sub

Private Sub cmdUltimo_Click()
rsPP.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

Sub cmdsalir_Click()
Unload Me
End Sub

Private Sub cargarG1()
GriItem(1).ColWidth(0) = 0
GriItem(1).ColWidth(1) = 1900
GriItem(1).ColWidth(2) = 700
GriItem(1).ColWidth(3) = 700
GriItem(1).ColWidth(4) = 700
GriItem(1).ColWidth(5) = 3700
GriItem(1).Rows = 0
GriItem(1).AddItem "" & vbTab & "Nombre" & vbTab & "Precio" & vbTab & "Descto." & vbTab & "Total" & vbTab & "Descripci�n"
GriItem(1).Rows = 2
GriItem(1).FixedRows = 1
GriItem(1).Rows = 1
End Sub

Private Sub cargarG2()
GriItem(2).ColWidth(0) = 0
GriItem(2).ColWidth(1) = 1800
GriItem(2).ColWidth(2) = 1500
GriItem(2).ColWidth(3) = 1200
GriItem(2).ColWidth(4) = 700
GriItem(2).ColWidth(5) = 700
GriItem(2).ColWidth(6) = 600
GriItem(2).ColWidth(7) = 4000
GriItem(2).Rows = 0
GriItem(2).AddItem "" & vbTab & "Nombre" & vbTab & "Droga" & vbTab & "Presentaci�n" & vbTab & "Precio" & vbTab & "Descto." & vbTab & "Total" & vbTab & "Descripci�n"
GriItem(2).Rows = 2
GriItem(2).FixedRows = 1
GriItem(2).Rows = 1
End Sub

Private Sub cargarG3()
GriItem(3).ColWidth(0) = 0
GriItem(3).ColWidth(1) = 1800
GriItem(3).ColWidth(2) = 1500
GriItem(3).ColWidth(3) = 700
GriItem(3).ColWidth(4) = 700
GriItem(3).ColWidth(5) = 700
GriItem(3).ColWidth(6) = 4000
GriItem(3).Rows = 0
GriItem(3).AddItem "" & vbTab & "Nombre" & vbTab & "Presentaci�n" & vbTab & "Precio" & vbTab & "Descto." & vbTab & "Total" & vbTab & "Descripci�n"
GriItem(3).Rows = 2
GriItem(3).FixedRows = 1
GriItem(3).Rows = 1
End Sub

Sub buscar()
busqueda = Input_Box("Buscar Servicio Pre Pago" & Chr(13) & "Ingrese el Nombre del Servicio:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    rsPP.Filter = ""
    If txtNombre = busqueda Then: rsPP.MoveNext
    rsPP.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rsPP.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbInformation, , "explain"
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
Set rsPP = Nothing
Set rsVad = Nothing
Set rst = Nothing
End Sub

Sub imprimir()
frmPrincipal.desactivararch
Set rptPrePago.DataSource = rsPP
rptPrePago.PrintReport True
Unload rptPrePago
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptPrePago.DataSource = rsPP
rptPrePago.Show
End Sub

