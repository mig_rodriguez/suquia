VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAgregarOC 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Agregar Item"
   ClientHeight    =   2400
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3510
   ControlBox      =   0   'False
   Icon            =   "frmAgregarOC.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2400
   ScaleWidth      =   3510
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox cmbProv 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   960
      Width           =   2295
   End
   Begin VB.ComboBox cmbMedicamento 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1200
      TabIndex        =   0
      Text            =   "cmbMedicamento"
      Top             =   480
      Width           =   2295
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   2040
      TabIndex        =   4
      Top             =   1440
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   393216
      Value           =   1
      BuddyControl    =   "txtCantidad"
      BuddyDispid     =   196613
      OrigLeft        =   2280
      OrigTop         =   1080
      OrigRight       =   2520
      OrigBottom      =   1455
      Max             =   100
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2029
      TabIndex        =   6
      Top             =   1920
      Width           =   975
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   506
      TabIndex        =   5
      Top             =   1920
      Width           =   975
   End
   Begin VB.TextBox txtCantidad 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1200
      TabIndex        =   3
      Text            =   "1"
      Top             =   1440
      Width           =   855
   End
   Begin VB.OptionButton optMedicamento 
      Caption         =   "Medicamento"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   368
      TabIndex        =   7
      Top             =   120
      Value           =   -1  'True
      Width           =   1695
   End
   Begin VB.OptionButton optArticulo 
      Caption         =   "Art�culo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2168
      TabIndex        =   8
      Top             =   120
      Width           =   975
   End
   Begin VB.ComboBox cmbArticulo 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1200
      TabIndex        =   1
      Text            =   "cmbArticulo"
      Top             =   480
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Label Label1 
      Caption         =   "Proveedor:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label lblCantidad 
      Caption         =   "Cantidad:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   150
      TabIndex        =   11
      Top             =   1560
      Width           =   735
   End
   Begin VB.Label lblNombre 
      Caption         =   "Nombre:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   150
      TabIndex        =   9
      Top             =   600
      Width           =   735
   End
End
Attribute VB_Name = "frmAgregarOC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim rsP As ADODB.Recordset
        Dim nocomp As Boolean

        Private Sub cmbArticulo_Change()
        If nocomp = False Then completar cmbArticulo Else: nocomp = False
        End Sub
        
        Private Sub cmbArticulo_KeyPress(KeyAscii As Integer)
        cmdAceptar.Enabled = False
        If KeyAscii = vbKeyBack Then cmbArticulo.Text = Left(cmbArticulo.Text, cmbArticulo.SelStart): nocomp = True
        If KeyAscii = vbKeyReturn Then validar cmbArticulo
        End Sub
        
        Private Sub cmbArticulo_LostFocus()
        validar cmbArticulo
        End Sub

        Private Sub cmbMedicamento_Change()
        If nocomp = False Then completar cmbMedicamento Else: nocomp = False
        End Sub

        Private Sub cmbMedicamento_KeyPress(KeyAscii As Integer)
        cmdAceptar.Enabled = False
        If KeyAscii = vbKeyBack Then cmbMedicamento.Text = Left(cmbMedicamento.Text, cmbMedicamento.SelStart): nocomp = True
        If KeyAscii = vbKeyReturn Then validar cmbMedicamento
        End Sub

        Private Sub cmbMedicamento_LostFocus()
        validar cmbMedicamento
        End Sub

Private Sub Form_Activate()
frmPrincipal.cerrar.Enabled = False
End Sub

Private Sub Form_Load()
frmAgregarOC.Top = 2055
frmAgregarOC.Left = 6345
frmOrdenCompra.Enabled = False
Set rst = New ADODB.Recordset
Set rst = abrirrs("select nombre,id_medicamento from medicamento where id_medicamento>0 and borrado='f'", False)
While rst.EOF = False
    cmbMedicamento.AddItem rst!nombre
    cmbMedicamento.ItemData(cmbMedicamento.NewIndex) = rst!id_medicamento
    rst.MoveNext
Wend
rst.Close
Set rst = abrirrs("select nombre,id_articulo from articulo where borrado='f'", False)
While rst.EOF = False
    cmbArticulo.AddItem rst!nombre
    cmbArticulo.ItemData(cmbArticulo.NewIndex) = rst!id_articulo
    rst.MoveNext
Wend
rst.Close
Set rsP = abrirrs("select p.id_proveedor, p.razon_social, p.tel,x.tipo_item,x.id_item from proveedor p, prov_item x where p.id_proveedor=x.id_proveedor and p.borrado='f'", False)
cmbMedicamento.ListIndex = 0
cmbArticulo.ListIndex = 0
cmbMedicamento_Click
End Sub

Private Sub cmdaceptar_Click()
If optMedicamento.Value = True Then: frmOrdenCompra.agregar 2, cmbMedicamento.ItemData(cmbMedicamento.ListIndex), txtCantidad, cmbProv.ItemData(cmbProv.ListIndex)
If optArticulo.Value = True Then: frmOrdenCompra.agregar 3, cmbArticulo.ItemData(cmbArticulo.ListIndex), txtCantidad, cmbProv.ItemData(cmbProv.ListIndex)
If cargado("frmConStock") = True Then
Dim tipoItem, c
If frmConStock.grdResultado.TextMatrix(frmConStock.grdResultado.Row, 0) = "Med." Then tipoItem = 2 Else tipoItem = 3
For c = 1 To frmOrdenCompra.grdDetalle.Rows - 1
    If frmOrdenCompra.grdDetalle.TextMatrix(c, 7) = tipoItem And frmOrdenCompra.grdDetalle.TextMatrix(c, 0) = frmConStock.grdResultado.TextMatrix(frmConStock.grdResultado.Row, 1) Then
        frmConStock.grdResultado.Col = 0
        frmConStock.grdResultado.ColSel = 4
        frmConStock.grdResultado.CellBackColor = vbGreen
        Exit For
    End If
Next
End If
Unload Me
End Sub

Private Sub cmdcancelar_Click()
frmOrdenCompra.Enabled = True
Unload Me
End Sub

Private Sub optArticulo_Click()
If cmbArticulo.ListIndex = -1 Then cmbArticulo.ListIndex = 0
If optArticulo.Value = True Then
cmbMedicamento.Visible = False
cmbArticulo.Visible = True
cmbArticulo_Click
End If
txtCantidad_Validate False
End Sub

Private Sub optMedicamento_Click()
If cmbMedicamento.ListIndex = -1 Then cmbMedicamento.ListIndex = 0
If optMedicamento.Value = True Then
cmbMedicamento.Visible = True
cmbArticulo.Visible = False
cmbMedicamento_Click
End If
txtCantidad_Validate False
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtCantidad_Validate(Cancel As Boolean)
If txtCantidad <> "" Then
    If IsNumeric(txtCantidad) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtCantidad = ""
    End If
End If
If Val(txtCantidad) > UpDown1.max Then: txtCantidad = UpDown1.max
End Sub

Private Sub cmbMedicamento_Click()
cmdAceptar.Enabled = True
CargarProveedor 2
End Sub

Private Sub cmbArticulo_Click()
cmdAceptar.Enabled = True
CargarProveedor 3
End Sub

Private Sub CargarProveedor(tipo As Integer)
cmbProv.Clear
rsP.MoveFirst
Do While Not rsP.EOF
    If tipo = 2 Then
        If rsP!tipo_item = tipo And cmbMedicamento.ItemData(cmbMedicamento.ListIndex) = rsP!id_item Then
            cmbProv.AddItem rsP(1)
            cmbProv.ItemData(cmbProv.NewIndex) = rsP(0)
        End If
    Else
        If rsP!tipo_item = tipo And cmbArticulo.ItemData(cmbArticulo.ListIndex) = rsP!id_item Then
            cmbProv.AddItem rsP(1)
            cmbProv.ItemData(cmbProv.NewIndex) = rsP(0)
        End If
    End If
    rsP.MoveNext
Loop
On Error Resume Next
cmbProv.ListIndex = 0
End Sub
Sub agregarStock(ByVal xId_Item As Long, ByVal xTipo_Item As Byte)
Dim c
optMedicamento.Enabled = False
optArticulo.Enabled = False
cmbArticulo.Enabled = False
cmbMedicamento.Enabled = False
Select Case xTipo_Item
    Case 2
        optMedicamento.Value = True
        optMedicamento_Click
        For c = 0 To cmbMedicamento.ListCount - 1
            If cmbMedicamento.ItemData(c) = xId_Item Then cmbMedicamento.ListIndex = c: Exit For
        Next c
    Case 3
        optArticulo.Value = True
        optArticulo_Click
        For c = 0 To cmbArticulo.ListCount - 1
            If cmbArticulo.ItemData(c) = xId_Item Then cmbArticulo.ListIndex = c: Exit For
        Next c
End Select
Me.Show
End Sub
