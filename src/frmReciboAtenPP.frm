VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmReciboAtenPP 
   Caption         =   "Generar Recibo Atenci�n Prepaga"
   ClientHeight    =   4800
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7650
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4800
   ScaleWidth      =   7650
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3840
      TabIndex        =   6
      Top             =   4200
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5520
      TabIndex        =   5
      Top             =   4200
      Width           =   1455
   End
   Begin VB.CommandButton cmdQuitar 
      Caption         =   "-"
      Enabled         =   0   'False
      Height          =   375
      Left            =   7095
      TabIndex        =   4
      Top             =   2760
      Width           =   375
   End
   Begin VB.CommandButton cmdAgregar 
      Caption         =   "+"
      Height          =   375
      Left            =   7095
      TabIndex        =   3
      Top             =   2280
      Width           =   375
   End
   Begin VB.TextBox txtNro 
      Enabled         =   0   'False
      Height          =   330
      Left            =   2640
      TabIndex        =   2
      Top             =   720
      Width           =   975
   End
   Begin VB.ComboBox cmbEmpleado 
      Height          =   315
      Left            =   5040
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1200
      Width           =   2415
   End
   Begin VB.ComboBox cmbCliente 
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   1200
      Width           =   2415
   End
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   1455
      Left            =   120
      TabIndex        =   7
      Top             =   2280
      Width           =   6840
      _ExtentX        =   12065
      _ExtentY        =   2566
      _Version        =   393216
      Rows            =   1
      Cols            =   4
      FixedRows       =   0
      FixedCols       =   0
      GridColor       =   128
      ScrollBars      =   2
   End
   Begin VB.Label lblDate 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "[Fecha]"
      Height          =   225
      Left            =   6555
      TabIndex        =   13
      Top             =   720
      Width           =   600
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   5640
      TabIndex        =   12
      Top             =   720
      Width           =   615
   End
   Begin VB.Label lblEmplado 
      Caption         =   "Empleado:"
      Height          =   255
      Left            =   4080
      TabIndex        =   11
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label lblCliente 
      Caption         =   "Cliente:"
      Height          =   255
      Left            =   315
      TabIndex        =   10
      Top             =   1200
      Width           =   735
   End
   Begin VB.Label lblNroFactura 
      Caption         =   "N�mero de Recibo:"
      Height          =   255
      Left            =   315
      TabIndex        =   9
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label lblTitulo 
      Caption         =   "Recibo Atenci�n Prepago"
      Height          =   255
      Left            =   2865
      TabIndex        =   8
      Top             =   240
      Width           =   1935
   End
End
Attribute VB_Name = "frmReciboAtenPP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim rstsp As ADODB.Recordset
Dim cmd As ADODB.Command
Dim ordenItem As Integer

Private Sub cmbCliente_Click()
cmd.CommandText = "delete from detalle_recatpp where temp>0"
cmd.Execute
grdDetalle.Clear
grdDetalle.FixedRows = 0
grdDetalle.Rows = 0
grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unitario" & vbTab & "Total"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
If grdDetalle.Rows > 1 Then
  cmdAceptar.Enabled = True
 Else
  cmdAceptar.Enabled = False
End If
End Sub

Private Sub cmdAceptar_Click()
Dim formaPago
cmd.CommandText = "insert into recibo_atpp values(" & txtNro & "," & cmbCliente.ItemData(cmbCliente.ListIndex) & ",'" & DateDB & "')"
cmd.Execute
rst.Source = "select * from detalle_recatpp where temp>0 and tipo_item=3"
rst.Open
While Not rst.EOF
    cmd.CommandText = "update articulo set stock_total=stock_total-" & rst!Cantidad & " where id_articulo=" & rst!id_item
    cmd.Execute
    rst.MoveNext
Wend
rst.Close
cmd.CommandText = "update detalle_recatpp set temp=0"
cmd.Execute
'Set rptFactura.DataSource = rst
'rptFactura.Show
MsgBox "mostrar reporte, lo que falta es hacer el reporte."
Unload Me
End Sub

Private Sub cmdAgregar_Click()
frmAgregar.mostrar Me
frmAgregar.Show
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub cmdQuitar_Click()
cmd.CommandText = "delete from detalle_recatpp where temp=" & ordenItem
cmd.Execute
If ordenItem > 1 Then
 grdDetalle.RemoveItem (ordenItem)
Else
 grdDetalle.Clear
 grdDetalle.FixedRows = 0
 grdDetalle.Rows = 0
 grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unitario" & vbTab & "Total"
 grdDetalle.Rows = 2
 grdDetalle.FixedRows = 1
 grdDetalle.Rows = 1
End If
ordenItem = ordenItem - 1
If ordenItem = 0 Then: cmdQuitar.Enabled = False
If ordenItem = 0 Then: cmdAceptar.Enabled = False
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Dim aa
Me.Height = 5205
Me.Width = 7770
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
lblDate.Caption = VBA.Date
grdDetalle.ColWidth(0) = 2835
grdDetalle.ColWidth(1) = 1300
grdDetalle.ColWidth(2) = 1300
grdDetalle.ColWidth(3) = 1300
Set rst = New ADODB.Recordset
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
rst.ActiveConnection = cnx
rst.CursorType = adOpenForwardOnly
rst.LockType = adLockReadOnly
rst.Source = "select nombre,apellido, id_cliente from cliente where id_cliente >0 order by id_cliente"
rst.Open

Set rstsp = New ADODB.Recordset
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
rstsp.ActiveConnection = cnx
rstsp.CursorType = adOpenForwardOnly
rstsp.LockType = adLockReadOnly
rstsp.Source = "select id_cliente from servicio_prepago"
rstsp.Open

While rst.EOF = False
    aa = 0
    rstsp.MoveFirst
    While rstsp.EOF = False
      If rst!id_cliente = rstsp!id_cliente And aa = 0 Then
         cmbCliente.AddItem rst!apellido & ", " & rst!nombre
         cmbCliente.ItemData(cmbCliente.NewIndex) = rst!id_cliente
         aa = 1
      End If
      rstsp.MoveNext
    Wend
    rst.MoveNext
Wend
rst.Close
rstsp.Close
rst.Source = "select nombre,apellido,legajo from empleado order by legajo"
rst.Open
While rst.EOF = False
    cmbEmpleado.AddItem rst!apellido & ", " & rst!nombre
    cmbEmpleado.ItemData(cmbEmpleado.NewIndex) = rst!legajo
    rst.MoveNext
Wend
rst.Close
cmbEmpleado.ListIndex = 0
cmbCliente.ListIndex = 0
grdDetalle.Rows = 0
grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unitario" & vbTab & "Total"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
rst.Source = "select nro_recibo from recibo_atpp order by nro_recibo"
rst.Open
If rst.RecordCount > 0 Then
  rst.MoveLast
  txtNro = rst!nro_recibo + 1
 Else
  txtNro = 1
End If
rst.Close
ordenItem = 0
End Sub

Sub agregar(xTipo_Item As Integer, xId_Item As Integer, xCantidad As Integer)
On Error GoTo catchErr
Select Case xTipo_Item
    Case 1
        rst.Source = "select nombre,precio from servicio where id_servicio=" & xId_Item
    Case 2
        rst.Source = "select nombre,precio from medicamento where id_medicamento=" & xId_Item
    Case 3
        rst.Source = "select nombre,precio from articulo where id_articulo=" & xId_Item
End Select
rst.Open
ordenItem = ordenItem + 1
cmd.CommandText = "insert into detalle_recatpp values(" & txtNro & "," & xId_Item & "," & xCantidad & "," & xTipo_Item & "," & ordenItem & ")"
cmd.Execute
grdDetalle.AddItem rst!nombre & vbTab & xCantidad & vbTab & rst!precio & vbTab & rst!precio * xCantidad
rst.Close
cmdQuitar.Enabled = True
frmReciboAtenPP.Enabled = True
cmdAceptar.Enabled = True
catchErr:
If Err.Number = -2147467259 Then
    MsgRapido "El item ya se encuentra en la factura.", Loro, vbExclamation, , "surprised"
    ordenItem = ordenItem - 1
    rst.Close
    frmReciboAtenPP.Enabled = True
    Exit Sub
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
cmd.CommandText = "delete from detalle_recatpp where temp>0"
cmd.Execute
frmPrincipal.desactivararch
End Sub
