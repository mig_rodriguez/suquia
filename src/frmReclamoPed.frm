VERSION 5.00
Begin VB.Form frmReclamoPed 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Emitir Reclamo de Pedido"
   ClientHeight    =   4380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5055
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmReclamoPed.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4380
   ScaleWidth      =   5055
   Begin VB.TextBox txtProveedor 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1920
      TabIndex        =   8
      Top             =   720
      Width           =   2895
   End
   Begin VB.CommandButton cmdbuscar 
      Caption         =   "&Buscar Proveedor"
      Default         =   -1  'True
      Height          =   375
      Left            =   3120
      TabIndex        =   7
      Top             =   120
      Width           =   1695
   End
   Begin VB.TextBox txtNroOrden 
      Height          =   330
      Left            =   1920
      MaxLength       =   5
      TabIndex        =   6
      Top             =   120
      Width           =   855
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2640
      TabIndex        =   2
      Top             =   3840
      Width           =   1335
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1080
      TabIndex        =   1
      Top             =   3840
      Width           =   1335
   End
   Begin VB.TextBox txtDescripcion 
      Height          =   1935
      Left            =   158
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   1680
      Width           =   4695
   End
   Begin VB.Label Label3 
      Caption         =   "Proveedor:"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   840
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "Descripci�n:"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "N�mero de Orden:"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   195
      Width           =   1575
   End
End
Attribute VB_Name = "frmReclamoPed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim cmd As ADODB.Command
Dim rst As ADODB.Recordset
Dim rstprov As ADODB.Recordset
Dim xNro As Long
Dim xid_proveedor As Integer

Private Sub cmdaceptar_Click()
 rst.Source = "select * from reclamo_pedido order by nro_reclamo"
 rst.Open
 rst.MoveLast
 xNro = (1 + rst(0))
 If Trim(txtDescripcion) = "" Then
    MsgRapido "Describa el reclamo.", Loro, vbExclamation, , "surprised"
    rst.Close
    Exit Sub
 End If
 Loro.Play "write"
 rst.AddNew
 rst(0) = xNro
 rst!fecha = Date
 rst!nro_orden = txtNroOrden
 rst!descripcion = txtDescripcion
 rst!proveedor = xid_proveedor
 rst.Update
 rst.Requery
 MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
 txtDescripcion = ""
 txtNroOrden = ""
 txtProveedor = ""
 xid_proveedor = 0
 rst.Close
 txtNroOrden.Enabled = True
 cmdAceptar.Enabled = False
 txtNroOrden.SetFocus
End Sub

Private Sub cmdBuscar_Click()
 If txtNroOrden = "" Then
    MsgRapido "Ingrese una orden de compra.", Loro, vbInformation, , "surprised"
    Exit Sub
   Else
    rst.Source = "select nro_orden from orden_compra where nro_orden=" & txtNroOrden
    rst.Open
 End If
 If rst.RecordCount = 0 Then MsgRapido "La Orden de Compra seleccionada no existe.", Loro, vbCritical, "", "decline": rst.Close: Exit Sub
 rst.Close
 rstprov.Source = "SELECT proveedor.razon_social, proveedor.id_proveedor FROM detalle_oc, proveedor where detalle_oc.nro_orden= " & txtNroOrden.Text & " and detalle_oc.id_proveedor=proveedor.id_proveedor group by proveedor.razon_social"
 rstprov.Open
 txtProveedor = rstprov!razon_social
 xid_proveedor = rstprov!id_proveedor
 rstprov.Close
 cmdAceptar.Enabled = True
 txtNroOrden.Enabled = False
End Sub

Private Sub cmdcancelar_Click()
 Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
txtNroOrden.SetFocus
End Sub

Private Sub Form_Load()
 Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
 Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
 Set rst = New ADODB.Recordset
 rst.ActiveConnection = cnx
 rst.CursorType = adOpenDynamic
 rst.LockType = adLockOptimistic
 Set rstprov = New ADODB.Recordset
 rstprov.ActiveConnection = cnx
 rstprov.LockType = adLockReadOnly
 rstprov.CursorType = adOpenForwardOnly
 Set cmd = New ADODB.Command
 cmd.ActiveConnection = cnx
 xid_proveedor = 0

End Sub

Private Sub Form_Unload(Cancel As Integer)
 frmPrincipal.desactivararch
 Set cmd = Nothing
 Set rst = Nothing
End Sub

Private Sub txtNroOrden_KeyPress(KeyAscii As Integer)
 If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
       KeyAscii = 0
    End If
 End If
End Sub
Sub buscar()
cmdBuscar_Click
End Sub
