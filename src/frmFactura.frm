VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmFactura 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Generar Factura"
   ClientHeight    =   6645
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8685
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFactura.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6645
   ScaleWidth      =   8685
   Begin VB.ComboBox cmbTarjeta 
      Enabled         =   0   'False
      Height          =   345
      Left            =   1080
      Style           =   2  'Dropdown List
      TabIndex        =   22
      Top             =   1920
      Width           =   3135
   End
   Begin VB.ComboBox cmbPlanPago 
      Enabled         =   0   'False
      Height          =   345
      Left            =   5520
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   1920
      Width           =   3135
   End
   Begin VB.OptionButton optEfectivo 
      Caption         =   "Efectivo"
      Height          =   255
      Left            =   1680
      TabIndex        =   20
      Top             =   1400
      Value           =   -1  'True
      Width           =   975
   End
   Begin VB.OptionButton optTarjeta 
      Caption         =   "Tarjeta"
      Height          =   255
      Left            =   2760
      TabIndex        =   19
      Top             =   1400
      Width           =   975
   End
   Begin VB.ComboBox cmbPrepago 
      Enabled         =   0   'False
      Height          =   345
      Left            =   5520
      Style           =   2  'Dropdown List
      TabIndex        =   18
      Top             =   1320
      Width           =   3135
   End
   Begin VB.CheckBox chkPrepago 
      Caption         =   "Prepago"
      Height          =   375
      Left            =   4320
      TabIndex        =   17
      Top             =   1320
      Width           =   1215
   End
   Begin VB.TextBox txtMontoTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   330
      Left            =   7320
      TabIndex        =   14
      Text            =   "0"
      Top             =   5640
      Width           =   825
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   375
      Left            =   2542
      TabIndex        =   13
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4687
      TabIndex        =   12
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CommandButton cmdQuitar 
      Caption         =   "-"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8282
      TabIndex        =   11
      Top             =   3480
      Width           =   375
   End
   Begin VB.CommandButton cmdAgregar 
      Caption         =   "+"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8282
      TabIndex        =   10
      Top             =   2880
      Width           =   375
   End
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   2775
      Left            =   30
      TabIndex        =   9
      Top             =   2640
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   4895
      _Version        =   393216
      Rows            =   1
      Cols            =   8
      FixedRows       =   0
      FixedCols       =   0
      GridColor       =   128
   End
   Begin VB.TextBox txtNro 
      BackColor       =   &H00E0E0E0&
      Enabled         =   0   'False
      Height          =   330
      Left            =   2040
      TabIndex        =   8
      Top             =   120
      Width           =   975
   End
   Begin VB.ComboBox cmbEmpleado 
      Height          =   345
      Left            =   5520
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   720
      Width           =   3135
   End
   Begin VB.ComboBox cmbCliente 
      Height          =   345
      Left            =   1050
      Sorted          =   -1  'True
      TabIndex        =   6
      Text            =   "cmbCliente"
      Top             =   720
      Width           =   3135
   End
   Begin VB.Label lblFormapago 
      AutoSize        =   -1  'True
      Caption         =   "Forma de Pago:"
      Height          =   225
      Left            =   240
      TabIndex        =   25
      Top             =   1400
      Width           =   1320
   End
   Begin VB.Label lblPlan 
      AutoSize        =   -1  'True
      Caption         =   "Plan de Pago:"
      Height          =   225
      Left            =   4320
      TabIndex        =   24
      Top             =   1920
      Width           =   1155
   End
   Begin VB.Label lblTarjeta 
      AutoSize        =   -1  'True
      Caption         =   "Tarjeta:"
      Height          =   225
      Left            =   240
      TabIndex        =   23
      Top             =   1920
      Width           =   615
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Detalle:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   120
      TabIndex        =   16
      Top             =   2400
      Width           =   630
   End
   Begin VB.Label lblTotal 
      Caption         =   "Monto Total: $"
      Height          =   255
      Left            =   5880
      TabIndex        =   15
      Top             =   5685
      Width           =   1215
   End
   Begin VB.Label lblDate 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "[Fecha]"
      Height          =   225
      Left            =   7395
      TabIndex        =   5
      Top             =   120
      Width           =   600
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   6480
      TabIndex        =   4
      Top             =   120
      Width           =   615
   End
   Begin VB.Label lblEmplado 
      AutoSize        =   -1  'True
      Caption         =   "Empleado:"
      Height          =   225
      Left            =   4320
      TabIndex        =   3
      Top             =   720
      Width           =   900
   End
   Begin VB.Label lblCliente 
      AutoSize        =   -1  'True
      Caption         =   "Cliente:"
      Height          =   225
      Left            =   240
      TabIndex        =   2
      Top             =   720
      Width           =   630
   End
   Begin VB.Label lblNroFactura 
      Caption         =   "N�mero de Factura:"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label lblTitulo 
      Caption         =   "Factura Tipo ""C"""
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3615
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "frmFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim rstDesc As ADODB.Recordset
Dim cmd As ADODB.Command
Dim ordenItem As Integer
Dim base As Integer
Dim nuevoUltPago As String
Dim pagandoPP As Boolean
Dim nocomp As Boolean
Private Sub cmbcliente_Change()
If nocomp = False Then completar cmbCliente Else: nocomp = False
End Sub

Private Sub cmbCliente_Click()
Dim descuento As Single
cmdAgregar.Enabled = True
txtMontoTotal = 0
base = 0
pagandoPP = False
grdDetalle.Clear
grdDetalle.Rows = 1
grdDetalle.FixedRows = 0
grdDetalle.Rows = 0
grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unitario ($)" & vbTab & "Descuento ($)" & vbTab & "Precio c/ Desc. ($)" & vbTab & "Total ($)"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
rst.Source = "select p.fecha_ultimo_pago as ultimo,s.nombre as servicio,m.nombre as mascota,d.id_servicio from servicio_prepago p,detalle_sp d,servicio s,mascota_hc m where p.id_prepago=d.id_prepago and m.id_hc=d.id_hc and d.id_servicio=s.id_servicio and p.id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
rst.Open
'No tiene Prepago
If rst.RecordCount = 0 Then
    rst.Close
    chkPrepago.Enabled = False
    cmbPrepago.Clear
    cmbPrepago.AddItem "Ninguno"
    cmbPrepago.ItemData(cmbPrepago.NewIndex) = -1
    cmbPrepago.ListIndex = 0
    If chkPrepago.Value = 1 Then optEfectivo = True: chkPrepago.Value = 0
'El Prepago est� vencido
ElseIf Date - DateValue(Format(rst!ultimo, "dd/mm/yyyy")) > 31 Then
    rst.Close
    chkPrepago.Enabled = False
    cmbPrepago.Clear
    cmbPrepago.AddItem "Ninguno"
    cmbPrepago.ItemData(cmbPrepago.NewIndex) = -1
    cmbPrepago.ListIndex = 0
    If chkPrepago.Value = 1 Then optEfectivo = True: chkPrepago.Value = 0
'Tiene Prepago al dia
Else
    rst.Close
    rst.Source = "select p.fecha_ultimo_pago as ultimo,s.nombre as servicio,m.nombre as mascota,d.id_servicio from servicio_prepago p,detalle_sp d,servicio s,mascota_hc m where p.id_prepago=d.id_prepago and m.id_hc=d.id_hc and d.id_servicio=s.id_servicio and p.id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
    rst.Open
    'cargo los prepagos
    cmbPrepago.Clear
    While Not rst.EOF
        cmbPrepago.AddItem rst!servicio & " => " & rst!mascota
        cmbPrepago.ItemData(cmbPrepago.NewIndex) = rst!id_servicio
        rst.MoveNext
    Wend
    rst.Close
    chkPrepago.Enabled = True
    chkPrepago.Value = 0
    cmbPrepago.ListIndex = 0
End If
    grdDetalle.ColWidth(0) = 4845
    grdDetalle.ColWidth(3) = 0
    grdDetalle.ColWidth(4) = 0
rst.Source = "select n.id_item,n.precio,s.nombre,count(n.id_item) as cant from nofacturado n,servicio s where n.id_item=s.id_servicio and n.tipo='ser' and n.id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex) & " group by n.id_item"
rst.Open
While rst.EOF = False
    grdDetalle.AddItem rst!nombre & vbTab & rst!cant & vbTab & rst!Precio & vbTab & "0" & vbTab & "0" & vbTab & rst!cant * rst!Precio & vbTab & rst!id_item & vbTab & "1"
    txtMontoTotal = Round(Val(txtMontoTotal) + rst!cant * rst!Precio, 2)
    base = base + 1
    rst.MoveNext
Wend
rst.Close
rst.Source = "select n.id_item,n.precio,m.nombre,count(n.id_item) as cant from nofacturado n,medicamento m where n.id_item=m.id_medicamento and n.tipo='med' and n.id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex) & " group by n.id_item"
rst.Open
While rst.EOF = False
    grdDetalle.AddItem rst!nombre & vbTab & rst!cant & vbTab & rst!Precio & vbTab & "0" & vbTab & "0" & vbTab & rst!cant * rst!Precio & vbTab & rst!id_item & vbTab & "2"
    txtMontoTotal = Round(Val(txtMontoTotal) + rst!cant * rst!Precio, 2)
    base = base + 1
    rst.MoveNext
Wend
rst.Close
If grdDetalle.Rows > 1 Then cmdAceptar.Enabled = True Else cmdAceptar.Enabled = False
End Sub

Private Sub cmbcliente_KeyPress(KeyAscii As Integer)
cmdAgregar.Enabled = False
cmdAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbCliente.Text = Left(cmbCliente.Text, cmbCliente.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbCliente
End Sub

Private Sub cmbcliente_LostFocus()
validar cmbCliente
End Sub

Private Sub cmbPrepago_Click()
Dim c
txtMontoTotal = 0
For c = 1 To grdDetalle.Rows - 1
    rstDesc.Source = "select descuento from vademecum where id_prepago=" & cmbPrepago.ItemData(cmbPrepago.ListIndex) & " and id_item=" & grdDetalle.TextMatrix(c, 6) & " and tipo_item=" & grdDetalle.TextMatrix(c, 7)
    rstDesc.Open
    If rstDesc.RecordCount <> 0 Then
        grdDetalle.TextMatrix(c, 3) = Round(rstDesc!descuento * Val(grdDetalle.TextMatrix(c, 2)), 2)
        grdDetalle.TextMatrix(c, 4) = Round(Val(grdDetalle.TextMatrix(c, 2)) - Val(grdDetalle.TextMatrix(c, 3)), 2)
        grdDetalle.TextMatrix(c, 5) = Round(Val(grdDetalle.TextMatrix(c, 4)) * Val(grdDetalle.TextMatrix(c, 1)), 2)
    Else
        grdDetalle.TextMatrix(c, 3) = 0
        grdDetalle.TextMatrix(c, 4) = Round(Val(grdDetalle.TextMatrix(c, 2)) - Val(grdDetalle.TextMatrix(c, 3)), 2)
        grdDetalle.TextMatrix(c, 5) = Round(Val(grdDetalle.TextMatrix(c, 4)) * Val(grdDetalle.TextMatrix(c, 1)), 2)
    End If
    txtMontoTotal = Round(Val(txtMontoTotal) + Val(grdDetalle.TextMatrix(c, 5)), 2)
    rstDesc.Close
Next
End Sub

Private Sub cmbTarjeta_Click()
cmbPlanPago.Clear
    rst.Source = "select nombre,id_plan from plan_pago where borrado='f' and id_tarjeta=" & cmbTarjeta.ItemData(cmbTarjeta.ListIndex)
    rst.Open
    While rst.EOF = False
        cmbPlanPago.AddItem rst!nombre
        cmbPlanPago.ItemData(cmbPlanPago.NewIndex) = rst!id_plan
        rst.MoveNext
    Wend
    rst.Close
If cmbPlanPago.ListCount = 0 Then cmbPlanPago.AddItem "Ninguno": cmbPlanPago.ItemData(cmbPlanPago.NewIndex) = -1
cmbPlanPago.ListIndex = 0
End Sub

Private Sub cmdaceptar_Click()
Dim formaPago
If Mensaje("�Desea emitir la factura?", Loro, Balloon, vbQuestion + vbYesNo) = vbNo Then Exit Sub
If cmbPlanPago.ListCount = 0 Then
    MsgRapido "Debe elegir un plan de pago.", Loro, vbCritical, "", "sad"
    Exit Sub
End If
If pagandoPP = True And chkPrepago = True Then
    MsgRapido "No puede usar el Prepago.", Loro, vbCritical, "", "sad"
    Exit Sub
End If
If optEfectivo = True Then
    formaPago = 1
Else
    formaPago = 2
End If
cmd.CommandText = "insert into factura(nro_factura,fecha,forma_pago,monto_total,id_empleado,id_cliente,id_tarjeta,id_plan,id_prepago) values(" & txtNro & ",'" & DateDB & "'," & formaPago & "," & txtMontoTotal & "," & cmbEmpleado.ItemData(cmbEmpleado.ListIndex) & "," & cmbCliente.ItemData(cmbCliente.ListIndex) & "," & cmbTarjeta.ItemData(cmbTarjeta.ListIndex) & "," & cmbPlanPago.ItemData(cmbPlanPago.ListIndex) & "," & cmbPrepago.ItemData(cmbPrepago.ListIndex) & ")"
cmd.Execute
Dim c
For c = base + 1 To grdDetalle.Rows - 1
    If Val(grdDetalle.TextMatrix(c, 7)) = 2 Then
    cmd.CommandText = "update medicamento set stock_total=stock_total-" & Val(grdDetalle.TextMatrix(c, 1)) & " where id_medicamento=" & Val(grdDetalle.TextMatrix(c, 6))
    cmd.Execute
    End If
Next c
For c = 1 To grdDetalle.Rows - 1
    If Val(grdDetalle.TextMatrix(c, 7)) = 3 Then
    cmd.CommandText = "update articulo set stock_total=stock_total-" & Val(grdDetalle.TextMatrix(c, 1)) & " where id_articulo=" & Val(grdDetalle.TextMatrix(c, 6))
    cmd.Execute
    End If
Next c
For c = 1 To grdDetalle.Rows - 1
    cmd.CommandText = "insert into detalle_factura(nro_factura,tipo_item,id_item,cantidad,precio) values(" & txtNro & "," & Val(grdDetalle.TextMatrix(c, 7)) & "," & Val(grdDetalle.TextMatrix(c, 6)) & "," & Val(grdDetalle.TextMatrix(c, 1)) & "," & Val(grdDetalle.TextMatrix(c, 5)) & ")"
    cmd.Execute
Next c
If pagandoPP = False Then
    cmd.CommandText = "delete from nofacturado where id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
    cmd.Execute
Else
    cmd.CommandText = "update servicio_prepago set fecha_ultimo_pago='" & Format(nuevoUltPago, "yyyy-mm-dd") & "' where id_cliente=" & cmbCliente.ItemData(cmbCliente.ListIndex)
    cmd.Execute
End If

Dim rstDetalle As ADODB.Recordset
Set rstDetalle = New ADODB.Recordset
rstDetalle.LockType = adLockOptimistic
rstDetalle.CursorType = adOpenKeyset
rstDetalle.Fields.Append "nombre", adVarChar, 30
rstDetalle.Fields.Append "cantidad", adVarChar, 10
rstDetalle.Fields.Append "precio", adVarChar, 10
rstDetalle.Fields.Append "descuento", adVarChar, 10
rstDetalle.Fields.Append "PrecConDesc", adVarChar, 10
rstDetalle.Fields.Append "total", adVarChar, 15
rstDetalle.Open
For c = 1 To grdDetalle.Rows - 1
    rstDetalle.AddNew
    rstDetalle!nombre = grdDetalle.TextMatrix(c, 0)
    rstDetalle!Cantidad = grdDetalle.TextMatrix(c, 1)
    rstDetalle!Precio = grdDetalle.TextMatrix(c, 2)
    rstDetalle!descuento = grdDetalle.TextMatrix(c, 3)
    rstDetalle!preccondesc = grdDetalle.TextMatrix(c, 4)
    rstDetalle!total = grdDetalle.TextMatrix(c, 5)
    rstDetalle.Update
Next c
Set rptFactura.DataSource = rstDetalle
If chkPrepago = False Then
    rptFactura.Sections(2).Controls(14).Visible = False
    rptFactura.Sections(2).Controls(15).Visible = False
    rptFactura.Sections(3).Controls(5).Visible = False
    rptFactura.Sections(3).Controls(6).Visible = False
End If
If optEfectivo = True Then
    rptFactura.Sections(2).Controls(17).Caption = "Efectivo"
Else
    rptFactura.Sections(2).Controls(17).Caption = "Tarjeta"
End If
rptFactura.Sections(5).Controls(2).Caption = txtMontoTotal
rptFactura.Sections(2).Controls(10).Caption = cmbCliente.List(cmbCliente.ListIndex)
rptFactura.Sections(2).Controls(11).Caption = cmbEmpleado.List(cmbEmpleado.ListIndex)
rptFactura.Sections(2).Controls(12).Caption = cmbTarjeta.List(cmbTarjeta.ListIndex)
rptFactura.Sections(2).Controls(13).Caption = cmbPlanPago.List(cmbPlanPago.ListIndex)
rptFactura.Show
Unload Me
End Sub

Private Sub cmdagregar_Click()
frmAgregar.mostrar Me
frmAgregar.Show
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub cmdQuitar_Click()
If grdDetalle.Row <= base Then Exit Sub
txtMontoTotal = Round(Val(txtMontoTotal) - Val(grdDetalle.TextMatrix(grdDetalle.Row, 5)), 2)
If grdDetalle.Rows > 2 Then
    grdDetalle.RemoveItem grdDetalle.Row
Else
grdDetalle.Clear
grdDetalle.FixedRows = 0
grdDetalle.Rows = 0
grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unitario ($)" & vbTab & "Descuento ($)" & vbTab & "Precio c/ Desc. ($)" & vbTab & "Total ($)"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
End If
If grdDetalle.Rows < 11 Then
    If chkPrepago.Value = 1 Then
        grdDetalle.ColWidth(0) = 1980
        grdDetalle.ColWidth(3) = 1300
        grdDetalle.ColWidth(4) = 1570
    Else
        grdDetalle.ColWidth(0) = 4845
        grdDetalle.ColWidth(3) = 0
        grdDetalle.ColWidth(4) = 0
    End If
End If
ordenItem = ordenItem - 1
If ordenItem = 0 Then: cmdQuitar.Enabled = False
If ordenItem = 0 Then: cmdAceptar.Enabled = False
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
lblDate.Caption = Date
grdDetalle.ColWidth(0) = 4845
grdDetalle.ColWidth(1) = 870
grdDetalle.ColWidth(2) = 1555
grdDetalle.ColWidth(3) = 0
grdDetalle.ColWidth(4) = 0
grdDetalle.ColWidth(5) = 810
grdDetalle.ColWidth(6) = 0
grdDetalle.ColWidth(7) = 0
grdDetalle.ColAlignment(1) = 7
grdDetalle.ColAlignment(2) = 7
grdDetalle.ColAlignment(3) = 7
grdDetalle.ColAlignment(4) = 7
grdDetalle.ColAlignment(5) = 7
Set rst = New ADODB.Recordset
Set rstDesc = New ADODB.Recordset
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
rst.ActiveConnection = cnx
rstDesc.ActiveConnection = cnx
rst.CursorType = adOpenForwardOnly
rstDesc.CursorType = adOpenDynamic
rst.LockType = adLockReadOnly
rstDesc.LockType = adLockReadOnly
rst.Source = "select nombre,apellido, id_cliente from cliente where borrado='f' order by id_cliente"
rst.Open
While rst.EOF = False
    cmbCliente.AddItem rst!apellido & ", " & rst!nombre
    cmbCliente.ItemData(cmbCliente.NewIndex) = rst!id_cliente
    rst.MoveNext
Wend
rst.Close
rst.Source = "select nombre,apellido,legajo from empleado where borrado='f' order by legajo"
rst.Open
While rst.EOF = False
    cmbEmpleado.AddItem rst!apellido & ", " & rst!nombre
    cmbEmpleado.ItemData(cmbEmpleado.NewIndex) = rst!legajo
    rst.MoveNext
Wend
rst.Close
rst.Source = "select distinct t.id_tarjeta,t.razon_social from tarjeta t,plan_pago p where t.id_tarjeta=p.id_tarjeta and t.borrado='f' and p.borrado='f' and t.id_tarjeta>0 order by t.id_tarjeta"
rst.Open
While rst.EOF = False
    cmbTarjeta.AddItem rst!razon_social
    cmbTarjeta.ItemData(cmbTarjeta.NewIndex) = rst!id_tarjeta
    rst.MoveNext
Wend
rst.Close
optEfectivo_Click
cmbEmpleado.ListIndex = 0
cmbCliente.ListIndex = 0
txtNro = nuevoid("nro_factura", "factura")
ordenItem = 0
base = 0
pagandoPP = False
End Sub
Sub agregar(xTipo_Item As Integer, xId_Item As Integer, xCantidad As Integer)
Dim c
Dim descuento As Single
Select Case xTipo_Item
    Case 1
        rst.Source = "select nombre,precio,id_rubro from servicio where id_servicio=" & xId_Item
    Case 2
        rst.Source = "select nombre,precio,0 as id_rubro from medicamento where id_medicamento=" & xId_Item
    Case 3
        rst.Source = "select nombre,precio,0 as id_rubro from articulo where id_articulo=" & xId_Item
End Select
rst.Open
For c = 1 To grdDetalle.Rows - 1
    If Val(grdDetalle.TextMatrix(c, 6)) = xId_Item And Val(grdDetalle.TextMatrix(c, 7)) = xTipo_Item Then
        MsgRapido "El item ya se encuentra en la factura.", Loro, vbExclamation, , "surprised"
        rst.Close
        frmFactura.Enabled = True
        Exit Sub
    End If
Next c
If chkPrepago.Value = 1 Then
    rstDesc.Source = "select descuento from vademecum where id_prepago=" & cmbPrepago.ItemData(cmbPrepago.ListIndex) & " and id_item=" & xId_Item & " and tipo_item=" & xTipo_Item
    rstDesc.Open
    If rstDesc.RecordCount = 0 Then
        descuento = 0
    Else
        descuento = rstDesc!descuento
    End If
    grdDetalle.AddItem rst!nombre & vbTab & xCantidad & vbTab & rst!Precio & vbTab & descuento * rst!Precio & vbTab & rst!Precio - (rst!Precio * descuento) & vbTab & (rst!Precio - (rst!Precio * descuento)) * xCantidad & vbTab & xId_Item & vbTab & xTipo_Item
    txtMontoTotal = Round(Val(txtMontoTotal) + (rst!Precio - (rst!Precio * descuento)) * xCantidad, 2)
    rstDesc.Close
Else
    grdDetalle.AddItem rst!nombre & vbTab & xCantidad & vbTab & rst!Precio & vbTab & "0" & vbTab & "0" & vbTab & rst!Precio * xCantidad & vbTab & xId_Item & vbTab & xTipo_Item
    txtMontoTotal = Round(Val(txtMontoTotal) + rst!Precio * xCantidad, 2)
End If
If rst!id_rubro = 7 Then pagandoPP = True
rst.Close
ordenItem = ordenItem + 1
If grdDetalle.Rows > 10 Then
    If chkPrepago.Value = 1 Then
        grdDetalle.ColWidth(0) = 1740
    Else
        grdDetalle.ColWidth(0) = 4620
    End If
End If
cmdQuitar.Enabled = True
frmFactura.Enabled = True
cmdAceptar.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
If pagandoPP = True Then frmRegPagoPP.Enabled = True
End Sub


Private Sub optEfectivo_Click()
Dim c
cmbTarjeta.Enabled = False
cmbPlanPago.Enabled = False
For c = 0 To cmbTarjeta.ListCount - 1
    If cmbTarjeta.ItemData(c) = -1 Then cmbTarjeta.RemoveItem c
Next c
cmbTarjeta.AddItem "Ninguna"
cmbTarjeta.ItemData(cmbTarjeta.NewIndex) = -1
cmbTarjeta.ListIndex = cmbTarjeta.NewIndex
End Sub
Private Sub chkPrepago_Click()
Dim c
If chkPrepago.Value = 0 Then
    txtMontoTotal = 0
    cmbPrepago.Enabled = False
    If grdDetalle.Rows > 10 Then
        grdDetalle.ColWidth(0) = 4620
    Else
        grdDetalle.ColWidth(0) = 4845
    End If
    grdDetalle.ColWidth(3) = 0
    grdDetalle.ColWidth(4) = 0
    For c = 1 To grdDetalle.Rows - 1
        grdDetalle.TextMatrix(c, 3) = 0
        grdDetalle.TextMatrix(c, 4) = 0
        grdDetalle.TextMatrix(c, 5) = Round(Val(grdDetalle.TextMatrix(c, 2)) * Val(grdDetalle.TextMatrix(c, 1)), 2)
        txtMontoTotal = Round(Val(txtMontoTotal) + Val(grdDetalle.TextMatrix(c, 5)), 2)
    Next c
Else
    cmbPrepago.Enabled = True
    If grdDetalle.Rows > 10 Then
        grdDetalle.ColWidth(0) = 1740
    Else
        grdDetalle.ColWidth(0) = 1980
    End If
    grdDetalle.ColWidth(3) = 1300
    grdDetalle.ColWidth(4) = 1570
    cmbPrepago_Click
End If
End Sub

Private Sub optTarjeta_Click()
Dim c
cmbTarjeta.Enabled = True
cmbPlanPago.Enabled = True
For c = 0 To cmbTarjeta.ListCount - 1
    If cmbTarjeta.ItemData(c) = -1 Then cmbTarjeta.RemoveItem c
Next c
cmbTarjeta.ListIndex = 0
End Sub
Sub PagarPrepago(xid_cliente As Long, Xmeses As Integer)
Dim c
For c = 0 To cmbCliente.ListCount - 1
    If cmbCliente.ItemData(c) = xid_cliente Then cmbCliente.ListIndex = c: Exit For
Next c
chkPrepago.Enabled = False
cmdAgregar.Enabled = False
cmdQuitar.Enabled = False
grdDetalle.Clear
grdDetalle.Rows = 1
grdDetalle.FixedRows = 0
grdDetalle.Rows = 0
grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unitario ($)" & vbTab & "Descuento ($)" & vbTab & "Precio c/ Desc. ($)" & vbTab & "Total ($)"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
rst.Source = "select s.nombre,d.id_servicio,d.monto,count(d.id_servicio) as cant,p.fecha_ultimo_pago from detalle_sp d,servicio_prepago p,servicio s where s.id_servicio=d.id_servicio and d.id_prepago=p.id_prepago and p.id_cliente=" & xid_cliente & " group by d.id_servicio"
rst.Open
nuevoUltPago = DateValue(rst!fecha_ultimo_pago) + 30 * Xmeses
txtMontoTotal = 0
While Not rst.EOF
    grdDetalle.AddItem rst!nombre & vbTab & rst!cant * Xmeses & vbTab & rst!monto & vbTab & "0" & vbTab & "0" & vbTab & rst!monto * rst!cant * Xmeses & vbTab & rst!id_servicio & vbTab & "1"
    txtMontoTotal = Round(Val(txtMontoTotal) + rst!monto * rst!cant * Xmeses, 2)
    rst.MoveNext
Wend
rst.Close
cmbCliente.Enabled = False
pagandoPP = True
cmdAceptar.Enabled = True
End Sub
