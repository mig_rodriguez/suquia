VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmactmascota 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Mascotas"
   ClientHeight    =   6525
   ClientLeft      =   2490
   ClientTop       =   570
   ClientWidth     =   8895
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6525
   ScaleWidth      =   8895
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   1451
      TabIndex        =   12
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   3604
      TabIndex        =   13
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   5749
      TabIndex        =   14
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4669
      TabIndex        =   16
      Top             =   6000
      Width           =   1695
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2531
      TabIndex        =   15
      Top             =   6000
      Width           =   1695
   End
   Begin VB.Frame frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   4695
      Index           =   1
      Left            =   240
      TabIndex        =   21
      Top             =   480
      Width           =   8415
      Begin VB.ComboBox cmbCliente 
         BackColor       =   &H80000018&
         Height          =   345
         Left            =   3000
         TabIndex        =   0
         Text            =   "Combo1"
         Top             =   240
         Width           =   3495
      End
      Begin VB.CommandButton cmdnuecli 
         Caption         =   "Nuevo C&liente"
         Height          =   495
         Left            =   6720
         TabIndex        =   45
         Top             =   120
         Width           =   1455
      End
      Begin VB.CommandButton cmdpricli 
         Height          =   375
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdatrcli 
         Height          =   375
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdadecli 
         Height          =   375
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdultcli 
         Height          =   375
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   240
         Width           =   375
      End
      Begin VB.Frame Fmas 
         Height          =   4095
         Left            =   0
         TabIndex        =   25
         Top             =   600
         Width           =   8300
         Begin VB.ComboBox cmbraza 
            Enabled         =   0   'False
            Height          =   345
            Index           =   1
            Left            =   4800
            TabIndex        =   3
            Text            =   "cmbraza"
            Top             =   960
            Visible         =   0   'False
            Width           =   2895
         End
         Begin VB.ComboBox cmbEspecie 
            Enabled         =   0   'False
            Height          =   345
            Left            =   1560
            Style           =   2  'Dropdown List
            TabIndex        =   2
            Top             =   960
            Width           =   1935
         End
         Begin VB.TextBox txtPd 
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            Height          =   330
            Left            =   6960
            TabIndex        =   56
            Text            =   "      /    / "
            Top             =   2160
            Width           =   975
         End
         Begin VB.TextBox txtPc 
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            Height          =   330
            Left            =   6960
            TabIndex        =   10
            Text            =   "      /    / "
            Top             =   2760
            Width           =   975
         End
         Begin VB.TextBox txtFN 
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            Height          =   330
            Left            =   4440
            TabIndex        =   6
            Text            =   "      /    / "
            Top             =   2160
            Width           =   975
         End
         Begin VB.TextBox txtFA 
            BackColor       =   &H8000000F&
            Enabled         =   0   'False
            Height          =   330
            Left            =   1560
            TabIndex        =   19
            TabStop         =   0   'False
            Text            =   "     /    / "
            Top             =   2160
            Width           =   975
         End
         Begin VB.TextBox Text1 
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "yyyy-MM-dd"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   0
            EndProperty
            Enabled         =   0   'False
            Height          =   570
            Index           =   5
            Left            =   1920
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   11
            Top             =   3360
            Width           =   6255
         End
         Begin VB.CheckBox chkpc 
            Caption         =   "Para Cruza"
            Enabled         =   0   'False
            Height          =   255
            Left            =   4080
            TabIndex        =   9
            Top             =   2760
            Width           =   1215
         End
         Begin MSComCtl2.DTPicker FecCru 
            Height          =   330
            Left            =   6960
            TabIndex        =   20
            Top             =   2760
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   24379393
            CurrentDate     =   37819
            MaxDate         =   73415
            MinDate         =   32874
         End
         Begin MSComCtl2.DTPicker FecDec 
            Height          =   330
            Left            =   6960
            TabIndex        =   50
            Top             =   2160
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   24379393
            CurrentDate     =   37819
            MaxDate         =   73415
            MinDate         =   32874
         End
         Begin MSComCtl2.DTPicker FecNac 
            Height          =   330
            Left            =   4440
            TabIndex        =   18
            Top             =   2160
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   24379393
            CurrentDate     =   37819
            MaxDate         =   73415
            MinDate         =   29221
         End
         Begin MSComCtl2.DTPicker FecAlta 
            Height          =   330
            Left            =   1560
            TabIndex        =   49
            Top             =   2160
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            Enabled         =   0   'False
            Format          =   24379393
            CurrentDate     =   37819
         End
         Begin VB.CommandButton cmdNueRaz 
            Caption         =   "+"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   7800
            TabIndex        =   48
            Top             =   960
            Width           =   375
         End
         Begin VB.TextBox Text1 
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "yyyy-MM-dd"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   0
            EndProperty
            Enabled         =   0   'False
            Height          =   330
            Index           =   1
            Left            =   3000
            MaxLength       =   20
            TabIndex        =   1
            Top             =   360
            Width           =   3135
         End
         Begin VB.ComboBox cmbraza 
            Enabled         =   0   'False
            Height          =   345
            Index           =   0
            Left            =   4800
            Style           =   2  'Dropdown List
            TabIndex        =   17
            Top             =   960
            Width           =   2895
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Hembra"
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   2520
            TabIndex        =   8
            Top             =   2760
            Width           =   1215
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Macho"
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   1440
            TabIndex        =   7
            Top             =   2760
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.CommandButton cmdPrimero 
            Height          =   375
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdAtras 
            Height          =   375
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdAdelante 
            Height          =   375
            Left            =   1200
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   360
            Width           =   375
         End
         Begin VB.CommandButton cmdUltimo 
            Height          =   375
            Left            =   1680
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   360
            Width           =   375
         End
         Begin VB.TextBox Text1 
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "yyyy-MM-dd"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   0
            EndProperty
            Enabled         =   0   'False
            Height          =   330
            Index           =   4
            Left            =   4800
            MaxLength       =   15
            TabIndex        =   5
            Top             =   1560
            Width           =   2895
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "yyyy-MM-dd"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   0
            EndProperty
            Enabled         =   0   'False
            Height          =   330
            Index           =   3
            Left            =   1560
            MaxLength       =   10
            TabIndex        =   4
            Top             =   1560
            Width           =   1935
         End
         Begin VB.TextBox Text1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "yyyy-MM-dd"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   0
            EndProperty
            Enabled         =   0   'False
            Height          =   330
            Index           =   2
            Left            =   7080
            MaxLength       =   10
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Especie:"
            Height          =   255
            Index           =   12
            Left            =   240
            TabIndex        =   57
            Top             =   960
            Width           =   1095
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Se�as Particulares:"
            Height          =   225
            Index           =   11
            Left            =   240
            TabIndex        =   53
            Top             =   3360
            Width           =   1620
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha de Cruza:"
            Height          =   255
            Index           =   10
            Left            =   5520
            TabIndex        =   52
            Top             =   2760
            Width           =   1335
         End
         Begin VB.Label Label1 
            Caption         =   "Mascota:"
            Height          =   255
            Index           =   0
            Left            =   2160
            TabIndex        =   42
            Top             =   360
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "N� HC"
            Height          =   255
            Index           =   9
            Left            =   6360
            TabIndex        =   41
            Top             =   360
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "N� de Tatuaje"
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   40
            Top             =   1560
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Pelaje:"
            Height          =   255
            Index           =   6
            Left            =   3960
            TabIndex        =   39
            Top             =   1560
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Raza:"
            Height          =   255
            Index           =   4
            Left            =   3960
            TabIndex        =   38
            Top             =   960
            Width           =   1095
         End
         Begin VB.Label Label1 
            Caption         =   "Deceso:"
            Height          =   255
            Index           =   1
            Left            =   5880
            TabIndex        =   37
            Top             =   2160
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Sexo:"
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   36
            Top             =   2760
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha de Nac.:"
            Height          =   255
            Index           =   5
            Left            =   3000
            TabIndex        =   35
            Top             =   2160
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha de alta:"
            Height          =   255
            Index           =   3
            Left            =   240
            TabIndex        =   34
            Top             =   2160
            Width           =   1335
         End
      End
      Begin VB.Label Label1 
         Caption         =   "Cliente:"
         Height          =   255
         Index           =   8
         Left            =   2160
         TabIndex        =   24
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   4575
      Index           =   2
      Left            =   240
      TabIndex        =   22
      Top             =   600
      Visible         =   0   'False
      Width           =   7935
      Begin MSFlexGridLib.MSFlexGrid grdClientes 
         Height          =   4455
         Left            =   840
         TabIndex        =   23
         Top             =   0
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   7858
         _Version        =   393216
         Rows            =   1
         Cols            =   4
         FixedRows       =   0
         FixedCols       =   0
         Redraw          =   -1  'True
         AllowBigSelection=   -1  'True
         FocusRect       =   0
         SelectionMode   =   1
      End
   End
   Begin VB.Frame frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   4575
      Index           =   3
      Left            =   240
      TabIndex        =   46
      Top             =   480
      Visible         =   0   'False
      Width           =   8175
      Begin VB.TextBox color 
         BackColor       =   &H00FFFFC0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   250
         Left            =   4560
         TabIndex        =   54
         Top             =   3960
         Width           =   300
      End
      Begin VB.CheckBox chkTodos 
         Caption         =   "Mostrar todas las mascotas "
         Height          =   255
         Left            =   960
         TabIndex        =   51
         Top             =   3960
         Width           =   3375
      End
      Begin MSFlexGridLib.MSFlexGrid GrdMas 
         Height          =   3615
         Left            =   960
         TabIndex        =   47
         Top             =   240
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   6376
         _Version        =   393216
         Rows            =   1
         Cols            =   5
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Mascota Internada"
         Height          =   225
         Left            =   4920
         TabIndex        =   55
         Top             =   3960
         Width           =   1515
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5175
      Left            =   120
      TabIndex        =   44
      Top             =   120
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   9128
      TabWidthStyle   =   2
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   3
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Clientes"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Mascotas"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmactmascota"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mintCurFrame As Integer
Dim xid_hc, xid_cliente, c As Integer
Dim busqueda As String, Inter, pCru, ultRaza As String
Dim rst As ADODB.Recordset
Dim rstmas As ADODB.Recordset
Dim rsr As ADODB.Recordset
Dim rse As ADODB.Recordset
Dim rstMxC As ADODB.Recordset
Dim bx As Boolean
Dim band As Boolean
Dim bMovmas As Boolean
Dim bunClick As Boolean
Dim nocomp As Boolean
Dim bRaza As Boolean
Dim Fn, Fd, Fc As Date

Private Sub cmdnuecli_Click()
frmactmascota.Enabled = False
frmactcliente.nuevo
End Sub

Private Sub cmdNueRaz_Click()
frmactmascota.Enabled = False
'masRaza = True
frmActRaza.nuevo
End Sub

Private Sub Form_Activate()
If mintCurFrame = 2 Or mintCurFrame = 3 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
    If cmdsalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
frmactmascota.Height = 6915
mintCurFrame = 1
bx = True
band = False
bMovmas = False
bunClick = False
bRaza = False
ultRaza = "xxx"
Set rst = New ADODB.Recordset
Set rstmas = New ADODB.Recordset
Set rsr = New ADODB.Recordset
Set rse = New ADODB.Recordset
Set rstMxC = New ADODB.Recordset
Set rst = abrirrs("select * from cliente where id_cliente<>0 and borrado = 'f' order by apellido", True)
Set rstmas = abrirrs("select * from mascota_hc where borrado = 'f' ", True)
Set rstMxC = abrirrs("select m.id_cliente, m.id_hc, m.nombre, m.para_cruza, c.nombre as nombrec, c.apellido, m.internado , r.nombre_raza from mascota_hc as m, cliente as c, raza as r where m.id_cliente=c.id_cliente and m.id_raza=r.id_raza and c.borrado = 'f' order by m.id_hc", True)
Set rse = abrirrs("select id_especie, nombre from especie where borrado = 'f' order by nombre", True)
Set rsr = abrirrs("select r.id_raza, r.nombre_raza, e.id_especie, e.nombre from raza r, especie e where r.especie=e.id_especie order by e.nombre", True)
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
cmdpricli.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdatrcli.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdadecli.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdultcli.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
comboEspecie
comboraza
comboCliente
busqueda = ""
loadGridMas
loadGrid
Refrescar
End Sub

Private Sub Refrescar()
Dim a, c
'Text1(0) = rst!apellido + ", " + rst!nombre
xid_cliente = cmbCliente.ItemData(cmbCliente.ListIndex)
If cmdsalir.Caption <> "&Cancelar" Then
 If bx = True Then
  rstmas.Filter = ""
  rstmas.Filter = "id_cliente = '" & cmbCliente.ItemData(cmbCliente.ListIndex) & "'"
 End If
If rstmas.RecordCount = 0 Then
 vacio
 Else
 If rstmas.RecordCount <> 1 And bMovmas = False Then
 cmdPrimero.Enabled = True
 cmdAtras.Enabled = True
 cmdAdelante.Enabled = True
 cmdUltimo.Enabled = True
 Else
 cmdPrimero.Enabled = False
 cmdAtras.Enabled = False
 cmdAdelante.Enabled = False
 cmdUltimo.Enabled = False
 End If
 cmdModificar.Enabled = True
 cmdBorrar.Enabled = True
 Text1(1) = rstmas!nombre
 Text1(2) = rstmas("id_hc")
 Text1(5) = rstmas!senias
 If rstmas("nro_tatuaje") <> "" Then
  Text1(3) = rstmas("nro_tatuaje")
  Else
  Text1(3) = ""
 End If
 Text1(4) = rstmas("pelo")
 If rstmas("sexo") = "m" Then
  Option1(0) = True
  Else
  Option1(1) = True
 End If
 If rstmas!para_cruza = "f" Then
  chkpc.Value = 0
  'FecCru.Visible = False
  txtPc.Visible = True
  Else
  chkpc.Value = 1
  'FecCru.Visible = True
  txtPc.Visible = False
  'FecCru.Enabled = True
  FecCru.Value = rstmas("fecha_cruza")
 End If
 FecAlta.Value = rstmas("fecha_alta")
 txtFA.Visible = False
 FecNac.Value = rstmas("fecha_nac")
 txtFN.Visible = False
 If rstmas("fecha_dec") <> "" Then
  'FecDec.Visible = True
  txtPd.Visible = False
  FecDec.Value = rstmas("fecha_dec")
  Else
  txtPd.Visible = True
  End If
 cmbRaza(0).ListIndex = 0
 For c = 0 To cmbRaza(0).ListCount - 1
 cmbRaza(0).ListIndex = c
 If cmbRaza(0).ItemData(c) = rstmas("id_raza") Then
 rsr.MoveFirst
 rsr.Find "id_raza =" & rstmas("id_raza")
  Exit For
 End If
 Next c
 cmbEspecie.ListIndex = 0
 For c = 0 To cmbEspecie.ListCount - 1
 cmbEspecie.ListIndex = c
 If cmbEspecie.ItemData(c) = rsr!id_especie Then
  Exit For
 End If
 Next c
End If
End If
End Sub
Private Sub vacio()
Dim c
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
For c = 1 To 5
Text1(c) = ""
Next c
txtFA.Visible = True
txtFN.Visible = True
txtPd.Visible = True
cmbRaza(0).ListIndex = -1
cmbRaza(1).Visible = False
cmbEspecie.ListIndex = -1
chkpc.Value = 1
Option1(0).Value = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdsalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        rstmas.CancelUpdate
        restablecer
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
Set rstmas = Nothing
Set rst = Nothing
Set rsr = Nothing
Set rse = Nothing
Set rstMxC = Nothing
End Sub

 ' MOVER CLIENTE
 
Private Sub cmdAdecli_Click()
cmdpricli.Enabled = True
cmdultcli.Enabled = True
If cmbCliente.ListIndex < cmbCliente.ListCount - 1 Then
cmbCliente.ListIndex = cmbCliente.ListIndex + 1
bx = True
Else
cmbCliente.ListIndex = 0
End If
Refrescar
End Sub
Private Sub cmdAtrcli_Click()
cmdpricli.Enabled = True
cmdultcli.Enabled = True
If cmbCliente.ListIndex <> 0 Then
cmbCliente.ListIndex = cmbCliente.ListIndex - 1
bx = True
Else
cmbCliente.ListIndex = cmbCliente.ListCount - 1
End If
Refrescar
End Sub
Private Sub cmdPricli_Click()
cmbCliente.ListIndex = 0
bx = True
cmdultcli.Enabled = True
cmdpricli.Enabled = False
Refrescar
End Sub

Private Sub cmdultcli_Click()
cmbCliente.ListIndex = cmbCliente.ListCount - 1
cmdultcli.Enabled = False
cmdpricli.Enabled = True
bx = True
Refrescar
End Sub

' MOVER MASCOTA

Private Sub cmdAdelante_Click()
If rstmas.EOF <> True Then: rstmas.MoveNext
If rstmas.EOF <> True Then
bx = False
Else
rstmas.MoveFirst
End If
Refrescar
End Sub

Private Sub cmdAtras_Click()
If rstmas.BOF <> True Then: rstmas.MovePrevious
If rstmas.BOF <> True Then
bx = False
Else
rstmas.MoveLast
End If
Refrescar
End Sub

Private Sub cmdPrimero_Click()
rstmas.MoveFirst
bx = False
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
bMovmas = True
Refrescar
bMovmas = False
End Sub

Private Sub cmdUltimo_Click()
rstmas.MoveLast
bx = False
cmdPrimero.Enabled = True
cmdUltimo.Enabled = False
bMovmas = True
Refrescar
bMovmas = False
End Sub

' COMANDOS ABM

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub

Private Sub grdClientes_Click()
bunClick = True
End Sub

Private Sub grdClientes_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    For c = 0 To cmbCliente.ListCount - 1
        cmbCliente.ListIndex = c
        If cmbCliente.ItemData(cmbCliente.ListIndex) = grdClientes.TextMatrix(grdClientes.Row, 3) Then Exit For
    Next
    cmdpricli.Enabled = True
    cmdultcli.Enabled = True
    Refrescar
    mintCurFrame = 1
    band = False
Else
If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If TabStrip1.SelectedItem.Index = 3 Then
    Frame1(2).Visible = False
    End If
    If Frame1(2).Visible = True Or Frame1(3).Visible Then
        bunClick = False
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        frmPrincipal.activararch
        bx = True
        cmdpricli.Enabled = True
        cmdultcli.Enabled = True
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
    If mintCurFrame = 3 Then
    cargarGrdMas
    chkTodos.Value = 0
    End If
End If
End Sub

'   NUEVO

Sub nuevo()
cmdNuevo.Enabled = False
cmdsalir.Caption = "&Cancelar"
cmdnuecli.Enabled = False
cmdGuardar.Enabled = True
cmdNueRaz.Enabled = True
vacio
TabStrip1.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
txtPc.Visible = True
txtPd.Visible = True
Dim c
For c = 1 To 5
Text1(c).Enabled = True
Next c
Text1(2).Enabled = False
FecNac.Enabled = True
FecNac.Value = Date
Option1(0).Enabled = True
Option1(1).Enabled = True
Option1(0).Value = True
chkpc.Enabled = True
chkpc.Value = 0
cmbRaza(1).Enabled = True
cmbRaza(1).Visible = True
cmbEspecie.Enabled = True
cmbEspecie.ListIndex = 0
rstmas.Filter = ""
rstmas.MoveLast
txtFA.Visible = False
txtFN.Visible = False
xid_hc = nuevoid("id_hc", "mascota_hc")
FecAlta = VBA.Date
Text1(2) = xid_hc
bRaza = True
Inter = "f"
Text1(1).SetFocus
Loro.Play "write"
rstmas.AddNew
End Sub

'   MODIFICAR

Sub modificar()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdsalir.Caption = "&Cancelar"
cmdnuecli.Enabled = False
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
cmdNueRaz.Enabled = True
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 1 To 5
Text1(c).Enabled = True
Next c
Text1(2).Enabled = False
If rstmas("fecha_dec") <> "" Then
  txtPd.Visible = False
  FecDec.Enabled = True
  Else
  txtPd.Visible = True
End If
FecNac.Enabled = True
Option1(0).Enabled = True
Option1(1).Enabled = True
chkpc.Enabled = True
cmbRaza(1).Enabled = True
cmbRaza(1).Visible = True
cmbEspecie.Enabled = True
xid_hc = rstmas!id_hc
Inter = rstmas!internado
bRaza = True
Me.cmbEspecie_Click
Text1(1).SetFocus
Loro.Play "write"
End Sub
Sub cerrar()
Unload Me
End Sub

'    GUARDAR

Sub guardar()
For c = 1 To 4
    If c = 3 Then: GoTo sig
    If Trim(Text1(c)) = "" Then
        MsgRapido "Faltan Datos Necesarios, Por Favor Compl�telos", Loro, , , "surprised"
        Exit Sub
    End If
sig:
Next c
If Text1(5) = "" Then: Text1(5) = " "
If FecNac.Value > FecAlta.Value Then
    MsgRapido "La Fecha de Nacimiento es Incorrecta, Por Favor Corr�jala", Loro, , , "surprised"
    Exit Sub
End If
If txtPd.Visible = False Then
If FecDec.Value < FecNac.Value Or FecDec.Value < FecAlta.Value Then
    MsgRapido "La Fecha de Deceso es Erronea, Por Favor Corr�jala", Loro, , , "surprised"
    Exit Sub
End If
End If
rstmas!id_hc = Text1(2).Text
rstmas!nombre = Text1(1).Text
rstmas!pelo = Text1(4).Text
rstmas!senias = Text1(5).Text
rstmas!id_cliente = cmbCliente.ItemData(cmbCliente.ListIndex)
If Text1(3) <> "" Then
rstmas!nro_tatuaje = Text1(3).Text
Else
rstmas!nro_tatuaje = Null
End If
If Option1(0).Value = True Then
 rstmas!sexo = "M"
 Else
 rstmas!sexo = "H"
End If
If chkpc.Value = 0 Then
 rstmas!para_cruza = "F"
 rstmas!fecha_cruza = Null
Else
 rstmas!para_cruza = "V"
 rstmas!fecha_cruza = FecCru.Value
 If FecCru.Value < FecNac.Value Then
    MsgRapido "La Fecha de Cruza es Erronea, Por Favor Corr�jala", Loro, , , "surprised"
    Exit Sub
 End If
End If
rstmas!id_raza = cmbRaza(1).ItemData(cmbRaza(1).ListIndex)
rstmas!fecha_alta = FecAlta.Value
rstmas!fecha_nac = FecNac.Value
rstmas!internado = Inter
If txtPd.Visible = False Then: rstmas!fecha_dec = FecDec.Value
rstmas!borrado = "f"
rstmas.Update
rstmas.Requery
rstMxC.Requery
'rstmas.MoveFirst
bx = True
restablecer
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar la Mascota Seleccionada", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rstmas!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rstmas.Update
rstmas.Requery
cmdAdelante_Click
loadGrid
End Sub

Private Sub chkpc_Click()
If chkpc.Value = 1 And cmdsalir.Caption = "&Cancelar" Then
FecCru.Enabled = True
txtPc.Visible = False
FecCru.Value = VBA.Date
Else
FecCru.Enabled = False
txtPc.Visible = True
End If
End Sub

' SOLO N�MEROS

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 3 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
    End If
End If
End Sub

Private Sub restablecer()
Dim c
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdsalir.Caption = "&Salir"
cmdnuecli.Enabled = True
cmdGuardar.Enabled = False
bRaza = False
cmdPrimero.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdUltimo.Enabled = True
cmdpricli.Enabled = True
cmdatrcli.Enabled = True
cmdadecli.Enabled = True
cmdultcli.Enabled = True
cmdNueRaz.Enabled = False
TabStrip1.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
For c = 1 To 5
Text1(c).Enabled = False
Next c
FecNac.Enabled = False
FecDec.Enabled = False
FecCru.Enabled = False
Option1(0).Enabled = False
Option1(1).Enabled = False
chkpc.Enabled = False
cmbRaza(1).Enabled = False
cmbRaza(1).Visible = False
cmbEspecie.Enabled = False
Refrescar
End Sub

Private Sub cmbCliente_Click()
bx = True
Refrescar
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
End Sub

Private Sub cmbcliente_Change()
If nocomp = False Then completar cmbCliente Else: nocomp = False
End Sub

Private Sub cmbcliente_KeyPress(KeyAscii As Integer)
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
If KeyAscii = vbKeyBack Then cmbCliente.Text = Left(cmbCliente.Text, cmbCliente.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbCliente
End Sub

Private Sub cmbcliente_LostFocus()
validar cmbCliente
End Sub

Sub preview()
frmPrincipal.desactivararch
If chkTodos.Value = 0 Then
rstMxC.Filter = "id_cliente = " & xid_cliente
End If
Set rptMascotas.DataSource = rstMxC
rptMascotas.Show
rstMxC.Filter = 0
End Sub
Sub imprimir()
frmPrincipal.desactivararch
If chkTodos.Value = 0 Then
rstMxC.Filter = "id_cliente = " & xid_cliente
End If
Set rptMascotas.DataSource = rsr
rptMascotas.PrintReport True
Unload rptMascotas
rstMxC.Filter = 0
End Sub

Sub buscar()
busqueda = Input_Box("Buscar Mascota." & Chr(13) & "Ingrese el Nombre de la mascota:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    rstmas.Filter = ""
    If Text1(1) = busqueda Then: rstmas.MoveNext
    rstmas.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rstmas.EOF <> True Then
    rst.Find "id_cliente = '" & rstmas!id_cliente & "'"
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, , , "explain"
    rstmas.Filter = "id_cliente = '" & rst!id_cliente & "'"
End If
End Sub

Private Sub Text1_Validate(Index As Integer, Cancel As Boolean)
If Index = 3 Then
    If Text1(Index) <> "" Then
    If IsNumeric(Text1(Index)) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        Text1(Index) = ""
    End If
    End If
End If
End Sub

'       CARGAR GRILLAS

Sub loadGrid()
rst.Requery
Dim regActual As Long
regActual = rst.AbsolutePosition
rst.MoveFirst
grdClientes.ColWidth(0) = 2000
grdClientes.ColWidth(1) = 2200
grdClientes.ColWidth(2) = 1600
grdClientes.ColWidth(3) = 0
grdClientes.Rows = 0
grdClientes.AddItem "Apellidos" & vbTab & "Nombres" & vbTab & "Documento"
grdClientes.Rows = 2
grdClientes.FixedRows = 1
grdClientes.Rows = 1
While rst.EOF = False
grdClientes.AddItem rst!apellido & vbTab & rst!nombre & vbTab & rst!tipo_documento & "  " & rst!nro_documento & vbTab & rst!id_cliente
rst.MoveNext
Wend
rst.AbsolutePosition = regActual
End Sub
Private Sub loadGridMas()
GrdMas.ColWidth(0) = 400
GrdMas.ColWidth(1) = 1800
GrdMas.ColWidth(2) = 1850
GrdMas.ColWidth(3) = 1100
GrdMas.ColWidth(4) = 2100
GrdMas.Rows = 0
GrdMas.AddItem " HC" & vbTab & " Nombre" & vbTab & " Raza " & vbTab & " Para Cruza" & vbTab & " Due�o "
GrdMas.Rows = 2
GrdMas.FixedRows = 1
GrdMas.Rows = 1
End Sub

Public Sub cargarGrdMas()
GrdMas.Rows = 1
rstMxC.MoveFirst
While rstMxC.EOF = False
If rstMxC(0) = xid_cliente Then
  If rstMxC(3) = "f" Then
    pCru = " No"
  Else
    pCru = " Si"
  End If
  GrdMas.AddItem rstMxC(1) & vbTab & rstMxC(2) & vbTab & rstMxC(7) & vbTab & pCru & vbTab & rstMxC(5) + ", " + rstMxC(4)
  If rstMxC(6) = "v" Then
        GrdMas.Row = GrdMas.Rows - 1
        For c = 0 To 4
        GrdMas.Col = c
        GrdMas.CellBackColor = &HFFFFC0
        Next
  End If
End If
rstMxC.MoveNext
Wend
rstMxC.MoveFirst
End Sub

Public Sub volver()
rst.Requery
rstmas.Requery
End Sub

Sub cmbEspecie_Click()
If cmdsalir.Caption = "&Salir" Then: Exit Sub
If ultRaza = cmbEspecie.List(cmbEspecie.ListIndex) Then: Exit Sub
rse.Source = "select id_raza, nombre_raza from raza where especie = " & cmbEspecie.ItemData(cmbEspecie.ListIndex) & " and borrado = 'f' order by nombre_raza"
rse.Open
cmbRaza(1).Clear
If rse.RecordCount = 0 Then
    rse.Close
    ultRaza = cmbEspecie.List(cmbEspecie.ListIndex)
    Exit Sub
End If
rse.MoveFirst
Do While Not rse.EOF
   cmbRaza(1).AddItem rse!nombre_raza 'Descripcion
   cmbRaza(1).ItemData(cmbRaza(1).NewIndex) = rse!id_raza
   rse.MoveNext
Loop
cmbRaza(1).ListIndex = 0
ultRaza = cmbEspecie.List(cmbEspecie.ListIndex)
rse.Close
End Sub

Private Sub cmbraza_Change(Index As Integer)
If Index = 0 Then Exit Sub
If nocomp = False Then completar cmbRaza(1) Else: nocomp = False
End Sub

Private Sub cmbraza_Click(Index As Integer)
If bRaza = True Then cmdGuardar.Enabled = True
End Sub

Private Sub cmbraza_KeyPress(KeyAscii As Integer, Index As Integer)
cmdGuardar.Enabled = False
If Index = 0 Then Exit Sub
If KeyAscii = vbKeyBack Then cmbRaza(1).Text = Left(cmbRaza(1).Text, cmbRaza(1).SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbRaza(1)
End Sub

Private Sub cmbraza_LostFocus(Index As Integer)
If Index = 0 Then Exit Sub
validar cmbRaza(1)
End Sub

Public Sub comboraza()
cmbRaza(0).Clear
rsr.Requery
If rsr.RecordCount = 0 Then
    Exit Sub
End If
rsr.MoveFirst
Do While Not rsr.EOF
   cmbRaza(0).AddItem rsr!nombre_raza 'Descripcion
   cmbRaza(0).ItemData(cmbRaza(0).NewIndex) = rsr!id_raza
   rsr.MoveNext
Loop
cmbRaza(0).ListIndex = 0
End Sub

Public Sub comboEspecie()
cmbEspecie.Clear
If rse.RecordCount = 0 Then
    Exit Sub
End If
rse.MoveFirst
Do While Not rse.EOF
   cmbEspecie.AddItem rse!nombre              'Descripcion
   cmbEspecie.ItemData(cmbEspecie.NewIndex) = rse!id_especie
   rse.MoveNext
Loop
cmbEspecie.ListIndex = 0
rse.Close
End Sub

Public Sub comboCliente()
cmbCliente.Clear
If rst.RecordCount = 0 Then
    Exit Sub
End If
rst.MoveFirst
Do While Not rst.EOF
   cmbCliente.AddItem rst!apellido + ", " + rst!nombre  'Descripcion
   cmbCliente.ItemData(cmbCliente.NewIndex) = rst!id_cliente
   rst.MoveNext
Loop
cmbCliente.ListIndex = 0
End Sub

' Mostrar Todas las mascotas

Private Sub chkTodos_Click()
If chkTodos.Value = 0 Then
    cargarGrdMas
Else
rstMxC.MoveFirst
GrdMas.Rows = 1
While rstMxC.EOF = False
If rstMxC(3) = "f" Then
pCru = " No"
Else
pCru = " Si"
End If
GrdMas.AddItem rstMxC(1) & vbTab & rstMxC(2) & vbTab & rstMxC(7) & vbTab & pCru & vbTab & rstMxC(5) + ", " + rstMxC(4)
If rstMxC(6) = "v" Then
        GrdMas.Row = GrdMas.Rows - 1
        For c = 0 To 4
        GrdMas.Col = c
        GrdMas.CellBackColor = &HFFFFC0
        Next
End If
rstMxC.MoveNext
Wend
rstMxC.MoveFirst
End If
End Sub


'Private Sub chkdec_Click()
'If chkDec.Value = 1 Then
'FecDec.MaxDate = Date
'FecDec.Value = Date
'FecDec.Enabled = True
'txtPd.Visible = False
'Else
'FecDec.Enabled = False
'txtPd.Visible = True
'End If
'End Sub

