VERSION 5.00
Begin VB.Form frmActPermiso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Administracion de Permisos"
   ClientHeight    =   6105
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8940
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActPermiso.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6105
   ScaleWidth      =   8940
   Begin VB.Frame fraProdYSer 
      Caption         =   "Productos y Servicios"
      Enabled         =   0   'False
      Height          =   495
      Left            =   8160
      TabIndex        =   26
      Top             =   4320
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Frame fraSistema 
      Caption         =   "Sistema"
      Enabled         =   0   'False
      Height          =   1095
      Left            =   120
      TabIndex        =   38
      Top             =   2880
      Width           =   2895
      Begin VB.CheckBox chkActEmpleado 
         Caption         =   "Actualizar Empleados"
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   240
         Width           =   2175
      End
      Begin VB.CheckBox chkRubro 
         Caption         =   "Actualizar Rubros"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   480
         Width           =   1815
      End
   End
   Begin VB.Frame fraAtencion 
      Caption         =   "Atencion"
      Enabled         =   0   'False
      Height          =   3495
      Left            =   3120
      TabIndex        =   7
      Top             =   480
      Width           =   2895
      Begin VB.CheckBox chkActServ 
         Caption         =   "Actualizar Servicios"
         Height          =   255
         Left            =   120
         TabIndex        =   51
         Top             =   1200
         Width           =   1935
      End
      Begin VB.CheckBox chkCruza 
         Caption         =   "Inf. de Animales para Cruza"
         Height          =   255
         Left            =   120
         TabIndex        =   47
         Top             =   2640
         Width           =   2655
      End
      Begin VB.CheckBox chkInfEnferm 
         Caption         =   "Informe de Enfermedades"
         Height          =   255
         Left            =   120
         TabIndex        =   46
         Top             =   2400
         Width           =   2655
      End
      Begin VB.CheckBox chkInfTurnos 
         Caption         =   "Informe de Turnos del Dia"
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   2160
         Width           =   2655
      End
      Begin VB.CheckBox chkActEspecie 
         Caption         =   "Actualizar Especies"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   960
         Width           =   2055
      End
      Begin VB.CheckBox chkActMascota 
         Caption         =   "Actualizar Mascotas"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   480
         Width           =   1935
      End
      Begin VB.CheckBox chkActRaza 
         Caption         =   "Actualizar Razas"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   720
         Width           =   1695
      End
      Begin VB.CheckBox chkConHc 
         Caption         =   "Consultar Historias Clinicas"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   2655
      End
      Begin VB.CheckBox chkReclamo 
         Caption         =   "Registrar Reclamos"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1920
         Width           =   2055
      End
      Begin VB.CheckBox chkTurnos 
         Caption         =   "Registrar Turnos"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1680
         Width           =   1815
      End
      Begin VB.CheckBox chkActClientes 
         Caption         =   "Actualizar Clientes"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame fraVentas 
      Caption         =   "Ventas"
      Enabled         =   0   'False
      Height          =   1575
      Left            =   6083
      TabIndex        =   27
      Top             =   2400
      Width           =   2775
      Begin VB.CheckBox chkActArtic 
         Caption         =   "Actualizar Art�culos"
         Height          =   255
         Left            =   120
         TabIndex        =   50
         Top             =   480
         Width           =   2175
      End
      Begin VB.CheckBox chkActMedicam 
         Caption         =   "Actualizar Medicamentos"
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   240
         Width           =   2535
      End
      Begin VB.CheckBox chkActTarjeta 
         Caption         =   "Actualizar Tarjetas"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   720
         Width           =   1815
      End
      Begin VB.CheckBox chkActPlanP 
         Caption         =   "Actualizar Planes de Pago"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   960
         Width           =   2535
      End
      Begin VB.CheckBox chkFacturar 
         Caption         =   "Facturar"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   1200
         Width           =   1815
      End
   End
   Begin VB.Frame fraPrepago 
      Caption         =   "Prepago"
      Enabled         =   0   'False
      Height          =   1095
      Left            =   2423
      TabIndex        =   21
      Top             =   3960
      Width           =   4095
      Begin VB.CheckBox chkInfMoros 
         Caption         =   "Inf. de Morosidad"
         Height          =   255
         Left            =   2040
         TabIndex        =   48
         Top             =   720
         Width           =   1935
      End
      Begin VB.CheckBox chkVtaPP 
         Caption         =   "Inf. de Prepagos"
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   720
         Width           =   1935
      End
      Begin VB.CheckBox chkActPrepago 
         Caption         =   "Actualizar Prepago"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Width           =   1935
      End
      Begin VB.CheckBox chkAfiliar 
         Caption         =   "Afiliar a Prepago"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   480
         Width           =   1815
      End
      Begin VB.CheckBox chkRecPago 
         Caption         =   "Registrar Pago"
         Height          =   255
         Left            =   2040
         TabIndex        =   23
         Top             =   240
         Width           =   1815
      End
      Begin VB.CheckBox chkVerMoros 
         Caption         =   "Verificar Morosidad"
         Height          =   255
         Left            =   2040
         TabIndex        =   22
         Top             =   480
         Width           =   1935
      End
   End
   Begin VB.Frame fraCompras 
      Caption         =   "Compras"
      Enabled         =   0   'False
      Height          =   1815
      Left            =   6083
      TabIndex        =   14
      Top             =   480
      Width           =   2775
      Begin VB.CheckBox chkActProveedores 
         Caption         =   "Actualizar Proveedores"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   2175
      End
      Begin VB.CheckBox chkStock 
         Caption         =   "Controlar Stock"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   480
         Width           =   1575
      End
      Begin VB.CheckBox chkOrden 
         Caption         =   "Orden de Compra"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   720
         Width           =   1815
      End
      Begin VB.CheckBox chkPago 
         Caption         =   "Registrar Pago a Prov."
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1200
         Width           =   2175
      End
      Begin VB.CheckBox chkMercaderia 
         Caption         =   "Tramitar O. Compra"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   960
         Width           =   2175
      End
      Begin VB.CheckBox chkRecPedido 
         Caption         =   "Reclamar Pedido"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1440
         Width           =   1815
      End
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   1463
      TabIndex        =   6
      Top             =   5160
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   3623
      TabIndex        =   5
      Top             =   5160
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   5783
      TabIndex        =   4
      Top             =   5160
      Width           =   1695
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4688
      TabIndex        =   3
      Top             =   5640
      Width           =   1695
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2528
      TabIndex        =   2
      Top             =   5640
      Width           =   1695
   End
   Begin VB.ComboBox cmbNombre 
      Height          =   345
      Left            =   3743
      TabIndex        =   0
      Text            =   "cmbNombre"
      Top             =   120
      Width           =   2295
   End
   Begin VB.Frame fraInformes 
      Caption         =   "Informes y Reportes"
      Enabled         =   0   'False
      Height          =   2295
      Left            =   83
      TabIndex        =   31
      Top             =   480
      Width           =   2895
      Begin VB.CheckBox chkConsaProv 
         Caption         =   "Consulta de Rec. a Prov."
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   1920
         Width           =   2415
      End
      Begin VB.CheckBox chkEstRec 
         Caption         =   "Est. de Reclamos"
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   1440
         Width           =   1815
      End
      Begin VB.CheckBox chkInfFrec 
         Caption         =   "Frecuencia"
         Height          =   255
         Left            =   120
         TabIndex        =   37
         Top             =   240
         Width           =   1815
      End
      Begin VB.CheckBox chkInfOc 
         Caption         =   "�rdenes de Compra"
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   480
         Width           =   2055
      End
      Begin VB.CheckBox chkInfCliHab 
         Caption         =   "Clientes hab. de Peluqueria"
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   720
         Width           =   2655
      End
      Begin VB.CheckBox chkInfVentas 
         Caption         =   "Ventas"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   960
         Width           =   1815
      End
      Begin VB.CheckBox chkEstPac 
         Caption         =   "Est. de Pacientes"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   1200
         Width           =   1815
      End
      Begin VB.CheckBox chkEstVta 
         Caption         =   "Est. de Ventas"
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   1680
         Width           =   1815
      End
   End
   Begin VB.Label lblNombre 
      Caption         =   "Nombre:"
      Height          =   255
      Left            =   2910
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "frmActPermiso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim BNuevo As Boolean

Private Sub cmbNombre_Click()
Refrescar
End Sub

Private Sub cmbNombre_KeyPress(KeyAscii As Integer)
If BNuevo = False Then KeyAscii = 0
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenDynamic
rst.LockType = adLockOptimistic
rst.Source = "select * from permiso where id_permiso>0"
rst.Open
loadCombo
End Sub
Private Sub loadCombo()
rst.MoveFirst
cmbNombre.Clear
While Not rst.EOF
    cmbNombre.AddItem rst!nombre
    cmbNombre.ItemData(cmbNombre.NewIndex) = rst!id_permiso
    rst.MoveNext
Wend
cmbNombre.ListIndex = 0
'refrescar
End Sub
Private Sub Refrescar()
rst.MoveFirst
rst.Find "id_permiso=" & cmbNombre.ItemData(cmbNombre.ListIndex), , adSearchForward
If rst!conhc = "v" Then chkConHc.Value = 1 Else chkConHc.Value = 0
If rst!actclientes = "v" Then chkActClientes.Value = 1 Else chkActClientes.Value = 0
If rst!actmascota = "v" Then chkActMascota.Value = 1 Else chkActMascota.Value = 0
If rst!actraza = "v" Then chkActRaza.Value = 1 Else chkActRaza.Value = 0
If rst!turnos = "v" Then chkTurnos.Value = 1 Else chkTurnos.Value = 0
If rst!reclamo = "v" Then chkReclamo.Value = 1 Else chkReclamo.Value = 0
If rst!cruza = "v" Then chkCruza.Value = 1 Else chkCruza.Value = 0
If rst!infturnos = "v" Then chkInfTurnos.Value = 1 Else chkInfTurnos.Value = 0
If rst!infenferm = "v" Then chkInfEnferm.Value = 1 Else chkInfEnferm.Value = 0
If rst!inffrec = "v" Then chkInfFrec.Value = 1 Else chkInfFrec.Value = 0
If rst!orden = "v" Then chkOrden.Value = 1 Else chkOrden.Value = 0
If rst!mercaderia = "v" Then chkMercaderia.Value = 1 Else chkMercaderia.Value = 0
If rst!stock = "v" Then chkStock.Value = 1 Else chkStock.Value = 0
If rst!recpedido = "v" Then chkRecPedido.Value = 1 Else chkRecPedido.Value = 0
If rst!actproveedores = "v" Then chkActProveedores.Value = 1 Else chkActProveedores.Value = 0
If rst!pago = "v" Then chkPago.Value = 1 Else chkPago.Value = 0
If rst!infventas = "v" Then chkInfVentas.Value = 1 Else chkInfVentas.Value = 0
If rst!infoc = "v" Then chkInfOc.Value = 1 Else chkInfOc.Value = 0
If rst!actprepago = "v" Then chkActPrepago.Value = 1 Else chkActPrepago.Value = 0
If rst!afiliar = "v" Then chkAfiliar.Value = 1 Else chkAfiliar.Value = 0
If rst!recpago = "v" Then chkRecPago.Value = 1 Else chkRecPago.Value = 0
If rst!vermoros = "v" Then chkVerMoros.Value = 1 Else chkVerMoros.Value = 0
If rst!infmoros = "v" Then chkInfMoros.Value = 1 Else chkInfMoros.Value = 0
If rst!actserv = "v" Then chkActServ.Value = 1 Else chkActServ.Value = 0
If rst!actmedicam = "v" Then chkActMedicam.Value = 1 Else chkActMedicam.Value = 0
If rst!actartic = "v" Then chkActArtic.Value = 1 Else chkActArtic.Value = 0
If rst!infclihab = "v" Then chkInfCliHab.Value = 1 Else chkInfCliHab.Value = 0
If rst!facturar = "v" Then chkFacturar.Value = 1 Else chkFacturar.Value = 0
If rst!estvta = "v" Then chkEstVta.Value = 1 Else chkEstVta.Value = 0
If rst!acttarjeta = "v" Then chkActTarjeta.Value = 1 Else chkActTarjeta.Value = 0
If rst!actplanp = "v" Then chkActPlanP.Value = 1 Else chkActPlanP.Value = 0
If rst!actempleado = "v" Then chkActEmpleado.Value = 1 Else chkActEmpleado.Value = 0
'If rst!servidor = "v" Then chkServidor.Value = 1 Else chkServidor.Value = 0
If rst!rubro = "v" Then chkRubro.Value = 1 Else chkRubro.Value = 0
If rst!estPac = "v" Then chkEstPac.Value = 1 Else chkEstPac.Value = 0
If rst!estRec = "v" Then chkEstRec.Value = 1 Else chkEstRec.Value = 0
If rst!vtaPP = "v" Then chkVtaPP.Value = 1 Else chkVtaPP.Value = 0
If rst!actespecie = "v" Then chkActEspecie.Value = 1 Else chkActEspecie.Value = 0
If rst!consaProv = "v" Then chkConsaProv.Value = 1 Else chkConsaProv.Value = 0
    '.password.value=1
    '.sesion.value=1
End Sub
Private Sub cerrar()
Unload Me
End Sub
Private Sub nuevo()
cmdnuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
BNuevo = True
fraAtencion.Enabled = True
fraCompras.Enabled = True
fraInformes.Enabled = True
fraPrepago.Enabled = True
fraProdYSer.Enabled = True
fraSistema.Enabled = True
fraVentas.Enabled = True
cmdGuardar.Enabled = True
cmdsalir.Caption = "&Cancelar"
rst.AddNew
cmbNombre.Text = ""
rst!id_permiso = nuevoid("id_permiso", "permiso")
End Sub
Private Sub modificar()
cmdnuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
fraAtencion.Enabled = True
fraCompras.Enabled = True
fraInformes.Enabled = True
fraPrepago.Enabled = True
fraProdYSer.Enabled = True
fraSistema.Enabled = True
fraVentas.Enabled = True
cmdGuardar.Enabled = True
cmdsalir.Caption = "&Cancelar"
End Sub
Private Sub guardar()
If Trim(cmbNombre.Text) = "" Then MsgRapido "Debe asignar un nombre al conjunto de permisos.", Loro, vbCritical, , "sad": Exit Sub
rst!nombre = cmbNombre.Text
If chkInfFrec.Value = 1 Or chkInfOc.Value = 1 Or chkInfCliHab.Value = 1 Or chkInfVentas.Value = 1 Or chkEstPac.Value = 1 Or chkEstVta.Value = 1 Or chkEstRec.Value = 1 Or chkConsaProv.Value = 1 Then rst!info = "v" Else rst!info = "f"
If chkActClientes.Value = 1 Or chkActMascota.Value = 1 Or chkActRaza.Value = 1 Or chkActEspecie.Value = 1 Or chkActServ.Value = 1 Or chkConHc.Value = 1 Or chkTurnos.Value = 1 Or chkReclamo.Value = 1 Or chkInfTurnos.Value = 1 Or chkInfEnferm.Value = 1 Or chkCruza.Value = 1 Then rst!atencion = "v" Else rst!atencion = "f"
If chkActProveedores.Value = 1 Or chkStock.Value = 1 Or chkOrden.Value = 1 Or chkMercaderia.Value = 1 Or chkPago.Value = 1 Or chkRecPedido.Value = 1 Then rst!compras = "v" Else rst!compras = "f"
'If chkActServ.Value = 1 Or chkActMedicam.Value = 1 Or chkActArtic.Value = 1 Then rst!prodyser = "v" Else rst!prodyser = "f"
If chkActMedicam.Value = 1 Or chkActArtic.Value = 1 Or chkActTarjeta.Value = 1 Or chkActPlanP.Value = 1 Or chkFacturar.Value = 1 Then rst!ventas = "v" Else rst!ventas = "f"
If chkActEmpleado.Value = 1 Or chkRubro.Value = 1 Then rst!sistema = "v" Else rst!sistema = "f"
If chkActPrepago.Value = 1 Or chkAfiliar.Value = 1 Or chkVtaPP.Value = 1 Or chkRecPago.Value = 1 Or chkVerMoros.Value = 1 Or chkInfMoros.Value = 1 Then rst!prepago = "v" Else rst!prepago = "f"

If chkConHc.Value = 1 Then rst!conhc = "v" Else rst!conhc = "f"
If chkActClientes.Value = 1 Then rst!actclientes = "v" Else rst!actclientes = "f"
If chkActMascota.Value = 1 Then rst!actmascota = "v" Else rst!actmascota = "f"
If chkActRaza.Value = 1 Then rst!actraza = "v" Else rst!actraza = "f"
If chkTurnos.Value = 1 Then rst!turnos = "v" Else rst!turnos = "f"
If chkReclamo.Value = 1 Then rst!reclamo = "v" Else rst!reclamo = "f"
If chkCruza.Value = 1 Then rst!cruza = "v" Else rst!cruza = "f"
If chkInfTurnos.Value = 1 Then rst!infturnos = "v" Else rst!infturnos = "f"
If chkInfEnferm.Value = 1 Then rst!infenferm = "v" Else rst!infenferm = "f"
If chkInfFrec.Value = 1 Then rst!inffrec = "v" Else rst!inffrec = "f"
If chkOrden.Value = 1 Then rst!orden = "v" Else rst!orden = "f"
If chkMercaderia.Value = 1 Then rst!mercaderia = "v" Else rst!mercaderia = "f"
If chkStock.Value = 1 Then rst!stock = "v" Else rst!stock = "f"
If chkRecPedido.Value = 1 Then rst!recpedido = "v" Else rst!recpedido = "f"
If chkActProveedores.Value = 1 Then rst!actproveedores = "v" Else rst!actproveedores = "f"
If chkPago.Value = 1 Then rst!pago = "v" Else rst!pago = "f"
If chkInfVentas.Value = 1 Then rst!infventas = "v" Else rst!infventas = "f"
If chkInfOc.Value = 1 Then rst!infoc = "v" Else rst!infoc = "f"
If chkActPrepago.Value = 1 Then rst!actprepago = "v" Else rst!actprepago = "f"
If chkAfiliar.Value = 1 Then rst!afiliar = "v" Else rst!afiliar = "f"
If chkRecPago.Value = 1 Then rst!recpago = "v" Else rst!recpago = "f"
If chkVerMoros.Value = 1 Then rst!vermoros = "v" Else rst!vermoros = "f"
If chkInfMoros.Value = 1 Then rst!infmoros = "v" Else rst!infmoros = "f"
If chkActServ.Value = 1 Then rst!actserv = "v" Else rst!actserv = "f"
If chkActMedicam.Value = 1 Then rst!actmedicam = "v" Else rst!actmedicam = "f"
If chkActArtic.Value = 1 Then rst!actartic = "v" Else rst!actartic = "f"
If chkInfCliHab.Value = 1 Then rst!infclihab = "v" Else rst!infclihab = "f"
If chkFacturar.Value = 1 Then rst!facturar = "v" Else rst!facturar = "f"
If chkEstVta.Value = 1 Then rst!estvta = "v" Else rst!estvta = "f"
If chkActTarjeta.Value = 1 Then rst!acttarjeta = "v" Else rst!acttarjeta = "f"
If chkActPlanP.Value = 1 Then rst!actplanp = "v" Else rst!actplanp = "f"
If chkActEmpleado.Value = 1 Then rst!actempleado = "v" Else rst!actempleado = "f"
If chkRubro.Value = 1 Then rst!rubro = "v" Else rst!rubro = "f"
If chkEstPac.Value = 1 Then rst!estPac = "v" Else rst!estPac = "f"
If chkActEspecie.Value = 1 Then rst!actespecie = "v" Else rst!actespecie = "f"
If chkVtaPP.Value = 1 Then rst!vtaPP = "v" Else rst!vtaPP = "f"
If chkEstRec.Value = 1 Then rst!estRec = "v" Else rst!estRec = "f"
If chkConsaProv.Value = 1 Then rst!consaProv = "v" Else rst!consaProv = "f"
rst!sesion = "v"
rst!servidor = "f"
rst!actPerm = "f"
rst.Update
restablecer
loadCombo
End Sub
Private Sub borrar()
Dim BBorrar As Boolean
If Mensaje("�Desea eliminar el conjunto de permisos " & cmbNombre.Text & "?", Loro, Balloon, vbYesNo + vbQuestion, , "surprised") = vbNo Then Exit Sub
rst.Close
rst.Source = "select * from perm_emp where id_permiso=" & cmbNombre.ItemData(cmbNombre.ListIndex)
rst.Open
If rst.RecordCount <> 0 Then BBorrar = False Else BBorrar = True
If cmbNombre.ItemData(cmbNombre.ListIndex) < 6 Then BBorrar = False
rst.Close
rst.Source = "select * from permiso where id_permiso>0"
rst.Open
rst.Find "id_permiso=" & cmbNombre.ItemData(cmbNombre.ListIndex), , adSearchForward
If BBorrar = False Then
    MsgRapido "No es posible eliminar el conjunto de permisos.", Loro, vbCritical, , "sad"
Else
    rst.Delete
End If
loadCombo
End Sub
Private Sub restablecer()
cmdnuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
fraAtencion.Enabled = False
fraCompras.Enabled = False
fraInformes.Enabled = False
fraPrepago.Enabled = False
fraProdYSer.Enabled = False
fraSistema.Enabled = False
fraVentas.Enabled = False
cmdGuardar.Enabled = False
cmdsalir.Caption = "&Salir"
cmbNombre.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdsalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        Loro.Stop
        rst.CancelUpdate
        restablecer
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
Set rst = Nothing
End Sub
