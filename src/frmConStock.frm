VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConStock 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Control de Stock"
   ClientHeight    =   5505
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10650
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConStock.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5505
   ScaleWidth      =   10650
   Begin VB.Frame fraCon 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   5415
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5295
      Begin MSComCtl2.UpDown UpDown 
         Height          =   330
         Left            =   3495
         TabIndex        =   15
         Top             =   4380
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtCant"
         BuddyDispid     =   196610
         OrigLeft        =   3600
         OrigTop         =   2880
         OrigRight       =   3840
         OrigBottom      =   3375
         Max             =   10000000
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.TextBox txtCant 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   2640
         MaxLength       =   3
         TabIndex        =   14
         Text            =   "1"
         Top             =   4380
         Width           =   855
      End
      Begin VB.Frame Frame1 
         Caption         =   "Consultar Stock de"
         Height          =   615
         Left            =   113
         TabIndex        =   7
         Top             =   120
         Width           =   5175
         Begin VB.OptionButton OptTodos 
            Caption         =   "Todos los Productos"
            Height          =   255
            Left            =   120
            TabIndex        =   9
            Top             =   240
            Value           =   -1  'True
            Width           =   2175
         End
         Begin VB.OptionButton OptListados 
            Caption         =   "S�lo Productos Listados"
            Height          =   255
            Left            =   2280
            TabIndex        =   8
            Top             =   240
            Width           =   2535
         End
      End
      Begin VB.CheckBox chkPredet 
         Caption         =   "Usar M�nimo Predeterminado"
         Height          =   285
         Left            =   1320
         TabIndex        =   5
         Top             =   3840
         Value           =   1  'Checked
         Width           =   2775
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "+"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4913
         TabIndex        =   4
         Top             =   1200
         Width           =   375
      End
      Begin VB.CommandButton cmdQuitar 
         Caption         =   "-"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4913
         TabIndex        =   3
         Top             =   1800
         Width           =   375
      End
      Begin VB.CommandButton cmdVerificar 
         Caption         =   "&Verificar"
         Height          =   375
         Left            =   780
         TabIndex        =   2
         Top             =   5040
         Width           =   1695
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   2940
         TabIndex        =   1
         Top             =   5040
         Width           =   1695
      End
      Begin MSFlexGridLib.MSFlexGrid grdLista 
         Height          =   2655
         Left            =   120
         TabIndex        =   6
         Top             =   1080
         Width           =   4695
         _ExtentX        =   8281
         _ExtentY        =   4683
         _Version        =   393216
         Rows            =   1
         Cols            =   4
         FixedRows       =   0
         FixedCols       =   0
         Enabled         =   0   'False
      End
      Begin VB.Label Label1 
         Caption         =   "Productos a controlar:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   840
         Width           =   2055
      End
      Begin VB.Label lblMinimo 
         Caption         =   "Verificar stock menor a                           unidades."
         Height          =   375
         Left            =   720
         TabIndex        =   10
         Top             =   4440
         Width           =   3975
      End
   End
   Begin VB.Frame fraRes 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   5415
      Left            =   5280
      TabIndex        =   11
      Top             =   0
      Width           =   5295
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Nueva Busqueda"
         Height          =   375
         Left            =   1800
         TabIndex        =   17
         Top             =   5040
         Width           =   1695
      End
      Begin MSFlexGridLib.MSFlexGrid grdResultado 
         Height          =   4575
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   8070
         _Version        =   393216
         Rows            =   1
         Cols            =   5
         FixedRows       =   0
         FixedCols       =   0
         FillStyle       =   1
         AllowUserResizing=   1
      End
      Begin VB.Label lblResultado 
         Caption         =   "Art�culos con Stock Bajo:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   120
         Width           =   2175
      End
   End
End
Attribute VB_Name = "frmConStock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Private Sub chkPredet_Click()
If chkPredet.Value = 1 Then
txtCant.Enabled = False
UpDown.Enabled = False
Else
txtCant.Enabled = True
UpDown.Enabled = True
End If
End Sub

Private Sub cmdagregar_Click()
frmAgregar.mostrar Me
frmAgregar.UpDown1.Visible = False
frmAgregar.txtCantidad.Visible = False
frmAgregar.lblCantidad.Visible = False
'frmAgregar.cmdAceptar.Top = 1080
'frmAgregar.cmdCancelar.Top = 1080
frmAgregar.Height = 2460
frmAgregar.lblStock.Visible = True
frmAgregar.optMedicamento.Value = True
frmAgregar.optMedicamento.Left = 600
frmAgregar.optArticulo.Left = 2100
frmAgregar.optServicio.Visible = False
frmAgregar.cmbServicio.Visible = False
frmAgregar.cmbMedicamento.Visible = True
frmAgregar.Show
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub cmdQuitar_Click()
grdLista.FixedRows = 0
grdLista.RemoveItem grdLista.Rows - 1
If grdLista.Rows = 1 Then
    grdLista.Rows = 2
    grdLista.FixedRows = 1
    grdLista.Rows = 1
    cmdQuitar.Enabled = False
Else
    grdLista.FixedRows = 1
End If
End Sub

Private Sub cmdVerificar_Click()
Me.Width = 10740
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
If OptTodos = False And grdLista.Rows = 1 Then: Exit Sub
'fraCon.Visible = False
'fraRes.Visible = True
'grdLista.Rows = 1
frmPrincipal.imprimir.Enabled = True
frmPrincipal.vista.Enabled = True
frmPrincipal.Toolbar2.Buttons(1).Enabled = True
frmPrincipal.Toolbar2.Buttons(2).Enabled = True
Dim tipo As String
Dim c
Dim listado As Boolean
grdResultado.Clear
grdResultado.Rows = 0
grdResultado.AddItem "Tipo" & vbTab & "C�digo" & vbTab & "Nombre" & vbTab & "Stock" & vbTab & "M�nimo"
grdResultado.Rows = 2
grdResultado.FixedRows = 1
grdResultado.Rows = 1
rst.Source = "select * from medicamento where id_medicamento>0"
rst.Open
tipo = "Med."
While Not rst.EOF
    listado = True
    If OptTodos = False Then
        listado = False
        For c = 1 To grdLista.Rows - 1
            If Val(grdLista.TextMatrix(c, 2)) = 2 And Val(grdLista.TextMatrix(c, 3)) = rst!id_medicamento Then: listado = True
        Next c
    End If
    If listado = True Then
        If chkPredet.Value = 1 Then
            If rst!stock_total < rst!stock_min Then
                grdResultado.AddItem tipo & vbTab & rst!id_medicamento & vbTab & rst!nombre & vbTab & rst!stock_total & vbTab & rst!stock_min
            End If
        Else
            If rst!stock_total < Val(txtCant) Then
                grdResultado.AddItem tipo & vbTab & rst!id_medicamento & vbTab & rst!nombre & vbTab & rst!stock_total & vbTab & rst!stock_min
            End If
        End If
    End If
    rst.MoveNext
Wend
rst.Close
rst.Source = "select * from articulo"
rst.Open
tipo = "Art."
While Not rst.EOF
    listado = True
    If OptTodos = False Then
        listado = False
        For c = 1 To grdLista.Rows - 1
            If Val(grdLista.TextMatrix(c, 2)) = 3 And Val(grdLista.TextMatrix(c, 3)) = rst!id_articulo Then: listado = True
        Next c
    End If
    If listado = True Then
        If chkPredet.Value = 1 Then
            If rst!stock_total < rst!stock_minimo Then
                grdResultado.AddItem tipo & vbTab & rst!id_articulo & vbTab & rst!nombre & vbTab & rst!stock_total & vbTab & rst!stock_minimo
            End If
        Else
            If rst!stock_total < Val(txtCant) Then
                grdResultado.AddItem tipo & vbTab & rst!id_articulo & vbTab & rst!nombre & vbTab & rst!stock_total & vbTab & rst!stock_minimo
            End If
        End If
    End If
    rst.MoveNext
Wend
rst.Close
For c = 1 To grdResultado.Rows - 1
    If Val(grdResultado.TextMatrix(c, 3)) < Val(grdResultado.TextMatrix(c, 4)) Then
        grdResultado.Row = c
        grdResultado.Col = 0
        grdResultado.ColSel = 4
        grdResultado.CellBackColor = 8454143
    End If
Next c
grdResultado.ColSel = 0
End Sub

Private Sub cmdvolver_Click()
Me.Width = 5500
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
If fraCon.Visible = False Then
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons(2).Enabled = True
Else
    frmPrincipal.imprimir.Enabled = False
    frmPrincipal.vista.Enabled = False
    frmPrincipal.Toolbar2.Buttons(1).Enabled = False
    frmPrincipal.Toolbar2.Buttons(2).Enabled = False
End If
End Sub

Private Sub Form_Load()
Me.Width = 5500
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenDynamic
rst.LockType = adLockReadOnly
txtCant.Enabled = False
UpDown.Enabled = False
grdLista.Rows = 0
grdLista.AddItem "Nombre" & vbTab & "Descripci�n"
grdLista.Rows = 2
grdLista.FixedRows = 1
grdLista.Rows = 1
grdLista.ColWidth(0) = 1740
grdLista.ColWidth(1) = 2855
grdLista.ColWidth(2) = 0
grdLista.ColWidth(3) = 0
grdResultado.Rows = 0
grdResultado.AddItem "Tipo" & vbTab & "C�digo" & vbTab & "Nombre" & vbTab & "Stock" & vbTab & "M�nimo"
grdResultado.Rows = 2
grdResultado.FixedRows = 1
grdResultado.Rows = 1
grdResultado.ColWidth(0) = 705
grdResultado.ColWidth(1) = 765
grdResultado.ColWidth(2) = 2025
grdResultado.ColWidth(3) = 810
grdResultado.ColWidth(4) = 765
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub


Private Sub grdresultado_DblClick()
If cargado("frmOrdenCompra") = False Then Exit Sub
Dim tipo As Byte
If grdResultado.TextMatrix(grdResultado.Row, 0) = "Art." Then tipo = 3 Else tipo = 2
frmAgregarOC.agregarStock Val(grdResultado.TextMatrix(grdResultado.Row, 1)), tipo
End Sub

Private Sub OptListados_Click()
If OptListados.Value = True Then
    cmdAgregar.Enabled = True
    grdLista.Enabled = True
    If grdLista.Rows > 1 Then
        cmdQuitar.Enabled = True
    Else
        cmdQuitar.Enabled = False
    End If
Else
    cmdAgregar.Enabled = False
    cmdQuitar.Enabled = False
    grdLista.Enabled = False
End If

End Sub

Private Sub OptTodos_Click()
If OptTodos.Value = True Then
cmdAgregar.Enabled = False
cmdQuitar.Enabled = False
grdLista.Enabled = False
Else
cmdAgregar.Enabled = True
cmdQuitar.Enabled = True
grdLista.Enabled = True
End If
End Sub
Sub agregar(xTipo_Item As Integer, xId_Item As Integer, xCantidad As Integer)
Dim c
For c = 1 To grdLista.Rows - 1
    If grdLista.TextMatrix(c, 2) = xTipo_Item And grdLista.TextMatrix(c, 3) = xId_Item Then
        Mensaje "El Item seleccionado ya se encuentra en la lista.", Loro, Balloon, vbInformation, , "decline"
        Me.Enabled = True
        Exit Sub
    End If
Next c
Select Case xTipo_Item
    Case 1
        rst.Source = "select nombre, descripcion from servicio where id_servicio=" & xId_Item
    Case 2
        rst.Source = "select nombre, descripcion from medicamento where id_medicamento=" & xId_Item
    Case 3
        rst.Source = "select nombre, descripcion from articulo where id_articulo=" & xId_Item
End Select
rst.Open
grdLista.AddItem rst!nombre & vbTab & rst!descripcion & vbTab & xTipo_Item & vbTab & xId_Item
rst.Close
Me.Enabled = True
cmdQuitar.Enabled = True
End Sub


Private Sub txtCant_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtCant_Validate(Cancel As Boolean)
If Trim(txtCant) <> "" Then
    If IsNumeric(txtCant) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtCant = 1
    End If
End If
End Sub
Sub preview()
info 1
End Sub
Sub imprimir()
info 2
End Sub
Sub info(ByVal accion As Integer)
Dim informe As ADODB.Recordset
Dim c
Set informe = New ADODB.Recordset
informe.Fields.Append "tipo", adVarChar, 5
informe.Fields.Append "cod", adVarChar, 10
informe.Fields.Append "nombre", adVarChar, 20
informe.Fields.Append "stock", adVarChar, 10
informe.Fields.Append "minimo", adVarChar, 10
informe.LockType = adLockOptimistic
informe.CursorType = adOpenDynamic
informe.Open
For c = 1 To grdResultado.Rows - 1
    informe.AddNew
    informe!tipo = grdResultado.TextMatrix(c, 0)
    informe!cod = grdResultado.TextMatrix(c, 1)
    informe!nombre = grdResultado.TextMatrix(c, 2)
    informe!stock = grdResultado.TextMatrix(c, 3)
    informe!minimo = grdResultado.TextMatrix(c, 4)
    informe.Update
Next c
Set rptStock.DataSource = informe
frmPrincipal.desactivararch
Select Case accion
    Case 1
        rptStock.Show
    Case 2
        rptStock.PrintReport True
        Unload rptStock
End Select
End Sub
