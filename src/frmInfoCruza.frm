VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmInfoCruza 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de Animales disponibles p/ Cruza"
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7455
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfoCruza.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4440
   ScaleWidth      =   7455
   Begin VB.ComboBox cmbEspecie 
      Height          =   345
      Left            =   960
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   2415
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   4080
      TabIndex        =   8
      Top             =   3960
      Width           =   1455
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1920
      TabIndex        =   7
      Top             =   3960
      Width           =   1455
   End
   Begin MSFlexGridLib.MSFlexGrid grdCruza 
      Height          =   2295
      Left            =   0
      TabIndex        =   6
      Top             =   1560
      Width           =   7440
      _ExtentX        =   13123
      _ExtentY        =   4048
      _Version        =   393216
      Rows            =   0
      Cols            =   9
      FixedRows       =   0
      FixedCols       =   0
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   495
      Left            =   5160
      TabIndex        =   5
      Top             =   720
      Width           =   1335
   End
   Begin VB.ComboBox cmbRaza 
      Height          =   345
      Left            =   4200
      TabIndex        =   2
      Text            =   "cmbRaza"
      Top             =   120
      Width           =   3135
   End
   Begin VB.Frame fraSexo 
      Caption         =   "Sexo:"
      Height          =   615
      Left            =   960
      TabIndex        =   9
      Top             =   600
      Width           =   3255
      Begin VB.OptionButton optHembra 
         Caption         =   "Hembra"
         Height          =   255
         Left            =   1673
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.OptionButton optMacho 
         Caption         =   "Macho"
         Height          =   255
         Left            =   473
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Resultados de la B�squeda:"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1320
      Width           =   2415
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Especie:"
      Height          =   225
      Left            =   105
      TabIndex        =   10
      Top             =   120
      Width           =   720
   End
   Begin VB.Label lblRaza 
      Caption         =   "Raza:"
      Height          =   255
      Left            =   3600
      TabIndex        =   0
      Top             =   120
      Width           =   495
   End
End
Attribute VB_Name = "frmInfoCruza"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim ultRaza As String
Dim Edad As String
Dim band, nocomp As Boolean

Private Sub cmdBuscar_Click()
grdCruza.Rows = 1
If cmbraza.ListCount = 0 Then Exit Sub
If optMacho.Value = True Then
    rst.Source = "select c.nombre,c.apellido,c.calle,c.numero,c.piso,c.tel,c.torre,c.depto,c.email,m.fecha_nac, m.nombre as mascota from cliente c,mascota_hc m where m.id_cliente=c.id_cliente and m.id_raza=" & cmbraza.ItemData(cmbraza.ListIndex) & " and m.para_cruza='v' and m.sexo='m'"
Else
    rst.Source = "select c.nombre,c.apellido,c.calle,c.numero,c.piso,c.tel,c.torre,c.depto,c.email,m.fecha_nac, m.nombre as mascota from cliente c,mascota_hc m where m.id_cliente=c.id_cliente and m.id_raza=" & cmbraza.ItemData(cmbraza.ListIndex) & " and m.para_cruza='v' and m.sexo='h'"
End If
rst.Open
If rst.RecordCount <> 0 Then
    cmdImprimir.Enabled = True
    While Not rst.EOF
        If Year(Date) - Year(rst!fecha_nac) = 0 Then
        Edad = 0 & " A�os " & (Month(Date) - Month(rst!fecha_nac)) & " Meses"
        'Int((DateValue(Date) - DateValue(Format(rst!fecha_nac, "d/mm/yyyy"))) / 365)
        Else
        Edad = Year(Date) - Year(rst!fecha_nac) & " A�os " & (Month(Date) - Month(rst!fecha_nac)) & " Meses"
        End If
        grdCruza.AddItem rst!apellido & ", " & rst!nombre & vbTab & rst!calle & " " & rst!numero & vbTab & rst!torre & vbTab & rst!piso & vbTab & rst!depto & vbTab & rst!tel & vbTab & rst!Email & vbTab & rst!mascota & vbTab & Edad
        rst.MoveNext
    Wend
    Else
    MsgRapido "No se encontraron Animales con estas Caracter�sticas", Loro, vbInformation, , "explain"
    cmdImprimir.Enabled = False
End If
rst.Close
End Sub

Private Sub cmdCerrar_Click()
Unload Me
End Sub

Private Sub cmdImprimir_Click()
info 1
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
frmPrincipal.vista.Enabled = True
frmPrincipal.imprimir.Enabled = True
band = True
End Sub

Private Sub Form_Load()
band = False
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
rst.Source = "select id_especie, nombre from especie where borrado = 'f' order by nombre"
rst.Open
Do While Not rst.EOF
   cmbEspecie.AddItem rst!nombre              'Descripcion
   cmbEspecie.ItemData(cmbEspecie.NewIndex) = rst!id_especie
   rst.MoveNext
Loop
cmbEspecie.ListIndex = 0
cmbraza.Clear
rst.Close
prepararGrilla
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
Set rst = Nothing
End Sub
Private Sub prepararGrilla()
grdCruza.Clear
grdCruza.Rows = 0
grdCruza.AddItem "Nombre" & vbTab & "Direccion" & vbTab & "Torre" & vbTab & "Piso" & vbTab & "Depto." & vbTab & "Tel�fono" & vbTab & "E-mail" & vbTab & "Mascota" & vbTab & "Edad de la mascota"
grdCruza.Rows = 2
grdCruza.FixedRows = 1
grdCruza.Rows = 1
grdCruza.ColWidth(0) = 2500
grdCruza.ColWidth(1) = 1800
grdCruza.ColWidth(2) = 400
grdCruza.ColWidth(3) = 400
grdCruza.ColWidth(4) = 400
grdCruza.ColWidth(5) = 1000
grdCruza.ColWidth(6) = 2100
grdCruza.ColWidth(7) = 1000
grdCruza.ColWidth(8) = 1725
End Sub
Sub imprimir()
info 1
End Sub
Sub preview()
info 2
End Sub
Private Sub info(accion As Byte)
Dim rstInfo As ADODB.Recordset
Dim f, c
Set rstInfo = New ADODB.Recordset
rstInfo.LockType = adLockOptimistic
rstInfo.CursorType = adOpenKeyset
rstInfo.Fields.Append "nombre", adVarChar, 45
rstInfo.Fields.Append "direccion", adVarChar, 30
rstInfo.Fields.Append "torre", adVarChar, 5
rstInfo.Fields.Append "piso", adVarChar, 5
rstInfo.Fields.Append "depto", adVarChar, 5
rstInfo.Fields.Append "tel", adVarChar, 15
rstInfo.Fields.Append "email", adVarChar, 30
rstInfo.Fields.Append "mascota", adVarChar, 30
rstInfo.Fields.Append "edad", adVarChar, 30
rstInfo.Open
For f = 1 To grdCruza.Rows - 1
    rstInfo.AddNew
    For c = 0 To grdCruza.Cols - 1
        rstInfo(c) = grdCruza.TextMatrix(f, c)
    Next c
Next f
Set rptInfoCruza.DataSource = rstInfo
Select Case accion
    Case 1
        rptInfoCruza.PrintReport
    Case 2
        rptInfoCruza.Show
End Select
Set rstInfo = Nothing
End Sub

Sub cmbEspecie_Click()
If band = False Then Exit Sub
If ultRaza = cmbEspecie.List(cmbEspecie.ListIndex) Then: Exit Sub
rst.Source = "select id_raza, nombre_raza from raza where especie = " & cmbEspecie.ItemData(cmbEspecie.ListIndex) & " and borrado = 'f' order by nombre_raza"
rst.Open
cmbraza.Clear
If rst.RecordCount = 0 Then
    rst.Close
    ultRaza = cmbEspecie.List(cmbEspecie.ListIndex)
    Exit Sub
End If
rst.MoveFirst
Do While Not rst.EOF
   cmbraza.AddItem rst!nombre_raza 'Descripcion
   cmbraza.ItemData(cmbraza.NewIndex) = rst!id_raza
   rst.MoveNext
Loop
cmbraza.ListIndex = 0
ultRaza = cmbEspecie.List(cmbEspecie.ListIndex)
rst.Close
End Sub

Private Sub cmbraza_Change()
If nocomp = False Then completar cmbraza Else: nocomp = False
End Sub

Private Sub cmbraza_Click()
cmdBuscar.Enabled = True
End Sub

Private Sub cmbraza_KeyPress(KeyAscii As Integer)
cmdBuscar.Enabled = False
If KeyAscii = vbKeyBack Then cmbraza.Text = Left(cmbraza.Text, cmbraza.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbraza
End Sub

Private Sub cmbraza_LostFocus()
validar cmbraza
End Sub
