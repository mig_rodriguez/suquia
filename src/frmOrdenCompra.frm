VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmOrdenCompra 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Emitir Orden de Compra"
   ClientHeight    =   4230
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8445
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOrdenCompra.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4230
   ScaleWidth      =   8445
   Begin VB.CommandButton cmdConStock 
      Caption         =   "Controlar &Stock"
      Height          =   375
      Left            =   6240
      TabIndex        =   9
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox txtMTotal 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      Height          =   330
      Left            =   4308
      TabIndex        =   7
      Text            =   "0"
      Top             =   3240
      Width           =   900
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2628
      TabIndex        =   6
      Top             =   3720
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4361
      TabIndex        =   5
      Top             =   3720
      Width           =   1455
   End
   Begin VB.CommandButton cmdQuitar 
      Caption         =   "-"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      TabIndex        =   4
      Top             =   1560
      Width           =   375
   End
   Begin VB.CommandButton cmdAgregar 
      Caption         =   "+"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7920
      TabIndex        =   3
      Top             =   960
      Width           =   375
   End
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   2415
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   7665
      _ExtentX        =   13520
      _ExtentY        =   4260
      _Version        =   393216
      Rows            =   1
      Cols            =   9
      FixedRows       =   0
      FixedCols       =   0
      GridColor       =   128
      HighLight       =   2
      FillStyle       =   1
   End
   Begin VB.TextBox txtNro 
      BackColor       =   &H80000018&
      Enabled         =   0   'False
      Height          =   330
      Left            =   1440
      TabIndex        =   1
      Top             =   1560
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Detalle:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   360
      Width           =   855
   End
   Begin VB.Label lblTotal 
      AutoSize        =   -1  'True
      Caption         =   "Monto Total:      $"
      Height          =   225
      Left            =   2880
      TabIndex        =   8
      Top             =   3285
      Width           =   1365
   End
   Begin VB.Label lblNroFactura 
      AutoSize        =   -1  'True
      Caption         =   "Orden N�mero:"
      Height          =   225
      Left            =   120
      TabIndex        =   0
      Top             =   1680
      Visible         =   0   'False
      Width           =   1275
   End
End
Attribute VB_Name = "frmOrdenCompra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Dim rsP As ADODB.Recordset
Dim cmd As ADODB.Command
Dim ordenItem, c As Integer

Private Sub cmdConStock_Click()
frmConStock.Show
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Set rst = New ADODB.Recordset
Set rsP = New ADODB.Recordset
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
frmOrdenCompra.Left = 1815
frmOrdenCompra.Top = 555
grdDetalle.ColWidth(0) = 650
grdDetalle.ColWidth(1) = 1600
grdDetalle.ColWidth(2) = 830
grdDetalle.ColWidth(3) = 1050
grdDetalle.ColWidth(4) = 520
grdDetalle.ColWidth(5) = 1750
grdDetalle.ColWidth(6) = 1100
grdDetalle.ColWidth(7) = 0
grdDetalle.ColWidth(8) = 0
grdDetalle.Rows = 0
grdDetalle.AddItem "N�Item" & vbTab & "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unit." & vbTab & "Total" & vbTab & "Proveedor" & vbTab & "TE(Prov)"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
Set rst = abrirrs("select nro_orden from orden_compra order by nro_orden", False)
If rst.RecordCount <> 0 Then
    rst.MoveLast
    txtNro = rst(0) + 1
Else
    txtNro = 1
End If
rst.Close
ordenItem = 0
End Sub

Private Sub cmdagregar_Click()
frmAgregarOC.Show
End Sub

Private Sub cmdQuitar_Click()
txtMTotal = Val(txtMTotal) - Val(grdDetalle.TextMatrix(grdDetalle.Row, 4))
If ordenItem > 1 Then
grdDetalle.RemoveItem (grdDetalle.Row)
Else
'grdDetalle.Clear
'grdDetalle.FixedRows = 0
'grdDetalle.Rows = 0
'grdDetalle.AddItem "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unitario" & vbTab & "Total"
'grdDetalle.Rows = 2
'grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
End If
ordenItem = ordenItem - 1
If ordenItem = 0 Then: cmdQuitar.Enabled = False
If ordenItem = 0 Then: cmdAceptar.Enabled = False
End Sub

Private Sub cmdaceptar_Click()
Dim PrimerFila As Integer
Dim UltimaFila As Integer
PrimerFila = 1
For c = 1 To grdDetalle.Rows - 1
    If grdDetalle.TextMatrix(c, 8) <> grdDetalle.TextMatrix(c - 1, 8) And c <> 1 Then
        UltimaFila = c - 1
        EmitirOrden PrimerFila, UltimaFila
        PrimerFila = c
    End If
Next c
UltimaFila = grdDetalle.Rows - 1
EmitirOrden PrimerFila, UltimaFila
Unload Me
End Sub

Sub EmitirOrden(Inicio As Integer, Fin As Integer)
Dim TotalOrden As Single
Dim xNro_orden As Long
For c = Inicio To Fin
    TotalOrden = Round(TotalOrden + Val(grdDetalle.TextMatrix(c, 4)), 2)
Next c
xNro_orden = nuevoid("nro_orden", "orden_compra")
cmd.CommandText = "insert into orden_compra(nro_orden,fecha_creacion,estado,fecha_recibido,pagada,total_estimado,legajo) values(" & xNro_orden & ",'" & DateDB & "',1,'','f'," & TotalOrden & "," & xLegajo & ")"
cmd.Execute
For c = Inicio To Fin
    cmd.CommandText = "insert into detalle_oc (nro_orden,id_item,cantidad,precio_estimado,id_proveedor,tipo_item,estado)  values(" & xNro_orden & "," & grdDetalle.TextMatrix(c, 0) & "," & grdDetalle.TextMatrix(c, 2) & "," & grdDetalle.TextMatrix(c, 4) & "," & Val(grdDetalle.TextMatrix(c, 8)) & "," & grdDetalle.TextMatrix(c, 7) & ",0)"
    cmd.Execute
Next
Set rst = Nothing
Set rst = New ADODB.Recordset
rst.Fields.Append "item", adVarChar, 10
rst.Fields.Append "nombre", adVarChar, 30
rst.Fields.Append "cantidad", adVarChar, 10
rst.Fields.Append "unitario", adVarChar, 10
rst.Fields.Append "precio", adVarChar, 10
rst.Fields.Append "proveedor", adVarChar, 30
rst.Fields.Append "tel", adVarChar, 15
rst.LockType = adLockOptimistic
rst.CursorType = adOpenKeyset
rst.Open
Dim i, j As Integer
For i = Inicio To Fin
    rst.AddNew
    For j = 0 To 6
        rst(j) = grdDetalle.TextMatrix(i, j)
    Next j
    rst.Update
Next i
Dim Reporte As rptOC
Set Reporte = New rptOC
Set Reporte.DataSource = rst
Reporte.Sections(2).Controls(7).Caption = Mid(frmPrincipal.StatusBar1.Panels(3).Text, 10)
Reporte.Sections(2).Controls(8).Caption = xNro_orden
Reporte.Sections(4).Controls(2).Caption = TotalOrden
Reporte.Show
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Sub agregar(xTipo_Item As Integer, xId_Item As Integer, xCantidad As Integer, Xprov As Integer)
Select Case xTipo_Item
    Case 2
        rst.Source = "select id_medicamento,nombre,precio from medicamento where id_medicamento=" & xId_Item
    Case 3
        rst.Source = "select id_articulo,nombre,precio from articulo where id_articulo=" & xId_Item
End Select
rst.Open
ordenItem = ordenItem + 1
For c = 1 To grdDetalle.Rows - 1
If IsNumeric(grdDetalle.TextMatrix(c, 0)) = True Then
    If grdDetalle.TextMatrix(c, 0) = xId_Item And grdDetalle.TextMatrix(c, 1) = rst!nombre Then
        MsgRapido "El item ya se encuentra en la Orden de Compra.", Loro, vbExclamation, , "surprised"
        ordenItem = ordenItem - 1
        rst.Close
        frmOrdenCompra.Enabled = True
        Exit Sub
    End If
End If
Next
Set rsP = abrirrs("select p.id_proveedor, p.razon_social, p.tel from proveedor p order by p.id_proveedor", False)
rsP.MoveFirst
Do While Not rsP.EOF
If rsP!id_proveedor = Xprov Then Exit Do
rsP.MoveNext
Loop
'Dim BNvoProv As Boolean
'BNvoProv = True
'For c = 1 To grdDetalle.Rows - 1
'    If Val(grdDetalle.TextMatrix(c, 9)) = Xprov Then BNvoProv = False: Exit For
'Next c
'
'If BNvoProv = True And grdDetalle.Rows >= 2 Then
'    grdDetalle.AddItem "N�Item" & vbTab & "Nombre" & vbTab & "Cantidad" & vbTab & "Precio Unit." & vbTab & "Total" & vbTab & "Estado" & vbTab & "Proveedor" & vbTab & "TE(Prov)" & vbTab & "0" & vbTab & Xprov
'    grdDetalle.Row = grdDetalle.Rows - 1
'    grdDetalle.Col = 0
'    grdDetalle.ColSel = 9
'    grdDetalle.CellBackColor = &H8000000F
'End If
grdDetalle.AddItem rst(0) & vbTab & rst!nombre & vbTab & xCantidad & vbTab & rst!Precio & vbTab & rst!Precio * xCantidad & vbTab & rsP!razon_social & vbTab & rsP!tel & vbTab & xTipo_Item & vbTab & Xprov
txtMTotal = Val(txtMTotal) + rst!Precio * xCantidad
rst.Close
grdDetalle.Col = 8
'grdDetalle.ColSel = 8
grdDetalle.Sort = 3
cmdQuitar.Enabled = True
frmOrdenCompra.Enabled = True
cmdAceptar.Enabled = True
End Sub
