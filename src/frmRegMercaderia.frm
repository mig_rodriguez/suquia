VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRegMercaderia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tramitar �rdenes de Compra"
   ClientHeight    =   6525
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7080
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegMercaderia.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6525
   ScaleWidth      =   7080
   Begin VB.TextBox txtNroRemito 
      Enabled         =   0   'False
      Height          =   330
      Left            =   5400
      TabIndex        =   22
      Top             =   600
      Width           =   1455
   End
   Begin VB.TextBox txtProveedor 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1560
      TabIndex        =   20
      Top             =   600
      Width           =   2295
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Default         =   -1  'True
      Height          =   375
      Left            =   4589
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2153
      TabIndex        =   4
      Top             =   5640
      Width           =   1215
   End
   Begin VB.CommandButton cmdRecibir 
      Caption         =   "&Recibir"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3713
      TabIndex        =   5
      Top             =   5640
      Width           =   1215
   End
   Begin VB.CommandButton cmdPagar 
      Caption         =   "&Pagar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   5273
      TabIndex        =   6
      Top             =   5640
      Width           =   1215
   End
   Begin VB.CommandButton cmdEnviar 
      Caption         =   "&Enviar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   593
      TabIndex        =   3
      Top             =   5640
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   3135
      Left            =   120
      TabIndex        =   2
      Top             =   2040
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   5530
      _Version        =   393216
      Rows            =   0
      Cols            =   9
      FixedRows       =   0
      FixedCols       =   0
   End
   Begin VB.CommandButton cmdVolver 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2753
      TabIndex        =   7
      Top             =   6120
      Width           =   1575
   End
   Begin VB.TextBox txtTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0FFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4905
      TabIndex        =   12
      Top             =   5160
      Width           =   825
   End
   Begin VB.TextBox txtPagada 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1560
      TabIndex        =   11
      Top             =   1560
      Width           =   1455
   End
   Begin VB.TextBox txtEstado 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1560
      TabIndex        =   10
      Top             =   1080
      Width           =   1455
   End
   Begin VB.TextBox txtFechaRec 
      Enabled         =   0   'False
      Height          =   330
      Left            =   5393
      TabIndex        =   9
      Top             =   1560
      Width           =   1455
   End
   Begin VB.TextBox txtFechaCreada 
      Enabled         =   0   'False
      Height          =   330
      Left            =   5400
      TabIndex        =   8
      Top             =   1080
      Width           =   1455
   End
   Begin VB.TextBox txtNroOrden 
      Height          =   330
      Left            =   3108
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "N� de Remito:"
      Height          =   255
      Left            =   3983
      TabIndex        =   21
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label lblProveedor 
      Caption         =   "Proveedor:"
      Height          =   255
      Left            =   240
      TabIndex        =   19
      Top             =   600
      Width           =   975
   End
   Begin VB.Label lblTotal 
      Caption         =   "Total:   $"
      Height          =   255
      Left            =   4200
      TabIndex        =   18
      Top             =   5160
      Width           =   735
   End
   Begin VB.Label lblPagada 
      Caption         =   "Pagada:"
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   1560
      Width           =   735
   End
   Begin VB.Label lblEstado 
      Caption         =   "Estado:"
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label lblFechaRec 
      Caption         =   "Fecha de Recepci�n:"
      Height          =   255
      Left            =   3360
      TabIndex        =   15
      Top             =   1560
      Width           =   1815
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha de Creaci�n:"
      Height          =   255
      Left            =   3360
      TabIndex        =   14
      Top             =   1080
      Width           =   1695
   End
   Begin VB.Label lblNroOrden 
      Caption         =   "Orden de Compra:"
      Height          =   255
      Left            =   1516
      TabIndex        =   13
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "frmRegMercaderia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim xNro_orden As Long
Dim rst As ADODB.Recordset
Dim cmd As ADODB.Command
Private Sub cmdBuscar_Click()
grdDetalle.Rows = 1
txtEstado = ""
txtPagada = ""
txtFechaRec = ""
txtFechaCreada = ""
txtTotal = ""
If Trim(txtNroOrden) = "" Then Exit Sub
txtNroOrden.SelStart = 0
txtNroOrden.SelLength = Len(txtNroOrden)
rst.Source = "select o.nro_orden,o.fecha_creacion,o.estado,o.fecha_recibido,o.pagada,o.total_estimado,o.nro_remito from orden_compra o where o.nro_orden=" & txtNroOrden
rst.Open
If rst.RecordCount = 0 Then
    MsgRapido "La orden de compra solicitada no existe.", Loro, vbCritical, , "decline"
    rst.Close
    Exit Sub
End If
txtNroRemito = rst!nro_remito
txtNroOrden = rst!nro_orden
xNro_orden = rst!nro_orden
txtFechaCreada = rst!fecha_creacion
On Error GoTo errCatch
txtFechaRec = rst!fecha_recibido
On Error GoTo 0
Select Case rst!estado
    Case 1
        txtEstado = "Pendiente"
        cmdEnviar.Enabled = True
        cmdCancelar.Enabled = True
        cmdRecibir.Enabled = False
    Case 2
        txtEstado = "Enviada"
        cmdEnviar.Enabled = False
        cmdCancelar.Enabled = True
        cmdRecibir.Enabled = True
    Case 3
        txtEstado = "Cancelada"
        cmdEnviar.Enabled = False
        cmdCancelar.Enabled = False
        cmdRecibir.Enabled = False
    Case 4
        txtEstado = "Recibida"
        cmdEnviar.Enabled = False
        cmdCancelar.Enabled = False
        cmdRecibir.Enabled = False
End Select
If rst!pagada = "v" Then
    txtPagada = "Si"
    cmdPagar.Enabled = False
Else
    txtPagada = "No"
    If rst!estado = 4 Then cmdPagar.Enabled = True Else cmdPagar.Enabled = False
End If
txtTotal = rst!total_estimado
rst.Close
grdDetalle.Clear
grdDetalle.FixedRows = 0
grdDetalle.Rows = 0
grdDetalle.AddItem "Nombre" & vbTab & "Proveedor" & vbTab & "Cantidad" & vbTab & "Precio est.($)" & vbTab & "Total est.($)" & vbTab & "Estado"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
rst.Source = "select d.id_item,d.id_proveedor,d.tipo_item,a.nombre,d.cantidad,d.precio_estimado,if(d.estado=1,'Almacenado',if(d.estado=2,'Devuelto','')) as estado,p.razon_social from detalle_oc d,proveedor p,articulo a where d.id_item=a.id_articulo and d.id_proveedor=p.id_proveedor and d.tipo_item=3 and d.nro_orden=" & txtNroOrden
rst.Open
While Not rst.EOF
    grdDetalle.AddItem rst!nombre & vbTab & rst!razon_social & vbTab & rst!Cantidad & vbTab & Round(rst!precio_estimado / rst!Cantidad, 2) & vbTab & rst!precio_estimado & vbTab & rst!estado & vbTab & rst!tipo_item & vbTab & rst!id_item & vbTab & rst!id_proveedor
    txtProveedor = rst!razon_social
    rst.MoveNext
Wend
rst.Close
rst.Source = "select d.id_item,d.id_proveedor,d.tipo_item,m.nombre,d.cantidad,d.precio_estimado,if(d.estado=1,'Almacenado',if(d.estado=2,'Devuelto','')) as estado,p.razon_social from detalle_oc d,proveedor p,medicamento m where d.id_item=m.id_medicamento and d.id_proveedor=p.id_proveedor and d.tipo_item=2 and d.nro_orden=" & txtNroOrden
rst.Open
While Not rst.EOF
    grdDetalle.AddItem rst!nombre & vbTab & rst!razon_social & vbTab & rst!Cantidad & vbTab & Round(rst!precio_estimado / rst!Cantidad, 2) & vbTab & rst!precio_estimado & vbTab & rst!estado & vbTab & rst!tipo_item & vbTab & rst!id_item & vbTab & rst!id_proveedor
    txtProveedor = rst!razon_social
    rst.MoveNext
Wend
rst.Close
If grdDetalle.Rows > 11 Then grdDetalle.ColWidth(0) = 2200 Else grdDetalle.ColWidth(0) = 2445
errCatch:
If Err.Number = 94 Then
    txtFechaRec = ""
    Resume Next
End If
End Sub

Private Sub cmdcancelar_Click()
If Mensaje("�Est� seguro de que desea cancelar la �rden de Compra?", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then Exit Sub
cmd.CommandText = "update orden_compra set estado=3 where nro_orden=" & xNro_orden
cmd.Execute
txtEstado = "Cancelada"
cmdEnviar.Enabled = False
cmdCancelar.Enabled = False
cmdRecibir.Enabled = False
cmdPagar.Enabled = False
End Sub

Private Sub cmdEnviar_Click()
cmd.CommandText = "update orden_compra set estado=2 where nro_orden=" & xNro_orden
cmd.Execute
txtEstado = "Enviada"
cmdEnviar.Enabled = False
cmdCancelar.Enabled = True
cmdRecibir.Enabled = True
cmdPagar.Enabled = False
End Sub

Private Sub cmdPagar_Click()
'txtPagada = "Si"
'cmdEnviar.Enabled = False
'cmdCancelar.Enabled = False
'cmdRecibir.Enabled = False
'cmdPagar.Enabled = False
frmRegPagoProv.Show
frmRegPagoProv.txtNroOrden = xNro_orden
frmRegPagoProv.buscar
End Sub

Private Sub cmdRecibir_Click()
Dim xNro_remito
xNro_remito = Input_Box("Ingrese el N�mero de Remito.", Loro, Balloon, , "write")
If Trim(xNro_remito) = "" Then Exit Sub
cmd.CommandText = "update orden_compra set nro_remito='" & xNro_remito & "' where nro_orden=" & xNro_orden
cmd.Execute
txtNroRemito = xNro_remito
cmd.CommandText = "update orden_compra set estado=4 where nro_orden=" & xNro_orden
cmd.Execute
cmd.CommandText = "update orden_compra set fecha_recibido='" & DateDB & "' where nro_orden=" & xNro_orden
cmd.Execute
txtFechaRec = Date
txtEstado = "Recibida"
cmdEnviar.Enabled = False
cmdCancelar.Enabled = False
cmdRecibir.Enabled = False
cmdPagar.Enabled = True
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
Set rst = New ADODB.Recordset
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenDynamic
grdDetalle.ColWidth(0) = 2445
grdDetalle.ColWidth(1) = 0
grdDetalle.ColWidth(2) = 870
grdDetalle.ColWidth(3) = 1200
grdDetalle.ColWidth(4) = 1095
grdDetalle.ColWidth(5) = 1125
grdDetalle.ColWidth(6) = 0
grdDetalle.ColWidth(7) = 0
grdDetalle.ColWidth(8) = 0
grdDetalle.AddItem "Nombre" & vbTab & "Proveedor" & vbTab & "Cantidad" & vbTab & "Precio est.($)" & vbTab & "Total est.($)" & vbTab & "Estado"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub cmdvolver_Click()
Unload Me
End Sub

Private Sub grdDetalle_DblClick()
If (grdDetalle.TextMatrix(grdDetalle.Row, 5) = "" Or grdDetalle.TextMatrix(grdDetalle.Row, 5) = "Devuelto") And txtEstado = "Recibida" Then
frmRegLote.cargar Val(grdDetalle.TextMatrix(grdDetalle.Row, 8)), Val(grdDetalle.TextMatrix(grdDetalle.Row, 6)), Val(grdDetalle.TextMatrix(grdDetalle.Row, 7)), Val(grdDetalle.TextMatrix(grdDetalle.Row, 2)), xNro_orden, Val(grdDetalle.TextMatrix(grdDetalle.Row, 3))
End If
End Sub

Private Sub txtNroOrden_GotFocus()
txtNroOrden.SelStart = 0
txtNroOrden.SelLength = Len(txtNroOrden)
End Sub

Private Sub txtNroOrden_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
End If
End Sub
Sub recargarTotal()
Dim c
txtTotal = "0"
For c = 1 To grdDetalle.Rows - 1
    txtTotal = Val(txtTotal) + Val(grdDetalle.TextMatrix(c, 4))
Next c
cmd.CommandText = "update orden_compra set total_estimado=" & txtTotal & " where nro_orden=" & xNro_orden
cmd.Execute
End Sub
Sub buscar()
cmdBuscar_Click
End Sub
