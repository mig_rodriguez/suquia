VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmactcliente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Clientes"
   ClientHeight    =   6525
   ClientLeft      =   2490
   ClientTop       =   570
   ClientWidth     =   9120
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmactcliente.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6525
   ScaleWidth      =   9120
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   1560
      TabIndex        =   18
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   3713
      TabIndex        =   19
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   5858
      TabIndex        =   20
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4778
      TabIndex        =   21
      Top             =   6000
      Width           =   1695
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2618
      TabIndex        =   17
      Top             =   6000
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   4695
      Index           =   1
      Left            =   240
      TabIndex        =   27
      Top             =   480
      Width           =   8655
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   1200
         MaxLength       =   30
         TabIndex        =   15
         Top             =   4320
         Width           =   3315
      End
      Begin MSComCtl2.DTPicker fecNac 
         Height          =   330
         Left            =   6600
         TabIndex        =   4
         Top             =   1680
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   24510465
         CurrentDate     =   37846
      End
      Begin VB.CommandButton cmdUltimo 
         Height          =   375
         Left            =   1920
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         Height          =   375
         Left            =   1440
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         Height          =   375
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   480
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   375
      End
      Begin VB.TextBox Text1 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   0
         EndProperty
         Enabled         =   0   'False
         Height          =   330
         Index           =   12
         Left            =   6000
         MaxLength       =   13
         TabIndex        =   16
         Top             =   4320
         Width           =   2415
      End
      Begin VB.ComboBox Combo1 
         Enabled         =   0   'False
         Height          =   345
         Left            =   6600
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   720
         Width           =   1335
      End
      Begin VB.TextBox Text1 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   1
         EndProperty
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   6600
         MaxLength       =   10
         TabIndex        =   3
         Top             =   1200
         Width           =   1335
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   1560
         MaxLength       =   20
         TabIndex        =   0
         Top             =   720
         Width           =   2775
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   1560
         MaxLength       =   20
         TabIndex        =   1
         Top             =   1200
         Width           =   2775
      End
      Begin VB.Frame Frame2 
         Caption         =   "Domicilio"
         Height          =   1935
         Left            =   0
         TabIndex        =   29
         Top             =   2160
         Width           =   8535
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   11
            Left            =   6000
            MaxLength       =   8
            TabIndex        =   14
            Top             =   1320
            Width           =   2415
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   10
            Left            =   6000
            MaxLength       =   25
            TabIndex        =   12
            Top             =   840
            Width           =   2415
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   9
            Left            =   6000
            MaxLength       =   8
            TabIndex        =   8
            Top             =   360
            Width           =   2415
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   8
            Left            =   1200
            MaxLength       =   30
            TabIndex        =   13
            Top             =   1320
            Width           =   3315
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   7
            Left            =   3870
            MaxLength       =   5
            TabIndex        =   11
            Top             =   840
            Width           =   645
         End
         Begin VB.TextBox Text1 
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   0
            EndProperty
            Enabled         =   0   'False
            Height          =   330
            Index           =   6
            Left            =   2520
            MaxLength       =   4
            TabIndex        =   10
            Top             =   840
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   5
            Left            =   1200
            MaxLength       =   6
            TabIndex        =   9
            Top             =   840
            Width           =   735
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   4
            Left            =   1200
            MaxLength       =   20
            TabIndex        =   7
            Top             =   360
            Width           =   3315
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo Postal:"
            Height          =   255
            Index           =   13
            Left            =   4680
            TabIndex        =   37
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Barrio:"
            Height          =   255
            Index           =   12
            Left            =   4680
            TabIndex        =   36
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Nro.:"
            Height          =   255
            Index           =   11
            Left            =   4680
            TabIndex        =   35
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Piso:"
            Height          =   255
            Index           =   10
            Left            =   2040
            TabIndex        =   34
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Dpto.:"
            Height          =   255
            Index           =   9
            Left            =   3360
            TabIndex        =   33
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Localidad:"
            Height          =   255
            Index           =   8
            Left            =   240
            TabIndex        =   32
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "Torre:"
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   31
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Calle:"
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   30
            Top             =   360
            Width           =   615
         End
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Masculino"
         Enabled         =   0   'False
         Height          =   375
         Index           =   0
         Left            =   1515
         TabIndex        =   5
         Top             =   1680
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Femenino"
         Enabled         =   0   'False
         Height          =   375
         Index           =   1
         Left            =   3120
         TabIndex        =   6
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "E-mail:"
         Height          =   225
         Index           =   15
         Left            =   240
         TabIndex        =   46
         Top             =   4320
         Width           =   585
      End
      Begin VB.Label Label1 
         Caption         =   "Tel�fono:"
         Height          =   330
         Index           =   14
         Left            =   4680
         TabIndex        =   44
         Top             =   4320
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Fecha de Nac.:"
         Height          =   255
         Index           =   5
         Left            =   4680
         TabIndex        =   43
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Nro. Documento:"
         Height          =   255
         Index           =   4
         Left            =   4680
         TabIndex        =   42
         Top             =   1200
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo Documento:"
         Height          =   255
         Index           =   3
         Left            =   4680
         TabIndex        =   41
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Sexo:"
         Height          =   255
         Index           =   2
         Left            =   360
         TabIndex        =   40
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Apellidos:"
         Height          =   255
         Index           =   1
         Left            =   360
         TabIndex        =   39
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Nombres:"
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   38
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   4695
      Index           =   2
      Left            =   240
      TabIndex        =   28
      Top             =   480
      Visible         =   0   'False
      Width           =   8655
      Begin MSFlexGridLib.MSFlexGrid grdClientes 
         Height          =   4595
         Left            =   0
         TabIndex        =   45
         Top             =   100
         Width           =   8655
         _ExtentX        =   15266
         _ExtentY        =   8096
         _Version        =   393216
         Rows            =   1
         Cols            =   6
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5175
      Left            =   120
      TabIndex        =   26
      Top             =   120
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   9128
      TabWidthStyle   =   2
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmactcliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mintCurFrame, xid_cliente As Integer
Dim busqueda As String
Dim rst As ADODB.Recordset
Dim band As Boolean

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Private Sub cmdsalir_Click()
   ' G U S T A V O
    Text1(2).MaxLength = 10
cerrar
End Sub

Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
    If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
frmactcliente.Height = 6915
mintCurFrame = 1
band = False
Set rst = New ADODB.Recordset
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
Combo1.AddItem "DNI"
Combo1.AddItem "LE"
Combo1.AddItem "LC"
Combo1.AddItem "CI"
busqueda = ""
rst.ActiveConnection = cnx
rst.LockType = adLockOptimistic
rst.CursorType = adOpenDynamic
rst.Source = "select * from cliente where id_cliente>0 and borrado = 'f' order by apellido, nombre"
rst.Open
loadGrid
Refrescar
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        Loro.Stop
        rst.CancelUpdate
        restablecer
        Cancel = 1
        Loro.Play "explain"
        Loro.Speak "Los cambios se han cancelado."
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
Set rst = Nothing
If cargado("frmactmascota") = True Then frmactmascota.Enabled = True
End Sub

Private Sub grdClientes_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rst.MoveFirst
    rst.Find "id_cliente = " & grdClientes.TextMatrix(grdClientes.Row, 5)
    Refrescar
    mintCurFrame = 1
    band = False
Else
If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
End If
End Sub
Sub nuevo()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 0 To 12
Text1(c).Enabled = True
Text1(c).Text = ""
Next c
fecNac.Enabled = True
Option1(0).Enabled = True
Option1(1).Enabled = True
Option1(0).Value = True
Combo1.Enabled = True
Combo1.ListIndex = 0
rst.MoveLast
xid_cliente = nuevoid("id_cliente", "cliente")
Text1(0).SetFocus
Loro.Play "writing"
rst.AddNew
   ' G U S T A V O
Text1(2).MaxLength = 8
End Sub
Sub modificar()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 0 To 12
Text1(c).Enabled = True
Next c
fecNac.Enabled = True
Option1(0).Enabled = True
Option1(1).Enabled = True
Combo1.Enabled = True
xid_cliente = rst!id_cliente
Text1(0).SetFocus
Loro.Play "writing"

        ' Quita los puntos del nro de documento
        Text1(2) = QuitarFormato(Text1(2))
        Text1(2).MaxLength = 8
End Sub
Sub cerrar()
Unload Me
End Sub


Sub guardar()
Text1(2).MaxLength = 10
Dim c
If Text1(5) = "" Then: Text1(5) = " "
If Text1(7) = "" Then: Text1(7) = " "
If Text1(3) = "" Then: Text1(3) = " "
For c = 0 To 4
    If c = 3 Then: GoTo sig
    If Trim(Text1(c)) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
sig:
Next c
For c = 8 To 11
    If Trim(Text1(c)) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
Next c
Loro.Stop
rst!id_cliente = xid_cliente
rst!nombre = Text1(0).Text
rst!apellido = Text1(1).Text
rst!nro_documento = Text1(2).Text
rst!calle = Text1(4).Text
rst!torre = Text1(5).Text
If Text1(6) = "" Then
rst!piso = Null
Else
rst!piso = Text1(6).Text
End If
rst!depto = Text1(7).Text
rst!localidad = Text1(8).Text
rst!numero = Text1(9).Text
rst!barrio = Text1(10).Text
rst!cod_postal = Text1(11).Text
If Text1(12) = "" Then
rst!tel = Null
Else
rst!tel = Text1(12).Text
End If
rst!Email = Text1(3).Text
If Option1(0).Value = True Then
rst!sexo = "m"
Else
rst!sexo = "f"
End If
rst!tipo_documento = Combo1.Text
rst!fecha_nac = fecNac.Value
rst!borrado = "f"
rst.Update
rst.Requery
rst.Find "id_cliente = " & xid_cliente
restablecer
loadGrid
Loro.Play "explain"
Loro.Speak "Los datos se han guardado."
cmdAdelante.SetFocus
' G U S T A V O

End Sub
Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Cliente Seleccionado", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rst!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rst.Update
rst.Requery
cmdAdelante_Click
loadGrid
End Sub
Sub imprimir()
frmPrincipal.desactivararch
Set rptListaClientes.DataSource = rst
rptListaClientes.PrintReport True
Unload rptListaClientes
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 2 Or Index = 6 Or Index = 12 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
    End If
End If
End Sub
Private Sub restablecer()
Dim c
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
TabStrip1.Enabled = True
cmdPrimero.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdUltimo.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
For c = 0 To 12
Text1(c).Enabled = False
Next c
fecNac.Enabled = False
Option1(0).Enabled = False
Option1(1).Enabled = False
Combo1.Enabled = False
Refrescar
End Sub

Private Sub Refrescar()
Dim aux As String
On Error Resume Next
Text1(0) = rst!nombre
Text1(1) = rst!apellido

aux = CStr(rst("nro_documento"))
'Text1(2) = rst("nro_documento")
Text1(2) = Formato(aux)

Text1(4) = rst("calle")
Text1(5) = ""
Text1(5) = rst("torre")
Text1(6) = ""
Text1(6) = rst("piso")
Text1(7) = ""
Text1(7) = rst("depto")
Text1(8) = rst("localidad")
Text1(9) = rst("numero")
Text1(10) = rst("barrio")
Text1(11) = rst("cod_postal")
Text1(12) = ""
Text1(12) = rst("tel")
Text1(3) = ""
Text1(3) = rst("email")
On Error GoTo 0
Select Case rst!tipo_documento
    Case "DNI"
        Combo1.ListIndex = 0
    Case "LE"
        Combo1.ListIndex = 1
    Case "LC"
        Combo1.ListIndex = 2
    Case "CI"
        Combo1.ListIndex = 3
End Select
If rst("sexo") = "m" Then
Option1(0) = True
Else
Option1(1) = True
End If
fecNac.Value = rst!fecha_nac
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptListaClientes.DataSource = rst
rptListaClientes.Show
End Sub
Sub buscar()
busqueda = Input_Box("Buscar Cliente." & Chr(13) & "Ingrese el Apellido:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    If Text1(1) = busqueda Then: rst.MoveNext
    rst.Find "apellido like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rst.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbCritical, , "explain"
End If
End Sub

Private Sub Text1_Validate(Index As Integer, Cancel As Boolean)
If Index = 2 Or Index = 6 Or Index = 12 Then
    If Text1(Index) <> "" Then
    If IsNumeric(Text1(Index)) = False Then
        MsgRapido "El dato no es v�lido.", Loro, vbCritical, , "surprised"
        Cancel = True
        Text1(Index) = ""
    End If
    End If
End If
End Sub
Private Sub loadGrid()
Dim regActual
regActual = rst.AbsolutePosition
rst.MoveFirst
grdClientes.ColWidth(0) = 2000
grdClientes.ColWidth(1) = 1800
grdClientes.ColWidth(2) = 1450
grdClientes.ColWidth(3) = 1300
grdClientes.ColWidth(4) = 2500
grdClientes.ColWidth(5) = 0
grdClientes.Rows = 0
grdClientes.AddItem "Apellidos" & vbTab & "Nombres" & vbTab & "Documento" & vbTab & "Tel�fono" & vbTab & "E-Mail"
grdClientes.Rows = 2
grdClientes.FixedRows = 1
grdClientes.Rows = 1
While rst.EOF = False
grdClientes.AddItem rst!apellido & vbTab & rst!nombre & vbTab & rst!tipo_documento & IIf(rst!tipo_documento <> "DNI", "  : ", ": ") & IIf(Len(rst!nro_documento) <> 8, "  ", "") & Formato(rst!nro_documento) & vbTab & rst!tel & vbTab & rst!Email & vbTab & rst!id_cliente
rst.MoveNext
Wend
rst.AbsolutePosition = regActual
grdClientes.Col = 0
grdClientes.ColSel = 1
grdClientes.Sort = 5
grdClientes.ColSel = 0
End Sub

'   Mover Cliente

Private Sub cmdPrimero_Click()
rst.MoveFirst
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAdelante_Click()
If rst.EOF <> True Then: rst.MoveNext
If rst.EOF <> True Then
Refrescar
Else
rst.MoveFirst
Refrescar
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
End Sub

Private Sub cmdAtras_Click()
If rst.BOF <> True Then: rst.MovePrevious
If rst.BOF <> True Then
Refrescar
Else
rst.MoveLast
Refrescar
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
End Sub

Private Sub cmdUltimo_Click()
rst.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

