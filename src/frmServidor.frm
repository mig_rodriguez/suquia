VERSION 5.00
Begin VB.Form frmServidor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuraci�n de Servidor de Datos"
   ClientHeight    =   2790
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4335
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmServidor.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2790
   ScaleWidth      =   4335
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkServidor 
      Caption         =   "Esta m�quina es el Servidor"
      Height          =   255
      Left            =   840
      TabIndex        =   8
      Top             =   240
      Width           =   2655
   End
   Begin VB.TextBox txtUnidad 
      Height          =   330
      Left            =   960
      TabIndex        =   1
      Top             =   1200
      Width           =   495
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2513
      TabIndex        =   4
      Top             =   2280
      Width           =   1335
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   473
      TabIndex        =   3
      Top             =   2280
      Width           =   1335
   End
   Begin VB.TextBox txtRuta 
      Height          =   330
      Left            =   960
      TabIndex        =   2
      Top             =   1680
      Width           =   3255
   End
   Begin VB.TextBox txtMaquina 
      Height          =   330
      Left            =   960
      TabIndex        =   0
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label lblUnidad 
      Caption         =   "Unidad:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   735
   End
   Begin VB.Label lblRuta 
      Caption         =   "Ruta:"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   495
   End
   Begin VB.Label lblMaquina 
      Caption         =   "Servidor:"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   720
      Width           =   855
   End
End
Attribute VB_Name = "frmServidor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub chkServidor_Click()
If chkServidor.Value = 1 Then
    txtMaquina.Enabled = False
    txtMaquina.BackColor = -2147483644
    txtUnidad.SetFocus
Else
    txtMaquina.Enabled = True
    txtMaquina.BackColor = -2147483643
    txtMaquina.SetFocus
End If
End Sub

Private Sub cmdAceptar_Click()
If Trim(txtUnidad) = "" Or Trim(txtRuta) = "" Then Exit Sub
If chkServidor.Value = 1 And Trim(txtMaquina) = "" Then Exit Sub
Dim loc As String
If chkServidor.Value = 0 Then
    loc = "\\" & txtMaquina & "\" & txtUnidad & "\" & txtRuta
Else
    loc = txtUnidad & ":\" & txtRuta
End If
SaveSetting "Suquia", "Settings", "DBLocation", loc
Unload Me
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub
