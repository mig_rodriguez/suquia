VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmInfoEnferm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de Enfermedades"
   ClientHeight    =   4515
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4845
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfoEnferm.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4515
   ScaleWidth      =   4845
   Begin MSFlexGridLib.MSFlexGrid grdDetalle 
      Height          =   2055
      Left            =   90
      TabIndex        =   6
      Top             =   2400
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   3625
      _Version        =   393216
      Rows            =   1
      Cols            =   4
      FixedRows       =   0
      FixedCols       =   0
      AllowUserResizing=   1
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Mostrar"
      Default         =   -1  'True
      Height          =   375
      Left            =   975
      TabIndex        =   4
      Top             =   1800
      Width           =   1335
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2535
      TabIndex        =   5
      Top             =   1800
      Width           =   1335
   End
   Begin MSComCtl2.UpDown UpDown 
      Height          =   330
      Left            =   3833
      TabIndex        =   9
      Top             =   720
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   393216
      Value           =   1
      BuddyControl    =   "txtNroCasos"
      BuddyDispid     =   196611
      OrigLeft        =   3120
      OrigTop         =   960
      OrigRight       =   3360
      OrigBottom      =   1335
      Max             =   9999999
      Min             =   1
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtNroCasos 
      Alignment       =   1  'Right Justify
      Height          =   330
      Left            =   3218
      MaxLength       =   3
      TabIndex        =   1
      Text            =   "1"
      Top             =   720
      Width           =   615
   End
   Begin VB.TextBox txtEnfermedad 
      Height          =   330
      Left            =   1440
      MaxLength       =   30
      TabIndex        =   0
      Top             =   240
      Width           =   3135
   End
   Begin MSComCtl2.DTPicker DtInicio 
      Height          =   375
      Left            =   1080
      TabIndex        =   2
      Top             =   1200
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   24510465
      CurrentDate     =   35796
      MinDate         =   32874
   End
   Begin MSComCtl2.DTPicker DtFin 
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   1200
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      Format          =   24510465
      CurrentDate     =   37861
      MinDate         =   32874
   End
   Begin VB.Label lblInicio 
      Caption         =   "Desde el"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   1200
      Width           =   855
   End
   Begin VB.Label lblFin 
      Caption         =   "Hasta el"
      Height          =   255
      Left            =   2640
      TabIndex        =   10
      Top             =   1200
      Width           =   735
   End
   Begin VB.Label lblNroCasos 
      Caption         =   "Cantidad de Casos:"
      Height          =   255
      Left            =   1388
      TabIndex        =   8
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label lblEnfemedad 
      Caption         =   "Enfermedad:"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   240
      Width           =   1095
   End
End
Attribute VB_Name = "frmInfoEnferm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset

Private Sub cmdAceptar_Click()
Dim fInicio, fFin
grdDetalle.Rows = 1
If DtInicio.Value > DtFin.Value Then
    MsgRapido "Error: Fecha desde mayor que fecha hasta", Loro, vbCritical, , "surprised"
    Exit Sub
Else
    fInicio = Format(DtInicio, "yyyy-mm-dd")
    fFin = Format(DtFin, "yyyy-mm-dd")
    rst.Source = "select c.nombre,c.apellido,m.nombre as mascota,count(m.id_hc) as cantidad, d.sintomas from cliente c,mascota_hc m,detalle_hc d where m.id_hc=d.id_hc and c.id_cliente=m.id_cliente and d.fecha>='" & fInicio & "' and d.fecha<='" & fFin & "' and d.sintomas like '%" & txtEnfermedad & "%' group by m.id_hc"
    rst.Open
    If rst.RecordCount <> 0 Then
        While Not rst.EOF
            If rst!Cantidad >= Val(txtNroCasos) Then grdDetalle.AddItem rst!apellido & ", " & rst!nombre & vbTab & rst!mascota & vbTab & rst!Cantidad & vbTab & rst!Sintomas
            rst.MoveNext
        Wend
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        MsgRapido "No existen datos", Loro, vbInformation, , "explain"
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
    End If
    rst.Close
End If
End Sub

Private Sub cmdsalir_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
frmPrincipal.vista.Enabled = True
frmPrincipal.imprimir.Enabled = True
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 3
DtFin.MaxDate = Date
DtFin = Date
DtInicio.MaxDate = Date
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenForwardOnly
rst.LockType = adLockReadOnly
loadGrid
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub loadGrid()
grdDetalle.ColWidth(0) = 2340
grdDetalle.ColWidth(1) = 1440
grdDetalle.ColWidth(2) = 600
grdDetalle.ColWidth(3) = 1500
grdDetalle.Rows = 0
grdDetalle.AddItem "Cliente" & vbTab & "Mascota" & vbTab & "Casos" & vbTab & "Enfermedad"
grdDetalle.Rows = 2
grdDetalle.FixedRows = 1
grdDetalle.Rows = 1
End Sub

Private Sub txtNroCasos_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
End If
End If
End Sub

Private Sub txtNroCasos_Validate(Cancel As Boolean)
If txtNroCasos = "" Or Val(txtNroCasos) = 0 Then txtNroCasos = "1"
End Sub
Sub imprimir()
If grdDetalle.Rows > 1 Then info 2
End Sub
Sub preview()
If grdDetalle.Rows > 1 Then info 1
End Sub
Private Sub info(ByVal accion As Byte)
Dim rstInfo As ADODB.Recordset
Dim c
Set rstInfo = New ADODB.Recordset
rstInfo.CursorType = adOpenKeyset
rstInfo.LockType = adLockOptimistic
rstInfo.Fields.Append "cliente", adVarChar, 30
rstInfo.Fields.Append "mascota", adVarChar, 7
rstInfo.Fields.Append "cantidad", adVarChar, 7
rstInfo.Fields.Append "sintoma", adVarChar, 50
rstInfo.Open
For c = 1 To grdDetalle.Rows - 1
    rstInfo.AddNew
    rstInfo!cliente = grdDetalle.TextMatrix(c, 0)
    rstInfo!mascota = grdDetalle.TextMatrix(c, 1)
    rstInfo!Cantidad = grdDetalle.TextMatrix(c, 2)
    rstInfo!sintoma = grdDetalle.TextMatrix(c, 3)
    rstInfo.Update
Next c
Set rptInfoEnferm.DataSource = rstInfo
rptInfoEnferm.Sections(1).Controls(6).Caption = Format(DtInicio.Value, "long date") & "  y  " & Format(DtFin.Value, "long date")
Select Case accion
    Case 1
        rptInfoEnferm.Show
    Case 2
        rptInfoEnferm.PrintReport True
        Unload rptInfoEnferm
End Select
End Sub

