VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmInternacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Internacion"
   ClientHeight    =   4785
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6930
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInternacion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   6930
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   2895
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4575
      Begin VB.TextBox txtJaula 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   1725
         TabIndex        =   29
         Top             =   1080
         Width           =   975
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   2445
         TabIndex        =   14
         Top             =   2400
         Width           =   1335
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   645
         TabIndex        =   13
         Top             =   2400
         Width           =   1335
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   330
         Left            =   2460
         TabIndex        =   8
         Top             =   600
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   393216
         BuddyControl    =   "txtPeso"
         BuddyDispid     =   196615
         OrigLeft        =   2880
         OrigTop         =   1080
         OrigRight       =   3120
         OrigBottom      =   1455
         Max             =   9999
         Enabled         =   -1  'True
      End
      Begin VB.TextBox txtTiempo 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   1
         EndProperty
         Height          =   330
         Left            =   2925
         TabIndex        =   7
         Text            =   "1"
         Top             =   2040
         Width           =   735
      End
      Begin VB.TextBox txtSintomas 
         Height          =   330
         Left            =   900
         MaxLength       =   50
         TabIndex        =   6
         Top             =   1560
         Width           =   3015
      End
      Begin VB.TextBox txtPeso 
         Alignment       =   1  'Right Justify
         Height          =   330
         Left            =   1725
         TabIndex        =   5
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox txtHora 
         Enabled         =   0   'False
         Height          =   330
         Left            =   2805
         TabIndex        =   2
         Text            =   "Text1"
         Top             =   120
         Width           =   1215
      End
      Begin VB.TextBox txtFecha 
         Enabled         =   0   'False
         Height          =   330
         Left            =   1005
         TabIndex        =   1
         Text            =   "Text1"
         Top             =   120
         Width           =   1215
      End
      Begin MSComCtl2.UpDown UpDown2 
         Height          =   330
         Left            =   3660
         TabIndex        =   9
         Top             =   2040
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtTiempo"
         BuddyDispid     =   196613
         OrigLeft        =   2880
         OrigTop         =   1080
         OrigRight       =   3120
         OrigBottom      =   1455
         Max             =   9999
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label lbljaula 
         Caption         =   "Jaula N�"
         Height          =   255
         Left            =   960
         TabIndex        =   28
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label lblTiempo 
         Caption         =   "Tiempo estimado de Internaci�n:                        dias."
         Height          =   255
         Left            =   165
         TabIndex        =   12
         Top             =   2040
         Width           =   4335
      End
      Begin VB.Label lblSintomas 
         Caption         =   "Motivo:"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   1560
         Width           =   615
      End
      Begin VB.Label lblPeso 
         Caption         =   "Peso:                          Kgrs."
         Height          =   255
         Left            =   1125
         TabIndex        =   10
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label lblHora 
         Caption         =   "Hora:"
         Height          =   255
         Left            =   2325
         TabIndex        =   4
         Top             =   120
         Width           =   495
      End
      Begin VB.Label lblFecha 
         Caption         =   "Fecha:"
         Height          =   255
         Left            =   405
         TabIndex        =   3
         Top             =   120
         Width           =   615
      End
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   4695
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   6975
      Begin VB.TextBox txtUbic 
         Enabled         =   0   'False
         Height          =   330
         Left            =   4425
         TabIndex        =   30
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton cmdVolver 
         Caption         =   "&Volver"
         Height          =   375
         Left            =   3998
         TabIndex        =   27
         Top             =   4250
         Width           =   1935
      End
      Begin VB.CommandButton cmdNuevaObs 
         Caption         =   "&Nueva Observaci�n"
         Height          =   375
         Left            =   998
         TabIndex        =   26
         Top             =   4250
         Width           =   1935
      End
      Begin MSFlexGridLib.MSFlexGrid grdObser 
         Height          =   2175
         Left            =   98
         TabIndex        =   25
         Top             =   1920
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   3836
         _Version        =   393216
         Rows            =   1
         Cols            =   9
         FixedRows       =   0
         FixedCols       =   0
         AllowUserResizing=   1
      End
      Begin VB.TextBox txtFechaInter 
         Enabled         =   0   'False
         Height          =   330
         Left            =   2078
         TabIndex        =   21
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox txtFechaAlta 
         Enabled         =   0   'False
         Height          =   330
         Left            =   5438
         TabIndex        =   20
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox txtMotivo 
         Enabled         =   0   'False
         Height          =   330
         Left            =   2438
         TabIndex        =   17
         Top             =   1080
         Width           =   4095
      End
      Begin VB.TextBox txtMascota 
         Enabled         =   0   'False
         Height          =   330
         Left            =   2010
         TabIndex        =   16
         Top             =   120
         Width           =   1560
      End
      Begin VB.Label Label1 
         Caption         =   "Jaula N�"
         Height          =   255
         Left            =   3705
         TabIndex        =   31
         Top             =   120
         Width           =   855
      End
      Begin VB.Label lblObservaciones 
         Caption         =   "Observaciones Realizadas:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   1560
         Width           =   2415
      End
      Begin VB.Label lblFechaAlta 
         Caption         =   "Fecha de Alta Tentativa:"
         Height          =   255
         Left            =   3398
         TabIndex        =   23
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label lblFechaInter 
         Caption         =   "Fecha de Internaci�n:"
         Height          =   255
         Left            =   278
         TabIndex        =   22
         Top             =   600
         Width           =   1815
      End
      Begin VB.Label lblMotivo 
         Caption         =   "Motivo de la Internaci�n:"
         Height          =   255
         Left            =   398
         TabIndex        =   19
         Top             =   1080
         Width           =   2055
      End
      Begin VB.Label lblMascota 
         Caption         =   "Mascota:"
         Height          =   255
         Left            =   1170
         TabIndex        =   18
         Top             =   120
         Width           =   735
      End
   End
End
Attribute VB_Name = "frmInternacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim xid_hc As Integer
Dim xPeso As Single
Dim internado As Boolean
Dim rst As ADODB.Recordset
Dim xNro_ficha As Integer
Dim cmd As ADODB.Command

Private Sub cmdaceptar_Click()
Dim op
op = Mensaje("�Desea guardar los datos ingresados?", Loro, Balloon, vbQuestion + vbYesNoCancel, , "getattention")
Select Case op
    Case vbYes
If Trim(txtPeso) = "" Or Trim(txtJaula) = "" Then Exit Sub
If Trim(txtSintomas) = "" Then
MsgRapido "Ingrese los s�ntomas.", Loro, vbExclamation, , "dontrecognize"
Exit Sub
End If
Dim alta As String
alta = Format(Date + txtTiempo, "yyyy-mm-d")
cmd.CommandText = "update mascota_hc set internado='v', fecha_internacion='" & DateDB & "',fecha_fininter='" & alta & "',motivo_internacion='" & txtSintomas & "' where id_hc=" & xid_hc
cmd.Execute
cmd.CommandText = "insert into detalle_hc values(" & xid_hc & ",'" & DateDB & "','" & TimeDB & "'," & txtPeso & ",'" & txtSintomas & "',4,0,' '," & xLegajo & ",' ','Inicio de Internaci�n')"
cmd.Execute
cmd.CommandText = "insert into ficha_internacion(nro_ficha,id_hc,peso,descripcion,id_medicamento,id_empleado,fecha,hora,patologia,evolucion,dosis,jaula) values(" & xNro_ficha & "," & xid_hc & "," & txtPeso & ",'Inicio de internaci�n',0," & xLegajo & ",'" & DateDB & "','" & TimeDB & "','','',''," & txtJaula & ")"
cmd.Execute
frmhistoria.loadGrid
frmhistoria.Reload
rst.Open
Set rptTurno.DataSource = rst
rptTurno.Sections(1).Controls(3).Caption = "Recibo de propriedad de " & frmhistoria.cmbMascota.List(frmhistoria.cmbMascota.ListIndex)
rptTurno.Sections(1).Controls(5).Caption = "Jaula N�: " & txtJaula
rptTurno.Sections(1).Controls(4).Caption = "Fecha de alta tentativa " & Date + txtTiempo
Unload Me
    Case vbNo
        frmhistoria.Enabled = True
        Unload Me
    Case vbCancel
        Exit Sub
End Select
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub cmdNuevaObs_Click()
frmInternacion.Enabled = False
frmNuevaObs.setID xid_hc, xPeso, xNro_ficha, Val(txtUbic)
frmNuevaObs.Show
End Sub

Private Sub cmdvolver_Click()
frmhistoria.Reload
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

'formulario: width 4650
'            height 3135
Private Sub Form_Load()
On Error GoTo procErr
frmInternacion.Top = 930
frmInternacion.Left = 2085
txtFecha = Date
txtHora = Time
If internado = False Then
Frame2.Visible = False
frmInternacion.Width = 4650
frmInternacion.Height = 3300
Else
Frame1.Visible = False
Me.Caption = "Ficha de Internaci�n"
End If
txtPeso = xPeso
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenDynamic
rst.LockType = adLockOptimistic
rst.Source = "select max(nro_ficha) from ficha_internacion"
rst.Open
xNro_ficha = rst(0).Value + 1
rst.Close
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
If internado = True Then
    rst.Source = "select nombre,fecha_internacion,fecha_fininter,motivo_internacion from mascota_hc where id_hc=" & xid_hc
    rst.Open
    txtMascota = rst!nombre
    txtFechaInter = rst!fecha_internacion
    txtFechaAlta = rst!fecha_fininter
    txtMotivo = rst!motivo_internacion
    rst.Close
    loadGrid
End If
procErr:
If Err.Number = 94 Then
    Resume Next
End If
End Sub
Sub setID(paramId_hc As Integer, paramPeso As String, paramInter As Boolean)
xid_hc = paramId_hc
xPeso = Val(paramPeso)
internado = paramInter
frmhistoria.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmhistoria.Enabled = True
Unload Me
End Sub

Private Sub txtPeso_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtPeso_Validate(Cancel As Boolean)
If IsNumeric(txtPeso) = False Then
MsgRapido "El dato no es v�lido.", Loro, vbCritical, , "surprised"
Cancel = True
txtPeso = xPeso
End If
End Sub

Private Sub txtTiempo_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
If KeyAscii <> 8 And KeyAscii <> 7 Then
KeyAscii = 0
End If
End If
End Sub

Private Sub txtTiempo_Validate(Cancel As Boolean)
If IsNumeric(txtTiempo) = False Then
MsgRapido "El dato no es v�lido.", Loro, vbCritical, , "surprised"
Cancel = True
txtTiempo = 1
End If
End Sub
Sub loadGrid()
grdObser.Clear
grdObser.Rows = 0
grdObser.AddItem "Fecha" & vbTab & "Hora" & vbTab & "Peso" & vbTab & "Patologia" & vbTab & "Observacion" & vbTab & "Medicacion" & vbTab & "Dosis" & vbTab & "Evoluci�n" & vbTab & "Empleado"
grdObser.Rows = 2
grdObser.FixedRows = 1
grdObser.Rows = 1
grdObser.ColWidth(0) = 1080
grdObser.ColWidth(1) = 1260
grdObser.ColWidth(2) = 1050
grdObser.ColWidth(3) = 1740
grdObser.ColWidth(4) = 1155
grdObser.ColWidth(5) = 1080
If rst.State = adStateOpen Then rst.Close
rst.Source = "select f.fecha,f.hora,f.peso,f.jaula,f.descripcion,m.nombre,e.apellido,f.patologia,f.evolucion,f.dosis from ficha_internacion f,medicamento m,empleado e where f.id_medicamento=m.id_medicamento and f.id_empleado=e.legajo and f.id_hc=" & xid_hc & " order by f.fecha,f.hora"
rst.Open
While rst.EOF = False
    grdObser.AddItem rst!fecha & vbTab & Right(rst(1).Value, 13) & vbTab & rst!peso & " Kgrs." & vbTab & rst!patologia & vbTab & rst!descripcion & vbTab & rst!nombre & vbTab & rst!dosis & vbTab & rst!evolucion & vbTab & rst!apellido
    rst.MoveNext
Wend
rst.MoveLast
txtUbic = rst!jaula
rst.Close
End Sub
Private Sub UpDown1_DownClick()
txtPeso = Val(txtPeso) - 0.1
End Sub

Private Sub UpDown1_UpClick()
txtPeso = Val(txtPeso) + 0.1
End Sub
