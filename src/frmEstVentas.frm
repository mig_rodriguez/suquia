VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmEstVentas 
   Caption         =   "Estad�sticas de Ventas Generales"
   ClientHeight    =   5250
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6915
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmEstVentas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5250
   ScaleWidth      =   6915
   Begin VB.Frame fraUltimo 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   375
      Left            =   2520
      TabIndex        =   14
      Top             =   1320
      Width           =   2655
      Begin VB.TextBox txtultimo 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   1
         EndProperty
         Height          =   330
         Left            =   720
         TabIndex        =   4
         Text            =   "1"
         Top             =   0
         Width           =   615
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   330
         Left            =   1335
         TabIndex        =   5
         Top             =   0
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtultimo"
         BuddyDispid     =   196610
         OrigLeft        =   2880
         OrigTop         =   960
         OrigRight       =   3120
         OrigBottom      =   1335
         Max             =   12
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label lblPeriodo 
         AutoSize        =   -1  'True
         Caption         =   "a�os."
         Height          =   225
         Left            =   1680
         TabIndex        =   16
         Top             =   0
         Width           =   465
      End
      Begin VB.Label lblUltimos 
         Caption         =   "�ltimos"
         Height          =   255
         Left            =   0
         TabIndex        =   15
         Top             =   0
         Width           =   735
      End
   End
   Begin VB.Frame fraPeriodo 
      Caption         =   "Ventas"
      Height          =   615
      Left            =   2520
      TabIndex        =   13
      Top             =   600
      Width           =   2655
      Begin VB.OptionButton optAnios 
         Caption         =   "Por A�os"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.OptionButton optMeses 
         Caption         =   "Por Meses"
         Height          =   255
         Left            =   1320
         TabIndex        =   3
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   3975
      Index           =   1
      Left            =   200
      TabIndex        =   8
      Top             =   600
      Width           =   6495
      Begin VB.Frame Frame2 
         Caption         =   "Tipo de Gr�fico"
         Height          =   855
         Left            =   120
         TabIndex        =   17
         Top             =   0
         Width           =   1935
         Begin VB.CheckBox chkApilado 
            Caption         =   "Datos Apilados"
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   240
            Value           =   1  'Checked
            Width           =   1575
         End
         Begin VB.CheckBox chkPorcentajes 
            Caption         =   "Porcentajes"
            Height          =   255
            Left            =   120
            TabIndex        =   18
            Top             =   480
            Width           =   1335
         End
      End
      Begin MSChart20Lib.MSChart Grafico1 
         Height          =   3255
         Left            =   -240
         OleObjectBlob   =   "frmEstVentas.frx":1272
         TabIndex        =   9
         Top             =   840
         Width           =   6735
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2400
      TabIndex        =   0
      Top             =   4800
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   3975
      Index           =   2
      Left            =   200
      TabIndex        =   10
      Top             =   600
      Width           =   6495
      Begin VB.CommandButton cmdQuitar 
         Caption         =   "Quitar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   600
         Width           =   1215
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "Agregar"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   120
         Width           =   1215
      End
      Begin MSChart20Lib.MSChart Grafico2 
         Height          =   3135
         Left            =   -240
         OleObjectBlob   =   "frmEstVentas.frx":3533
         TabIndex        =   11
         Top             =   840
         Width           =   6735
      End
      Begin VB.ListBox lstItems 
         Height          =   960
         Left            =   360
         TabIndex        =   12
         Top             =   2400
         Visible         =   0   'False
         Width           =   1335
      End
   End
   Begin MSComctlLib.TabStrip Strip 
      Height          =   4575
      Left            =   100
      TabIndex        =   1
      Top             =   120
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   8070
      MultiRow        =   -1  'True
      TabMinWidth     =   18
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Composici�n seg�n Rubro"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Evoluci�n seg�n Item"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmEstVentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As ADODB.Recordset
Private mintCurFrame As Integer

Private Sub chkApilado_Click()
If chkApilado = 1 Then Grafico1.Stacking = True Else Grafico1.Stacking = False
End Sub

Private Sub chkPorcentajes_Click()
If chkPorcentajes = 0 Then
    graficar
Else
    Dim c, i
    Dim total As Double
    Grafico1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "%"
    For c = 1 To Grafico1.RowCount
        total = Val(Grafico1.ChartData(c, 2)) + Val(Grafico1.ChartData(c, 3)) + Val(Grafico1.ChartData(c, 4))
        Grafico1.Row = c
        For i = 1 To 3
            Grafico1.Column = i
            If total = 0 Then Grafico1.Data = 0 Else Grafico1.Data = Val(Grafico1.Data) * 100 / total
        Next i
    Next c
End If
End Sub

Private Sub cmdaceptar_Click()
Unload Me
End Sub

Private Sub cmdagregar_Click()
frmAgregar.mostrar Me
frmAgregar.UpDown1.Visible = False
frmAgregar.txtCantidad.Visible = False
frmAgregar.lblCantidad.Visible = False
frmAgregar.cmdAceptar.Top = 1080
frmAgregar.cmdCancelar.Top = 1080
frmAgregar.Height = 2000
frmAgregar.lblStock.Visible = False
frmAgregar.Show
End Sub

Private Sub cmdQuitar_Click()
lstItems.RemoveItem Grafico2.ColumnCount - 1
Grafico2.ColumnCount = Grafico2.ColumnCount - 1
graficar2
cmdAgregar.Enabled = True
If Grafico2.ColumnCount = 0 Then cmdQuitar.Enabled = False
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
Me.Width = 7725
Me.Height = 6150
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.LockType = adLockReadOnly
rst.CursorType = adOpenForwardOnly
graficar
End Sub

Private Sub Form_Resize()
If Me.Width < 7725 And Me.WindowState = 0 Then Me.Width = 7725
If Me.Height < 6150 And Me.WindowState = 0 Then Me.Height = 6150
On Error Resume Next
cmdAceptar.Left = (Me.ScaleWidth - cmdAceptar.Width) / 2
cmdAceptar.Top = Me.ScaleHeight - cmdAceptar.Height
Strip.Width = Me.ScaleWidth - 200
Strip.Height = Me.ScaleHeight - 600
Frame1(1).Width = Strip.Width - 110
Frame1(1).Height = Strip.Height - 510
Frame1(2).Width = Strip.Width - 110
Frame1(2).Height = Strip.Height - 510
fraPeriodo.Left = (Me.ScaleWidth - fraPeriodo.Width) / 2
fraUltimo.Left = (Me.ScaleWidth - fraUltimo.Width) / 2
Grafico1.Width = Me.ScaleWidth
Grafico1.Height = Frame1(1).Height * 0.85
Grafico2.Width = Me.ScaleWidth
Grafico2.Height = Frame1(2).Height * 0.85
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub optAnios_Click()
lblPeriodo.Caption = "a�os."
If mintCurFrame = 1 Then graficar Else graficar2
End Sub

Private Sub optMeses_Click()
lblPeriodo.Caption = "meses."
If mintCurFrame = 1 Then graficar Else graficar2
End Sub

Private Sub Strip_Click()
If Strip.SelectedItem.Index = mintCurFrame Then Exit Sub
Frame1(Strip.SelectedItem.Index).Visible = True
Frame1(mintCurFrame).Visible = False
mintCurFrame = Strip.SelectedItem.Index
If mintCurFrame = 1 Then graficar Else graficar2
End Sub

Private Sub txtultimo_Change()
If Trim(txtultimo) = "" Then Exit Sub
If txtultimo > 12 Then txtultimo = "12"
If txtultimo < 1 Then txtultimo = "1"
If mintCurFrame = 1 Then graficar Else graficar2
End Sub

Private Sub txtultimo_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub graficar()
Dim c, i
Me.MousePointer = 11
Dim totalRow As Double
Dim xFecha As String
Grafico1.RowCount = txtultimo
If optAnios = True Then
    xFecha = Year(Date) - txtultimo + 1 & "-01-01"
Else
    If (Month(Date) - txtultimo + 1) < 1 Then xFecha = Year(Date) - 1 & "-" & Month(Date) - txtultimo + 13 & "-01" Else xFecha = Year(Date) & "-" & Month(Date) - txtultimo + 1 & "-01"
End If
rst.Source = "select f.fecha,d.tipo_item,sum(d.precio) as monto from detalle_factura d,factura f  where f.nro_factura=d.nro_factura and fecha<='" & DateDB & "' and fecha>='" & xFecha & "' group by d.tipo_item,f.fecha order by f.fecha,d.tipo_item"
rst.Open
i = Grafico1.RowCount - 1
Grafico1.RowLabelIndex = 1
For c = 1 To Grafico1.RowCount
    Grafico1.Row = c
    If optAnios = True Then
        Grafico1.RowLabel = Year(Date) - i
    Else
        If Month(Date) - i < 1 Then
            Grafico1.RowLabel = MonthName(Month(Date) - i + 12, True)
        Else
            Grafico1.RowLabel = MonthName(Month(Date) - i, True)
        End If
    End If
    i = i - 1
Next c
For c = 1 To Grafico1.RowCount
    For i = 1 To 3
        Grafico1.Row = c
        Grafico1.Column = i
        Grafico1.Data = 0
    Next i
Next c
While Not rst.EOF
    If optMeses = True Then
        If Val(Right(rst!fecha, 4)) = Year(Date) - 1 Then
            Grafico1.Row = Mid(rst!fecha, 4, 2) - Month(Date) + txtultimo - 12
        Else
            Grafico1.Row = Mid(rst!fecha, 4, 2) - Month(Date) + txtultimo
        End If
    Else
        Grafico1.Row = Year(rst!fecha) - Year(Date) + txtultimo
    End If
    Grafico1.Column = rst!tipo_item
    Grafico1.Data = Val(Grafico1.Data) + Val(rst!monto)
    rst.MoveNext
Wend
Grafico1.RowLabelIndex = 2
For c = 1 To Grafico1.RowCount
    Grafico1.Row = c
    totalRow = 0
    For i = 1 To 3
        Grafico1.Column = i
        totalRow = totalRow + Val(Grafico1.Data)
    Next i
    Grafico1.RowLabel = "$" & totalRow
Next c
rst.Close
Grafico1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "Monto($)"
Me.MousePointer = 0
If chkPorcentajes = 1 Then chkPorcentajes_Click
End Sub
Private Sub graficar2()
Dim c, i
Dim xFecha As String
'Armar la cadena de la fecha
If optAnios = True Then
    xFecha = Year(Date) - txtultimo + 1 & "-01-01"
Else
    If (Month(Date) - txtultimo + 1) < 1 Then xFecha = Year(Date) - 1 & "-" & Month(Date) - txtultimo + 13 & "-01" Else xFecha = Year(Date) & "-" & Month(Date) - txtultimo + 1 & "-01"
End If
'Dividir los periodos de tiempo y nombrarlos
Grafico2.RowCount = txtultimo
i = Grafico2.RowCount - 1
Grafico2.RowLabelIndex = 1
For c = 1 To Grafico2.RowCount
    Grafico2.Row = c
    If optAnios = True Then
        Grafico2.RowLabel = Year(Date) - i
    Else
        If Month(Date) - i < 1 Then
            Grafico2.RowLabel = MonthName(Month(Date) - i + 12, True)
        Else
            Grafico2.RowLabel = MonthName(Month(Date) - i, True)
        End If
    End If
    i = i - 1
Next c
'limpiar el grafico
For c = 1 To Grafico2.RowCount
    For i = 1 To Grafico2.ColumnCount
        Grafico2.Row = c
        Grafico2.Column = i
        Grafico2.Data = 0
    Next i
Next c
'Armar el recordset
If lstItems.ListCount = 0 Then Exit Sub
rst.Source = "select f.fecha,d.tipo_item,d.id_item,sum(d.precio) as monto from factura f, detalle_factura d where f.nro_factura=d.nro_factura and f.fecha<='" & DateDB & "' and f.fecha>='" & xFecha & "' and ("
For c = 0 To lstItems.ListCount - 1
    If c > 0 Then rst.Source = rst.Source & " or "
    rst.Source = rst.Source & " (d.tipo_item=" & Val(lstItems.List(c)) & " and d.id_item=" & lstItems.ItemData(c) & ")"
Next c
If optMeses = True Then
    rst.Source = rst.Source & ") group by mid(f.fecha,6,2),d.id_item,d.tipo_item order by f.fecha,d.tipo_item,d.id_item"
Else
    rst.Source = rst.Source & ") group by mid(f.fecha,1,4),d.id_item,d.tipo_item order by f.fecha,d.tipo_item,d.id_item"
End If
rst.Open
'Cargar los Datos
While Not rst.EOF
    'determinar el periodo de tiempo
    If optMeses = True Then
        If Val(Right(rst!fecha, 4)) = Year(Date) - 1 Then
            Grafico2.Row = Mid(rst!fecha, 4, 2) - Month(Date) + txtultimo - 12
        Else
            Grafico2.Row = Mid(rst!fecha, 4, 2) - Month(Date) + txtultimo
        End If
    Else
        Grafico2.Row = Right(rst!fecha, 4) - Year(Date) + txtultimo
    End If
    'Determinar el item
    For c = 0 To lstItems.ListCount - 1
        If Val(lstItems.List(c)) = rst!tipo_item And lstItems.ItemData(c) = rst!id_item Then Grafico2.Column = c + 1
    Next c
    'Establecer el valor
    Grafico2.Data = Val(Grafico2.Data) + Val(rst!monto)
    rst.MoveNext
Wend
rst.Close
Grafico2.Plot.Axis(VtChAxisIdY).AxisTitle.Text = "Monto($)"
End Sub
Sub agregar(xTipo_Item As Integer, xId_Item As Integer, xCantidad As Integer)
Dim c
For c = 0 To lstItems.ListCount - 1
    If Val(lstItems.List(c)) = xTipo_Item And lstItems.ItemData(c) = xId_Item Then
        MsgRapido "El Item ya est� en el gr�fico.", Loro, vbInformation, , "alert"
        Me.Enabled = True
        Exit Sub
    End If
Next c
lstItems.AddItem xTipo_Item
lstItems.ItemData(lstItems.NewIndex) = xId_Item
Select Case xTipo_Item
    Case 1
        rst.Source = "select nombre from servicio where id_servicio=" & xId_Item
    Case 2
        rst.Source = "select nombre from medicamento where id_medicamento=" & xId_Item
    Case 3
        rst.Source = "select nombre from articulo where id_articulo=" & xId_Item
End Select
rst.Open
Grafico2.ColumnCount = Grafico2.ColumnCount + 1
Grafico2.Column = Grafico2.ColumnCount
Grafico2.ColumnLabel = rst!nombre
rst.Close
If lstItems.ListCount = 10 Then cmdAgregar.Enabled = False
Me.Enabled = True
cmdQuitar.Enabled = True
graficar2
End Sub

