VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmInfoTurnos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de Turnos"
   ClientHeight    =   5295
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6285
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInfoTurnos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5295
   ScaleWidth      =   6285
   Begin MSComctlLib.ImageList imgLst 
      Left            =   5160
      Top             =   4320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker dtFecha 
      Height          =   375
      Left            =   4680
      TabIndex        =   7
      Top             =   480
      Width           =   1320
      _ExtentX        =   2328
      _ExtentY        =   661
      _Version        =   393216
      Format          =   24576001
      CurrentDate     =   37815
      MinDate         =   32874
   End
   Begin VB.ComboBox cmbTipoTurno 
      Height          =   345
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   480
      Width           =   1935
   End
   Begin VB.CommandButton cmdPreview 
      Caption         =   "&Vista Previa"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3495
      TabIndex        =   4
      Top             =   4320
      Width           =   1455
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1335
      TabIndex        =   3
      Top             =   4320
      Width           =   1455
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   2175
      TabIndex        =   2
      Top             =   4800
      Width           =   1935
   End
   Begin MSFlexGridLib.MSFlexGrid grdInforme 
      Height          =   3135
      Left            =   255
      TabIndex        =   1
      Top             =   960
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   5530
      _Version        =   393216
      Rows            =   0
      Cols            =   8
      FixedRows       =   0
      FixedCols       =   0
      BackColorSel    =   -2147483643
      ForeColorSel    =   -2147483640
      FillStyle       =   1
      MergeCells      =   3
      AllowUserResizing=   1
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   4080
      TabIndex        =   8
      Top             =   480
      Width           =   615
   End
   Begin VB.Label lblTipoTurno 
      Caption         =   "Tipo de Turno:"
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblTitulo 
      Caption         =   "Informe de Turnos "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2115
      TabIndex        =   0
      Top             =   120
      Width           =   2055
   End
End
Attribute VB_Name = "frmInfoTurnos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Enum Order
    orderAsc = 1
    orderDesc = 2
End Enum
Dim lastOrder As Order
Dim lastSort As Integer
Dim rst As ADODB.Recordset
Private Sub cmbTipoTurno_Click()
If Me.Visible = True Then Reload
End Sub

Private Sub cmdaceptar_Click()
Unload Me
End Sub

Private Sub cmdImprimir_Click()
imprimir
End Sub

Private Sub cmdPreview_Click()
preview
End Sub
Private Sub dtFecha_Change()
Reload
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
frmPrincipal.vista.Enabled = True
frmPrincipal.imprimir.Enabled = True
End Sub
Sub preview()
info 1
End Sub
Sub imprimir()
info 2
End Sub
Sub info(ByVal accion As Integer)
Dim informe As ADODB.Recordset
Set informe = New ADODB.Recordset
informe.Fields.Append "hora", adVarChar, 30
informe.Fields.Append "servicio", adVarChar, 50
informe.Fields.Append "cliente", adVarChar, 50
informe.Fields.Append "mascota", adVarChar, 30
informe.Fields.Append "fecha_sol", adVarChar, 30
informe.Fields.Append "motivo", adVarChar, 100
informe.Fields.Append "direccion", adVarChar, 100
informe.LockType = adLockOptimistic
informe.CursorType = adOpenDynamic
informe.Open
rst.MoveFirst
While Not rst.EOF
    informe.AddNew
    informe!hora = Right(rst!hora, 13)
    informe!servicio = rst(1).Value
    informe!cliente = rst(2).Value & ", " & rst(3).Value
    informe!mascota = rst(4).Value
    informe!fecha_sol = rst(5).Value
    informe!motivo = rst!motivo
    informe!direccion = rst!calle & " " & rst!numero & ", Dpto. " & rst!depto & ", Piso " & rst!piso
    informe.Update
    rst.MoveNext
Wend
Set rptInfoTurnos.DataSource = informe
rptInfoTurnos.Sections(1).Controls(5).Caption = Format(dtFecha.Value, "long date")
frmPrincipal.desactivararch
Select Case accion
    Case 1
        rptInfoTurnos.Show
    Case 2
        rptInfoTurnos.PrintReport True
        Unload rptInfoTurnos
End Select
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
imgLst.ListImages.Add 1, , LoadPicture(App.Path & "\bitmaps\Asc.bmp")
imgLst.ListImages.Add 2, , LoadPicture(App.Path & "\bitmaps\desc.bmp")
imgLst.MaskColor = RGB(192, 192, 192)
With cmbTipoTurno
.AddItem "Todos los turnos"
.ItemData(cmbTipoTurno.NewIndex) = 0
.AddItem "Att. a Domicilio"
.ItemData(cmbTipoTurno.NewIndex) = 3
.AddItem "Peluquer�a"
.ItemData(cmbTipoTurno.NewIndex) = 9
.AddItem "Cirug�a"
.ItemData(cmbTipoTurno.NewIndex) = 8
.ListIndex = 0
End With
dtFecha.Value = Date
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenDynamic
rst.LockType = adLockReadOnly
Reload
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmPrincipal.desactivararch
End Sub

Private Sub grdInforme_Click()
Dim xcol As Integer
If grdInforme.Col = 5 Or grdInforme.Col = 6 Then Exit Sub
If grdInforme.MouseRow <> 0 Then Exit Sub
xcol = grdInforme.Col
grdInforme.Row = 0
grdInforme.Col = 0
grdInforme.ColSel = 4
Set grdInforme.CellPicture = Nothing
With grdInforme
.Col = xcol
.MergeCol(0) = False
.MergeCol(1) = False
.MergeCol(2) = False
.MergeCol(3) = False
.MergeCol(4) = False
.MergeCol(.Col) = True
End With
If lastSort = grdInforme.Col Then
    If lastOrder = orderDesc Then
        grdInforme.Sort = 1
        lastOrder = orderAsc
        Set grdInforme.CellPicture = imgLst.ListImages(1).Picture
    Else
        grdInforme.Sort = 2
        lastOrder = orderDesc
        Set grdInforme.CellPicture = imgLst.ListImages(2).Picture
    End If
Else
    grdInforme.Sort = 1
    lastOrder = orderAsc
    Set grdInforme.CellPicture = imgLst.ListImages(1).Picture
End If
lastSort = grdInforme.Col
End Sub

Private Sub Reload()
If rst.State = 1 Then rst.Close
If cmbTipoTurno.ItemData(cmbTipoTurno.ListIndex) = 0 Then
    rst.Source = "select t.hora,s.nombre,c.apellido,c.nombre,m.nombre,t.fecha_solicitud,t.motivo,c.calle,c.numero,c.piso,c.depto,if(t.estado=1,'Reservado',if(t.estado=2,'Cancelado',if(t.estado=3,'Utilizado','No Utilizado'))) as estado from turno t,mascota_hc m,cliente c,servicio s where t.id_servicio=s.id_servicio and t.id_hc=m.id_hc and m.id_cliente=c.id_cliente and t.fecha='" & Format(dtFecha.Value, "yyyy-mm-d") & "' order by t.hora"
Else
    rst.Source = "select t.hora,s.nombre,c.apellido,c.nombre,m.nombre,t.fecha_solicitud,t.motivo,c.calle,c.numero,c.piso,c.depto,if(t.estado=1,'Reservado',if(t.estado=2,'Cancelado',if(t.estado=3,'Utilizado','No Utilizado'))) as estado from turno t,mascota_hc m,cliente c,servicio s where t.id_servicio=s.id_servicio and t.id_hc=m.id_hc and m.id_cliente=c.id_cliente and t.fecha='" & Format(dtFecha.Value, "yyyy-mm-d") & "' and t.id_servicio=" & cmbTipoTurno.ItemData(cmbTipoTurno.ListIndex) & " order by t.hora"
End If
rst.Open
With grdInforme
.Clear
.Rows = 2
.FixedRows = 0
.Rows = 0
.AddItem "Hora" & vbTab & "Servicio" & vbTab & "Cliente" & vbTab & "Mascota" & vbTab & "Fecha de Solicitud" & vbTab & "Estado" & vbTab & "Motivo" & vbTab & "Direcci�n"
.Rows = 2
.FixedRows = 1
.Rows = 1
.ColWidth(0) = 1290
.ColWidth(1) = 1845
.ColWidth(2) = 1440
.ColWidth(3) = 1080
.ColWidth(4) = 1770
.Row = 0
.Col = 0
.ColSel = 4
.CellPictureAlignment = 7
.ColSel = 0
.MergeCol(0) = True
End With
If rst.RecordCount <> 0 Then
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
        cmdImprimir.Enabled = True
        cmdPreview.Enabled = True
        While Not rst.EOF
            grdInforme.AddItem Right(rst!hora, 13) & vbTab & rst(1).Value & vbTab & rst(2).Value & ", " & rst(3).Value & vbTab & rst(4).Value & vbTab & rst(5).Value & vbTab & rst!estado & vbTab & rst!motivo & vbTab & rst!calle & " " & rst!numero & ", Dpto. " & rst!depto & ", Piso " & rst!piso
            rst.MoveNext
        Wend
        grdInforme.Row = 0
        grdInforme.Col = 0
        Set grdInforme.CellPicture = imgLst.ListImages(1).Picture
        rst.Close
Else
        'MsgRapido "No existen datos", Loro, vbInformation, , "explain"
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = False
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = False
        cmdImprimir.Enabled = False
        cmdPreview.Enabled = False
        grdInforme.Rows = 1
End If
End Sub
