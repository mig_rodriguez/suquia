VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmActProveedor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Proveedores"
   ClientHeight    =   5760
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7770
   Icon            =   "frmActProveedor.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   7770
   Begin TabDlg.SSTab SSTab1 
      Height          =   4695
      Left            =   30
      TabIndex        =   24
      Top             =   30
      Width           =   7665
      _ExtentX        =   13520
      _ExtentY        =   8281
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Actualizaci�n"
      TabPicture(0)   =   "frmActProveedor.frx":1272
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label1(14)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label1(15)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label3"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label4"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label5"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label6"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdPrimero"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "CmdAtras"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "CmdAdelante"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "CmdUltimo"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Frame2"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Text1(0)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Text1(12)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Text1(13)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Text1(1)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Text1(3)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Text1(10)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Combo1"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).ControlCount=   19
      TabCaption(1)   =   "Listado"
      TabPicture(1)   =   "frmActProveedor.frx":128E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Gridprovee"
      Tab(1).ControlCount=   1
      Begin MSFlexGridLib.MSFlexGrid Gridprovee 
         Height          =   3975
         Left            =   -74880
         TabIndex        =   39
         Top             =   480
         Width           =   7365
         _ExtentX        =   12991
         _ExtentY        =   7011
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.ComboBox Combo1 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   1560
         Width           =   2535
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   10
         Left            =   5940
         MaxLength       =   11
         TabIndex        =   6
         Top             =   1080
         Width           =   1515
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   5940
         MaxLength       =   25
         TabIndex        =   8
         Top             =   1560
         Width           =   1515
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   1800
         MaxLength       =   20
         TabIndex        =   5
         Top             =   1080
         Width           =   2535
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   13
         Left            =   1320
         MaxLength       =   30
         TabIndex        =   16
         Top             =   4200
         Width           =   3020
      End
      Begin VB.TextBox Text1 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   11274
            SubFormatType   =   0
         EndProperty
         Enabled         =   0   'False
         Height          =   330
         Index           =   12
         Left            =   6000
         MaxLength       =   13
         TabIndex        =   17
         Top             =   4200
         Width           =   1455
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   4320
         MaxLength       =   29
         TabIndex        =   4
         Top             =   480
         Width           =   3135
      End
      Begin VB.Frame Frame2 
         Caption         =   "Domicilio"
         Height          =   1935
         Left            =   120
         TabIndex        =   23
         Top             =   2040
         Width           =   7440
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   8
            Left            =   1200
            MaxLength       =   20
            TabIndex        =   14
            Top             =   1320
            Width           =   3020
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   4
            Left            =   1200
            MaxLength       =   20
            TabIndex        =   9
            Top             =   360
            Width           =   3020
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   5
            Left            =   1200
            MaxLength       =   6
            TabIndex        =   11
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox Text1 
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "0"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   11274
               SubFormatType   =   0
            EndProperty
            Enabled         =   0   'False
            Height          =   330
            Index           =   6
            Left            =   2400
            MaxLength       =   4
            TabIndex        =   12
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   7
            Left            =   3600
            MaxLength       =   5
            TabIndex        =   13
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   9
            Left            =   5880
            MaxLength       =   8
            TabIndex        =   10
            Top             =   360
            Width           =   1455
         End
         Begin VB.TextBox Text1 
            Enabled         =   0   'False
            Height          =   330
            Index           =   11
            Left            =   5880
            MaxLength       =   8
            TabIndex        =   15
            Top             =   1320
            Width           =   1455
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Calle:"
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   31
            Top             =   360
            Width           =   390
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Torre:"
            Height          =   195
            Index           =   7
            Left            =   240
            TabIndex        =   30
            Top             =   840
            Width           =   420
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Localidad:"
            Height          =   195
            Index           =   8
            Left            =   240
            TabIndex        =   29
            Top             =   1320
            Width           =   735
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Dpto.:"
            Height          =   195
            Index           =   9
            Left            =   3120
            TabIndex        =   28
            Top             =   840
            Width           =   435
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Piso:"
            Height          =   195
            Index           =   10
            Left            =   1920
            TabIndex        =   27
            Top             =   840
            Width           =   345
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Nro.:"
            Height          =   195
            Index           =   11
            Left            =   4680
            TabIndex        =   26
            Top             =   360
            Width           =   345
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Postal:"
            Height          =   195
            Index           =   13
            Left            =   4560
            TabIndex        =   25
            Top             =   1320
            Width           =   1020
         End
      End
      Begin VB.CommandButton CmdUltimo 
         Height          =   375
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton CmdAdelante 
         Height          =   375
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton CmdAtras 
         Height          =   375
         Left            =   840
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   480
         Width           =   375
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   360
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   480
         Width           =   375
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Condici�n Impositiva:"
         Height          =   195
         Left            =   240
         TabIndex        =   38
         Top             =   1560
         Width           =   1500
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Cuenta Bancaria:"
         Height          =   195
         Left            =   4560
         TabIndex        =   37
         Top             =   1560
         Width           =   1230
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "CUIT:"
         Height          =   195
         Left            =   4560
         TabIndex        =   36
         Top             =   1080
         Width           =   420
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Rubro:"
         Height          =   195
         Left            =   240
         TabIndex        =   35
         Top             =   1080
         Width           =   480
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "E-mail:"
         Height          =   195
         Index           =   15
         Left            =   240
         TabIndex        =   34
         Top             =   4200
         Width           =   465
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tel�fono:"
         Height          =   195
         Index           =   14
         Left            =   4680
         TabIndex        =   33
         Top             =   4200
         Width           =   675
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Razon Social:"
         Height          =   195
         Left            =   3000
         TabIndex        =   32
         Top             =   600
         Width           =   990
      End
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2160
      TabIndex        =   21
      Top             =   5280
      Width           =   1575
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4080
      TabIndex        =   22
      Top             =   5280
      Width           =   1575
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   4920
      TabIndex        =   20
      Top             =   4800
      Width           =   1575
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   3120
      TabIndex        =   19
      Top             =   4800
      Width           =   1575
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   1275
      TabIndex        =   18
      Top             =   4800
      Width           =   1575
   End
End
Attribute VB_Name = "frmActProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private xid_proveedor As Integer
Dim busqueda As String
Dim rst As ADODB.Recordset
Dim band As Boolean

Private Sub cmdAdelante_Click()
If rst.RecordCount > 0 Then
  If rst.EOF <> True Then: rst.MoveNext
   If rst.EOF <> True Then
   Else
   rst.MoveFirst
   End If
  cmdPrimero.Enabled = True
  CmdUltimo.Enabled = True
  Refrescar
End If
End Sub

Private Sub cmdAtras_Click()
If rst.RecordCount > 0 Then
  If rst.BOF <> True Then: rst.MovePrevious
   If rst.BOF <> True Then
   Else
   rst.MoveLast
   End If
  cmdPrimero.Enabled = True
  CmdUltimo.Enabled = True
  Refrescar
End If
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Private Sub cmdPrimero_Click()
If rst.RecordCount > 0 Then
  cmdPrimero.Enabled = False
  CmdUltimo.Enabled = True
  rst.MoveFirst
  Refrescar
End If
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub

Private Sub cmdUltimo_Click()
If rst.RecordCount > 0 Then
  rst.MoveLast
  CmdUltimo.Enabled = False
  cmdPrimero.Enabled = True
  Refrescar
End If
End Sub

Private Sub Form_Activate()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
If SSTab1.Tab = 1 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
 Else
    If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
     Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
SSTab1.Tab = 0
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.Source = "select * from proveedor where borrado = 'f' order by id_proveedor"
rst.CursorType = adOpenDynamic
rst.LockType = adLockOptimistic
rst.Open
band = False
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
CmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
CmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
CmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
Combo1.AddItem "Monotributo"
Combo1.AddItem "Resp Inscripto"
Combo1.AddItem "Exento"
Combo1.AddItem "Cons Final"
If rst.RecordCount > 0 Then
    busqueda = ""
    Refrescar
    loadGrid
  Else
    MsgBox ("no hay datos para mostrar")
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
     Else
        Loro.Stop
        rst.CancelUpdate
        restablecer
        If rst.RecordCount > 0 Then
          rst.MoveFirst
          Refrescar
        End If
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
rst.Close
End Sub

Private Sub gridprovee_DblClick()
band = True
Me.SSTab1_Click 0
SSTab1 = 0
Me.SSTab1_Click 1
End Sub

Private Sub Refrescar()
Dim c
For c = 0 To 13
    Text1(0) = ""
Next
Text1(0) = rst!razon_social

On Error GoTo err1
Text1(7) = rst!depto

On Error GoTo err2
Text1(13) = rst!Email

On Error GoTo err3
Text1(1) = rst!rubro

On Error GoTo err4
Text1(4) = rst!calle

On Error GoTo err5
Text1(8) = rst!localidad

On Error GoTo err6
Text1(3) = rst!cta_bancaria

On Error GoTo err7
Text1(5) = rst!torre

On Error GoTo err8
Text1(9) = rst!numero

On Error GoTo err9
Text1(11) = rst!cod_postal

On Error GoTo 0

If Not rst!piso Then
  Text1(6) = rst!piso
 Else
  Text1(6) = ""
End If

If Not rst!cuit Then
  Text1(10) = rst!cuit
 Else
  Text1(10) = ""
End If

If Not rst!tel Then
  Text1(12) = rst!tel
 Else
  Text1(12) = ""
End If

Select Case rst!cond_impositiva
    Case 1
        Combo1.ListIndex = 0
    Case 2
        Combo1.ListIndex = 1
    Case 3
        Combo1.ListIndex = 2
    Case 4
        Combo1.ListIndex = 3
End Select

err1:
 If Err.Number = 94 Then
   Text1(7) = ""
   Resume Next
 End If
err2:
 If Err.Number = 94 Then
   Text1(13) = ""
   Resume Next
 End If
err3:
 If Err.Number = 94 Then
   Text1(1) = ""
   Resume Next
 End If
err4:
 If Err.Number = 94 Then
   Text1(4) = ""
   Resume Next
 End If
err5:
 If Err.Number = 94 Then
   Text1(8) = ""
    Resume Next
 End If
err6:
 If Err.Number = 94 Then
   Text1(3) = ""
    Resume Next
 End If
err7:
 If Err.Number = 94 Then
   Text1(5) = ""
    Resume Next
 End If
err8:
 If Err.Number = 94 Then
   Text1(9) = ""
    Resume Next
 End If
err9:
 If Err.Number = 94 Then
   Text1(11) = ""
    Resume Next
 End If
 
End Sub

Private Sub loadGrid()
Dim regActual As Long
regActual = rst.AbsolutePosition
rst.MoveFirst
Gridprovee.ColWidth(0) = 350
Gridprovee.ColWidth(1) = 3000
Gridprovee.ColWidth(2) = 1900
Gridprovee.ColWidth(3) = 1170
Gridprovee.ColWidth(4) = 2565
Gridprovee.Rows = 0
Gridprovee.AddItem "  Nro." & vbTab & "Raz�n Social" & vbTab & "Rubro" & vbTab & "Tel�fono" & vbTab & "E-Mail"
Gridprovee.Rows = 2
Gridprovee.FixedRows = 1
Gridprovee.Rows = 1
While rst.EOF = False
Gridprovee.AddItem rst!id_proveedor & vbTab & rst!razon_social & vbTab & rst!rubro & vbTab & rst!tel & vbTab & rst!Email
rst.MoveNext
Wend
rst.AbsolutePosition = regActual
End Sub

Sub SSTab1_Click(PreviousTab As Integer)
If band = True Then
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rst.MoveFirst
    rst.Find "id_proveedor = " & Gridprovee.TextMatrix(Gridprovee.Row, 0)
    Refrescar
    band = False
Else
If SSTab1.Tab = 1 Then
   cmdNuevo.Enabled = False
   cmdModificar.Enabled = False
   cmdBorrar.Enabled = False
   frmPrincipal.desactivararch
   frmPrincipal.imprimir.Enabled = True
   frmPrincipal.vista.Enabled = True
   frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
   frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
 Else
   cmdNuevo.Enabled = True
   cmdModificar.Enabled = True
   cmdBorrar.Enabled = True
   frmPrincipal.activararch
   'If rst.RecordCount > 0 Then
    ' rst.AbsolutePosition = Gridprovee.Row
    ' Refrescar
 End If
End If
End Sub

Sub nuevo()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
SSTab1.TabEnabled(1) = False
cmdPrimero.Enabled = False
CmdAtras.Enabled = False
CmdAdelante.Enabled = False
CmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 0 To 13
  If c = 2 Then
     c = c + 1
  End If
  Text1(c).Enabled = True
  Text1(c).Text = ""
Next c
Combo1.Enabled = True
Combo1.ListIndex = 0
If rst.RecordCount > 0 Then
  rst.MoveLast
  xid_proveedor = nuevoid("id_proveedor", "proveedor")
 Else
  xid_proveedor = 1
End If
Loro.Play "writing"
rst.AddNew
End Sub

Sub modificar()
If rst.RecordCount > 0 Then
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
SSTab1.TabEnabled(1) = False
cmdPrimero.Enabled = False
CmdAtras.Enabled = False
CmdAdelante.Enabled = False
CmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 0 To 13
  If c = 2 Then
     c = c + 1
  End If
  Text1(c).Enabled = True
Next c
Combo1.Enabled = True
xid_proveedor = rst!id_proveedor
Loro.Play "writing"
End If
End Sub

Sub cerrar()
Unload Me
End Sub
Sub guardar()
Dim c
For c = 0 To 13
    If c = 2 Then
       c = c + 1
    End If
    If c = 3 Or c = 5 Or c = 6 Or c = 7 Or c = 12 Or c = 13 Then: GoTo sig
    If Text1(c) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
sig:
Next c
Loro.Stop
rst!id_proveedor = xid_proveedor
rst!razon_social = Text1(0).Text
rst!rubro = Text1(1).Text
If Text1(3) = "" Then
  rst!cta_bancaria = Null
 Else
  rst!cta_bancaria = Text1(3).Text
End If
rst!calle = Text1(4).Text
If Text1(5) = "" Then
  rst!torre = Null
 Else
  rst!torre = Text1(5).Text
End If
If Text1(6) = "" Then
  rst!piso = Null
 Else
  rst!piso = Text1(6).Text
End If
If Text1(7) = "" Then
  rst!depto = Null
 Else
  rst!depto = Text1(7).Text
End If
rst!localidad = Text1(8).Text
rst!numero = Text1(9).Text
rst!cuit = Text1(10).Text
rst!cod_postal = Text1(11).Text
If Text1(12) = "" Then
  rst!tel = Null
 Else
  rst!tel = Text1(12).Text
End If

If Text1(13) = "" Then
  rst!Email = Null
 Else
  rst!Email = Text1(13).Text
End If
rst!borrado = "f"
rst!cond_impositiva = (Combo1.ListIndex) + 1
rst.Update
restablecer
rst.Requery
rst.MoveFirst
rst.Find "id_proveedor = " & xid_proveedor
loadGrid
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub

Private Sub restablecer()
Dim c
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
SSTab1.TabEnabled(1) = True
cmdPrimero.Enabled = True
CmdAtras.Enabled = True
CmdAdelante.Enabled = True
CmdUltimo.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
For c = 0 To 13
  If c = 2 Then
     c = c + 1
  End If
  Text1(c).Enabled = False
Next c
Combo1.Enabled = False
End Sub

Private Sub Text1_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 12 Or Index = 6 Or Index = 10 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
       If KeyAscii <> 8 And KeyAscii <> 7 Then
          KeyAscii = 0
       End If
    End If
End If
End Sub

Sub buscar()
busqueda = ""
busqueda = Input_Box("Buscar Proveedor." & VBA.Strings.Chr(13) & "Ingrese Raz�n Social:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    If Text1(1) = busqueda Then: rst.MoveNext
    rst.Find "razon_social like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rst.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbCritical, , "explain"
End If
End Sub

Private Sub Text1_lostfocus(Index As Integer)
For Index = 0 To 13
  If Index = 2 Then
    Index = Index + 1
  End If
  Text1(Index) = Trim(Text1(Index))
Next Index
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Proveedor Seleccionado", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rst!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rst.Update
rst.Requery
cmdAdelante_Click
loadGrid
End Sub

Sub imprimir()
'frmPrincipal.desactivararch
'Set rptProveedores.DataSource = rst
'rptProveedores.PrintReport True
'Unload rptProveedores
End Sub

Sub preview()
'frmPrincipal.desactivararch
'Set rptProveedores.DataSource = rst
'rptProveedores.Show
End Sub
