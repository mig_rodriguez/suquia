VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmActMedicamento 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Medicamentos"
   ClientHeight    =   6465
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8235
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActMedicamento.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6465
   ScaleWidth      =   8235
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   1110
      TabIndex        =   14
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   3263
      TabIndex        =   15
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   5430
      TabIndex        =   16
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2190
      TabIndex        =   13
      Top             =   6000
      Width           =   1695
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4350
      TabIndex        =   17
      Top             =   6000
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4815
      Index           =   1
      Left            =   120
      TabIndex        =   22
      Top             =   480
      Width           =   8040
      Begin VB.ListBox lstProveedores 
         Height          =   735
         Left            =   1260
         TabIndex        =   46
         Top             =   3960
         Width           =   2415
      End
      Begin VB.ListBox lstTodos 
         Height          =   735
         Left            =   4020
         TabIndex        =   45
         Top             =   3960
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.CommandButton cmdAgregar 
         Caption         =   "<"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3720
         TabIndex        =   43
         Top             =   3960
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.CommandButton cmdQuitar 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3720
         TabIndex        =   42
         Top             =   4425
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.TextBox txtMed 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   5280
         TabIndex        =   27
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   1200
         MaxLength       =   30
         TabIndex        =   0
         Top             =   720
         Width           =   2655
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   570
         Index           =   6
         Left            =   4005
         MaxLength       =   199
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   12
         Top             =   3320
         Width           =   3975
      End
      Begin VB.TextBox txtMed 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   330
         Index           =   5
         Left            =   5280
         MaxLength       =   11
         TabIndex        =   6
         Top             =   1680
         Width           =   975
      End
      Begin VB.CommandButton cmdPrimero 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2085
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2565
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3045
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdUltimo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3525
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   240
         Width           =   375
      End
      Begin VB.ComboBox cmbRubro 
         Enabled         =   0   'False
         Height          =   345
         Left            =   1245
         TabIndex        =   11
         Top             =   3360
         Width           =   2655
      End
      Begin VB.CommandButton cmdNuerub 
         Caption         =   "+"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   26
         Top             =   3360
         Width           =   375
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   1200
         MaxLength       =   30
         TabIndex        =   2
         Top             =   1200
         Width           =   2655
      End
      Begin VB.TextBox txtMed 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   330
         Index           =   3
         Left            =   1200
         MaxLength       =   30
         TabIndex        =   4
         Top             =   1680
         Width           =   690
      End
      Begin VB.TextBox txtMed 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   330
         Index           =   4
         Left            =   3120
         MaxLength       =   30
         TabIndex        =   5
         Top             =   1680
         Width           =   735
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   330
         Index           =   7
         Left            =   5280
         MaxLength       =   30
         TabIndex        =   1
         Top             =   720
         Width           =   2655
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   330
         Index           =   8
         Left            =   5280
         MaxLength       =   30
         TabIndex        =   3
         Top             =   1200
         Width           =   2655
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   330
         Index           =   9
         Left            =   1200
         MaxLength       =   30
         TabIndex        =   7
         Top             =   2160
         Width           =   2655
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   330
         Index           =   10
         Left            =   5760
         MaxLength       =   30
         TabIndex        =   8
         Top             =   2640
         Width           =   2175
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   330
         Index           =   11
         Left            =   1200
         MaxLength       =   30
         TabIndex        =   9
         Top             =   2640
         Width           =   2655
      End
      Begin VB.TextBox txtMed 
         Enabled         =   0   'False
         Height          =   330
         Index           =   12
         Left            =   5280
         MaxLength       =   30
         TabIndex        =   10
         Top             =   2160
         Width           =   2655
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Proveedores:"
         Height          =   225
         Index           =   14
         Left            =   120
         TabIndex        =   44
         Top             =   3960
         Width           =   1095
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Droga:"
         Height          =   225
         Index           =   8
         Left            =   4000
         TabIndex        =   41
         Top             =   720
         Width           =   555
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "N�mero:"
         Height          =   225
         Index           =   5
         Left            =   4000
         TabIndex        =   40
         Top             =   240
         Width           =   720
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Nombre  :"
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   39
         Top             =   720
         Width           =   810
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Precio en $:"
         Height          =   225
         Index           =   1
         Left            =   4000
         TabIndex        =   38
         Top             =   1680
         Width           =   975
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n:"
         Height          =   225
         Index           =   4
         Left            =   4005
         TabIndex        =   37
         Top             =   3080
         Width           =   1035
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Rubro:"
         Height          =   225
         Index           =   6
         Left            =   645
         TabIndex        =   36
         Top             =   3360
         Width           =   555
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Laboratorio :"
         Height          =   225
         Index           =   7
         Left            =   120
         TabIndex        =   35
         Top             =   1200
         Width           =   1035
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Stock Total :"
         Height          =   225
         Index           =   2
         Left            =   120
         TabIndex        =   34
         Top             =   1680
         Width           =   990
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Stock M�nimo :"
         Height          =   225
         Index           =   3
         Left            =   1920
         TabIndex        =   33
         Top             =   1680
         Width           =   1185
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Presentaci�n:"
         Height          =   225
         Index           =   9
         Left            =   4000
         TabIndex        =   32
         Top             =   1200
         Width           =   1140
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Posolog�a:"
         Height          =   225
         Index           =   10
         Left            =   120
         TabIndex        =   31
         Top             =   2160
         Width           =   885
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Contraindicaciones:"
         Height          =   225
         Index           =   11
         Left            =   4000
         TabIndex        =   30
         Top             =   2640
         Width           =   1650
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Colaterales:"
         Height          =   225
         Index           =   12
         Left            =   120
         TabIndex        =   29
         Top             =   2640
         Width           =   1005
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Precauciones:"
         Height          =   225
         Index           =   13
         Left            =   4000
         TabIndex        =   28
         Top             =   2160
         Width           =   1185
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Index           =   2
      Left            =   120
      TabIndex        =   23
      Top             =   480
      Width           =   7935
      Begin MSFlexGridLib.MSFlexGrid grdMed 
         Height          =   4455
         Left            =   480
         TabIndex        =   25
         Top             =   240
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   7858
         _Version        =   393216
         Rows            =   1
         Cols            =   6
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5295
      Left            =   45
      TabIndex        =   24
      Top             =   45
      Width           =   8175
      _ExtentX        =   14420
      _ExtentY        =   9340
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmActMedicamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rsMed As ADODB.Recordset
Dim rsRub As ADODB.Recordset
Dim rstprov As ADODB.Recordset
Dim cmd As ADODB.Command
Dim c, mintCurFrame As Integer
Dim band As Boolean
Dim busqueda As String

Private Sub cmdagregar_Click()
If lstTodos.ListIndex = -1 Then Exit Sub
lstProveedores.AddItem lstTodos.List(lstTodos.ListIndex)
lstProveedores.ItemData(lstProveedores.NewIndex) = lstTodos.ItemData(lstTodos.ListIndex)
lstTodos.RemoveItem lstTodos.ListIndex
End Sub

Private Sub cmdQuitar_Click()
If lstProveedores.ListIndex = -1 Then Exit Sub
lstTodos.AddItem lstProveedores.List(lstProveedores.ListIndex)
lstTodos.ItemData(lstTodos.NewIndex) = lstProveedores.ItemData(lstProveedores.ListIndex)
lstProveedores.RemoveItem lstProveedores.ListIndex
End Sub

Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
 If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
band = False
Set rsMed = New ADODB.Recordset
Set rsRub = New ADODB.Recordset
Set rstprov = New ADODB.Recordset
Set cmd = New ADODB.Command
Set rsMed = abrirrs("select * from medicamento where id_medicamento > 0 and borrado='f' order by nombre", True)
Set rsRub = abrirrs("select * from rubro where id_rubro < 10000 and id_tipo = 10002 and borrado='f' order by id_rubro", False)
cmd.ActiveConnection = cnx
rstprov.ActiveConnection = cnx
rstprov.CursorType = adOpenForwardOnly
rstprov.LockType = adLockReadOnly
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
cargarRubro
loadGrid
rsMed.MoveFirst
Refrescar
End Sub

Private Sub lstProveedores_DblClick()
If lstTodos.Visible = True Then
    cmdQuitar_Click
End If
End Sub

Private Sub lstTodos_DblClick()
cmdagregar_Click
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rsMed.MoveFirst
    rsMed.Find "id_medicamento = " & grdMed.TextMatrix(grdMed.Row, 5)
    Refrescar
    mintCurFrame = 1
    band = False
Else
If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
End If
End Sub

Private Sub Refrescar()
txtMed(0) = rsMed!id_medicamento
txtMed(1) = rsMed!nombre
txtMed(2) = rsMed!laboratorio
txtMed(3) = rsMed!stock_total
txtMed(4) = rsMed!stock_min
txtMed(5) = rsMed!Precio
txtMed(6) = rsMed!descripcion
txtMed(7) = rsMed!monodroga
txtMed(8) = rsMed!presentacion
txtMed(9) = rsMed!posologia
txtMed(10) = rsMed!contraindicaciones
txtMed(11) = rsMed!colaterales
txtMed(12) = rsMed!precauciones
For c = 0 To cmbRubro.ListCount - 1
cmbRubro.ListIndex = c
If cmbRubro.ItemData(c) = rsMed!id_rubro Then
    Exit For
End If
Next
rstprov.Source = "select p.razon_social,x.id_proveedor from proveedor p,prov_item x where x.id_proveedor=p.id_proveedor and x.tipo_item=2 and x.id_item=" & txtMed(0)
rstprov.Open
lstProveedores.Clear
While Not rstprov.EOF
    lstProveedores.AddItem rstprov!razon_social
    lstProveedores.ItemData(lstProveedores.NewIndex) = rstprov!id_proveedor
    rstprov.MoveNext
Wend
rstprov.Close
End Sub

Public Sub cargarRubro()
cmbRubro.Clear
rsRub.Requery
If rsRub.RecordCount = 0 Then
    Exit Sub
End If
rsRub.MoveFirst
Do While Not rsRub.EOF
   cmbRubro.AddItem rsRub!nombre
   cmbRubro.ItemData(cmbRubro.NewIndex) = rsRub!id_rubro
   rsRub.MoveNext
Loop
cmbRubro.ListIndex = 0
End Sub

'   Mover Medicamento

Private Sub cmdPrimero_Click()
rsMed.MoveFirst
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAdelante_Click()
If rsMed.EOF <> True Then: rsMed.MoveNext
If rsMed.EOF <> True Then
Else
rsMed.MoveFirst
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAtras_Click()
If rsMed.BOF <> True Then: rsMed.MovePrevious
If rsMed.BOF <> True Then
Else
rsMed.MoveLast
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdUltimo_Click()
rsMed.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

'    NUEVO MEDICAMENTO

Private Sub cmdNuevo_Click()
nuevo
End Sub
Sub nuevo()
cmdPrimero.Enabled = False
cmdUltimo.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdModificar.Enabled = False
cmdNuevo.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
cmdNuerub.Enabled = True
rsMed.MoveLast
txtMed(0) = nuevoid("id_medicamento", "medicamento")
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Loro.Play "write"
rsMed.AddNew
For c = 1 To 12
txtMed(c).Enabled = True
txtMed(c) = ""
Next
lstProveedores.Clear
lstTodos.Visible = True
cmdAgregar.Visible = True
cmdQuitar.Visible = True
cmbRubro.Enabled = True
rstprov.Source = "select id_proveedor,razon_social from proveedor where borrado='f' order by razon_social"
rstprov.Open
lstTodos.Clear
While Not rstprov.EOF
    lstTodos.AddItem rstprov!razon_social
    lstTodos.ItemData(lstTodos.NewIndex) = rstprov!id_proveedor
    rstprov.MoveNext
Wend
rstprov.Close
txtMed(1).SetFocus
End Sub

'   MODIFICAR MEDICAMENTO

Private Sub cmdModificar_Click()
modificar
End Sub
Sub modificar()
cmdPrimero.Enabled = False
cmdUltimo.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdNuerub.Enabled = True
cmdSalir.Caption = "&Cancelar"
TabStrip1.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Loro.Play "write"
cmdGuardar.Enabled = True
For c = 1 To 12
txtMed(c).Enabled = True
Next
cmbRubro.Enabled = True
lstTodos.Visible = True
cmdAgregar.Visible = True
cmdQuitar.Visible = True
rstprov.Source = "select id_proveedor,razon_social from proveedor where borrado='f' order by razon_social"
rstprov.Open
lstTodos.Clear
Dim a
Dim listado As Boolean
While Not rstprov.EOF
    listado = False
    For a = 0 To lstProveedores.ListCount - 1
        If lstProveedores.ItemData(a) = rstprov!id_proveedor Then listado = True: Exit For
    Next a
    If listado = False Then
        lstTodos.AddItem rstprov!razon_social
        lstTodos.ItemData(lstTodos.NewIndex) = rstprov!id_proveedor
    End If
    rstprov.MoveNext
Wend
rstprov.Close
txtMed(1).SetFocus
End Sub

'   GUARDAR

Private Sub cmdGuardar_Click()
guardar
End Sub
Sub guardar()
For c = 1 To 7
If c = 6 Then: c = 7
If Trim(txtMed(c)) = "" Then
    If c >= 3 And c <= 4 Then
        txtMed(c) = 0
    Else
        MsgRapido "Faltan Datos Necesarios, Por Favor Compl�telos", Loro, , , "surprised"
        txtMed(c).SetFocus
        Exit Sub
    End If
End If
Next c
If lstProveedores.ListCount < 1 Then MsgRapido "Debe indicar al menos un proveedor.", Loro, vbCritical, , "surprised": Exit Sub
rsMed!id_medicamento = txtMed(0)
rsMed!nombre = txtMed(1)
rsMed!laboratorio = txtMed(2)
rsMed!stock_total = txtMed(3)
rsMed!stock_min = txtMed(4)
rsMed!descripcion = txtMed(6)
rsMed!Precio = txtMed(5)
rsMed!monodroga = txtMed(7)
rsMed!presentacion = txtMed(8)
rsMed!posologia = txtMed(9)
rsMed!contraindicaciones = txtMed(10)
rsMed!colaterales = txtMed(11)
rsMed!precauciones = txtMed(12)
rsMed!id_rubro = cmbRubro.ItemData(cmbRubro.ListIndex)
rsMed!borrado = "f"
cmd.CommandText = "delete from prov_item where tipo_item=2 and id_item=" & txtMed(0)
cmd.Execute
Dim a
For a = 0 To lstProveedores.ListCount - 1
    cmd.CommandText = "insert into prov_item(tipo_item,id_item,id_proveedor) values(2," & txtMed(0) & "," & lstProveedores.ItemData(a) & ")"
    cmd.Execute
Next a
rsMed.Update
rsMed.Requery
rsMed.Find "id_medicamento =" & txtMed(0)
restablecer
loadGrid
cmdAdelante.SetFocus
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Medicamento Seleccionado", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rsMed!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rsMed.Update
rsMed.Requery
cmdAdelante_Click
loadGrid
End Sub

Private Sub restablecer()
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdNuevo.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
TabStrip1.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
cmdNuerub.Enabled = False
For c = 1 To 12
txtMed(c).Enabled = False
Next
cmbRubro.Enabled = False
lstTodos.Visible = False
cmdAgregar.Visible = False
cmdQuitar.Visible = False
Refrescar
End Sub

Sub cmdsalir_Click()
Unload Me
End Sub

' SOLO N�MEROS

Private Sub txtMed_KeyPress(Index As Integer, KeyAscii As Integer)
If Index >= 3 And Index <= 5 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
    End If
End If
End Sub

Private Sub txtMed_Validate(Index As Integer, Cancel As Boolean)
If Index >= 3 And Index <= 5 Then
    If txtMed(Index) <> "" Then
    If IsNumeric(txtMed(Index)) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtMed(Index) = ""
    End If
    End If
End If
End Sub

Sub buscar()
busqueda = Input_Box("Buscar Medicamento" & Chr(13) & "Ingrese el Nombre del Medicamento:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    rsMed.Filter = ""
    If txtMed(1) = busqueda Then: rstmas.MoveNext
    rsMed.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rsMed.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, , , "explain"
End If
End Sub

Private Sub cmdNuerub_Click()
frmActRubro.Show
Me.Enabled = False
frmActRubro.nuevo
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        rsMed.CancelUpdate
        restablecer
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
Set rsRub = Nothing
Set rsMed = Nothing
End Sub

Private Sub grdMed_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Private Sub loadGrid()
Dim regActual As Long
regActual = rsMed.AbsolutePosition
rsMed.MoveFirst
grdMed.ColWidth(0) = 1550
grdMed.ColWidth(1) = 1550
grdMed.ColWidth(2) = 1550
grdMed.ColWidth(3) = 1550
grdMed.ColWidth(4) = 700
grdMed.ColWidth(5) = 0
grdMed.Rows = 0
grdMed.AddItem "Droga" & vbTab & "Nombre" & vbTab & "Laboratorio" & vbTab & "Presentaci�n" & vbTab & "Stock"
grdMed.Rows = 2
grdMed.FixedRows = 1
grdMed.Rows = 1
While rsMed.EOF = False
grdMed.AddItem rsMed!monodroga & vbTab & rsMed!nombre & vbTab & rsMed!laboratorio & vbTab & rsMed!presentacion & vbTab & rsMed!stock_total & vbTab & rsMed!id_medicamento
rsMed.MoveNext
Wend
rsMed.AbsolutePosition = regActual
End Sub

Sub imprimir()
frmPrincipal.desactivararch
Set rptMedicamentos.DataSource = rsMed
rptMedicamentos.PrintReport True
Unload rptMedicamentos
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptMedicamentos.DataSource = rsMed
rptMedicamentos.Show
End Sub

