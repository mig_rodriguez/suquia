VERSION 5.00
Begin VB.Form frmLoadDb 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Iniciando Base de Datos"
   ClientHeight    =   1020
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   4140
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLoadDb.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1020
   ScaleWidth      =   4140
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   3600
      Top             =   600
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Top             =   240
      Width           =   480
   End
   Begin VB.Label Label2 
      Caption         =   "Por favor espere mientras es iniciado."
      Height          =   255
      Left            =   720
      TabIndex        =   1
      Top             =   600
      Width           =   3015
   End
   Begin VB.Label Label1 
      Caption         =   "El Servidor de Datos se encuenta desactivado."
      Height          =   255
      Left            =   720
      TabIndex        =   0
      Top             =   240
      Width           =   3495
   End
End
Attribute VB_Name = "frmLoadDb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
Dim DBLocation As String
Image1.Picture = LoadPicture(App.Path & "\bitmaps\connect.ico")
On Error GoTo catchErr
DBLocation = GetSetting("Suquia", "Settings", "DBLocation")
Shell DBLocation
Timer1.Enabled = True
catchErr:
If Err.Number = 53 Then
    MsgBox "Hubo un error al intentar iniciar el servidor de datos." & Chr(13) & Chr(13) & "Aseg�rese de que el nombre del servidor es correcto" & Chr(13) & "y que el mismo se encuentra encendido.", vbCritical
    End
End If
End Sub
Private Sub Timer1_Timer()
Unload Me
End Sub
