VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmActRubro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Rubros"
   ClientHeight    =   4530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4950
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActRubro.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4530
   ScaleWidth      =   4950
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2520
      TabIndex        =   7
      Top             =   4080
      Width           =   1215
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1208
      TabIndex        =   3
      Top             =   4080
      Width           =   1215
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   3188
      TabIndex        =   6
      Top             =   3600
      Width           =   1215
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   1868
      TabIndex        =   5
      Top             =   3600
      Width           =   1215
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Default         =   -1  'True
      Height          =   375
      Left            =   548
      TabIndex        =   4
      Top             =   3600
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   1
      Left            =   120
      TabIndex        =   13
      Top             =   400
      Width           =   4695
      Begin VB.TextBox txtRub 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   3360
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   285
         Width           =   1095
      End
      Begin VB.TextBox txtRub 
         Enabled         =   0   'False
         Height          =   570
         Index           =   2
         Left            =   360
         MaxLength       =   199
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   2040
         Width           =   4095
      End
      Begin VB.CommandButton cmdPrimero 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   330
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   285
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   810
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   285
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1290
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   285
         Width           =   375
      End
      Begin VB.CommandButton cmdUltimo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1770
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   285
         Width           =   375
      End
      Begin VB.TextBox txtRub 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   2040
         MaxLength       =   20
         TabIndex        =   0
         Top             =   840
         Width           =   2415
      End
      Begin VB.ComboBox cmbRubro 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         TabIndex        =   1
         Text            =   "Combo1"
         Top             =   1320
         Width           =   2415
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Nombre :"
         Height          =   225
         Index           =   0
         Left            =   360
         TabIndex        =   19
         Top             =   885
         Width           =   765
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n:"
         Height          =   225
         Index           =   4
         Left            =   360
         TabIndex        =   18
         Top             =   1725
         Width           =   1035
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "N�mero:"
         Height          =   225
         Index           =   5
         Left            =   2490
         TabIndex        =   17
         Top             =   405
         Width           =   720
      End
      Begin VB.Label lblSer 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Rubro :"
         Height          =   225
         Index           =   1
         Left            =   360
         TabIndex        =   16
         Top             =   1365
         Width           =   1260
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   2
      Left            =   120
      TabIndex        =   14
      Top             =   400
      Width           =   4695
      Begin MSFlexGridLib.MSFlexGrid grdRub 
         Height          =   2655
         Left            =   60
         TabIndex        =   20
         Top             =   240
         Width           =   4600
         _ExtentX        =   8123
         _ExtentY        =   4683
         _Version        =   393216
         Rows            =   1
         Cols            =   3
         FixedRows       =   0
         FixedCols       =   0
         ForeColor       =   -2147483642
         FocusRect       =   0
         SelectionMode   =   1
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   3495
      Left            =   45
      TabIndex        =   12
      Top             =   45
      Width           =   4850
      _ExtentX        =   8546
      _ExtentY        =   6165
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmActRubro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rsRub As ADODB.Recordset
Dim rsRubI As ADODB.Recordset
Dim c, mintCurFrame As Integer
Dim band As Boolean
Dim busqueda As String

Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
    If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
band = False
Set rsRub = New ADODB.Recordset
Set rsRub = abrirrs("select id_rubro, nombre from rubro where id_rubro > 9999 order by id_rubro", False)
cargarRubro
rsRub.Close
Set rsRub = abrirrs("select * from rubro where id_rubro < 10000 and borrado = 'f' order by id_rubro ", True)
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
loadGrid
rsRub.MoveFirst
Refrescar
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rsRub.MoveFirst
    rsRub.Find "id_rubro = " & grdRub.TextMatrix(grdRub.Row, 2)
    Refrescar
    mintCurFrame = 1
    band = False
Else
If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
End If
End Sub

Private Sub Refrescar()
txtRub(0) = rsRub!id_rubro
txtRub(1) = rsRub!nombre
txtRub(2) = rsRub!descripcion
For c = 0 To cmbRubro.ListCount - 1
cmbRubro.ListIndex = c
If cmbRubro.ItemData(c) = rsRub!id_tipo Then
    Exit For
End If
Next
End Sub

'   Mover Rubro

Private Sub cmdPrimero_Click()
rsRub.MoveFirst
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAdelante_Click()
If rsRub.EOF <> True Then: rsRub.MoveNext
If rsRub.EOF <> True Then
Else
rsRub.MoveFirst
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAtras_Click()
If rsRub.BOF <> True Then: rsRub.MovePrevious
If rsRub.BOF <> True Then
Else
rsRub.MoveLast
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdUltimo_Click()
rsRub.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

'   NUEVO RUBRO

Private Sub cmdNuevo_Click()
nuevo
End Sub

Sub nuevo()
cmdPrimero.Enabled = False
cmdUltimo.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdModificar.Enabled = False
cmdNuevo.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
rsRub.MoveLast
txtRub(0) = rsRub!id_rubro + 1 'nuevoid("id_rubro", "rubro")
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Loro.Play "write"
rsRub.AddNew
For c = 1 To 2
txtRub(c).Enabled = True
txtRub(c) = ""
Next
cmbRubro.Enabled = True
txtRub(1).SetFocus
End Sub

'   MODIFICAR RUBRO

Private Sub cmdModificar_Click()
modificar
End Sub

Sub modificar()
cmdPrimero.Enabled = False
cmdUltimo.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Loro.Play "write"
For c = 1 To 2
txtRub(c).Enabled = True
Next
cmbRubro.Enabled = True
txtRub(1).SetFocus
End Sub

'   GUARDAR

Private Sub cmdGuardar_Click()
guardar
End Sub

Sub guardar()
If Trim(txtRub(1)) = "" Then
    MsgRapido "Faltan Datos Necesarios, Por Favor Compl�telos", Loro, , , "surprised"
    txtRub(1).SetFocus
    Exit Sub
End If
rsRub!id_rubro = txtRub(0)
rsRub!nombre = txtRub(1)
rsRub!descripcion = txtRub(2)
rsRub!id_tipo = cmbRubro.ItemData(cmbRubro.ListIndex)
rsRub!borrado = "f"
rsRub.Update
rsRub.Requery
rsRub.Find "id_rubro = " & txtRub(0)
restablecer
loadGrid
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
cmdAdelante.SetFocus
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Rubro Seleccionada", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rsRub!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rsRub.Update
rsRub.Requery
cmdAdelante_Click
loadGrid
End Sub

Private Sub restablecer()
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdNuevo.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
cmbRubro.Enabled = False
For c = 1 To 2
txtRub(c).Enabled = False
Next
Refrescar
End Sub

Sub cmdsalir_Click()
Unload Me
End Sub

Sub buscar()
busqueda = Input_Box("Buscar Rubro" & Chr(13) & "Ingrese el Nombre del Rubro:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    rsRub.Filter = ""
    If txtRub(1) = busqueda Then: rstmas.MoveNext
    rsRub.Find "nombre like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rsRub.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, , , "explain"
End If
End Sub

Private Sub cargarRubro()
cmbRubro.Clear
If rsRub.RecordCount = 0 Then
    Exit Sub
End If
rsRub.MoveFirst
Do While Not rsRub.EOF
   cmbRubro.AddItem rsRub!nombre
   cmbRubro.ItemData(cmbRubro.NewIndex) = rsRub!id_rubro
   rsRub.MoveNext
Loop
cmbRubro.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        rsRub.CancelUpdate
        restablecer
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
rsRub.Close
If cargado("frmActArticulo") = True Then
frmActArticulo.Enabled = True
frmActArticulo.cargarRubro
End If
If cargado("frmActMedicamento") = True Then
frmActMedicamento.Enabled = True
frmActMedicamento.cargarRubro
End If
If cargado("frmActServicio") = True Then
frmActServicio.Enabled = True
frmActServicio.cargarRubro
End If
End Sub

Private Sub grdrub_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Private Sub loadGrid()
Dim regActual As Long
regActual = rsRub.AbsolutePosition
rsRub.MoveFirst
grdRub.ColWidth(0) = 1900
grdRub.ColWidth(1) = 2500
grdRub.ColWidth(2) = 0
grdRub.Rows = 0
grdRub.AddItem "Nombre" & vbTab & "Descripci�n"
grdRub.Rows = 2
grdRub.FixedRows = 1
grdRub.Rows = 1
While rsRub.EOF = False
grdRub.AddItem rsRub!nombre & vbTab & rsRub!descripcion & vbTab & rsRub!id_rubro
rsRub.MoveNext
Wend
rsRub.AbsolutePosition = regActual
End Sub

Sub imprimir()
crearRS
frmPrincipal.desactivararch
Set rptRubros.DataSource = rsRubI
rptRubros.PrintReport True
Unload rptRubros
End Sub

Sub preview()
crearRS
frmPrincipal.desactivararch
Set rptRubros.DataSource = rsRubI
rptRubros.Show
End Sub

Private Sub crearRS()
Set rsRubI = New ADODB.Recordset
Set rsRubI = abrirrs("select r.*, t.nombre as tipo from rubro r, rubro t where r.id_rubro < 10000 and r.id_tipo = t.id_rubro order by id_rubro ", False)
End Sub

