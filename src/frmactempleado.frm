VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmactempleado 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizaci�n de Empleados"
   ClientHeight    =   6525
   ClientLeft      =   2490
   ClientTop       =   570
   ClientWidth     =   9120
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmactempleado.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6525
   ScaleWidth      =   9120
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   1560
      TabIndex        =   0
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   3713
      TabIndex        =   1
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   5858
      TabIndex        =   2
      Top             =   5400
      Width           =   1695
   End
   Begin VB.CommandButton cmdSalir 
      Cancel          =   -1  'True
      Caption         =   "&Salir"
      Height          =   375
      Left            =   4778
      TabIndex        =   4
      Top             =   6000
      Width           =   1695
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2618
      TabIndex        =   3
      Top             =   6000
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   4695
      Index           =   1
      Left            =   240
      TabIndex        =   23
      Top             =   480
      Width           =   8655
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   2
         Left            =   5520
         MaxLength       =   20
         TabIndex        =   6
         Top             =   840
         Width           =   3015
      End
      Begin VB.CommandButton cmdUltimo 
         Height          =   375
         Left            =   2640
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         Height          =   375
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         Height          =   375
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   240
         Width           =   375
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox Text1 
         Enabled         =   0   'False
         Height          =   330
         Index           =   1
         Left            =   1200
         MaxLength       =   20
         TabIndex        =   5
         Top             =   840
         Width           =   3255
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         Height          =   330
         Index           =   0
         Left            =   6960
         MaxLength       =   20
         TabIndex        =   26
         Top             =   360
         Width           =   1575
      End
      Begin VB.Frame f2 
         Height          =   2535
         Index           =   1
         Left            =   240
         TabIndex        =   37
         Top             =   1800
         Width           =   8175
         Begin VB.TextBox txtdir 
            Enabled         =   0   'False
            Height          =   330
            Index           =   8
            Left            =   1320
            TabIndex        =   15
            Top             =   1800
            Width           =   3135
         End
         Begin VB.TextBox txtdir 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   9
            Left            =   6000
            TabIndex        =   16
            Top             =   1800
            Width           =   2055
         End
         Begin VB.TextBox txtdir 
            Enabled         =   0   'False
            Height          =   330
            Index           =   7
            Left            =   6000
            TabIndex        =   14
            Top             =   1320
            Width           =   2055
         End
         Begin VB.TextBox txtdir 
            Enabled         =   0   'False
            Height          =   330
            Index           =   6
            Left            =   1320
            TabIndex        =   13
            Top             =   1320
            Width           =   3135
         End
         Begin VB.TextBox txtdir 
            Enabled         =   0   'False
            Height          =   330
            Index           =   5
            Left            =   6000
            TabIndex        =   12
            Top             =   840
            Width           =   2055
         End
         Begin VB.TextBox txtdir 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   4
            Left            =   3840
            TabIndex        =   11
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox txtdir 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   3
            Left            =   2520
            TabIndex        =   10
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox txtdir 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   2
            Left            =   1320
            TabIndex        =   9
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox txtdir 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   1
            Left            =   6000
            TabIndex        =   8
            Top             =   360
            Width           =   2055
         End
         Begin VB.TextBox txtdir 
            Enabled         =   0   'False
            Height          =   330
            Index           =   0
            Left            =   1320
            TabIndex        =   7
            Top             =   360
            Width           =   3135
         End
         Begin VB.Label Label1 
            Caption         =   "Email:"
            Height          =   255
            Index           =   4
            Left            =   240
            TabIndex        =   47
            Top             =   1800
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Calle:"
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   45
            Top             =   360
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Torre:"
            Height          =   255
            Index           =   7
            Left            =   240
            TabIndex        =   44
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Localidad:"
            Height          =   255
            Index           =   8
            Left            =   240
            TabIndex        =   43
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "Dpto.:"
            Height          =   255
            Index           =   9
            Left            =   3240
            TabIndex        =   42
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Piso:"
            Height          =   255
            Index           =   10
            Left            =   2040
            TabIndex        =   41
            Top             =   840
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Nro.:"
            Height          =   255
            Index           =   11
            Left            =   4680
            TabIndex        =   40
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Barrio:"
            Height          =   255
            Index           =   12
            Left            =   4680
            TabIndex        =   39
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo Postal:"
            Height          =   255
            Index           =   13
            Left            =   4680
            TabIndex        =   38
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Tel�fono:"
            Height          =   255
            Index           =   2
            Left            =   4680
            TabIndex        =   46
            Top             =   1800
            Width           =   1215
         End
      End
      Begin VB.Frame f2 
         Height          =   2295
         Index           =   2
         Left            =   240
         TabIndex        =   48
         Top             =   1800
         Width           =   8175
         Begin MSComCtl2.DTPicker FecNac 
            Height          =   345
            Left            =   5400
            TabIndex        =   58
            Top             =   360
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   609
            _Version        =   393216
            Enabled         =   0   'False
            CalendarTitleBackColor=   -2147483638
            Format          =   24576001
            CurrentDate     =   37819
         End
         Begin VB.ComboBox cmbEc 
            Enabled         =   0   'False
            Height          =   345
            Left            =   5400
            Style           =   2  'Dropdown List
            TabIndex        =   20
            Top             =   960
            Width           =   2655
         End
         Begin VB.TextBox txtper 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   0
            Left            =   1680
            MaxLength       =   8
            TabIndex        =   19
            Top             =   960
            Width           =   1935
         End
         Begin VB.ComboBox cmbTd 
            Enabled         =   0   'False
            Height          =   345
            Left            =   1680
            Style           =   2  'Dropdown List
            TabIndex        =   18
            Top             =   360
            Width           =   1935
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Femenino"
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   4200
            TabIndex        =   22
            Top             =   1680
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Masculino"
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   2760
            TabIndex        =   21
            Top             =   1680
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.Label lblper 
            AutoSize        =   -1  'True
            Caption         =   "Sexo:"
            Height          =   225
            Index           =   5
            Left            =   2040
            TabIndex        =   53
            Top             =   1680
            Width           =   450
         End
         Begin VB.Label lblper 
            AutoSize        =   -1  'True
            Caption         =   "Estado Civil:"
            Height          =   225
            Index           =   3
            Left            =   3960
            TabIndex        =   52
            Top             =   960
            Width           =   1020
         End
         Begin VB.Label lblper 
            AutoSize        =   -1  'True
            Caption         =   "Nro.Documento:"
            Height          =   225
            Index           =   2
            Left            =   240
            TabIndex        =   51
            Top             =   960
            Width           =   1350
         End
         Begin VB.Label lblper 
            AutoSize        =   -1  'True
            Caption         =   "Fecha de Nac."
            Height          =   225
            Index           =   1
            Left            =   3960
            TabIndex        =   50
            Top             =   360
            Width           =   1185
         End
         Begin VB.Label lblper 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Doc.:"
            Height          =   225
            Index           =   0
            Left            =   240
            TabIndex        =   49
            Top             =   360
            Width           =   1080
         End
      End
      Begin VB.Frame f2 
         Height          =   1935
         Index           =   3
         Left            =   240
         TabIndex        =   54
         Top             =   1800
         Width           =   8175
         Begin VB.Frame fAgregar 
            Caption         =   "Agregar  �reas"
            Height          =   1695
            Left            =   3360
            TabIndex        =   64
            Top             =   240
            Visible         =   0   'False
            Width           =   2295
            Begin VB.CommandButton cmdCanArea 
               Caption         =   "Cerrar"
               Height          =   375
               Left            =   480
               TabIndex        =   66
               Top             =   1260
               Width           =   1335
            End
            Begin VB.ListBox LNAreas 
               Height          =   960
               Left            =   120
               TabIndex        =   65
               Top             =   240
               Width           =   2055
            End
         End
         Begin VB.CommandButton cmdQuiArea 
            Caption         =   "-"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5280
            Style           =   1  'Graphical
            TabIndex        =   63
            Top             =   1200
            Width           =   375
         End
         Begin VB.CommandButton cmdAgrArea 
            Caption         =   "+"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   5280
            Style           =   1  'Graphical
            TabIndex        =   62
            Top             =   720
            Width           =   375
         End
         Begin VB.ListBox LAreas 
            Enabled         =   0   'False
            Height          =   1185
            Left            =   5760
            TabIndex        =   60
            Top             =   480
            Width           =   2175
         End
         Begin VB.TextBox txtlab 
            Enabled         =   0   'False
            Height          =   330
            Index           =   1
            Left            =   1680
            MaxLength       =   20
            TabIndex        =   25
            Top             =   1320
            Width           =   3135
         End
         Begin VB.TextBox txtlab 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   0
            Left            =   1680
            MaxLength       =   11
            TabIndex        =   24
            Top             =   840
            Width           =   1695
         End
         Begin MSComCtl2.DTPicker FecIng 
            Height          =   345
            Left            =   1680
            TabIndex        =   59
            Top             =   360
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   609
            _Version        =   393216
            Enabled         =   0   'False
            CalendarTitleBackColor=   -2147483638
            Format          =   24576001
            CurrentDate     =   37819
         End
         Begin VB.Label Label2 
            Caption         =   "�reas de Incumbencia"
            Height          =   255
            Left            =   5760
            TabIndex        =   61
            Top             =   240
            Width           =   2175
         End
         Begin VB.Label lbllab 
            AutoSize        =   -1  'True
            Caption         =   "Cargo:"
            Height          =   225
            Index           =   2
            Left            =   240
            TabIndex        =   57
            Top             =   1320
            Width           =   555
         End
         Begin VB.Label lbllab 
            AutoSize        =   -1  'True
            Caption         =   "Fecha de Ingr."
            Height          =   225
            Index           =   1
            Left            =   240
            TabIndex        =   56
            Top             =   360
            Width           =   1170
         End
         Begin VB.Label lbllab 
            AutoSize        =   -1  'True
            Caption         =   "C.U.I.L."
            Height          =   225
            Index           =   0
            Left            =   240
            TabIndex        =   55
            Top             =   840
            Width           =   600
         End
      End
      Begin MSComctlLib.TabStrip TSemp 
         Height          =   3015
         Left            =   120
         TabIndex        =   17
         Top             =   1440
         Width           =   8415
         _ExtentX        =   14843
         _ExtentY        =   5318
         MultiRow        =   -1  'True
         TabStyle        =   1
         _Version        =   393216
         BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
            NumTabs         =   3
            BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Direcci�n"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Personales"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Laborales"
               ImageVarType    =   2
            EndProperty
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "Legajo:"
         Height          =   255
         Index           =   3
         Left            =   5640
         TabIndex        =   31
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Apellidos:"
         Height          =   255
         Index           =   1
         Left            =   4560
         TabIndex        =   30
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Nombres:"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   29
         Top             =   840
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   4695
      Index           =   2
      Left            =   360
      TabIndex        =   28
      Top             =   480
      Visible         =   0   'False
      Width           =   8415
      Begin MSFlexGridLib.MSFlexGrid grdemp 
         Height          =   4590
         Left            =   0
         TabIndex        =   36
         Top             =   120
         Width           =   8295
         _ExtentX        =   14631
         _ExtentY        =   8096
         _Version        =   393216
         Rows            =   1
         Cols            =   4
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5175
      Left            =   120
      TabIndex        =   27
      Top             =   120
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   9128
      TabWidthStyle   =   2
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmactempleado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mintCurFrame, xid_empleado, Selec, c, maxPerm As Integer
Dim busqueda As String
Dim xint As String
Dim bNue, band, BAdm As Boolean
Dim vecPer(1 To 5) As Integer
Dim rstemp As ADODB.Recordset
Dim rstPerm As ADODB.Recordset
Dim rstPermEmp As ADODB.Recordset
Dim cmd As ADODB.Command

Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
    If cmdSalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
frmactempleado.Height = 6915
mintCurFrame = 1
Selec = 1
band = False
fecNac.MaxDate = Date
FecIng.MaxDate = Date
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
Set rstPermEmp = New ADODB.Recordset
rstPermEmp.ActiveConnection = cnx
rstPermEmp.CursorType = adOpenDynamic
rstPermEmp.LockType = adLockReadOnly
Set rstPerm = New ADODB.Recordset
Set rstPerm = abrirrs("select id_permiso,nombre from permiso where id_permiso>0", False)
maxPerm = rstPerm.RecordCount
Set rstemp = New ADODB.Recordset
Set rstemp = abrirrs("select * from empleado where borrado = 'f' order by legajo", True)
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
combodoc cmbTd
cargarListas LNAreas
cmbEc.AddItem "Soltero"
cmbEc.ItemData(cmbEc.NewIndex) = 1
cmbEc.AddItem "Casado"
cmbEc.ItemData(cmbEc.NewIndex) = 2
cmbEc.AddItem "Divorciado"
cmbEc.ItemData(cmbEc.NewIndex) = 3
cmbEc.AddItem "Viudo"
cmbEc.ItemData(cmbEc.NewIndex) = 4
busqueda = ""
Refrescar
loadGrid
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdSalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        Loro.Stop
        rstemp.CancelUpdate
        restablecer
        Cancel = 1
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
'Set rsRub = Nothing
Set rstemp = Nothing
End Sub


'      REFRESCAR

Private Sub Refrescar()
On Error Resume Next
Text1(0) = rstemp!legajo
Text1(1) = rstemp!nombre
Text1(2) = rstemp!apellido
txtdir(0) = rstemp("calle")
txtdir(1) = rstemp("numero")
txtdir(2) = rstemp("torre")
txtdir(3) = rstemp("piso")
txtdir(4) = rstemp("depto")
txtdir(5) = rstemp("barrio")
txtdir(6) = rstemp("localidad")
txtdir(7) = rstemp("cod_postal")
txtdir(8) = rstemp("email")
txtdir(9) = rstemp("tel")
fecNac.Value = rstemp("fecha_nac")
txtper(0) = rstemp("nro_documento")
Select Case rstemp!tipo_documento
    Case "DNI"
        cmbTd.ListIndex = 0
    Case "LE"
        cmbTd.ListIndex = 1
    Case "LC"
        cmbTd.ListIndex = 2
    Case "CI"
        cmbTd.ListIndex = 3
End Select
If rstemp("sexo") = "m" Then
Option1(0) = True
Else
Option1(1) = True
End If
Dim c
For c = 0 To cmbEc.ListCount - 1
    cmbEc.ListIndex = c
    If cmbEc.ItemData(c) = rstemp("estado_civil") Then
    Exit For
    End If
Next c
txtlab(0) = rstemp("cuil")
txtlab(1) = rstemp("cargo")
FecIng.Value = rstemp("fecha_ingreso")
cargarListas LAreas
cargarListas LNAreas
LoadList rstemp!legajo, LAreas, LNAreas
End Sub

'       CARGAR GRILLA

Private Sub loadGrid()
Dim regActual As Long
regActual = rstemp.AbsolutePosition
rstemp.MoveFirst
grdemp.ColWidth(0) = 1500
grdemp.ColWidth(1) = 2500
grdemp.ColWidth(2) = 2000
grdemp.ColWidth(3) = 2000
grdemp.Rows = 0
grdemp.AddItem "Legajo" & vbTab & "Nombres" & vbTab & "Apellidos" & vbTab & "Cargo" '& vbTab & "Fecha de Nac." & vbTab & "Sexo" & vbTab & "Calle" & vbTab & "N�mero" & vbTab & "Torre" & vbTab & "Piso" & vbTab & "Dpto." & vbTab & "Barrio" & vbTab & "Localidad" & vbTab & "C.P." & vbTab & "Tel�fono" & vbTab & "E-Mail"
grdemp.Rows = 2
grdemp.FixedRows = 1
grdemp.Rows = 1
While rstemp.EOF = False
grdemp.AddItem rstemp!legajo & vbTab & rstemp!nombre & vbTab & rstemp!apellido & vbTab & rstemp!cargo ' & vbTab & rstemp!nro_documento & vbTab & rstemp!fecha_nac & vbTab & rstemp!sexo & vbTab & rstemp!calle & vbTab & rstemp!numero & vbTab & rstemp!torre & vbTab & rstemp!piso & vbTab & rstemp!depto & vbTab & rstemp!barrio & vbTab & rstemp!localidad & vbTab & rstemp!cod_postal & vbTab & rstemp!tel & vbTab & rstemp!email
rstemp.MoveNext
Wend
rstemp.AbsolutePosition = regActual
End Sub

'           MOVER EMPLEADO

Private Sub cmdAdelante_Click()
If rstemp.EOF <> True Then: rstemp.MoveNext
If rstemp.EOF <> True Then
Else
rstemp.MoveFirst
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAtras_Click()
If rstemp.BOF <> True Then: rstemp.MovePrevious
If rstemp.BOF <> True Then
Else
rstemp.MoveLast
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdPrimero_Click()
rstemp.MoveFirst
Refrescar
cmdPrimero.Enabled = False
cmdUltimo.Enabled = True
End Sub

Private Sub cmdUltimo_Click()
rstemp.MoveLast
Refrescar
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
End Sub

Private Sub grdemp_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rstemp.MoveFirst
    rstemp.Find "legajo = " & grdemp.TextMatrix(grdemp.Row, 0)
    Refrescar
    mintCurFrame = 1
    band = False
Else
If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        rstemp.AbsolutePosition = grdemp.Row
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
End If
End Sub

'       NUEVO EMPLEADO

Sub nuevo()
BAdm = False
If TSemp.SelectedItem.Index <> 1 Then
f2(1).Visible = True
f2(Selec).Visible = False
Selec = 1
TSemp.Tabs(1).Selected = True
End If
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 1 To 2
Text1(c).Enabled = True
Text1(c).Text = ""
Next c
For c = 0 To 9
txtdir(c).Enabled = True
txtdir(c).Text = ""
Next c
txtper(0).Enabled = True
txtper(0).Text = ""
For c = 0 To 1
txtlab(c).Enabled = True
txtlab(c).Text = ""
Next c
Option1(0).Enabled = True
Option1(1).Enabled = True
Option1(0).Value = True
cmbTd.Enabled = True
cmbTd.ListIndex = 0
fecNac.Enabled = True
FecIng.Enabled = True
FecIng.Value = Date
cmbEc.Enabled = True
cmbEc.ListIndex = 0
rstemp.MoveLast
Text1(0) = nuevoid("legajo", "empleado")
bNue = True
LAreas.Clear
LAreas.Enabled = True
cargarListas LNAreas
If LAreas.ListCount = 0 Then
cmdQuiArea.Enabled = False
Else
cmdQuiArea.Enabled = True
End If
If LAreas.ListCount = maxPerm Then
cmdAgrArea.Enabled = False
Else
cmdAgrArea.Enabled = True
End If
Text1(1).SetFocus
Loro.Play "writing"
rstemp.AddNew
End Sub

'       MODIFICAR

Sub modificar()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdSalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
Dim c
For c = 1 To 2
Text1(c).Enabled = True
Next c
For c = 0 To 9
txtdir(c).Enabled = True
Next c
txtper(0).Enabled = True
For c = 0 To 1
txtlab(c).Enabled = True
Next c
Option1(0).Enabled = True
Option1(1).Enabled = True
cmbTd.Enabled = True
cmbEc.Enabled = True
bNue = False
If BAdm = False Then
    LAreas.Enabled = True
    If LAreas.ListCount = 0 Then
        cmdQuiArea.Enabled = False
    Else
        cmdQuiArea.Enabled = True
    End If
    If LAreas.ListCount = maxPerm Then
        cmdAgrArea.Enabled = False
    Else
        cmdAgrArea.Enabled = True
    End If
End If
Text1(1).SetFocus
Loro.Play "writing"
End Sub
Sub cerrar()
Unload Me
End Sub

'       GUARDAR

Sub guardar()
'On Error GoTo error
If BAdm = False And LAreas.ListCount = 0 Then MsgRapido "El empleado debe tener asignada un �rea de incumbencia", Loro, vbCritical, "Error ", "surprised": Exit Sub
Dim c
If txtdir(2) = "" Then: txtdir(2) = " "
If txtdir(4) = "" Then: txtdir(4) = " "
If txtdir(8) = "" Then: txtdir(8) = " "
For c = 0 To 2
    If Trim(Text1(c)) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
Next c
For c = 0 To 1
    If Trim(txtdir(c)) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
Next c
For c = 5 To 7
    If Trim(txtdir(c)) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
Next c
For c = 0 To 1
    If Trim(txtlab(c)) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
Next c
    If Trim(txtper(0)) = "" Then
        MsgRapido "Los datos est�n incompletos", Loro, vbCritical, "surprised"
        Exit Sub
    End If
Loro.Stop
rstemp!legajo = Text1(0).Text
rstemp!nombre = Text1(1).Text
rstemp!apellido = Text1(2).Text
rstemp!nro_documento = txtper(0).Text
rstemp!calle = txtdir(0).Text
rstemp!numero = txtdir(1).Text
rstemp!torre = txtdir(2).Text
If txtdir(3) = "" Then
    rstemp!piso = Null
    Else
    rstemp!piso = txtdir(3).Text
End If
rstemp!depto = txtdir(4).Text
rstemp!localidad = txtdir(6).Text
rstemp!barrio = txtdir(5).Text
rstemp!cod_postal = txtdir(7).Text
If txtdir(9) = "" Then
    rstemp!tel = Null
    Else
    rstemp!tel = txtdir(9).Text
End If
rstemp!Email = txtdir(8).Text
If Option1(0).Value = True Then
    rstemp!sexo = "m"
    Else
    rstemp!sexo = "f"
End If
rstemp!tipo_documento = cmbTd.Text
rstemp!fecha_nac = fecNac.Value
rstemp!estado_civil = cmbEc.ItemData(cmbEc.ListIndex)
rstemp!cuil = txtlab(0).Text
rstemp!fecha_ingreso = FecIng.Value
rstemp!cargo = txtlab(1)
If bNue Then
rstemp!password = txtper(0).Text
rstemp!nivel_acceso = "v"
MsgRapido "Su nuevo Password es su DNI", Loro, vbInformation, , "getattention"
End If
cmd.CommandText = "delete from perm_emp where legajo=" & Text1(0)
cmd.Execute
If BAdm = True Then
    cmd.CommandText = "insert into perm_emp(legajo,id_permiso) values(" & Text1(0) & ",0)"
    cmd.Execute
Else
    For c = 0 To LAreas.ListCount - 1
        cmd.CommandText = "insert into perm_emp(legajo,id_permiso) values(" & Text1(0) & "," & LAreas.ItemData(c) & ")"
        cmd.Execute
    Next c
End If
rstemp!borrado = "f"
rstemp.Update
rstemp.Requery
rstemp.Find "legajo = " & Text1(0)
restablecer
loadGrid
cmdAdelante.SetFocus
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
error:
If Err.Number = 380 Then
    MsgRapido "El empleado debe tener asignada un �rea de incumbencia", Loro, vbCritical, "Error ", "surprised"
    Exit Sub
End If
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar el Empleado Seleccionado", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rstemp!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rstemp.Update
rstemp.Requery
cmdAdelante_Click
loadGrid
End Sub

'       RESTABLECER

Private Sub restablecer()
Dim c
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdSalir.Caption = "&Salir"
cmdGuardar.Enabled = False
TabStrip1.Enabled = True
cmdPrimero.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdUltimo.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
For c = 0 To 2
Text1(c).Enabled = False
Next c
For c = 0 To 9
txtdir(c).Enabled = False
Next c
txtper(0).Enabled = False
For c = 0 To 1
txtlab(c).Enabled = False
Next c
Option1(0).Enabled = False
Option1(1).Enabled = False
cmbTd.Enabled = False
cmbEc.Enabled = False
fecNac.Enabled = False
FecIng.Enabled = False
LAreas.Enabled = False
cmdQuiArea.Enabled = False
cmdAgrArea.Enabled = False
fAgregar.Visible = False
Refrescar
End Sub

Private Sub Txtdir_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 3 Or Index = 9 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
    End If
End If
End Sub

Private Sub Txtlab_KeyPress(Index As Integer, KeyAscii As Integer)
If Index = 0 Then
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
    End If
End If
End Sub

Private Sub Txtper_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 Then
    KeyAscii = 0
    End If
    End If
End Sub

Sub imprimir()
frmPrincipal.desactivararch
Set rptEmpleados.DataSource = rstemp
rptEmpleados.PrintReport True
Unload rptEmpleados
End Sub

Sub preview()
frmPrincipal.desactivararch
Set rptEmpleados.DataSource = rstemp
rptEmpleados.Show
End Sub

Sub buscar()
busqueda = Input_Box("Buscar Empleado." & Chr(13) & "Ingrese el Apellido:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    If Text1(1) = busqueda Then: rstemp.MoveNext
    rstemp.Find "apellido like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rstemp.EOF <> True Then
    Refrescar
Else
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbCritical, , "explain"
End If
End Sub

'       VALIDAR TXT

Private Sub txtdir_Validate(Index As Integer, Cancel As Boolean)
If Index = 3 Or Index = 9 Then
    If txtdir(Index) <> "" Then
    If IsNumeric(txtdir(Index)) = False Then
        MsgRapido "El dato no es v�lido.", Loro, vbCritical, , "surprised"
        Cancel = True
        txtdir(Index) = ""
    End If
    End If
End If
End Sub

Private Sub txtper_Validate(Index As Integer, Cancel As Boolean)
    If txtper(Index) <> "" Then
    If IsNumeric(txtper(Index)) = False Then
        MsgRapido "El dato no es v�lido.", Loro, vbCritical, , "surprised"
        Cancel = True
        txtper(Index) = ""
    End If
    End If
End Sub
Private Sub txtlab_Validate(Index As Integer, Cancel As Boolean)
If Index = 0 Then
    If txtlab(Index) <> "" Then
    If IsNumeric(txtlab(Index)) = False Then
        MsgRapido "El dato no es v�lido.", Loro, vbCritical, , "surprised"
        Cancel = True
        txtlab(Index) = ""
    End If
    End If
End If
End Sub

'           AMB

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub

Private Sub TSemp_Click()
If TSemp.SelectedItem.Index = Selec Then Exit Sub
    f2(TSemp.SelectedItem.Index).Visible = True
    If TSemp.SelectedItem.Index = 3 Then
        f2(2).Visible = False
    End If
    f2(Selec).Visible = False
    Selec = TSemp.SelectedItem.Index
End Sub

Sub cargarListas(List As ListBox)
List.Clear
rstPerm.MoveFirst
While Not rstPerm.EOF
    List.AddItem rstPerm!nombre
    List.ItemData(List.NewIndex) = rstPerm!id_permiso
    rstPerm.MoveNext
Wend
End Sub
 
Sub LoadList(xLegajo As Long, List As ListBox, List2 As ListBox)
Dim c
List.Clear
rstPermEmp.Source = "select legajo,id_permiso from perm_emp where legajo=" & xLegajo
rstPermEmp.Open
BAdm = False
While Not rstPermEmp.EOF
    If rstPermEmp!id_permiso = 0 Then
        BAdm = True
    Else
        For c = 0 To List2.ListCount - 1
            If List2.ItemData(c) = rstPermEmp!id_permiso Then
                List.AddItem List2.List(c)
                List.ItemData(List.NewIndex) = List2.ItemData(c)
                List2.RemoveItem c
                Exit For
            End If
        Next c
    End If
rstPermEmp.MoveNext
Wend
If BAdm = True Then
    List.Clear
    cargarListas List2
End If
rstPermEmp.Close
End Sub

Private Sub cmdAgrArea_Click()
fAgregar.Visible = True
End Sub

Private Sub cmdQuiArea_Click()
On Error Resume Next
If LAreas.ListIndex = -1 Then Exit Sub
LNAreas.AddItem LAreas.Text
LNAreas.ItemData(LNAreas.NewIndex) = LAreas.ItemData(LAreas.ListIndex)
LAreas.RemoveItem (LAreas.ListIndex)
If LAreas.ListCount = 0 Then
    cmdQuiArea.Enabled = False
End If
cmdAgrArea.Enabled = True
End Sub

Private Sub cmdCanArea_Click()
fAgregar.Visible = False
End Sub

Private Sub LNAreas_DblClick()
LAreas.AddItem LNAreas.Text
LAreas.ItemData(LAreas.NewIndex) = LNAreas.ItemData(LNAreas.ListIndex)
LNAreas.RemoveItem (LNAreas.ListIndex)
cmdQuiArea.Enabled = True
If LNAreas.ListCount = 0 Then
    fAgregar.Visible = False
    cmdAgrArea.Enabled = False
End If
End Sub

