VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmActRaza 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actualizacion de Razas"
   ClientHeight    =   6930
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6495
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActRaza.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6930
   ScaleWidth      =   6495
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3420
      TabIndex        =   5
      Top             =   6480
      Width           =   1695
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   1380
      TabIndex        =   4
      Top             =   6480
      Width           =   1695
   End
   Begin VB.CommandButton cmdBorrar 
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   4320
      TabIndex        =   3
      Top             =   6000
      Width           =   1695
   End
   Begin VB.CommandButton cmdModificar 
      Caption         =   "&Modificar"
      Height          =   375
      Left            =   2400
      TabIndex        =   2
      Top             =   6000
      Width           =   1695
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   480
      TabIndex        =   1
      Top             =   6000
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   5175
      Index           =   1
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   6015
      Begin VB.Frame Frafoto 
         Height          =   3135
         Left            =   3360
         TabIndex        =   26
         Top             =   -120
         Width           =   2655
         Begin VB.CommandButton cmdBorFoto 
            Caption         =   "B&orrar"
            Enabled         =   0   'False
            Height          =   345
            Left            =   1320
            TabIndex        =   29
            Top             =   2640
            Width           =   1215
         End
         Begin VB.CommandButton cmdCargar 
            Caption         =   "C&argar"
            Enabled         =   0   'False
            Height          =   345
            Left            =   120
            TabIndex        =   28
            Top             =   2640
            Width           =   1095
         End
         Begin VB.PictureBox Foto 
            Enabled         =   0   'False
            Height          =   2295
            Left            =   120
            ScaleHeight     =   2235
            ScaleWidth      =   2355
            TabIndex        =   27
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.CommandButton cmdNueEsp 
         Caption         =   "+"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         TabIndex        =   25
         Top             =   1080
         Width           =   330
      End
      Begin VB.ComboBox cmbEspecie 
         Enabled         =   0   'False
         Height          =   345
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   1080
         Width           =   1575
      End
      Begin VB.ComboBox cmbTamanio 
         Enabled         =   0   'False
         Height          =   345
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   2040
         Width           =   1935
      End
      Begin VB.CommandButton cmdUltimo 
         Height          =   375
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   0
         Width           =   375
      End
      Begin VB.CommandButton cmdAdelante 
         Height          =   375
         Left            =   1680
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   0
         Width           =   375
      End
      Begin VB.CommandButton cmdAtras 
         Height          =   375
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   0
         Width           =   375
      End
      Begin VB.CommandButton cmdPrimero 
         Height          =   375
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   0
         Width           =   375
      End
      Begin VB.TextBox txtProblemas 
         Enabled         =   0   'False
         Height          =   615
         Left            =   0
         MaxLength       =   100
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   14
         Top             =   4560
         Width           =   6015
      End
      Begin VB.TextBox txtCaracteristicas 
         Enabled         =   0   'False
         Height          =   1095
         Left            =   0
         MaxLength       =   65000
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   13
         Top             =   3120
         Width           =   6015
      End
      Begin VB.TextBox txtProcedencia 
         Enabled         =   0   'False
         Height          =   330
         Left            =   1200
         MaxLength       =   20
         TabIndex        =   11
         Top             =   1560
         Width           =   1935
      End
      Begin VB.TextBox txtNombre 
         Enabled         =   0   'False
         Height          =   330
         Left            =   1200
         MaxLength       =   20
         TabIndex        =   10
         Top             =   600
         Width           =   1935
      End
      Begin MSComDlg.CommonDialog CDialog 
         Left            =   2400
         Top             =   2520
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label lblProblemas 
         Caption         =   "Problemas Cong�nitos:"
         Height          =   255
         Left            =   0
         TabIndex        =   22
         Top             =   4320
         Width           =   2055
      End
      Begin VB.Label lblCaracteristicas 
         Caption         =   "Caracter�sticas:"
         Height          =   255
         Left            =   0
         TabIndex        =   21
         Top             =   2760
         Width           =   1335
      End
      Begin VB.Label lblEspecie 
         Caption         =   "Especie:"
         Height          =   255
         Left            =   0
         TabIndex        =   20
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label lblTamanio 
         Caption         =   "Tama�o:"
         Height          =   255
         Left            =   0
         TabIndex        =   19
         Top             =   2040
         Width           =   855
      End
      Begin VB.Label lblProcedencia 
         Caption         =   "Procedencia:"
         Height          =   255
         Left            =   0
         TabIndex        =   18
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label lblNombre 
         Caption         =   "Nombre:"
         Height          =   255
         Left            =   0
         TabIndex        =   17
         Top             =   600
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   5175
      Index           =   2
      Left            =   240
      TabIndex        =   16
      Top             =   600
      Width           =   6015
      Begin MSFlexGridLib.MSFlexGrid grdRazas 
         Height          =   4575
         Left            =   0
         TabIndex        =   23
         Top             =   0
         Width           =   6015
         _ExtentX        =   10610
         _ExtentY        =   8070
         _Version        =   393216
         Rows            =   1
         Cols            =   6
         FixedRows       =   0
         FixedCols       =   0
         FocusRect       =   0
         SelectionMode   =   1
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   5775
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   10186
      TabWidthStyle   =   2
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Actualizaci�n"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Listado"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmActRaza"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mintCurFrame, xid_raza, c As Integer
Dim busqueda As String
Dim xespecie, xtamanio As String
Dim band, bNue As Boolean
Dim rst As ADODB.Recordset
Dim rse As ADODB.Recordset
Dim rsr As ADODB.Recordset
Dim fs
Dim borFoto As Boolean

Private Sub cmdAdelante_Click()
If rst.EOF <> True Then: rst.MoveNext
If rst.EOF <> True Then
Else
rst.MoveFirst
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdAtras_Click()
If rst.BOF <> True Then: rst.MovePrevious
If rst.BOF <> True Then
Else
rst.MoveLast
End If
cmdPrimero.Enabled = True
cmdUltimo.Enabled = True
Refrescar
End Sub

Private Sub cmdBorFoto_Click()
borFoto = True
Foto.Picture = Nothing
CDialog.FileName = ""
End Sub

Private Sub cmdNueEsp_Click()
frmActRaza.Enabled = False
frmActEspecie.nuevo
End Sub

Private Sub cmdPrimero_Click()
rst.MoveFirst
cmdUltimo.Enabled = True
cmdPrimero.Enabled = False
Refrescar
End Sub

Private Sub cmdUltimo_Click()
rst.MoveLast
cmdUltimo.Enabled = False
cmdPrimero.Enabled = True
Refrescar
End Sub

Private Sub cmdsalir_Click()
cerrar
End Sub

Private Sub cmdBorrar_Click()
borrar
End Sub

Private Sub cmdGuardar_Click()
guardar
End Sub

Private Sub cmdModificar_Click()
modificar
End Sub

Private Sub cmdNuevo_Click()
nuevo
End Sub
Private Sub Form_Activate()
If mintCurFrame = 2 Then
    frmPrincipal.desactivararch
    frmPrincipal.imprimir.Enabled = True
    frmPrincipal.vista.Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
    frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
Else
    If cmdsalir.Caption = "&Cancelar" Then
        frmPrincipal.desactivararch
        frmPrincipal.guardar.Enabled = True
        frmPrincipal.Toolbar1.Buttons(3).Enabled = True
    Else
        frmPrincipal.activararch
        frmPrincipal.guardar.Enabled = False
        frmPrincipal.Toolbar1.Buttons(3).Enabled = False
    End If
End If
End Sub

Private Sub Form_Load()
Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
mintCurFrame = 1
band = False
bNue = False
Set fs = CreateObject("Scripting.FileSystemObject")
Set rst = New ADODB.Recordset
Set rse = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.CursorType = adOpenDynamic
rst.LockType = adLockOptimistic
rst.Source = "select * from raza where borrado = 'f'"
rst.Open
Set rse = abrirrs("select id_especie, nombre from especie", True)
cmdPrimero.Picture = LoadPicture(App.Path & "\bitmaps\Movfirst.bmp")
cmdAtras.Picture = LoadPicture(App.Path & "\bitmaps\Movprevi.bmp")
cmdAdelante.Picture = LoadPicture(App.Path & "\bitmaps\Movnext.bmp")
cmdUltimo.Picture = LoadPicture(App.Path & "\bitmaps\Movlast.bmp")
comboEspecie
cmbTamanio.AddItem "Chico"
cmbTamanio.ItemData(cmbTamanio.NewIndex) = 1
cmbTamanio.AddItem "Mediano"
cmbTamanio.ItemData(cmbTamanio.NewIndex) = 2
cmbTamanio.AddItem "Grande"
cmbTamanio.ItemData(cmbTamanio.NewIndex) = 3
cmbTamanio.AddItem "Muy Grande"
cmbTamanio.ItemData(cmbTamanio.NewIndex) = 4
busqueda = ""
Refrescar
loadGrid
End Sub

Private Sub Form_Unload(Cancel As Integer)
If cmdsalir.Caption = "&Cancelar" Then
    If Mensaje("�Desea cancelar los cambios efectuados?", Loro, Balloon, vbYesNo + vbQuestion, , "getattention") = vbNo Then
        Cancel = 1
        Exit Sub
    Else
        Loro.Stop
        rst.CancelUpdate
        restablecer
        rst.MoveFirst
        Refrescar
        Cancel = 1
        'Loro.Play "explain"
        MsgRapido "Los cambios se han cancelado.", Loro, vbInformation, , "explain"
        Exit Sub
    End If
End If
frmPrincipal.desactivararch
rst.Close
If cargado("frmactmascota") = True Then frmactmascota.Enabled = True: frmactmascota.comboraza
End Sub

Sub TabStrip1_Click()
If band = True Then
    Frame1(1).Visible = True
    Frame1(2).Visible = False
    cmdNuevo.Enabled = True
    cmdModificar.Enabled = True
    cmdBorrar.Enabled = True
    frmPrincipal.activararch
    rst.MoveFirst
    rst.Find "id_raza = " & grdRazas.TextMatrix(grdRazas.Row, 5)
    Refrescar
    mintCurFrame = 1
    band = False
Else
   If TabStrip1.SelectedItem.Index = mintCurFrame Then Exit Sub
    Frame1(TabStrip1.SelectedItem.Index).Visible = True
    Frame1(mintCurFrame).Visible = False
    If Frame1(2).Visible = True Then
        cmdNuevo.Enabled = False
        cmdModificar.Enabled = False
        cmdBorrar.Enabled = False
        frmPrincipal.desactivararch
        frmPrincipal.imprimir.Enabled = True
        frmPrincipal.vista.Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(1).Enabled = True
        frmPrincipal.Toolbar2.Buttons.Item(2).Enabled = True
    Else
        cmdNuevo.Enabled = True
        cmdModificar.Enabled = True
        cmdBorrar.Enabled = True
        frmPrincipal.activararch
        Refrescar
    End If
    mintCurFrame = TabStrip1.SelectedItem.Index
End If
End Sub

Private Sub Refrescar()
If rst.RecordCount <> 0 Then
    txtNombre = rst!nombre_raza
    txtProcedencia = rst!procedencia
    For c = 0 To cmbEspecie.ListCount - 1
        If cmbEspecie.ItemData(c) = rst!especie Then cmbEspecie.ListIndex = c: Exit For
    Next
    On Error GoTo err1
    txtCaracteristicas = rst!caracteristicas
    On Error GoTo err2
    txtProblemas = rst!prob_congenitos
    On Error Resume Next
    Foto.Picture = LoadPicture()
    Foto.Picture = LoadPicture(App.Path & "\fotos\" & rst!id_raza & ".ftr", vbLPCustom, vbLPColor)
    On Error GoTo 0
    For c = 0 To cmbTamanio.ListCount - 1
        cmbTamanio.ListIndex = c
        If cmbTamanio.ItemData(c) = rst!especie Then: Exit For
    Next c
End If
CDialog.FileName = ""
err1:
If Err.Number = 94 Then
txtCaracteristicas = ""
Resume Next
End If
err2:
If Err.Number = 94 Then
txtProblemas = ""
Resume Next
End If
End Sub

Sub nuevo()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdsalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
txtNombre.Enabled = True
cmbEspecie.Enabled = True
txtProcedencia.Enabled = True
cmbTamanio.Enabled = True
txtCaracteristicas.Enabled = True
txtProblemas.Enabled = True
cmdNueEsp.Enabled = True
cmdCargar.Enabled = True
cmdBorFoto.Enabled = True
txtNombre = ""
cmbEspecie.ListIndex = 0
txtProcedencia = ""
cmbTamanio.ListIndex = 0
txtCaracteristicas = ""
txtProblemas = ""
rst.MoveLast
xid_raza = nuevoid("id_raza", "raza")
Loro.Play "writing"
rst.AddNew
Foto.Picture = Nothing
CDialog.FileName = ""
bNue = True
End Sub

Sub modificar()
cmdNuevo.Enabled = False
cmdModificar.Enabled = False
cmdBorrar.Enabled = False
cmdsalir.Caption = "&Cancelar"
cmdGuardar.Enabled = True
TabStrip1.Enabled = False
cmdPrimero.Enabled = False
cmdAtras.Enabled = False
cmdAdelante.Enabled = False
cmdUltimo.Enabled = False
frmPrincipal.desactivararch
frmPrincipal.guardar.Enabled = True
frmPrincipal.Toolbar1.Buttons(3).Enabled = True
txtNombre.Enabled = True
cmbEspecie.Enabled = True
cmdCargar.Enabled = True
cmdBorFoto.Enabled = True
txtProcedencia.Enabled = True
txtCaracteristicas.Enabled = True
txtProblemas.Enabled = True
cmbTamanio.Enabled = True
cmdNueEsp.Enabled = True
bNue = False
xid_raza = rst!id_raza
CDialog.FileName = ""
Loro.Play "writing"
End Sub

Sub borrar()
If Mensaje("Est� seguro de querer eliminar la Raza Seleccionada", Loro, Balloon, vbQuestion + vbYesNo, , "getattention") = vbNo Then: Exit Sub
rst!borrado = "v"
MsgRapido "Los Datos se han Borrado.", Loro, vbInformation, , "annouce"
rst.Update
rst.Requery
cmdAdelante_Click
loadGrid
End Sub

Sub guardar()
If Trim(txtNombre) = "" Or Trim(txtProcedencia) = "" Then
    MsgRapido "Los datos est�n incompletos", Loro, vbCritical, , "surprised"
    Exit Sub
End If
If CDialog.FileName <> "" Then
    FileCopy CDialog.FileName, App.Path & "\fotos\" & xid_raza & ".ftr"
End If
If borFoto = True Then
    If fs.fileexists(App.Path & "\fotos\" & xid_raza & ".ftr") = True Then fs.deletefile App.Path & "\fotos\" & xid_raza & ".ftr"
End If
Loro.Stop
rst!id_raza = xid_raza
rst!nombre_raza = txtNombre.Text
rst!procedencia = txtProcedencia
rst!especie = cmbEspecie.ItemData(cmbEspecie.ListIndex)
rst!caracteristicas = txtCaracteristicas
rst!prob_congenitos = txtProblemas
rst!tamanio = cmbTamanio.ItemData(cmbTamanio.ListIndex)
rst!borrado = "f"
rst.Update
restablecer
rst.Requery
loadGrid
'Loro.Play "explain"
'Loro.Speak "Los datos se han guardado."
MsgRapido "Los datos se han guardado.", Loro, vbInformation, , "explain"
End Sub
Sub cerrar()
Unload Me
End Sub
Private Sub restablecer()
cmdNuevo.Enabled = True
cmdModificar.Enabled = True
cmdBorrar.Enabled = True
cmdsalir.Caption = "&Salir"
cmdGuardar.Enabled = False
TabStrip1.Enabled = True
cmdPrimero.Enabled = True
cmdAtras.Enabled = True
cmdAdelante.Enabled = True
cmdUltimo.Enabled = True
frmPrincipal.activararch
frmPrincipal.guardar.Enabled = False
txtNombre.Enabled = False
txtProcedencia.Enabled = False
cmbEspecie.Enabled = False
cmdNueEsp.Enabled = False
cmdCargar.Enabled = False
cmdBorFoto.Enabled = False
txtCaracteristicas.Enabled = False
txtProblemas.Enabled = False
cmbTamanio.Enabled = False
bNue = False
End Sub

Private Sub loadGrid()
Dim regActual As Long
regActual = rst.AbsolutePosition
rst.MoveFirst
grdRazas.ColWidth(0) = 1800
grdRazas.ColWidth(1) = 1250
grdRazas.ColWidth(2) = 1540
grdRazas.ColWidth(3) = 1250
grdRazas.ColWidth(4) = 0
grdRazas.ColWidth(5) = 0
grdRazas.Rows = 0
grdRazas.AddItem "Nombre" & vbTab & "Especie" & vbTab & "Procedencia" & vbTab & "Tama�o"
grdRazas.Rows = 2
grdRazas.FixedRows = 1
grdRazas.Rows = 1
While rst.EOF = False
Select Case rst!tamanio
    Case 1
        xtamanio = "Chico"
    Case 2
        xtamanio = "Mediano"
    Case 3
        xtamanio = "Grande"
    Case 4
        xtamanio = "Muy Grande"
End Select
rse.MoveFirst
Do While Not rse.EOF
    If rse!id_especie = rst!especie Then
        xespecie = rse!nombre
        rse.MoveLast
    End If
    rse.MoveNext
Loop
grdRazas.AddItem rst!nombre_raza & vbTab & xespecie & vbTab & rst!procedencia & vbTab & xtamanio & vbTab & rst!prob_congenitos & vbTab & rst!id_raza
rst.MoveNext
Wend
rst.AbsolutePosition = regActual
End Sub

Sub comboEspecie()
cmbEspecie.Clear
rse.Requery
If rse.RecordCount = 0 Then
    Exit Sub
End If
rse.MoveFirst
Do While Not rse.EOF
   cmbEspecie.AddItem rse!nombre 'Descripcion
   cmbEspecie.ItemData(cmbEspecie.NewIndex) = rse!id_especie
   rse.MoveNext
Loop
cmbEspecie.ListIndex = 0
End Sub

Private Sub grdrazas_DblClick()
band = True
Me.TabStrip1_Click
TabStrip1.Tabs(1).Selected = True
End Sub

Sub buscar()
Dim regActual
busqueda = Input_Box("Buscar Raza." & Chr(13) & "Ingrese el Nombre:", Loro, Balloon, busqueda, "searching")
If busqueda <> "" Then
    regActual = rst.AbsolutePosition
    If InStr(1, txtNombre, busqueda, vbTextCompare) = 0 Then: rst.Find "nombre_raza like '" & busqueda & "*'"
Else
    Loro.Stop
    Exit Sub
End If
Loro.Stop
If rst.EOF <> True Then
    Refrescar
Else
    rst.AbsolutePosition = regActual
    MsgRapido "No se hall� ninguna coincidencia.", Loro, vbExclamation, , "explain"
End If
End Sub

'GENERAR rs
Private Sub GrarRs()
Set rsr = Nothing
Set rsr = New ADODB.Recordset
rsr.Fields.Append "nombre", adVarChar, 30
rsr.Fields.Append "especie", adVarChar, 30
rsr.Fields.Append "procedencia", adVarChar, 30
rsr.Fields.Append "tamanio", adVarChar, 30
rsr.Fields.Append "problemas", adVarChar, 100
rsr.LockType = adLockOptimistic
rsr.CursorType = adOpenKeyset
rsr.Open
Dim i, j As Integer
For i = 1 To grdRazas.Rows - 1
    rsr.AddNew
    For j = 0 To grdRazas.Cols - 2
        rsr(j) = grdRazas.TextMatrix(i, j)
    Next j
    rsr.Update
Next i
End Sub

Private Sub cmdcargar_Click()
CDialog.Filter = "Todas las Imagenes|*.jpg;*.gif;*.bmp;*.ico;*.wmf;*.emf|Archivos JPEG (*.jpg)|*.jpg|Compuserve GIF (*.gif)|*.gif|Mapa de Bits de Windows (*.bmp)|*.bmp|Iconos de Windows (*.ico)|*.ico|Metarchivos (*.wmf)|*.wmf|Metarchivos Mejorados (*.emf)|*.emf"
CDialog.InitDir = App.Path & "\fotos"
CDialog.ShowOpen
If Right(CDialog.FileTitle, 3) = "jpg" Or Right(CDialog.FileTitle, 3) = "gif" Or Right(CDialog.FileTitle, 3) = "bmp" Or Right(CDialog.FileTitle, 3) = "ico" Or Right(CDialog.FileTitle, 3) = "wmf" Or Right(CDialog.FileTitle, 3) = "emf" Then
    Foto.Picture = LoadPicture(CDialog.FileName)
    borFoto = False
Else
    Exit Sub
End If
End Sub

Sub imprimir()
GrarRs
frmPrincipal.desactivararch
Set rptListaRazas.DataSource = rsr
rptListaRazas.PrintReport True
Unload rptListaRazas
End Sub
Sub preview()
GrarRs
frmPrincipal.desactivararch
Set rptListaRazas.DataSource = rsr
rptListaRazas.Show
End Sub
