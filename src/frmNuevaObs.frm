VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmNuevaObs 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nueva Observaci�n"
   ClientHeight    =   4515
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5025
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmNuevaObs.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4515
   ScaleWidth      =   5025
   Begin VB.TextBox txtJaula 
      Height          =   330
      Left            =   3626
      TabIndex        =   20
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtDosis 
      Height          =   330
      Left            =   3720
      MaxLength       =   15
      TabIndex        =   7
      Top             =   3000
      Width           =   1095
   End
   Begin VB.TextBox txtEvolucion 
      Height          =   330
      Left            =   1312
      MaxLength       =   15
      TabIndex        =   8
      Top             =   3480
      Width           =   3495
   End
   Begin VB.TextBox txtPatologia 
      Height          =   330
      Left            =   1312
      MaxLength       =   30
      TabIndex        =   4
      Top             =   1560
      Width           =   3375
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2685
      TabIndex        =   10
      Top             =   3960
      Width           =   1455
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   885
      TabIndex        =   9
      Top             =   3960
      Width           =   1455
   End
   Begin VB.ComboBox cmbMedicacion 
      Height          =   345
      Left            =   1320
      Sorted          =   -1  'True
      TabIndex        =   6
      Text            =   "cmbMedicacion"
      Top             =   3000
      Width           =   1695
   End
   Begin VB.TextBox txtDescripcion 
      Height          =   735
      Left            =   105
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   2160
      Width           =   4815
   End
   Begin VB.CheckBox chkAlta 
      Caption         =   "&Dar de Alta"
      Height          =   255
      Left            =   1905
      TabIndex        =   3
      Top             =   1200
      Width           =   1215
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   330
      Left            =   1758
      TabIndex        =   13
      Top             =   720
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   393216
      BuddyControl    =   "txtPeso"
      BuddyDispid     =   196618
      OrigLeft        =   2760
      OrigTop         =   1320
      OrigRight       =   3000
      OrigBottom      =   1695
      Max             =   999
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtPeso 
      Height          =   330
      Left            =   1023
      TabIndex        =   2
      Top             =   720
      Width           =   735
   End
   Begin VB.TextBox txtHora 
      Enabled         =   0   'False
      Height          =   330
      Left            =   3165
      TabIndex        =   1
      Top             =   240
      Width           =   1335
   End
   Begin VB.TextBox txtFecha 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1125
      TabIndex        =   0
      Top             =   240
      Width           =   1335
   End
   Begin VB.Label lbljaula 
      Caption         =   "Jaula N�"
      Height          =   255
      Left            =   2838
      TabIndex        =   21
      Top             =   720
      Width           =   855
   End
   Begin VB.Label lblDosis 
      Caption         =   "Dosis:"
      Height          =   255
      Left            =   3120
      TabIndex        =   19
      Top             =   3000
      Width           =   615
   End
   Begin VB.Label lblEvolucion 
      Caption         =   "Evoluci�n:"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   3480
      Width           =   855
   End
   Begin VB.Label lblPatologia 
      Caption         =   "Patolog�a:"
      Height          =   255
      Left            =   337
      TabIndex        =   17
      Top             =   1560
      Width           =   855
   End
   Begin VB.Label lblMedicamento 
      Caption         =   "Medicaci�n:"
      Height          =   255
      Left            =   240
      TabIndex        =   16
      Top             =   3000
      Width           =   1095
   End
   Begin VB.Label lblObservacion 
      Caption         =   "Acciones tomadas:"
      Height          =   255
      Left            =   1725
      TabIndex        =   15
      Top             =   1920
      Width           =   1575
   End
   Begin VB.Label lblPeso 
      Caption         =   "Peso:                          Kgrs."
      Height          =   255
      Left            =   423
      TabIndex        =   14
      Top             =   720
      Width           =   2175
   End
   Begin VB.Label lblHora 
      Caption         =   "Hora:"
      Height          =   255
      Left            =   2685
      TabIndex        =   12
      Top             =   240
      Width           =   495
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   525
      TabIndex        =   11
      Top             =   240
      Width           =   615
   End
End
Attribute VB_Name = "frmNuevaObs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim xid_hc As Integer
Dim xPeso As Single
Dim xNro_ficha As Integer
Dim nocomp As Boolean
Dim cmd As ADODB.Command
Dim rst As ADODB.Recordset
Dim xNro_jaula

Private Sub cmbMedicacion_Change()
If nocomp = False Then completar cmbMedicacion Else: nocomp = False
End Sub

Private Sub cmbMedicacion_Click()
cmdAceptar.Enabled = True
End Sub

Private Sub cmbMedicacion_KeyPress(KeyAscii As Integer)
cmdAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbMedicacion.Text = Left(cmbMedicacion.Text, cmbMedicacion.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbMedicacion
End Sub

Private Sub cmbMedicacion_LostFocus()
validar cmbMedicacion
End Sub

Private Sub cmdaceptar_Click()
Dim op, c
Dim xPrecio
op = Mensaje("�Desea guardar los datos ingresados?", Loro, Balloon, vbQuestion + vbYesNoCancel, , "getattention")
Select Case op
    Case vbYes
If Trim(txtPeso) = "" Or Trim(txtPatologia) = "" Or Trim(txtDescripcion) = "" Or Trim(txtEvolucion) = "" Or cmbMedicacion.ListIndex = -1 Then MsgRapido "Los datos est�n incompletos.", Loro, vbCritical, , "sad": Exit Sub
If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) <> 0 And Trim(txtDosis) = "" Then MsgRapido "Los datos est�n incompletos.", Loro, vbCritical, , "sad": Exit Sub
If cmbMedicacion.ItemData(cmbMedicacion.ListIndex) <> 0 Then
    rst.Source = "select stock_total,precio from medicamento where id_medicamento=" & cmbMedicacion.ItemData(cmbMedicacion.ListIndex)
    rst.Open
    If rst!stock_total < 1 Then
        MsgRapido "El stock del medicamento seleccionado se ha agotado.", Loro, vbInformation, , "surprised"
        rst.Close
        Exit Sub
    End If
    cmd.CommandText = "update medicamento set stock_total=stock_total-1 where id_medicamento=" & cmbMedicacion.ItemData(cmbMedicacion.ListIndex)
    cmd.Execute
    cmd.CommandText = "insert into nofacturado(id_cliente,fecha,hora,tipo,id_item,precio) values(" & frmhistoria.cmbCliente.ItemData(frmhistoria.cmbCliente.ListIndex) & ",'" & DateDB & "','" & TimeDB & "','med'," & cmbMedicacion.ItemData(cmbMedicacion.ListIndex) & "," & rst!Precio & ")"
    cmd.Execute
    rst.Close
End If
cmd.CommandText = "insert into ficha_internacion(nro_ficha,id_hc,peso,descripcion,id_medicamento,id_empleado,fecha,hora,patologia,evolucion,dosis,jaula) values(" & xNro_ficha & "," & xid_hc & "," & xPeso & ",'" & txtDescripcion & "'," & cmbMedicacion.ItemData(cmbMedicacion.ListIndex) & "," & xLegajo & ",'" & DateDB & "','" & TimeDB & "','" & txtPatologia & "','" & txtEvolucion & "','" & txtDosis & "'," & txtJaula & ")"
cmd.Execute
If chkAlta.Value = 1 Then
    rst.Source = "select precio from servicio where id_servicio=4"
    rst.Open
    xPrecio = rst!Precio
    rst.Close
    rst.Source = "select fecha_internacion,id_cliente from mascota_hc where id_hc=" & xid_hc
    rst.Open
    For c = 0 To DateValue(Format(rst!fecha_internacion, "dd/mm/yyyy")) - Date
        cmd.CommandText = "insert into nofacturado(id_cliente,fecha,hora,tipo,id_item,precio) values(" & rst!id_cliente & ",'" & DateDB & "','" & TimeDB & "','ser',4," & xPrecio & ")"
        cmd.Execute
    Next c
    rst.Close
    cmd.CommandText = "update mascota_hc set internado='f' where id_hc=" & xid_hc
    cmd.Execute
    cmd.CommandText = "insert into detalle_hc values(" & xid_hc & ",'" & DateDB & "','" & TimeDB & "'," & txtPeso & ",' ',4,0,' '," & xLegajo & ",' ','Fin de Internaci�n')"
    cmd.Execute
End If
frmInternacion.loadGrid
Unload Me
    Case vbNo
        Unload Me
    Case vbCancel
        Exit Sub
End Select
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
End Sub

Private Sub Form_Load()
frmNuevaObs.Top = 1155
frmNuevaObs.Left = 3285
Set rst = New ADODB.Recordset
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
rst.ActiveConnection = cnx
rst.CursorType = adOpenForwardOnly
rst.LockType = adLockReadOnly
rst.Source = "select max(nro_ficha)+1 as nro from ficha_internacion"
rst.Open
xNro_ficha = rst!nro
rst.Close
rst.Source = "select nombre,id_medicamento from medicamento where borrado='f' order by id_medicamento"
rst.Open
txtFecha = Date
txtHora = Time
txtPeso = xPeso
txtJaula = xNro_jaula
While rst.EOF = False
    cmbMedicacion.AddItem rst!nombre
    cmbMedicacion.ItemData(cmbMedicacion.NewIndex) = rst!id_medicamento
    rst.MoveNext
Wend
rst.Close
cmbMedicacion.ListIndex = 0
End Sub
Sub setID(ByVal paramId_hc As Integer, ByVal paramPeso As Single, ByVal paramNroFicha As Integer, ByVal nro_jaula As Integer)
xid_hc = paramId_hc
xPeso = paramPeso
xNro_ficha = paramNroFicha
xNro_jaula = nro_jaula
End Sub

Private Sub Form_Unload(Cancel As Integer)
frmInternacion.Enabled = True
End Sub
Private Sub txtPeso_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtPeso_Validate(Cancel As Boolean)
If txtPeso <> "" Then
    If IsNumeric(txtPeso) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtPeso = ""
    End If
End If
End Sub
Private Sub UpDown1_DownClick()
txtPeso = Val(txtPeso) - 0.1
End Sub

Private Sub UpDown1_UpClick()
txtPeso = Val(txtPeso) + 0.1
End Sub
