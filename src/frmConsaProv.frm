VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConsaProv 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Reclamos a Proveedores"
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7755
   Icon            =   "frmConsaProv.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5055
   ScaleWidth      =   7755
   Begin VB.Frame Frame2 
      Caption         =   "Filtrar por Proveedor:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   4800
      TabIndex        =   9
      Top             =   2280
      Width           =   2775
      Begin VB.CommandButton cmdaceptar 
         Caption         =   "&Aceptar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   960
         TabIndex        =   11
         Top             =   840
         Width           =   975
      End
      Begin VB.ComboBox cboxprov 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   360
         Width           =   2535
      End
   End
   Begin VB.TextBox txtDescripcion 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   3120
      Width           =   4335
   End
   Begin VB.CommandButton cmdver 
      Caption         =   "&Ver Descripci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5280
      TabIndex        =   6
      Top             =   1800
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ordenar por:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   4800
      TabIndex        =   2
      Top             =   240
      Width           =   2775
      Begin VB.OptionButton optprov 
         Caption         =   "Proveedor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   960
         Width           =   1575
      End
      Begin VB.OptionButton optfecha 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   1335
      End
      Begin VB.OptionButton optnroorden 
         Caption         =   "Orden de Compra"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Value           =   -1  'True
         Width           =   1815
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grid1 
      Height          =   2295
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   4048
      _Version        =   393216
      Cols            =   3
      FixedCols       =   0
      AllowBigSelection=   0   'False
      HighLight       =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdsalir 
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6480
      TabIndex        =   0
      Top             =   4560
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Reclamos Registrados:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   12
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label lbldescripcion 
      Caption         =   "Descripci�n:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   2880
      Width           =   1095
   End
End
Attribute VB_Name = "frmConsaProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rst As ADODB.Recordset
Dim rstprov As ADODB.Recordset

Private Sub cmdaceptar_Click()
 grid1.Clear
 rst.Source = "select r.nro_orden, r.fecha, p.razon_social, r.descripcion from reclamo_pedido r, proveedor p where r.nro_reclamo>0 and r.proveedor=p.id_proveedor and p.razon_social= '" & cboxprov.Text & "' order by r.nro_orden"
 rst.Open
 grid1.Rows = 0
 grid1.AddItem "Nro Orden" & vbTab & "Fecha" & vbTab & "Raz�n Social"
 grid1.Rows = 2
 grid1.FixedRows = 1
 grid1.Rows = 1
 If rst.RecordCount > 0 Then
    rst.MoveFirst
    While rst.EOF = False
       grid1.AddItem rst!nro_orden & vbTab & rst!fecha & vbTab & rst!razon_social
       rst.MoveNext
    Wend
   Else
    MsgRapido "No existen reclamos de pedidos.", Loro, vbInformation, , "surprised"
 End If
 rst.Close
End Sub

Private Sub cmdsalir_Click()
 Unload Me
End Sub

Private Sub cmdver_Click()
 If grid1.Rows > 1 Then
   Dim a�o, mes, dia As Integer
   Dim fecha
   dia = Day(grid1.TextMatrix(grid1.Row, 1))
   mes = Month(grid1.TextMatrix(grid1.Row, 1))
   a�o = Year(grid1.TextMatrix(grid1.Row, 1))
   fecha = a�o & "-" & mes & "-" & dia
   rst.Source = "select r.nro_orden, r.fecha, p.razon_social, r.descripcion from reclamo_pedido r, proveedor p where r.nro_reclamo>0 and r.proveedor=p.id_proveedor and r.nro_orden= " & grid1.TextMatrix(grid1.Row, 0) & " and r.fecha= '" & fecha & "' and p.razon_social= '" & grid1.TextMatrix(grid1.Row, 2) & "'"
   rst.Open
   txtDescripcion.Text = rst(3)
   rst.Close
 End If
End Sub

Private Sub Form_Load()
 Me.Left = (frmPrincipal.ScaleWidth - Me.Width) / 2
 Me.Top = (frmPrincipal.ScaleHeight - Me.Height) / 2
 Set rst = New ADODB.Recordset
 rst.ActiveConnection = cnx
 rst.CursorType = adOpenDynamic
 rst.LockType = adLockOptimistic
 rst.Source = "select r.nro_orden, r.fecha, p.razon_social, r.descripcion from reclamo_pedido r, proveedor p where r.nro_reclamo>0 and r.proveedor=p.id_proveedor order by r.nro_orden asc"
 rst.Open
 Set rstprov = New ADODB.Recordset
 rstprov.ActiveConnection = cnx
 rstprov.CursorType = adOpenDynamic
 rstprov.LockType = adLockOptimistic
 rstprov.Source = "select razon_social, id_proveedor from proveedor order by razon_social"
 rstprov.Open
 rstprov.MoveFirst
 While rstprov.EOF = False
    cboxprov.AddItem rstprov(0)
    rstprov.MoveNext
 Wend
 rstprov.Close
 grid1.ColWidth(2) = 2300
 grid1.Rows = 0
 grid1.AddItem "Nro Orden" & vbTab & "Fecha" & vbTab & "Raz�n Social"
 grid1.Rows = 2
 grid1.FixedRows = 1
 grid1.Rows = 1
 If rst.RecordCount > 0 Then
    rst.MoveFirst
    While rst.EOF = False
      grid1.AddItem rst!nro_orden & vbTab & rst!fecha & vbTab & rst!razon_social
      rst.MoveNext
    Wend
   Else
    MsgRapido "No existen reclamos de pedidos.", Loro, vbInformation, , "surprised"
 End If
 rst.Close
End Sub

Private Sub optfecha_Click()
 If optfecha.Value = True Then
    grid1.Clear
    rst.Source = "select r.nro_orden, r.fecha, p.razon_social, r.descripcion from reclamo_pedido r, proveedor p where r.nro_reclamo>0 and r.proveedor=p.id_proveedor order by r.fecha"
    rst.Open
    grid1.Rows = 0
    grid1.AddItem "Nro Orden" & vbTab & "Fecha" & vbTab & "Raz�n Social"
    grid1.Rows = 2
    grid1.FixedRows = 1
    grid1.Rows = 1
    If rst.RecordCount > 0 Then
       rst.MoveFirst
       While rst.EOF = False
         grid1.AddItem rst!nro_orden & vbTab & rst!fecha & vbTab & rst!razon_social
         rst.MoveNext
       Wend
      Else
       MsgRapido "No existen reclamos de pedidos.", Loro, vbInformation, , "surprised"
    End If
    rst.Close
 End If
End Sub

Private Sub optnroorden_Click()
 If optnroorden.Value = True Then
    grid1.Clear
    rst.Source = "select r.nro_orden, r.fecha, p.razon_social, r.descripcion from reclamo_pedido r, proveedor p where r.nro_reclamo>0 and r.proveedor=p.id_proveedor order by r.nro_orden asc"
    rst.Open
    grid1.Rows = 0
    grid1.AddItem "Nro Orden" & vbTab & "Fecha" & vbTab & "Raz�n Social"
    grid1.Rows = 2
    grid1.FixedRows = 1
    grid1.Rows = 1
    If rst.RecordCount > 0 Then
       rst.MoveFirst
       While rst.EOF = False
         grid1.AddItem rst!nro_orden & vbTab & rst!fecha & vbTab & rst!razon_social
         rst.MoveNext
       Wend
      Else
       MsgRapido "No existen reclamos de pedidos.", Loro, vbInformation, , "surprised"
    End If
    rst.Close
 End If
End Sub

Private Sub optprov_Click()
 If optprov.Value = True Then
    grid1.Clear
    rst.Source = "select r.nro_orden, r.fecha, p.razon_social, r.descripcion from reclamo_pedido r, proveedor p where r.nro_reclamo>0 and r.proveedor=p.id_proveedor order by p.razon_social"
    rst.Open
    grid1.Rows = 0
    grid1.AddItem "Nro Orden" & vbTab & "Fecha" & vbTab & "Raz�n Social"
    grid1.Rows = 2
    grid1.FixedRows = 1
    grid1.Rows = 1
    If rst.RecordCount > 0 Then
       rst.MoveFirst
       While rst.EOF = False
         grid1.AddItem rst!nro_orden & vbTab & rst!fecha & vbTab & rst!razon_social
         rst.MoveNext
       Wend
      Else
       MsgRapido "No existen reclamos de pedidos.", Loro, vbInformation, , "surprised"
    End If
    rst.Close
 End If
End Sub
