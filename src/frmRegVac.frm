VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRegVac 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registrar Vacunaci�n"
   ClientHeight    =   3570
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegVac.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3570
   ScaleWidth      =   4680
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   653
      TabIndex        =   5
      Top             =   3000
      Width           =   1575
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2453
      TabIndex        =   6
      Top             =   3000
      Width           =   1575
   End
   Begin VB.ComboBox cmbVacuna 
      Height          =   345
      Left            =   1080
      Sorted          =   -1  'True
      TabIndex        =   3
      Text            =   "cmbVacuna"
      Top             =   1920
      Width           =   3375
   End
   Begin MSComCtl2.UpDown UpDown 
      Height          =   330
      Left            =   2641
      TabIndex        =   1
      Top             =   840
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   582
      _Version        =   393216
      Value           =   1
      BuddyControl    =   "txtPeso"
      BuddyDispid     =   196614
      OrigLeft        =   2040
      OrigTop         =   840
      OrigRight       =   2280
      OrigBottom      =   1215
      Max             =   10000000
      Min             =   1
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtDosis 
      Height          =   330
      Left            =   1080
      TabIndex        =   4
      Top             =   2400
      Width           =   3375
   End
   Begin VB.TextBox txtSintomas 
      Height          =   330
      Left            =   1080
      MaxLength       =   50
      TabIndex        =   2
      Top             =   1440
      Width           =   3375
   End
   Begin VB.TextBox txtPeso 
      Height          =   330
      Left            =   1905
      TabIndex        =   0
      Text            =   "10"
      Top             =   840
      Width           =   735
   End
   Begin VB.TextBox txtHora 
      Enabled         =   0   'False
      Height          =   330
      Left            =   3113
      TabIndex        =   8
      Top             =   240
      Width           =   1215
   End
   Begin VB.TextBox txtFecha 
      Enabled         =   0   'False
      Height          =   330
      Left            =   953
      TabIndex        =   7
      Top             =   240
      Width           =   1215
   End
   Begin VB.Label lblDosis 
      Caption         =   "Dosis:"
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   2400
      Width           =   615
   End
   Begin VB.Label lblPeso 
      Caption         =   "Peso:"
      Height          =   255
      Left            =   1373
      TabIndex        =   11
      Top             =   840
      Width           =   495
   End
   Begin VB.Label lblGrs 
      Caption         =   "Kgrs."
      Height          =   255
      Left            =   2933
      TabIndex        =   14
      Top             =   840
      Width           =   375
   End
   Begin VB.Label lblVacuna 
      Caption         =   "Vacuna:"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   1920
      Width           =   735
   End
   Begin VB.Label lblSintomas 
      Caption         =   "S�ntomas:"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   1440
      Width           =   855
   End
   Begin VB.Label lblHora 
      Caption         =   "Hora:"
      Height          =   255
      Left            =   2633
      TabIndex        =   10
      Top             =   240
      Width           =   495
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   353
      TabIndex        =   9
      Top             =   240
      Width           =   615
   End
End
Attribute VB_Name = "frmRegVac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim xid_hc As Integer
Dim xPeso As String
Dim nocomp As Boolean

Private Sub cmbVacuna_Change()
If nocomp = False Then completar cmbVacuna Else: nocomp = False
End Sub
Private Sub cmbVacuna_Click()
cmdAceptar.Enabled = True
End Sub

Private Sub cmbVacuna_KeyPress(KeyAscii As Integer)
cmdAceptar.Enabled = False
If KeyAscii = vbKeyBack Then cmbVacuna.Text = Left(cmbVacuna.Text, cmbVacuna.SelStart): nocomp = True
If KeyAscii = vbKeyReturn Then validar cmbVacuna
End Sub

Private Sub cmbVacuna_LostFocus()
validar cmbVacuna
End Sub

Private Sub cmdaceptar_Click()
Dim op
op = Mensaje("�Desea guardar los datos ingresados?", Loro, Balloon, vbQuestion + vbYesNoCancel, , "getattention")
Select Case op
    Case vbYes
If Trim(txtPeso) = "" Or Trim(txtSintomas) = "" Or Trim(txtDosis) = "" Then
    MsgRapido "Los datos est�n incompletos", Loro, vbCritical, , "surprised"
    Exit Sub
End If
Dim Xprecio
Dim rst As ADODB.Recordset
Set rst = New ADODB.Recordset
rst.ActiveConnection = cnx
rst.Source = "select id_medicamento, stock_total,precio from medicamento where id_medicamento=" & cmbVacuna.ItemData(cmbVacuna.ListIndex)
rst.CursorType = adOpenForwardOnly
rst.LockType = adLockOptimistic
rst.Open
If rst!stock_total = 0 Then
    MsgRapido "El stock de la vacuna seleccionada se ha agotado.", Loro, vbInformation, , "surprised"
    Exit Sub
End If
rst!stock_total = rst!stock_total - 1
rst.Update
Xprecio = rst!Precio
rst.Close
Dim cmd As ADODB.Command
Set cmd = New ADODB.Command
cmd.ActiveConnection = cnx
cmd.CommandText = "insert into detalle_hc values(" & xid_hc & ",'" & DateDB & "','" & TimeDB & "'," & txtPeso & ",'" & txtSintomas & "',2," & cmbVacuna.ItemData(cmbVacuna.ListIndex) & ",' '," & xLegajo & ",'" & txtDosis & "',' ')"
cmd.Execute
cmd.CommandText = "insert into nofacturado(id_cliente,fecha,hora,tipo,id_item,precio) values(" & frmhistoria.cmbCliente.ItemData(frmhistoria.cmbCliente.ListIndex) & ",'" & DateDB & "','" & TimeDB & "','med'," & cmbVacuna.ItemData(cmbVacuna.ListIndex) & "," & Xprecio & ")"
cmd.Execute
rst.Source = "select precio from servicio where id_servicio=2"
rst.Open
Xprecio = rst!Precio
rst.Close
cmd.CommandText = "insert into nofacturado(id_cliente,fecha,hora,tipo,id_item,precio) values(" & frmhistoria.cmbCliente.ItemData(frmhistoria.cmbCliente.ListIndex) & ",'" & DateDB & "','" & TimeDB & "','ser',2," & Xprecio & ")"
cmd.Execute
frmhistoria.loadGrid
frmhistoria.Enabled = True
Unload Me
    Case vbNo
        frmhistoria.Enabled = True
        Unload Me
    Case vbCancel
        Exit Sub
End Select
End Sub

Private Sub cmdcancelar_Click()
frmhistoria.Enabled = True
Unload Me
End Sub

Private Sub Form_Activate()
frmPrincipal.desactivararch
frmPrincipal.cerrar.Enabled = True
txtPeso = xPeso
End Sub

Private Sub Form_Load()
frmRegVac.Top = 1215
frmRegVac.Left = 3330
Dim rstMedicamento As ADODB.Recordset
Set rstMedicamento = New ADODB.Recordset
rstMedicamento.ActiveConnection = cnx
rstMedicamento.Source = "select id_medicamento, nombre from medicamento where id_rubro=1 and borrado='f'"
rstMedicamento.CursorType = adOpenForwardOnly
rstMedicamento.LockType = adLockReadOnly
rstMedicamento.Open
txtFecha = Date
txtHora = Time
While rstMedicamento.EOF = False
    cmbVacuna.AddItem rstMedicamento!nombre
    cmbVacuna.ItemData(cmbVacuna.NewIndex) = rstMedicamento!id_medicamento
    rstMedicamento.MoveNext
Wend
rstMedicamento.Close
cmbVacuna.ListIndex = 0
frmhistoria.Enabled = False
End Sub

Private Sub txtPeso_KeyPress(KeyAscii As Integer)
If KeyAscii < vbKey0 Or KeyAscii > vbKey9 Then
    If KeyAscii <> 8 And KeyAscii <> 7 And KeyAscii <> 46 Then
    KeyAscii = 0
    End If
End If
End Sub

Private Sub txtPeso_Validate(Cancel As Boolean)
If txtPeso <> "" Then
    If IsNumeric(txtPeso) = False Then
        MsgRapido "El dato no es v�lido.", Loro, , , "surprised"
        Cancel = True
        txtPeso = ""
    End If
End If
End Sub
Sub setID(paramId_hc As Integer, paramPeso As String)
xid_hc = paramId_hc
xPeso = paramPeso
End Sub

Private Sub UpDown_DownClick()
txtPeso = Val(txtPeso) - 0.1
End Sub

Private Sub UpDown_UpClick()
txtPeso = Val(txtPeso) + 0.1
End Sub
